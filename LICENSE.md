This is...complicated.

The short version is that [the blog](https://john.colagioia.net/blog/)
on which all the images in this repository are shown is licensed under
the terms of the [Creative Commons Attribution-Share Alike 4.0
International License](http://creativecommons.org/licenses/by-sa/4.0/),
so the overall repository is released under the same license.

However, any individual item in the repository might have originally
been released under a more "liberal" license, ranging from public
domain to CC-BY-SA, and most require some attribution.  The following
isn't ideal, but until I can build a more reasonable table, here are
the Markdown links and the credits from each post, which *should* be
enough information to cover everyone's licensing requirements.

 * title: Greetings 🎉
 * ![Howdy](/blog/assets/word-cafe-warm-advertising-france-sign-706601-pxhere.com.jpg "Bonjour!")
 * Untitled header photograph [from PxHere](https://pxhere.com/en/photo/706601), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Seeking Refuge
 * ![The cover](/blog/assets/Refuge-Cover.png "Seeking Refuge's Cover")
 * title: Choosing Blogging Software
 * ![Clippings](/blog/assets/writing-wood-newspaper-europe-money-church-822714-pxhere.com.jpg "Clippings")
 * Untitled header photograph [from PxHere](https://pxhere.com/en/photo/822714), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Metal Umlauts, Searching, and Other Unicode Fun
 * ![Header Image](/blog/assets/writing-book-vintage-antique-texture-old-629962-pxhere.com.jpg "Writing with diacritical marks")
 * Untitled header photograph [from PxHere](https://pxhere.com/en/photo/629962), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Social and Anti-Social Media
 * ![Radio News](/blog/assets/Radio_News_Sep_1928_Cover.png "Radio News September 1928")
 * ![Screenshot from an Open Cola commercial mock-up](/blog/assets/opencola.png)
 * !['I have never had any dealings with Messrs. Gabriel, and beg to ask by what right do they disturb me by a telegram which is evidently simply the medium of advertisement ?'](/blog/assets/Times-1864-06-01.png)
 * ![Generic social media](/blog/assets/wall.png)
 * ![C'mon, better than _that_...](/blog/assets/Smartphone_Zombies.png)
 * The header image is from the cover of Hugo Gernsback's **Radio News**, September 1928.  The complaint about unsolicited commercial telegraph advertisements comes from **The Times**, 01 June 1864.  The picture of the _Smartphone Zombies_ is licensed [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0) by Ccmsharma2.  The Open Cola screenshot comes from [a mock-up advertisement](https://www.youtube.com/watch?v=dMD2SF_E0gg) for the original [open source soft drink](https://en.wikipedia.org/wiki/OpenCola_(drink)), created and licensed [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0) by [springload1](https://www.youtube.com/user/springload1).
 * title: SlackBackup
 * ![Sentiment Analysis](/blog/assets/slackbackup1.png "Sentiment Analysis")
 * ![Main Page](/blog/assets/slackbackup2.png "Configuration")
 * title: 🎄 Unraveling Christmas 🎄
 * ![Christmas lights tied on a rope](/blog/assets/christmas-lights-free-pictures-1-2515-3.jpg "Christmas lights")
 * ![Nast's Original Santa Claus](/blog/assets/8261713052_f93056fab0_o.jpg)
 * [Christmas lights tied on a rope](https://www.1millionfreepictures.com/2013/12/2-christmas-lights-free-pictures-2515.html) by [an Unknown former Google+ user](https://plus.google.com/116074968591568480442), dedicated to the public domain on [1 Million Free Pictures](https://www.1millionfreepictures.com/).  The early image of Santa Claus is *A Christmas Furlough*, by Thomas Nast, from an 1863 issue of **Harper's Weekly**.  [**Santa Claus Conquers the Martians**](https://en.wikipedia.org/wiki/Santa_Claus_Conquers_the_Martians), which has fallen into the public domain, is available from [the Internet Archive](https://archive.org/details/SantaClausConquersTheMartians1964_201705).
 * title: Upgrading the Blog
 * ![Editing](/blog/assets/writing-pencil-pen-red-paper-lip-1363790-pxhere.com.jpg "Editing")
 * title: "Tag: ${tag}"
 * Untitled header photograph [from PxHere](https://pxhere.com/en/photo/1363790), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Licenses and Copyleft
 * ![Hands-in](/blog/assets/hand-black-and-white-game-photo-love-heart-617724-pxhere.com.jpg "Hands in")
 * Header image is [Hands](https://pxhere.com/en/photo/617724) by an unknown photographer from [PxHere](https://pxhere.com), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: 🔭 Looking Back on 2019
 * ![Kekeno looking back](/blog/assets/nature-wildlife-zoo-fur-biology-mammal-171768-pxhere.com.jpg "Kekeno looking back")
 * The header image is [Kekeno](https://pxhere.com/en/photo/171768) by an unknown photographer from [PxHere](https://pxhere.com), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: New Year on the Horizon 🎊
 * ![Prairie Horizon](/blog/assets/landscape-nature-grass-horizon-cloud-plant-1407864-pxhere.com.jpg "Horizon on the Prairie")
 * Untitled header photograph [from PxHere](https://pxhere.com/en/photo/1407864) by [Felix Mittermeier](https://felix-mittermeier.de), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: 🍾 Happy Calendar-Changing Day 🎆
 * ![2020](/blog/assets/happy-newyear-2020-card-fireworks-new-years-day-sky-1601384-pxhere.com.jpg "2020")
 * The header image is [New Year 2020](https://pxhere.com/en/photo/1601384) by [Henk Adriaan Meijer](https://pxhere.com/en/photographer/244493), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Introduction
 * ![Gas Clouds](/blog/assets/eso1335a.png "Gas Clouds")
 * The header image is [Two very different glowing gas clouds in the Large Magellanic Cloud](https://www.eso.org/public/usa/images/eso1335a/) by the [European Southern Observatory](https://www.eso.org), available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Tweets from 12/30 to 01/03
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown
 * ![Social graph](/blog/assets/Social_graph.gif "Social graph")
 * title: My Programming Languages
 * ![Lots of code](/blog/assets/texture-wall-line-color-blue-html-58668-pxhere.com.jpg "Lots of code")
 * Header image is [Hands](https://pxhere.com/en/photo/58668) by an unknown photographer from [PxHere](https://pxhere.com), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Son of...Bicker!
 * ![Bicker logo](/blog/assets/bicker-logo.png "Bicker")
 * I guess I just released the old **Bicker** logo under the blog's CC-BY-SA license.  And Bicky the mascot will probably need a reboot, too, since it's a safe bet I don't have any of the original art files left from those days.
 * title: Real Life in Star Trek, The Man Trap
 * ![Kepler-20e](/blog/assets/PIA14888~orig.png "Kepler-20e")
 * ![Tabasco peppers](/blog/assets/Tabasco_peppers.JPG "Tabasco peppers")
 * ![Martha, last of the passenger pigeons](/blog/assets/Martha_last_passenger_pigeon_1914.jpg "Save Martha!")
 * ![pile of American bison skulls waiting to be ground for fertilizer](/blog/assets/Bison_skull_pile_edit.jpg "Photo from the 1870s of a pile of American bison skulls waiting to be ground for fertilizer.")
 * The header image is [Kepler-20e -- The Smallest Exoplanet Artist Concept](https://images.nasa.gov/details-PIA14888) by the [NASA/Ames/JPL-Caltech](https://www.jpl.nasa.gov/), in the public domain as the work of the United States government.  The [bison bone pile](https://commons.wikimedia.org/wiki/File:Bison_skull_pile_edit.jpg) photograph is from outside a glueworks in Detroit, 1892, courtesy of Wikimedia Commons.  The picture of [Martha in her enclosure](https://en.wikipedia.org/wiki/Martha_(passenger_pigeon)#/media/File:Martha_last_passenger_pigeon_1914.jpg) is by Enno Meyer, 1912, from [Published figures and plates of the extinct passenger pigeon](https://archive.org/details/publishedfigures00shuf).  The picture of [Tabasco peppers](https://en.wikipedia.org/wiki/Tabasco_pepper#/media/File:Tabasco_peppers.JPG) comes from [the USDA](https://www.usda.gov/).
 * title: Tweets from 01/06 to 01/10
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - Diaspora
 * ![Diaspora Earth](/blog/assets/earthD2.png "Diaspora Earth")
 * The header image is *Earth Diaspora*, by the [Diaspora Foundation](https://diasporafoundation.org/) and made available under the [Creative Commons Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/).
 * title: Souring on Quora
 * ![Question Trees](/blog/assets/tree-nature-forest-sunlight-trunk-bark-40819-pxhere.com.jpg "Question Trees")
 * The header image is [Kekeno](https://pxhere.com/en/photo/40819) by an unknown photographer from [PxHere](https://pxhere.com), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Bicker Progress - Mid-January
 * ![Early construction](/blog/assets/work-architecture-structure-construction-line-facade-805034-pxhere.com.jpg "Early construction")
 * The header image is [unnamed](https://pxhere.com/en/photo/805034) by an anonymous [PxHere](https://pxhere.com/) photographer and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/). [Lock](https://pixnio.com/objects/fastener-device-lock-padlock-security-metal-rust-iron) is by [Bicanski](https://pixnio.com/author/bicanski) and also made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/) on [PIXNIO](https://pixnio.com/).
 * title: Real Life in Star Trek, Charlie X
 * ![Bernal Sphere](/blog/assets/External_view_of_a_Bernal_sphere.jpg "Bernal Sphere")
 * ![Ace of Spades](/blog/assets/AceSpadesRound.png "Ace of Spades")
 * The header image is an [Artist's impression of the exterior view of a Bernal sphere space habitat design](https://commons.wikimedia.org/wiki/File:External_view_of_a_Bernal_sphere.jpg) by [Rick Guidice](http://rickguidice.com/), due to its resemblance to a cargo ship and in the public domain as a work of [NASA](https://www.nasa.gov/).
 * title: Tweets from 01/13 to 01/17
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - The Fediverse
 * ![Network](/blog/assets/iot-technology-concept-city-transport-smart-1575603-pxhere.com.jpg "Network")
 * The header image is [Big data connections. IOT](https://pxhere.com/en/photo/1575603) by [asawin](https://pxhere.com/en/photographer/2102671) from [PxHere](https://pxhere.com), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: My Teaching Background
 * ![Old classroom](/blog/assets/oldschool-free-license-cc0.jpg "My classrooms never looked this relaxed")
 * ![Smalltalk-76 on the Xerox Alto](/blog/assets/Smalltalk-76.png "Smalltalk-76 on the Xerox Alto")
 * The header image is [Classroom](https://skitterphoto.com/photos/191/classroom) by [Peter Heeling](https://skitterphoto.com/photographers/7/peter-heeling), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  The picture of Polytechnic's original campus at Brooklyn's 99 Livingston Street is in the public domain, found on [Wikimedia Commons](https://en.wikipedia.org/wiki/New_York_University_Tandon_School_of_Engineering#/media/File:99Livingston_wObservatory_150x200.jpg).  The [Xerox Alto screenshot](https://en.wikipedia.org/wiki/Xerox_Alto#/media/File:Smalltalk-76.png) by [SUMIM.ST](https://commons.wikimedia.org/w/index.php?title=User:SUMIM.ST) is made available under a [Creative Commons Attribution-Share Alike 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/); the Alto software itself *should* be in the public domain, since the United States didn't extend copyright protections to software until 1980.
 * title: Bicker Progress - MLK Day
 * ![Welding](/blog/assets/light-construction-repair-welding-darkness-lighting-598394-pxhere.com.jpg "Welding")
 * The header image is [unnamed](https://pxhere.com/en/photo/598394) by an anonymous [PxHere](https://pxhere.com/) photographer and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Where No Man Has Gone Before
 * ![Milky Way](/blog/assets/eso0932a.jpg "Milky Way")
 * ![Dehner and Kirk with a phaser rifle prop](/blog/assets/William-Shatner-Sally-Kellerman-Star-Trek-1966.png "Be vewwy vewwy quiet...")
 * ![Captain Kirk about to throw a rock](/blog/assets/William-Shatner-Star-Trek-first-episode-1966.png "I got a rock...")
 * The header image is [The Milky Way panorama](https://www.eso.org/public/usa/images/eso0932a/), in honor of the episode about leaving the galaxy, by the [European Southern Observatory](https://www.eso.org) and S. Brunier, available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.  The images of [Sally Kellerman and William Shatner](https://commons.wikimedia.org/wiki/File:William_Shatner_Sally_Kellerman_Star_Trek_1966.JPG) and [William Shatner and his rock](https://commons.wikimedia.org/wiki/File:William_Shatner_Star_Trek_first_episode_1966.JPG) were published without copyright notices---for the purposes of republication as publicity, even---and so are in the public domain.
 * title: Tweets from 01/20 to 01/24
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.  The image of [Dr. King](https://commons.wikimedia.org/wiki/File:Martin_Luther_King_Jr_NYWTS.jpg) by Dick DeMarsico of the World Telegram, donated to the Library of Congress.  The image [TOI 700, a planetary system 100 light-years away in the constellation Dorado, is home to TOI 700 d, the first Earth-size habitable-zone planet discovered by NASA's Transiting Exoplanet Survey Satellite](https://images.nasa.gov/details-PIA23408) by NASA's Goddard Space Flight Center is in the public domain as a work of the United States government.  [Snapshot of the Antarctic Ozone Hole 2010](https://images.nasa.gov/details-GSFC_20171208_Archive_e001962) (not the specific image used by The Intercept for the article, but close enough) by NASA Goddard is (also) in the public domain as a work of the United States government.  [Lessig with fellow Creative Commons board member Joi Ito](https://commons.wikimedia.org/wiki/File:Lawrence_lessig,_joi_ito.jpg) (also **not** the image used by VICE, which appears to not be under a free culture license despite being of "the free culture guy") by [LAI Ryanne](https://www.flickr.com/people/96941606@N00) is available under a [Creative Commons Attribution 2.0 license](https://creativecommons.org/licenses/by/2.0/).
 * title: Free Social Networking Showdown - Scuttlebutt
 * ![Hermit Crab](/blog/assets/hand-photography-animal-cute-leg-finger-960645-pxhere.com.jpg "Hermit Crab")
 * ![Hermies](/blog/assets/hermies.png "Hermies")
 * The header image is [Hermit Crab](https://pxhere.com/en/photo/960645) by an unknown photographer from [PxHere](https://pxhere.com), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  [Hermies](https://en.wikipedia.org/wiki/Secure_Scuttlebutt#/media/File:Hermies.png), the Secure Scuttlebutt mascot, by [Paul Frazee](https://pfrazee.hashbase.io/), is available under a [Creative Commons Attribution-Share Alike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: The Worst Programming Language?
 * ![Sisyphus](/blog/assets/Friedrich_John_nach_Matthäus_Loder_Sisyphus_ubs_G_0825_II.jpg "Caution: Sisyphus at work")
 * The header image is [Sisyphus, Copper Engraving](https://commons.wikimedia.org/wiki/Category:Sisyphus#/media/File:Friedrich_John_nach_Matth%C3%A4us_Loder_Sisyphus_ubs_G_0825_II.jpg) by John Freidrich, in the public domain.
 * title: Bicker Progress - January Waning
 * ![Royal penguins arguing](/blog/assets/Royal_penguins_arguing.jpg "Royal penguins arguing")
 * ![Avatar](https://seccdn.libravatar.org/avatar/561dc8a580d94f8a9826c109f44abda7?s=400&forcedefault=y&default=robohash "Robohash avatar for one of my e-mail addresses")
 * ![linked list](/blog/assets/Singly-linked-list.svg "A linked list representation")
 * ![Our paragraph](/blog/assets/bicker-ipsum-20100123.png "Paragraph of random text")
 * The header image is [Royal penguins arguing](https://commons.wikimedia.org/wiki/File:Royal_penguins_arguing.jpg) by [Brocken Inaglory](https://sites.google.com/site/thebrockeninglory/) and is made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported License](https://creativecommons.org/licenses/by-sa/3.0/deed.en).  The [linked list](https://commons.wikimedia.org/wiki/File:Singly-linked-list.svg) by [Lasindi](https://commons.wikimedia.org/w/index.php?title=User:Lasindi) has been released into the public domain.
 * title: Small Technology Notes
 * ![Gears](/blog/assets/work-technology-vintage-wheel-retro-clock-606288-pxhere.com.jpg "Gears")
 * The header image is [work, technology, vintage, wheel, retro, clock](https://pxhere.com/en/photo/606288), by an anonymous PxHere photographer, is made available under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Blish Supplement
 * ![Psychic](/blog/assets/hands-1461555427fAd.jpg "Psychic")
 * The header image is [Hands](https://publicdomainpictures.net/en/view-image.php?image=162559&picture=hands) by Hana Chramostova, released into the public domain.  The three-dimensional model of [sodium thiopental](https://en.wikipedia.org/wiki/File:Sodium-thiopental-3D-vdW-2.png) by Benjah-bmm27 has been released to the public domain.
 * title: Tweets from 01/27 to 01/31
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - Minds
 * ![Chimp Brain](/blog/assets/Chimp_Brain_in_a_jar.jpg "Chimp Brain in a Jar")
 * The header image is [Chimp Brain in a jar](https://commons.wikimedia.org/wiki/File:Chimp_Brain_in_a_jar.jpg) by [Gaetan Lee](https://www.flickr.com/photos/43078695@N00), available under the [Creative Commons Attribution 2.0 Generic license](https://creativecommons.org/licenses/by/2.0/deed.en)...because "*Minds*" and because it's centralized and contained in the jar.
 * title: Colagioia Industries
 * ![Orrery](/blog/assets/Orrery_designed_by_William_Pearson_made_by_Robert_Fidler-1813-1822.png "Orrery")
 * The header image is [Orrery designed by William Pearson, made by Robert Fidler, 1813-1822](https://commons.wikimedia.org/wiki/File:Orrery_designed_by_William_Pearson,_made_by_Robert_Fidler,1813-1822.jpg) by the [Science Museum Group](https://www.sciencemuseumgroup.org.uk/), made available under the terms of a [Creative Commons Attribution-Share Alike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/deed.en).
 * title: Bicker Progress - Groundhog Day (Belated)
 * ![Groundhog](/blog/assets/animal-wildlife-zoo-america-mammal-squirrel-1362487-pxhere.com.jpg "Groundhog, interrupted")
 * The header image is [Groundhog](https://pxhere.com/en/photo/1362487) by an anonymous PxHere photographer and is made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported LicenseCC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Recutils — Small Technology Notes
 * ![Gears](/blog/assets/work-technology-vintage-wheel-retro-clock-606288-pxhere.com.jpg "Gears")
 * The header image is [work, technology, vintage, wheel, retro, clock](https://pxhere.com/en/photo/606288), by an anonymous PxHere photographer, is made available under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, The Naked Time
 * ![Icy exoplanet](/blog/assets/eso0603a.png "Icy exoplanet")
 * ![Spock's analysis](/blog/assets/Star-Trek-The-Naked-Time.png "Spock's analysis")
 * The header image is [Icy exoplanet (artist's impression)](https://www.eso.org/public/images/eso0603a/) by the [European Southern Observatory](https://www.eso.org), available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.  [A promotional photo for the Star Trek: The Original Series episode "The Naked Time".](https://commons.wikimedia.org/wiki/File:Star_Trek_The_Naked_Time.jpg) is also used, as a photographic work without a copyright notice, meant for republication as publicity.
 * title: Tweets from 02/03 to 02/07
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - Friendica
 * ![Mist in a park](/blog/assets/sunrise-trees-fog-canon-XC10-nature-tree-1446447-pxhere.com.jpg "Mist in a park")
 * The header image is [sunlight beams through trees at sunrise in a foggy park](https://pxhere.com/en/photo/1446447) by [Rob Smith](https://pxhere.com/en/photographer/1206799), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  It was chosen in reference to Friendica's prior name, *Mistpark*, since no relevant free-licensed images are available.
 * title: The Barnes & Noble "Diverse Covers" Thing
 * ![Carter G. Woodson](/blog/assets/Carter_G_Woodson_1915.png "Carter G. Woodson")
 * The header image is [Dr. Carter G. Woodson](https://commons.wikimedia.org/wiki/File:Dr._Carter_G._Woodson_(1875-1950),_Carter_G._Woodson_Home_National_Historic_Site,_1915._(18f7565bf62142c0ad7fff83701ca5f6).jpg "Wikimedia_Commons"), the creator of what we now call Black History Month, the photograph dating back to 1915.  The picture of Emma Dunham Kelley comes from the frontispiece of Megda, available at [Wikimedia Commons](https://en.wikipedia.org/wiki/Emma_Dunham_Kelley-Hawkins#/media/File:Emma_Dunham_Kelley,_aka_Forget-me-not,_ca._1891.jpg).
 * title: Bicker Progress - (Almost) Inventors' Day
 * ![Invention of the Compass](/blog/assets/New_Inventions_of_Modern_Times_-Nova_Reperta-_The_Invention_of_the_Compass_plate_2_MET_DP841121.png "Invention of the Compass")
 * The header image is [Invention of the Compass](https://commons.wikimedia.org/wiki/File:New_Inventions_of_Modern_Times_-Nova_Reperta-,_The_Invention_of_the_Compass,_plate_2_MET_DP841121.jpg) from **New Inventions of Modern Times**, circa 1600, courtesy of [The Metropolitan Museum of Art](https://www.metmuseum.org/art/collection/search/659663).
 * title: Real Life in Star Trek, The Enemy Within
 * ![Super-Earth HD 85512 b](/blog/assets/eso1134b.png "Super-Earth HD 85512 b")
 * The header image is [Artists's impression of one of more than 50 new exoplanets found by HARPS: the rocky super-Earth HD 85512 b](https://www.eso.org/public/images/eso1134b/) by the [European Southern Observatory](https://www.eso.org) and M. Kornmesser, available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Tweets from 02/10 to 02/14
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - Twister
 * ![Tornado](/blog/assets/cloud-sky-atmosphere-dark-weather-storm-1086328-pxhere.com.jpg "It's a twister!")
 * The header image is [unnamed](https://pxhere.com/en/photo/1086328) by an anonymous [PxHere](https://pxhere.com/) photographer and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  It was chosen in reference to the project name, since no relevant free-licensed images are available.
 * title: My Software
 * ![Orrery](/blog/assets/Orrery_designed_by_William_Pearson_made_by_Robert_Fidler-1813-1822.png "Orrery")
 * The header image is [Orrery designed by William Pearson, made by Robert Fidler, 1813-1822](https://commons.wikimedia.org/wiki/File:Orrery_designed_by_William_Pearson,_made_by_Robert_Fidler,1813-1822.jpg) by the [Science Museum Group](https://www.sciencemuseumgroup.org.uk/), made available under the terms of a [Creative Commons Attribution-Share Alike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/deed.en).
 * title: Bicker Progress - First (Belated) Mensiversary
 * ![Antique Printing Press](/blog/assets/wood-antique-paper-ink-printing-art-919542-pxhere.com.jpg "Antique Printing Press")
 * ![An example reply](/blog/assets/bicker-reply.png "An example reply")
 * The header image is [Antique Printing Press](https://pxhere.com/en/photo/919542) by an anonymous [PxHere](https://pxhere.com/) photographer and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Mudd's Women
 * ![Asteroids](/blog/assets/eso0526b.jpg "Asteroids")
 * ![Ace of Spades](/blog/assets/AceSpadesRound.png "Ace of Spades")
 * The header image is [Artist's impression of the triple asteroid system, 87 Sylvia](https://www.eso.org/public/images/eso0526b/) by the [European Southern Observatory](https://www.eso.org), available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Tweets from 02/17 to 02/21
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - ZeroNet
 * ![A net in empty space](/blog/assets/basket-net-1.jpg "A net in empty space")
 * The header image is [Basketball net close up](https://skitterphoto.com/photos/4702/basketball-net-close-up) by [Peter Heeling](https://skitterphoto.com/photographers/7/peter-heeling) and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  It was chosen in reference to the project name, obviously, since no relevant free-licensed images are available.
 * title: Victor Séjour's Le Mulâtre
 * ![Labadee, Haiti](/blog/assets/6527934_920ed523ed_c.png "Labadee, near Cap-Haïtien or 'The Cape'")
 * The image of [Victor Séjour](https://commons.wikimedia.org/wiki/File:Victor_S%C3%A9jour.jpg) is from Wikimedia Commons, long passed into the public domain.  The picture of [Labadee, Haiti](https://www.flickr.com/photos/kencurtis/6527934/in/photolist-zswA-KrDPHu-RUX3R4-2ckddkq-2icCwsJ-2czRqBm-dnQWHv-7vDawg-7wz86b-7xEgz8-itAMUy-2er7CD3-9jziwc-sast3i-Mn5KMD-7N8Y5F-7ykCN3-2isak2r-Mn5KgD-7vThrA-ntQEoz-dCS7Fg-7vzoVt-FvHgdW-7vGJ1c-2ewHQGU-MUTknQ-7yhcFc-Mn5LPi-afXbUE-s8aEKo-Mn5KzV-cYtCe-MTRZHo-s8aExQ-5VyomE-fNi2S3-quAdnA-bx41GU-cwauoJ-2aCiw8E-k5QrKu-7xqms8-7xqm8p-bQiRbg-7x6mw2-68mSCh-7x6SoF-7vGJ18-9bTRjg/) by [Ken Curtis](https://www.flickr.com/photos/kencurtis/) is made available under the terms of a [Creative Commons Attribution-Share Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Bicker Progress - End of February
 * ![Cat peeking out](/blog/assets/cute-pet-kitten-cat-mammal-close-up-997331-pxhere.com.jpg "Cat peeking out")
 * The header image is [untitled](https://pxhere.com/en/photo/997331) by an anonymous [PxHere](https://pxhere.com/) photographer and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, What Are Little Girls Made Of?
 * ![Exoplanet](/blog/assets/eso0722b.jpg "Exoplanet")
 * The header image is [The Earth-like planet Gliese 581 c (artist's impression)](https://www.eso.org/public/images/eso0722b/) by the [European Southern Observatory](https://www.eso.org), available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Tweets from 02/24 to 02/28
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - Libertree
 * ![Maple Leaves](/blog/assets/autumn-atmosphere-1.jpg)
 * The header image is [Yellow maple leafs](https://skitterphoto.com/photos/4757/yellow-maple-leafs) by [Peter Heeling](https://skitterphoto.com/photographers/7/peter-heeling), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  It was chosen in reference to Libertree's prior name, *Maple*, since no relevant free-licensed images are available.
 * title: On Electability and Complicity
 * ![Vote](/blog/assets/sign-banner-street-sign-signage-vote-political-747227-pxhere.com.jpg "Vote")
 * The header image is [untitled](https://pxhere.com/en/photo/747227) by an anonymous [PxHere](https://pxhere.com/) photographer and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  Besides being an apparent Wikipedia hoax, I also use Dave Hepler as a background player in my [League of the Silver Bat]({% post_url 2019-12-14-seeking-refuge %}) novel.
 * title: Bicker Progress - In Like a Lion
 * ![Cat peeking out](/blog/assets/cute-pet-kitten-cat-mammal-close-up-997331-pxhere.com.jpg "Cat peeking out")
 * ![Screenshot with fake punctuation](/blog/assets/no-punctuation-test.png "Screenshot with fake punctuation")
 * ![No-longer-broken tables](/blog/assets/bicker-table-display.png "No-longer-broken tables")
 * The header image is [untitled](https://pxhere.com/en/photo/997331) by an anonymous [PxHere](https://pxhere.com/) photographer and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Happy (Orthodox?) Pi Day, Tomorrow!
 * ![Pi](/blog/assets/computer-technology-car-vintage-retro-number-753544-pxhere.com.jpg "Mechanical Calculator Pi")
 * The header image is [Pi](https://pxhere.com/en/photo/753544) by an anonymous PxHere photographer, available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Miri
 * ![Exact Duplicate of Earth](/blog/assets/blue-marble.png "Exact Duplicate of Earth")
 * The header image is [The Blue Marble](https://commons.wikimedia.org/wiki/File:The_Earth_seen_from_Apollo_17.jpg) by the [Apollo 17](https://en.wikipedia.org/wiki/Apollo_17) astronauts on December 7, 1972, placed in the public domain as a work of NASA and not otherwise noted.  What better representation for an exact duplicate of Earth than the exact duplicate's exact duplicate...?
 * title: Tweets from 03/02 to 03/06
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - Pump.io
 * ![Garden Pump](/blog/assets/tree-water-grass-trail-lawn-vintage-1358186-pxhere.com.jpg)
 * The header image is [unnamed](https://pxhere.com/en/photo/1358186) by an anonymous [PxHere](https://pxhere.com/) photographer and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  It was chosen in reference to the project's name, since no relevant free-licensed images are available.
 * title: We Are What We Do
 * ![Labels](/blog/assets/kisscc0-label-paper-textile-printing-marketing-design-5afdb270c28a29.7801036915265757287968.png "Labels")
 * The header image is [Label Paper Textile Printing Marketing](https://www.kisscc0.com/photo/label-paper-textile-printing-marketing-design-cn2vdr/) by an anonymous/unknown [KissCC0](https://www.kisscc0.com/) photographer and is [made available](https://www.kisscc0.com/license.html) under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Project Updates, Harriet Tubman Day
 * ![Harriet Tubman](/blog/assets/NMAAHC-2017_30_48_001.png "Harriet Tubman")
 * ![Open Tray](/blog/assets/recommended-panel.png "Open Tray")
 * ![Example output](/blog/assets/console-character-profiles.png "Sample output")
 * ![Example](/blog/assets/chargen-leaflet-example.png "An example")
 * The header image is [Matte collodion print of Harriet Tubman](https://www.si.edu/object/nmaahc_2017.30.48), long since in the public domain, but appropriate, given that tomorrow is [Harriet Tubman Day](https://en.wikipedia.org/wiki/Harriet_Tubman_Day) and the Trump administration has punted on putting Tubman on the twenty-dollar bill...
 * title: Colors for Programmers
 * ![Color at Nasir al Molk](/blog/assets/Nasir-al_molk_-1.png "Color at Nasir al Molk")
 * The header image is [Eine Innenansicht der Nasir-ol-Molk-Moschee in Schiras, Iran](https://commons.wikimedia.org/wiki/File:Nasir-al_molk_-1.jpg) by [Ayyoubsabawiki](https://commons.wikimedia.org/w/index.php?title=User:Ayyoubsabawiki&action=edit&redlink=1) and is released under the [Creative Commons Attribution-Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Dagger of the Mind
 * ![Artist's impression of exoplanet HR8799e](/blog/assets/eso1905a.png "Artist's impression of exoplanet HR8799e")
 * The header image is [AGRAVITY instrument breaks new ground in exoplanet imaging](https://www.eso.org/public/usa/images/eso1905a/) by the [European Southern Observatory](https://www.eso.org) and Calçada, available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Tweets from 03/09 to 03/13
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * ![Swamp wallaby](/blog/assets/swamp-wallabies_1600.jpg "Swamp wallaby")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.  [Swamp Wallaby and Joey](https://commons.wikimedia.org/wiki/File:Swamp_Wallaby_and_Joey_Lisarow.jpg) by [WikiWookie](https://commons.wikimedia.org/wiki/User:WikiWookie).
 * title: Free Social Networking Showdown - RetroShare
 * ![A RetroShare Network](/blog/assets/retro_network.png "A RetroShare Network")
 * The header image is [retro_network](https://github.com/RetroShare/documentation/blob/master/docs/img/concept/retro_network.png) by [the RetroShare Team](https://github.com/RetroShare) and is made available under the terms of a [Creative Commons Attribution-Share Alike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/deed.en).
 * title: I Have a Banana in My Ear! 🍌
 * ![Bananas](/blog/assets/PIXNIO-1959511.png "Bananas")
 * ![corona tumular](/blog/assets/572px-Spanish_Royal_Crown_1crop.png "corona tumular")
 * ![Flatten the Curve](/blog/assets/640px-Covid-19-curves-graphic-social-v3.gif "Flatten the Curve")
 * The header image is [untitled bananas](https://pixnio.com/flora-plants/fruits/banana-pictures/marketplace-produce-fruit-vegetable-food-banana-grow-bunch) by an Toper Domingo from [PIXNIO](https://pixnio.com/), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  [Spanish Royal Crown](https://commons.wikimedia.org/wiki/File:Spanish_Royal_Crown_1crop.jpg) by [TheRichic](https://commons.wikimedia.org/wiki/User:TheRichic) and [Flatten the Curve](https://commons.wikimedia.org/wiki/File:Covid-19-curves-graphic-social-v3.gif) by Siouxsie Wiles and Toby Morris (the latter courtesy of [The Spinoff](https://thespinoff.co.nz/society/09-03-2020/the-three-phases-of-covid-19-and-how-we-can-make-it-manageable/)) have been made available under the terms of the [Creative Commons Attribution-Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en).  The [panicked scream](https://freesound.org/people/Erdie/sounds/165613/) by [Erdie](https://freesound.org/people/Erdie/) is available under the terms of the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/3.0/) license.
 * title: Developer Journal, Ides-Adjacent
 * ![La morte di Cesare](/blog/assets/Vincenzo_Camuccini_-_La_morte_di_Cesare.png "La morte di Cesare")
 * The header image is [La morte di Cesare](Vincenzo_Camuccini_-_La_morte_di_Cesare.png) by Vincenzo Camuccini (1802).
 * title: Small-D date Night
 * ![Calendar](/blog/assets/writing-book-diary-paper-page-brand-12446-pxhere.com.jpg "Calendar")
 * The header image is [untitled calendar](https://pxhere.com/en/photo/12446) by an unknown photographer from [PxHere](https://pxhere.com), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, The Corbomite Maneuver
 * ![Plasma Globe](/blog/assets/glowing-sun-atmosphere-glow-lamp-electricity-1236265-pxhere.com.png "Not the Fesarius")
 * ![Rand and Kirk](/blog/assets/Grace-Lee-Whitney-William-Shatner-Corbomite-Manuever-Star-Trek-1966.png "Never touch the basket 'do.  Never")
 * The header image is a [plasma globe](https://pxhere.com/en/photo/1236265) by an unknown [PxHere](https://pxhere.com/) and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  The image of [Grace Lee Whitney and William Shatner](https://commons.wikimedia.org/wiki/File:Grace_Lee_Whitney_William_Shatner_Corbomite_Manuever_Star_Trek_1966.jpg) was published without copyright notices---for the purposes of republication as publicity, even---and so is in the public domain.
 * title: Tweets from 03/16 to 03/20
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - twtxt
 * ![Text](/blog/assets/abc-alphabet-alphabet-letter-baby-basic-blank-1437975-pxhere.com.jpg "Text")
 * The header image is [untitled letters](https://pxhere.com/en/photo/1437975) by an unknown photographer from [PxHere](https://pxhere.com), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Open Oubliette
 * ![Game pieces](/blog/assets/wood-white-play-food-color-community-730015-pxhere.com.jpg "Game pieces")
 * ![An example Open Oubliette board](/blog/assets/oubliette-board.png "An example Open Oubliette board")
 * The header image is [untitled](https://pxhere.com/en/photo/730015) by an anonymous [PxHere](https://pxhere.com/) photographer and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Three Days into Spring
 * ![Orchard in Spring](/blog/assets/Alfred_Sisley_047.png "Orchard in Spring")
 * The header image is [Orchard in Spring](https://commons.wikimedia.org/wiki/File:Alfred_Sisley_047.jpg) (1881) by [Alfred Sisley](https://en.wikipedia.org/wiki/Alfred_Sisley).
 * title: Real Life in Star Trek, The Menagerie, Part I
 * ![Building a Space Colony](/blog/assets/ARC-1976-AC76-0296.png "Building a Space Colony")
 * ![original Ouija board](/blog/assets/Original_ouija_board.jpg "the original Ouija board")
 * The header image is [Building a Space Colony](https://images.nasa.gov/details-ARC-1976-AC76-0296) by Rick Guidice, in the public domain by NASA policy.
 * title: Tweets from 03/23 to 03/27
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.  [Businesses](https://www.flickr.com/photos/omarsg/30007726175) by [Omar Salvatierra Gracida](https://www.flickr.com/photos/omarsg/) is made available under the terms of the [Creative Commons Attribution 2.0 Generic license](https://creativecommons.org/licenses/by/2.0/).
 * title: Free Social Networking Showdown - Matrix
 * ![A RetroShare Network](/blog/assets/retro_network.png "A RetroShare Network")
 * The header image is [retro_network](https://github.com/RetroShare/documentation/blob/master/docs/img/concept/retro_network.png) by [the RetroShare Team](https://github.com/RetroShare) and is made available under the terms of a [Creative Commons Attribution-Share Alike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/deed.en).
 * title: A Free Culture License Wish List
 * ![Cooperation](/blog/assets/hand-number-finger-community-together-handle-773437-pxhere.com.png "Cooperation")
 * ![Overgrown Brick](/blog/assets/overgrown-brick.png "Overgrown Brick")
 * ![Book Ring](/blog/assets/bookringbasic_preview_featured.jpg "Book Ring")
 * Untitled header photograph [from PxHere](https://pxhere.com/en/photo/773437), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  The [book ring](https://www.thingiverse.com/thing:144660) by [Frank Erfurth](https://www.thingiverse.com/alohapussycat/about) is available under the terms of the [Creative Commons Attribution-Share Alike 3.0 license](https://creativecommons.org/licenses/by-sa/3.0/).
 * title: Developer Journal, End of March
 * ![Look to the skies](/blog/assets/sky-cloud-blue-nature-pink-daytime-1434369-pxhere.com.png "Look to the skies")
 * title: Real Life in Star Trek, The Menagerie, Part II
 * ![Trouvelot's Jupiter, upside-down](/blog/assets/trouvelot-jupiter-upside-down.jpg "Trouvelot's Jupiter, upside-down")
 * The header image is [The planet Jupiter: Observed November 1, 1880, at 9h. 30m. P.M.](https://digitalcollections.nypl.org/items/510d47dd-e821-a3d9-e040-e00a18064a99) by Étienne Léopold Trouvelot, long since passed into the public domain.  It was chosen because it made a brief appearance (upside-down, as it is here) in the previous episode, near the end of the teaser, outside of Mendez's office.
 * title: Tweets from 03/30 to 04/03
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * ![Spreading through the chain](/blog/assets/ncase-tracing-chain.png "Spreading through the chain")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - gitgeist
 * ![gitgeist's architecture](/blog/assets/GitgeistArchitecture.png "gitgeist's architecture")
 * The header image is the system architecture diagram from the [gitgeist repository](https://github.com/opersys/gitgeist-poc), made available under the terms of the [Apache License v2.0](https://www.apache.org/licenses/LICENSE-2.0).
 * title: Database Basics
 * ![Laptop showing data](/blog/assets/laptop-computer-writing-keyboard-brand-design-104014-pxhere.com.png "Laptop showing data")
 * The header image is [untitled](https://pxhere.com/en/photo/104014) by an anonymous [PxHere](https://pxhere.com) photographer, made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Early April, Minus the Fools
 * ![Look to the skies](/blog/assets/sky-cloud-blue-nature-pink-daytime-1434369-pxhere.com.png "Look to the skies")
 * ![Example note export](/blog/assets/Miniboost-Letter-Frequencies-Example.png "Example note export")
 * The header image is [untitled](https://pxhere.com/en/photo/1434369) by [Lotteszka](https://pxhere.com/en/photographer/995459) from [PxHere](https://pxhere.com), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, The Conscience Of The King
 * ![V404 Cygni](/blog/assets/GSFC_20171208_Archive_e000698.png "V404 Cygni")
 * ![Ellen Terry as Lady Macbeth](/blog/assets/Ellen_Terry_plays_Lady_Macbeth.png "Ellen Terry as Lady Macbeth, which is definitely not foreshadowing")
 * ![Henry Irving's prompt book describing a possible way to show the Ghost](/blog/assets/Harvard_Theatre_Collection_-_TS_Promptbooks_Sh_154.75,_Henry_Irving_1874.png "Henry Irving's prompt book")
 * The header image is [ NASA Missions Monitor a Waking Black Hole ](https://images.nasa.gov/details-GSFC_20171208_Archive_e000698) by NASA's Goddard Space Flight Center, in the public domain as the work of the United States government.  Caspar David Friedrich's [Wanderer above the Sea of Fog](https://commons.wikimedia.org/wiki/File:Caspar_David_Friedrich_-_Wanderer_above_the_sea_of_fog.jpg) was released in 1818, [Ellen Terry as Lady Macbeth](https://commons.wikimedia.org/wiki/File:Ellen_Terry_plays_Lady_Macbeth.jpg) comes from the 1888 production, and Henry Irving's [prompt book](https://commons.wikimedia.org/wiki/File:Harvard_Theatre_Collection_-_TS_Promptbooks_Sh_154.75,_Henry_Irving_1874.jpg) is from his 1874 production.
 * title: Tweets from 04/06 to 04/10
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - And the Rest
 * ![Extra parts](/blog/assets/work-old-workshop-steel-construction-repair-760380.png "Extra parts")
 * The header image is [untitled](https://pxhere.com/en/photo/760380) by an anonymous [PxHere](https://pxhere.com/) photographer and is made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Advice for Aspiring Career-Hoppers, Part I
 * ![Women programming](/blog/assets/25388747874_f1cf9235c8_o.png "Women programming")
 * The header image is [Untitled](https://www.flickr.com/photos/wocintechchat/25388747874/) by [WOCinTech Chat](https://www.flickr.com/photos/wocintechchat/), made available under the terms of the [Creative Commons Attribution 2.0 Generic license](https://creativecommons.org/licenses/by/2.0/).
 * title: Developer Journal, Jefferson's Birthday
 * ![Monticello](/blog/assets/Thomas_Jeffersons_Monticello.png "Monticello")
 * ![The emoji](/blog/assets/seeking-refuge-emoji-example-1-01.png "the emoji")
 * The header image is [Thomas Jefferson's Monticello](https://commons.wikimedia.org/wiki/File:Thomas_Jefferson%27s_Monticello.JPG) by [Martin Falbisoner](https://commons.wikimedia.org/wiki/User:Martin_Falbisoner), made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported license](https://creativecommons.org/licenses/by-sa/3.0/deed.en).  And yes, Jefferson *was* a huge hypocrite who preached equality out of one side of his mouth while protecting slavery (an institution some of his own children were born into) out of the other **and** he abandoned all of his alleged principles of governance the second he was elected President.  But naming the post "Tax Hangover" sounded like it was going to make the search for a header image much more difficult, so here we are...
 * title: Experimenting with Worker Threads
 * ![Parallel threads](/blog/assets/2744507570_ba4ba165f4_k.png "Parallel threads")
 * The header image is [Threads](https://www.flickr.com/photos/ndanger/2744507570/in/photolist-5bwju3-oHmhfe-azNZVL-nRD4oA-9Av7cj-9Av72w-9AscDz-8wm2jQ-dXJied-dwoaAQ-e9hWJ3-5WvZrj-7x5JHd-bAQF6a-5prZKP-UtHaz4-KRhP9J-bparyB-UBPvhy-3Sccxf-3ScbYu-BNk4ye-KFJvL8-31V1f-jSbB3-2hPw6RT-2hPC4Gf-2hPVhpW-23myQcm-2hPJdj7-2hPTp9W-2hPJetM-2hQ33FL-2hPfRAc-2hPgXqs-2hPzVN2-23EyPNh-ripaJo-rUyG2-HJPhB-873DBk-bAFsAn-aE3xwB-8vHt7g-73p4V4-oh8NZ1-873EQT-iiLRjP-876Ne9-kJLWEe) by [Dave Gingrich](https://www.flickr.com/photos/ndanger/) and made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic license](https://creativecommons.org/licenses/by-sa/2.0/).
 * title: Real Life in Star Trek, Balance of Terror
 * ![Asteroids](/blog/assets/fettu-asteroids_cc.png "Asteroids")
 * The header image is [Image still from the planetarium show "From Earth to the Universe"](https://www.eso.org/public/images/fettu-asteroids_cc/) by the [European Southern Observatory](https://www.eso.org) and T. Matsopoulos, ESO/S. Brunier, available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.  The [1846 lithography about the solar system](https://en.wikipedia.org/wiki/File:Planet-Vulcan_1846_003790.jpg) cropped from [the version here](https://www.loc.gov/resource/g3180.ct003790) has been in the public domain for a long time.
 * title: Tweets from 04/13 to 04/17
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Social Networking Showdown - Final Thoughts
 * ![Social graph](/blog/assets/structure-play-color-community-colorful-together-818671-pxhere.com.png "Social graph")
 * The header image is [untitled](https://pxhere.com/en/photo/818671) by an anonymous [PxHere](https://pxhere.com) photographer, made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Advice for Aspiring Career-Hoppers, Part II
 * ![Hands at a keyboard](/blog/assets/25388715424_75a7389c41_c.png "Hands at a keyboard")
 * The header image is [Untitled](https://www.flickr.com/photos/wocintechchat/25388715424/) by [WOCinTech Chat](https://www.flickr.com/photos/wocintechchat/), made available under the terms of the [Creative Commons Attribution 2.0 Generic license](https://creativecommons.org/licenses/by/2.0/).
 * title: Developer Journal, 中国语言日
 * ![Chinese writing](/blog/assets/6556307995_971219df67_c.png "Chinese writing")
 * The header image is [My written Chinese](https://www.flickr.com/photos/pablobm/6556307995/) by [Pablo BM](https://www.flickr.com/photos/pablobm/), made available under the terms of the [Creative Commons Attribution 2.0 Generic license](https://creativecommons.org/licenses/by/2.0/), and chosen in honor of the United Nations' [Chinese Language Day](https://en.wikipedia.org/wiki/UN_Chinese_Language_Day), a holiday for which I am utterly unprepared...
 * title: Real Life in Star Trek, Shore Leave
 * ![Alice and the White Rabbit](/blog/assets/4382428505_a10c7597bc_o.jpg "Alice and the White Rabbit")
 * The header image is [Alice in Wonderland: Alice meets the White Rabbit](https://www.flickr.com/photos/43021516@N06/4382428505/) by the Margaret Winifred Tarrant, first published in 1916.
 * title: Tweets from 04/20 to 04/24
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Distributed Community Curation
 * ![Community](/blog/assets/prague-shadows.jpg "Community, back when clustering on a city street didn't risk people's lives")
 * The header image is [Group of people in the sun](https://skitterphoto.com/photos/4612/group-of-people-in-the-sun) by [Peter Heeling](https://skitterphoto.com/photographers/7/peter-heeling), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Advice for Aspiring Career-Hoppers, Part III
 * ![Discussing code](/blog/assets/25392428253_f0416515b6_c.png "Discussing code")
 * ![MVC Workflow](/blog/assets/MVC-Process.svg "MVC Workflow")
 * The header image is [Untitled](https://www.flickr.com/photos/wocintechchat/25392428253/) by [WOCinTech Chat](https://www.flickr.com/photos/wocintechchat/), made available under the terms of the [Creative Commons Attribution 2.0 Generic license](https://creativecommons.org/licenses/by/2.0/).  The [MVC Worklow Diagram](https://commons.wikimedia.org/wiki/File:MVC-Process.svg) has been created and released into the public domain by [RegisFrey](https://en.wikipedia.org/wiki/User:RegisFrey).
 * title: Developer Journal, End of April
 * ![Sweet Pea](/blog/assets/Sweet_Pea_27711557850.png "Sweet Pea")
 * The header image is [Sweet Pea](https://www.flickr.com/photos/eileenmak/27711557850/) by [eileenmak](https://www.flickr.com/people/22807534@N02), made available under the terms of the [Creative Commons Attribution 2.0 Generic license](https://creativecommons.org/licenses/by/2.0/), one of April's birth flowers.
 * title: Real Life in Star Trek, The Galileo Seven
 * ![Quasar](/blog/assets/eso1229a.png "Quasar")
 * title: Tweets from 04/27 to 05/01
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Advice for Aspiring Career-Hoppers, Part IV
 * ![Head to head](/blog/assets/25392378763_a276a9e847_o.png "Head to head")
 * ![Kanban board](/blog/assets/Kanban_board_example.png "Kanban board")
 * The header image is [Untitled](https://www.flickr.com/photos/wocintechchat/25392378763/) by [WOCinTech Chat](https://www.flickr.com/photos/wocintechchat/), made available under the terms of the [Creative Commons Attribution 2.0 Generic license](https://creativecommons.org/licenses/by/2.0/).  The [Kanban board example](https://en.wikipedia.org/wiki/File:Kanban_board_example.jpg) is by Dr. Ian Mitchell and made available under the terms of the [Creative Commons Attribution Share-Alike 2.5 Generic license](https://creativecommons.org/licenses/by-sa/2.5/).
 * title: Developer Journal, If You Give a Mouse a Sci-Fi Franchise...
 * ![Graflex 3 cell flash handle](/blog/assets/8482367681_45105c3a5e_c.png "Graflex 3 cell flash handle")
 * The header image is [The real thing!](https://www.flickr.com/photos/steevithak/8482367681/) by [Steve Rainwater](https://www.flickr.com/photos/steevithak/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic license](https://creativecommons.org/licenses/by-sa/2.0/), and chosen because that camera flash handle happens to be the basis for the original lightsaber prop.
 * title: Real Life in Star Trek, The Squire of Gothos
 * ![Harpsichord](/blog/assets/ClavecinRuckersTaskin.png "Harpsichord")
 * The header image is [Clavecin par Andreas Ruckers (Anvers, 1646) ravalé par Pascal Taskin (Paris, 1780)](https://commons.wikimedia.org/wiki/File:ClavecinRuckers%26Taskin.JPG) by [Gérard Janot](https://commons.wikimedia.org/wiki/User:G%C3%A9rard_Janot), available under a [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) License.
 * title: Tweets from 05/04 to 05/08
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club - Orang-U
 * ![Movie poster](/blog/assets/orang-u-an-ape-goes-to-college-movie-poster.png "Movie poster")
 * ![James Graduating](/blog/assets/orang-u-james.png "James Graduating")
 * ![Haunted MBTA](/blog/assets/orang-u-mbta-med.png "Haunted MBTA")
 * The header image is, of course, the movie's release poster.  The picture of [James at his graduation](https://www.instagram.com/p/BfYphpoD0UQ/) is by [Sarah](https://www.instagram.com/taekwondogirl/).  The MBTA map is on the **Orang-U** website.  All have been made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
 * title: Advice for Aspiring Career-Hoppers, Part V
 * ![Mapping things out](/blog/assets/rgyglpwwjb0-richard-tilney-bassett.png "Mapping things out")
 * The header image is [Untitled](https://magdeleine.co/photo-richard-tilney-bassett-n-858/) by [Richard Tilney-Bassett](https://magdeleine.co/author/richard-tilney-bassett/), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, International Nurses Eve
 * ![Surgery](/blog/assets/table-equipment-biology-human-health-organ-894304-pxhere.com.png "Surgery")
 * Untitled header photograph [from PxHere](https://pxhere.com/en/photo/894304), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Arena
 * ![Not a Gorn](/blog/assets/nature-wildlife-green-macro-reptile-amphibian-587140-pxhere.com.png "Not a Gorn")
 * The header image is [Untitled](https://pxhere.com/en/photo/587140) by the Gabriel González, available under a [Creative Commons Attribution 2.0 Generic](http://creativecommons.org/licenses/by/2.0/) License.
 * title: Tweets from 05/11 to 05/15
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Jathia's Wager
 * ![Title Screen](/blog/assets/Screenshot-from-Jathias-Wager.png "Title Screen")
 * The header image is a screen capture of the title screen, under the same license as every other frame.
 * title: Advice for Aspiring Career-Hoppers, Part VI
 * ![Meeting](/blog/assets/25900940602_0021bda347_c.png "Meeting")
 * The header image is [Untitled](https://www.flickr.com/photos/wocintechchat/25900940602/) by [WOCinTech Chat](https://www.flickr.com/photos/wocintechchat/), made available under the terms of the [Creative Commons Attribution 2.0 Generic License](https://creativecommons.org/licenses/by/2.0/).
 * title: Developer Journal, Culture Freedom Day
 * ![Culture Freedom Day Banner](/blog/assets/Cfd-announce1.png "Culture Freedom Day Banner")
 * The header image is the official [Culture Freedom Day banner](https://commons.wikimedia.org/wiki/File:Cfd-announce1.png) (from 2012) by <http://www.culturefreedomday.org>---which doesn't appear to handle Culture Freedom Day anymore, for some reason---and made available under the terms of the [Creative Commons Attribution 3.0 Unported license](https://creativecommons.org/licenses/by/3.0/deed.en).
 * title: Real Life in Star Trek, Tomorrow Is Yesterday
 * ![The XF-104 prototype](/blog/assets/Lockheed_XF-104_modified.png "The XF-104 prototype")
 * ![Project Orion](/blog/assets/NASA-project-orion-artist.png "Project Orion")
 * ![AMPEX FR-100B](/blog/assets/ampex-fr-100b-ad.png "AMPEX FR-100B")
 * ![AMPEX FR-900](/blog/assets/Ampex_FR-900_at_LOIRP.png "AMPEX FR-900")
 * The header image is a [Lockheed XF-104](https://commons.wikimedia.org/wiki/File:Lockheed_XF-104_(modified).jpg) (prototype for the plane John Christopher flies) by the United States Air Force, placed into the public domain as a work by an employee of the Federal government.  The advertisement for the AMPEX FR-100B is from [**Aviation Week** for September 14<sup>th</sup>, 1959](https://archive.org/details/Aviation_Week_1959-09-14/mode/1up), which appears to have been published without a copyright statement in the relevant places, placing it in the public domain.  The [APMEX FR-900](https://commons.wikimedia.org/wiki/File:Ampex_FR-900_at_LOIRP.jpg) at the [Lunar Orbiter Image Recovery Project](https://en.wikipedia.org/wiki/en:Lunar_Orbiter_Image_Recovery_Project) by [Misternuvistor](https://en.wikipedia.org/wiki/en:User:Misternuvistor) is available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en).  The [Artists Conception of a Project Orion Spacecraft](https://commons.wikimedia.org/wiki/File:NASA-project-orion-artist.jpg) is distributed by NASA (no artist listed), which releases all content into the public domain unless otherwise noted.
 * title: Tweets from 05/18 to 05/22
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Seeds, Prologue to Chapter 3
 * ![Prologue](/blog/assets/ff718beeefb6de94473d74370ad156a687f61ba0d5603f3de237b98197483e7c.png "Prologue")
 * The header image is the prologue's cover image, made available under the same CC-BY-SA 4.0 license as the remainder of the comic.
 * title: Advice for Aspiring Career-Hoppers, Part VII
 * ![Pre-Social Distancing Meeting](/blog/assets/25721006480_cb6ec67706_c.png "Pre-Social Distancing Meeting")
 * The header image is [Untitled](https://www.flickr.com/photos/wocintechchat/25721006480/) by [WOCinTech Chat](https://www.flickr.com/photos/wocintechchat/), made available under the terms of the [Creative Commons Attribution 2.0 Generic License](https://creativecommons.org/licenses/by/2.0/).
 * title: Developer Journal, Africa Day
 * ![Africa](/blog/assets/Africa_and_Europe_from_a_Million_Miles_Away.png "Africa")
 * ![Von Luschan scale](/blog/assets/Felix_von_Luschan_Skin_Color_chart2.svg "Von Luschan scale")
 * ![Victor Mono in Action](/blog/assets/screenshot-2020-05-25-07-08-21.png "Victor Mono in Action")
 * The header image is [Africa and Europe from a Million Miles Away](https://www.nasa.gov/image-feature/africa-and-europe-from-a-million-miles-away) by NASA, dedicated to the public domain by policy.
 * title: Real Life in Star Trek, Court Martial
 * ![Justice-y Things](/blog/assets/wood-hammer-rule-justice-horizontal-court-719590-pxhere.com.png "Justice-y Things")
 * ![Les joueurs d'échecs](/blog/assets/s-900-0190b7.png "Les joueurs d'échecs")
 * The header image is [Untitled](https://pxhere.com/en/photo/719590) by an anonymous PxHere user and available under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 05/25 to 05/29
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Seeds, Chapter 4 to Chapter 4.5
 * ![Chapter 4](/blog/assets/eded2b8a4c10e5f0e2a48bc58adc26264bf7462742f0440896dc63ae71b8a5b5.png "Chapter 4")
 * The header image is Chapter 4's cover image, made available under the same CC-BY-SA 4.0 license as the remainder of the comic.
 * title: Re-Imagining Law Enforcement
 * ![Poor People's March](/blog/assets/Poor_Peoples_March_at_Lafayette_Park_and_on_Connecticut_Avenue_LOC_15173086528.png "Poor People's March")
 * The header image is [Poor People's March at Lafayette Park and on Connecticut Avenue](https://commons.wikimedia.org/wiki/File:Poor_People%27s_March_at_Lafayette_Park_and_on_Connecticut_Avenue_(LOC)_(15173086528).jpg) and placed into the public domain.
 * title: Developer Journal, Global Day of Parents
 * ![Children and One Ringer](/blog/assets/Children_gather_around_a_game_of_Ayo.png "Children and One Ringer")
 * The header image is [Children Play in Ayo Tournament](https://commons.wikimedia.org/wiki/File:Children_gather_around_a_game_of_Ayo.jpg) by [Adedotun Oluwatosin Ajibade](https://commons.wikimedia.org/wiki/User:Dotun55) and made available under the terms of the [Creative Commons Attribution-Share Alike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/deed.en).  Because I would be screaming if someone dropped that statement in *my* lap without explanation, Wikipedia has [an excellent article](https://en.wikipedia.org/wiki/Oware) on Ayo or Oware.
 * title: Real Life in Star Trek, The Return of the Archons
 * ![Cloaked figure](/blog/assets/971815560_d42ab54d9f_o.png "Cloaked figure")
 * The header image is [ep3ani11](https://www.flickr.com/photos/ianmalcm/971815560/in/photolist-2tSNUG-26NoQCc-2tSNME-2tNoZ4-2tNHCk-2bZ35ci-2tNJ3z-2tNHMF-2tSNxN-2tNpzK-2tNpwF-2tNoMk-2tNpH4-2tSPdm-7oUF72-2dZPojY-XFhj7Q-2tT7hA-2tT7uA-2tNpMt-2hbit9M-HzNCjL-2tSNjm-2tNpCV-2tNoBv-2tSNou-ZKF2wF-2tNpEX-2iXPubJ-MKU2y6-TLrKTh-RMPyD8-Lax6pG-GgjSub-wc3C53-rLgcJU-2hjRmHZ-2iDEQNu-W31YpQ-wg5DpX-2kkmjB-w8tZTQ-T72tcn-2ghdpZv-2iSGxvG-cAsJwo-DbDatQ-2e1grTj-Vjz2j8-2tSNby) by the [Justin Sewell](https://www.flickr.com/photos/ianmalcm/), available under a [Creative Commons Attribution 2.0 Generic](http://creativecommons.org/licenses/by/2.0/) License.  I (hastily) edited the image to blur out the model's face and possessions and present my version under the same license, not that I can imagine anybody wanting it.  But, it's surprisingly hard to just find a Free Culture picture of a person in a cloak...
 * title: Tweets from 06/01 to 06/05
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Seeds, Chapter 5 to Chapter 6
 * ![Chapter 5](/blog/assets/71155d730557ad43a9473c7d022b26226cbf1b9c48e3ccb27de7c5f76923928b.png "Chapter 5")
 * The header image is Chapter 5's cover image, made available under the same CC-BY-SA 4.0 license as the remainder of the comic.
 * title: Some Updates
 * ![Under Construction](/blog/assets/structure-pattern-line-art-nederland-rotterdam-473033-pxhere.com.png "Under Construction")
 * The header image is [Scaffolding](https://pxhere.com/en/photo/473033) by Frans Berkelaar, made available under the terms of the [Creative Commons Attribution 2.0 Generic License](https://creativecommons.org/licenses/by/2.0/).
 * title: Developer Journal, World Oceans Day
 * ![The Atlantic Ocean](/blog/assets/Nr_Vorupoer_2013-12-29_1_cropped.png "The Atlantic Ocean")
 * The header image is [Coast of Nørre Vorupør at sunset](https://commons.wikimedia.org/wiki/File:Nr_Vorupoer_2013-12-29_1_cropped.jpg) by [Slaunger](https://commons.wikimedia.org/wiki/User:Slaunger), made available under the terms of [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en).
 * title: Real Life in Star Trek, Space Seed
 * ![Big Falcon Rocket at stage separation](/blog/assets/BFR_at_stage_separation_2-2018.png "Big Falcon Rocket at stage separation")
 * ![John M. Fabian sleeping](/blog/assets/S07-26-1439~orig.png "John M. Fabian sleeping")
 * ![Satan presiding at the Infernal Council](/blog/assets/Martin_John_Satan_presiding_at_the_Infernal_Council_1824.png "Satan presiding at the Infernal Council")
 * The header image is [Big Falcon Rocket at stage separation](https://commons.wikimedia.org/wiki/File:BFR_at_stage_separation_2-2018.jpg) by the [SpaceX Flickr](https://www.eso.org) has been made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).  I couldn't find any pictures of spacecraft used by fascist despots to escape justice from Earth, so we'll have to settle for a picture of a spacecraft funded and marketed by a [tyrannical](https://www.theguardian.com/technology/2018/apr/19/tesla-california-factory-investigated-safety-concerns-model-3) CEO [looking to escape consequences](https://www.coinspeaker.com/elon-musk-inspiration-spacex/) on Earth.  [Mission Specialist Fabian Sleeps on Middeck](https://images.nasa.gov/details-S07-26-1439) has been released by the Johnson Space Center into the public domain through NASA policy.  [Satan Presiding at the Infernal Council](https://commons.wikimedia.org/wiki/File:Martin,_John_-_Satan_presiding_at_the_Infernal_Council_-_1824.JPG) by John Martin, dates to 1824 and has long been in the public domain.
 * title: Tweets from 06/15 to 06/12
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * ![Tree Roots](/blog/assets/33446766913_5c78c4561a_b.png "Tree Roots")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.  [Tree Roots](https://www.flickr.com/photos/davidstanleytravel/33446766913/) by [David Stanley](https://www.flickr.com/photos/davidstanleytravel/) is made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Free Culture Book Club — Stardrifter - Motherload
 * ![Motherload](/blog/assets/pb-motherload-01.png "Motherload")
 * The header image is **Motherload**'s cover image, made available under the same CC-BY-SA 3.0 license as the remainder of the series.
 * title: Sitting Is the New Blah, Blah, Blah
 * ![Namibian squirrels waiting for their standing desks](/blog/assets/Xerus_inauris.png "Namibian squirrels waiting for their standing desks")
 * The header image is [Xerus inauris](https://commons.wikimedia.org/wiki/File:Xerus_inauris.jpg) by [Hans Hillewaert](https://commons.wikimedia.org/wiki/User:Biopics) and is made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported license](https://creativecommons.org/licenses/by-sa/3.0/deed.en).
 * title: Developer Journal, Global Wind Day
 * ![Wind blowing](/blog/assets/Cherry_tree_moving_in_the_wind_1.gif "Wind blowing")
 * The header image is [Cherry tree moving in the wind](https://en.wikipedia.org/wiki/File:Cherry_tree_moving_in_the_wind_1.gif) by [W.carter](https://commons.wikimedia.org/wiki/User:W.carter), released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).
 * title: Writing Browser Extensions
 * ![Plugs](/blog/assets/Reisestecker.png "Plugs")
 * The header image is [Fotowerkstatt](https://commons.wikimedia.org/wiki/File:Reisestecker.jpg) by [Mattes](https://commons.wikimedia.org/wiki/User:Mattes), released into the public domain.
 * title: Real Life in Star Trek, A Taste of Armageddon
 * ![US Naval War College War Game](/blog/assets/NWC_wargame_1958.png "US Naval War College War Game")
 * The header image is [A wargame at the US Naval War College in 1958, using the newly installed Navy Electronic Warfare Simulator](https://commons.wikimedia.org/wiki/File:NWC_wargame_1958.jpg) by the United States Naval War College, in the public domain as a work of the United States government.
 * title: Tweets from 06/15 to 06/19
 * ![Emancipation Day Celebration band, June 19, 1900](/blog/assets/Emancipation-Day-Celebration-band-June-19-1900.png "Emancipation Day Celebration band, June 19, 1900")
 * ![Juneteenth Flag](/blog/assets/Juneteenth_Flag_Variation.png "Juneteenth flag")
 * Header image is [Emancipation Day Celebration band, June 19, 1900](https://commons.wikimedia.org/wiki/File:Emancipation_Day_Celebration_band,_June_19,_1900.png) by Mrs. Charles Stephenson (Grace Murray).  The [Juneteenth Flag Variation](Juneteenth_Flag_Variation.png) by Saturnsorbit has been made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/deed.en).
 * title: Free Culture Book Club - Elephants Dream
 * ![Elephants Dream poster](/blog/assets/ElephantsDreamPoster.png "Elephants Dream poster")
 * The header image is **Elephants Dream**'s official movie poster, made available under the same CC-BY 2.5 license as the remainder of the series.
 * title: Words Are Hard, I Guess...
 * ![Words](/blog/assets/hand-pattern-finger-palm-colorful-arm-942723-pxhere.com.jpg "Words")
 * The header image is [Untitled](https://pxhere.com/en/photo/942723) by an anonymous PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, World Humanist Day (Belated)
 * ![8 Ways to Change the World](/blog/assets/ObjetivosDoMilenio.png "8 Ways to Change the World")
 * The header image is [8 Jeitos de Mudar o Mundo](https://commons.wikimedia.org/wiki/File:Metas.jpg) by (seemingly now defunct) Objetivos do milênio, placed into the public domain.
 * title: Writing Browser Extensions with Configuration
 * ![Plugs](/blog/assets/computer-technology-cable-internet-line-food-779821-pxhere.com.png "Plugs")
 * The header image is [Untitled](https://pxhere.com/en/photo/779821) by an anonymous PxHere user, made available under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, This Side of Paradise
 * ![Exoplanet](/blog/assets/eso1414a.png "Exoplanet")
 * The header image is [Artist’s impression of the planet Beta Pictoris b](https://www.eso.org/public/images/eso1414a/) by the [ESO](https://www.eso.org) and L. Calçada/N. Risinger, available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Tweets from 06/22 to 06/26
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club - Won't You Come In?
 * ![Illustration of Torrent](/blog/assets/wont-you-come-in002color-presto.jpg "Illustration of Torrent")
 * The header image is an illustration from the story, made available under the same CC-BY-SA 3.0 license as the remainder of the series.
 * title: Mid-Year Media Updates
 * ![The 1928 Science and Invention Television](/blog/assets/Science_and_Invention_Television_1928.png "The 1928 Science and Invention Television")
 * The header image is [Science and Invention Television 1928](https://commons.wikimedia.org/wiki/File:Science_and_Invention_Television_1928.jpg), in the public domain.  It was the header image of an article with instructions for building a television receiver to watch experimental broadcasts alongside radio shows, in case anybody would like one.
 * title: Developer Journal, Stonewall Uprising (Belated)
 * ![Stonewall Inn, 1969](/blog/assets/Stonewall_Inn_1969.jpg "Stonewall Inn, 1969")
 * The header image is---probably obviously---the [Stonewall Inn, 1969](https://commons.wikimedia.org/wiki/File:Stonewall_Inn_1969.jpg) by Diana Davies with the New York Public Library, made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, The Devil in the Dark
 * ![Not a Horta](/blog/assets/Aa_large.png "Not a Horta")
 * The header image is [Aa large](https://commons.wikimedia.org/wiki/File:Aa_large.jpg) by the [United States Geological Survey](https://www.usgs.gov/), released into the public domain as a work of the United States Federal government.  Far from a Horta, it's an ʻaʻā flow from [Kīlauea](https://en.wikipedia.org/wiki/K%C4%ABlauea).  The Polynesians apparently refer to [Sirius](https://en.wikipedia.org/wiki/Sirius) as ʻAʻā, interestingly enough, but the *Star Trek* franchise rarely refers to one of our nearest and most famous neighbors, primarily in off-handed references such as what we saw at the end of [*Arena*]({% post_url 2020-05-14-arena %}).
 * title: Tweets from 06/29 to 07/03
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club - Forgotten
 * ![Title Screen](/blog/assets/forgottenblade.png "Title Screen for the game-within-a-game")
 * The header image is **Forgotten Blade**'s title screen, the game within the game, and as such has been released under the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Physical Media and Media Events - Threat or Menace?
 * ![Lots of video](/blog/assets/man-person-group-people-woman-play-918265-pxhere.com.png "Lots of video")
 * The header image is [Untitled](https://pxhere.com/en/photo/918265), released under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Bloody Thursday (Belated)
 * ![A confrontation](/blog/assets/Confrontation_between_a_policeman_wielding_a_night_stick_and_a_striker_during_the_San_Francisco_General_Strike,_1934_-_NARA_-_541926.jpg "A confrontation")
 * The header image is [Confrontation between a policeman wielding a night stick and a striker during the San Francisco General Strike, 1934 - NARA](https://commons.wikimedia.org/wiki/File:Confrontation_between_a_policeman_wielding_a_night_stick_and_a_striker_during_the_San_Francisco_General_Strike,_1934_-_NARA_-_541926.jpg), courtesy of the U.S. Information Agency and placed into the public domain as a work prepared by the United States government.
 * title: Writing Visual Studio Code Extensions
 * ![VSCodium](/blog/assets/vscodium.png "VSCodium")
 * ![Hello, back](/blog/assets/vscrat-hello.png "Hello, back")
 * The header image is the sample image for [VSCodium](https://vscodium.com/) and made available (like the application and their entire site) under the terms of the MIT License.
 * title: Real Life in Star Trek, Errand of Mercy
 * ![A medieval abbey](/blog/assets/Abbey-of-senanque-provence-gordes.png "A medieval abbey")
 * The header image is [Abbey of Senanque](https://commons.wikimedia.org/wiki/File:Abbey-of-senanque-provence-gordes.jpg) by [Greudin](https://commons.wikimedia.org/wiki/User:Greudin), released into the public domain.
 * title: Tweets from 07/06 to 07/10
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club - Magnificent Mechanical Man §1–12
 * ![A Circus](/blog/assets/9478457090_7fe132b72b_o.png "A circus, the book's cover image")
 * The header image is the novel's cover image, and as such has been released under the same CC-BY 3.0 license.
 * title: The Paradox of Tolerance
 * ![Shame](/blog/assets/hand-man-person-male-leg-portrait-987530-pxhere.com.png "A man covering his face in (presumed) shame")
 * The header image is [untitled](https://pxhere.com/en/photo/987530) by an anonymous photographer, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Ludi Apollinares
 * ![Apollo and the Muses](/blog/assets/Parnassus_Andrea_Appiani_1811.png "Apollo and the Muses")
 * The header image is [Parnassus](https://commons.wikimedia.org/wiki/File:Parnassus,_Andrea_Appiani_(1811).jpg) by [Andrea Appiani](https://en.wikipedia.org/wiki/Andrea_Appiani), long in the public domain.
 * title: Configuring a Visual Studio Code Extension
 * ![Visual Studio Codium](/blog/assets/vscodium.png "Visual Studio Codium")
 * ![A Configuration Option](/blog/assets/vscoderat-2020-07-13-config.png "A configuration option!")
 * The header image is the sample image for [VSCodium](https://vscodium.com/) and made available (like the application and their entire site) under the terms of the MIT License.  Yes, I re-used the same image from last week.  Sorry!
 * title: Real Life in Star Trek, The Alternative Factor
 * ![Trifid Nebula](/blog/assets/eso0930a.png "Trifid Nebula")
 * The header image is [The Trifid Nebula](https://www.eso.org/public/usa/images/eso0930a/) by the [ESO](https://www.eso.org), available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.  I figured that it gets enough screen time in the episode that it was worth using as the episode image.
 * title: Tweets from 07/13 to 07/17
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club - Magnificent Mechanical Man §13–24
 * ![A Circus](/blog/assets/9478457090_7fe132b72b_o.png "A circus, the book's cover image")
 * The header image is the novel's cover image, and as such has been released under the same CC-BY 3.0 license.
 * title: Copyright Searches
 * ![The Statute of Anne](/blog/assets/Statute_of_anne.jpg "The Statute of Anne, arguably the world's first copyright law")
 * ![Exciting Comics #9](/blog/assets/exciting9indicia.png "Exciting Comics #9's indicia")
 * ![The renewals for 1969](/blog/assets/exciting1969renewals.png "The renewals for 1969")
 * ![The renewals for 1970](/blog/assets/exciting1970renewals.png "The renewals for 1970")
 * The header image is the start to the Statute of Anne, what's typically identified as the world's first copyright law, old enough to itself be in the public domain.  The **Exciting Comics #9** incidia is used in the spirit of Fair Use.  The excerpts of **The Catalog of Copyright Entries** are in the public domain, as works of a United States government agency.
 * title: Developer Journal, Moon Landing Anniversary
 * ![Buzz Aldrin](/blog/assets/Aldrin_Apollo_11_original.png "Buzz Aldrin")
 * The header image is [Buzz Aldrin on the Moon, with Neil Armstrong seen in the helmet's reflection](https://commons.wikimedia.org/wiki/File:Aldrin_Apollo_11_original.jpg), in the public domain by NASA policy.
 * title: Real Life in Star Trek, The City on the Edge of Forever
 * ![Not the Guardian](/blog/assets/landscape-sea-nature-sand-rock-wilderness-701261-pxhere.com.png "Not the Guardian")
 * The header image is [Delicate Arch](https://pxhere.com/en/photo/701261) by an anonymous photographer, released under the terms of the [Creative Commons CC0 1.0 Public Domain Declaration](https://creativecommons.org/publicdomain/zero/1.0/), taken at Utah's [Arches National Park](https://en.wikipedia.org/wiki/Arches_National_Park), specifically [Delicate Arch](https://en.wikipedia.org/wiki/Delicate_Arch), a likely inspiration for the Guardian.
 * title: Tweets from 07/20 to 07/24
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club - Magnificent Mechanical Man §25–36
 * ![A Circus](/blog/assets/9478457090_7fe132b72b_o.png "A circus, the book's cover image")
 * The header image is the novel's cover image, and as such has been released under the same CC-BY 3.0 license.
 * title: Developer Journal, Korean War Veterans Armistice Day
 * ![South Korean refugees](/blog/assets/South_Korean_refugees_mid-1950.png "South Korean refugees")
 * The header image is [South Korean refugees mid-1950](https://commons.wikimedia.org/wiki/File:South_Korean_refugees_mid-1950.jpg), in the public domain as a work of the United States government.
 * title: Changing the Primary Git Branch
 * ![Branches](/blog/assets/tree-nature-branch-winter-black-and-white-plant-997803-pxhere.com-1.png "Branches")
 * The header image is [tree, nature, branch, winter, black and white, plant, trunk, bark, high, monochrome, twig, trees, twigs, woods, branches, tall, monochrome photography, atmospheric phenomenon, woody plant](https://pxhere.com/en/photo/997803) by an anonymous photographer, released under the terms of [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Operation—Annihilate!
 * ![Close enough](/blog/assets/apple-water-dish-food-produce-breakfast-1065008-pxhere.com.png "Parasitic thing? Apple pancake? Something like that")
 * ![Building R1](/blog/assets/TRW_building_R1.png "TRW Building R1")
 * ![Wong Baker Faces Scale](/blog/assets/面部表情疼痛评分量表.png "The Wong Baker Faces Scale")
 * The header image is [untitled](https://pxhere.com/en/photo/1065008) by an anonymous photographer, made available under a [Creative Commons CC0 1.0 Universal  Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  [TRW building R1](https://commons.wikimedia.org/wiki/File:TRW_building_R1.jpg) by [Doug Coldwell](https://commons.wikimedia.org/wiki/User:Doug_Coldwell) has been made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.  The [Facial Expression Pain Rating Scale](https://commons.wikimedia.org/wiki/File:%E9%9D%A2%E9%83%A8%E8%A1%A8%E6%83%85%E7%96%BC%E7%97%9B%E8%AF%84%E5%88%86%E9%87%8F%E8%A1%A8.png) is from China's NHC and, as such, is not covered by copyright law.  I picked the Chinese version, because I believe that the scale itself only dates to 2009 or so, making any re-creations in the United States a *bit* suspect, even if entirely different art is used, whereas the work of a large government has at least some inherent legitimacy.
 * title: Tweets from 07/27 to 07/31
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club - Magnificent Mechanical Man §37–49
 * ![A Circus](/blog/assets/9478457090_7fe132b72b_o.png "A circus, the book's cover image")
 * The header image is the novel's cover image, and as such has been released under the same CC-BY 3.0 license.
 * title: Wanted — Ethical Media Consumption
 * ![Some media](/blog/assets/aditya_ram21.png "Some media")
 * The header image is [Adityaram Media having the studios for movie shootings](https://morguefile.com/p/956254) by [Adityaram](https://morguefile.com/creative/Adityaram), made available under the terms of the [Morgue File license](https://morguefile.com/license) that I *think* is basically compatible with the blog's license.
 * title: Developer Journal, Friendship Day
 * ![Friends, pre-pandemic](/blog/assets/african-american-african-descent-american-asian-black-casual-1444755-pxhere.com.png "Friends, pre-pandemic")
 * The header image is [Friends looking at a smartphone](https://pxhere.com/en/photo/1444755) by an anonymous [RawPixel](http://www.rawpixel.com/) photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Season 1 Summary
 * ![Watching the Galaxy](/blog/assets/lv_Brunier_hb-1.png "Watching the Galaxy")
 * The header image is [The Galactic Centre above the ESO 3.6-metre telescope](https://www.eso.org/public/images/lv_Brunier_hb-1/) by the [ESO](https://www.eso.org) an S. Brunier, made available under the terms of the [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Tweets from 08/03 to 08/07
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Morevna, Episode 3
 * ![Princess Marya](/blog/assets/Morevna3Screenshot.png "Princess Marya")
 * The header image is **Forgotten Blade**'s title screen, the game within the game, and as such has been released under the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Indigenous Peoples Day
 * ![People indigenous to Paraguay or Brazil](/blog/assets/Guarani_Family.png "People indigenous to Paraguay or Brazil")
 * The header image is [Guarani Family](https://commons.wikimedia.org/wiki/File:Guarani_Family.JPG) by [Robertobra](https://commons.wikimedia.org/wiki/User:Robertobra), released under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported license](https://creativecommons.org/licenses/by-sa/3.0/deed.en).
 * title: Real Life in Star Trek, Amok Time
 * ![Not actually Vulcan](/blog/assets/Sine-Ngayène-TP13.png "Not actually Vulcan...")
 * ![Spock and T'Pring](/blog/assets/Spock_and_TPring.jpg "Spock and T'Pring")
 * The header image adapted from [Stone circles of Sine Ngayène, Senegal, Kaolack province](https://commons.wikimedia.org/wiki/File:Stone_circles_Sine_Ngay%C3%A8ne_TP13.jpg) by [Tobias 67](https://commons.wikimedia.org/wiki/User:Tobias_67), available under a [Creative Commons Attribution Share-Alike 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/) License.  [Spock and T'Pring](https://commons.wikimedia.org/wiki/File:Spock_and_T%27Pring.jpg) (Arlene Martel and Leonard Nimoy) is also used, as a photographic work without a copyright notice, meant for republication as publicity.
 * title: Tweets from 08/10 to 08/14
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Prelude to Whitespace
 * ![Transitioning to Whitespace](/blog/assets/20200105_203339.gif "Transitioning to Whitespace")
 * The header image is [*Into the White*](https://valianttheywere.blogspot.com/2020/01/white-space-into-white.html) by Sean Robert Meaney, placed into the public domain.
 * title: Fiction — Grandfathers, Butterflies, and Patient Minus-One
 * ![Camp Funston](/blog/assets/Camp-Funston-Kansas-NCP-1603.png "Camp Funston, clearly the worst possible name for an emergency hospital")
 * The header image is [Emergency hospital during Influenza epidemic, Camp Funston, Kansas - NCP 1603](https://commons.wikimedia.org/wiki/File:Emergency_hospital_during_Influenza_epidemic,_Camp_Funston,_Kansas_-_NCP_1603.jpg), from the [Otis Historical Archives, National Museum of Health and Medicine](https://www.flickr.com/people/99129398@N00) and in the public domain based on its age.
 * title: Developer Journal, Marcus Garvey's Birthday
 * ![Marcus Garvey](/blog/assets/Marcus-Garvey-1924-08-05.png "Marcus Garvey")
 * The header image is [Marcus Garvey, National Hero of Jamaica, full-length, seated at desk](https://commons.wikimedia.org/wiki/File:Marcus_Garvey_1924-08-05.jpg) by George Grantham, its copyright expired in January of this year unless it already had previously.
 * title: Real Life in Star Trek, Who Mourns for Adonais?
 * ![Apollo e Dafne](/blog/assets/Piero-del-pollaiolo-apollo-e-dafne-1470-75-ca.png "Apollo e Dafne")
 * The header image is [Apollo e Dafne](https://commons.wikimedia.org/wiki/File:Piero_del_pollaiolo,_apollo_e_dafne,_1470-75_ca._(nat._gallery)_02.JPG) by Piero del Pollaiolo and long in the public domain.
 * title: Tweets from 08/17 to 08/21
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Pentagon
 * ![Pentagon movie poster](/blog/assets/pentagon-2008-the-hill-productions.png "Pentagon movie poster")
 * The header image is **Pentagon**'s movie poster, released under the same terms as the movie itself.
 * title: Impostor Syndrome
 * ![Nobody will notice...](/blog/assets/PugImposter.png "Pug Impostor")
 * The header image is [Pug Imposter 'Pug Love'](https://wunderstock.com/photo/pug-imposter-pug-love_8426525097) by [DaPuglet](https://www.flickr.com/photos/43810158@N07) on [Wunderstock](https://wunderstock.com), released under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Developer Journal, International Strange Music Day
 * ![Glass Armonica](/blog/assets/Glassarmonica.png "Glass Armonica")
 * The header images is [Glass Armonica](https://commons.wikimedia.org/wiki/File:Glassarmonica.jpg) by Vince Flango, released into the public domain by the creator.
 * title: Real Life in Star Trek, The Changeling
 * ![Not Nomad](/blog/assets/Capstone-graphic-13feb20-0.png "Not the Nomad Probe")
 * The header image is [Illustration of the Cislunar Autonomous Positioning System Technology Operations and Navigation Experiment](https://commons.wikimedia.org/wiki/File:Capstone_graphic_13feb20_0.jpg) by [NASA](https://www.nasa.gov/directorates/spacetech/small_spacecraft/NASA_CubeSats_Play_Big_Role_in_Lunar_Exploration), placed into the public domain by agency policy.
 * title: Tweets from 08/24 to 08/28
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * ![Coffee Beans](/blog/assets/java-coffee-beans.jpg "Coffee Beans")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Virtuoso Prologue
 * ![Virtuoso Logo](/blog/assets/virtuoso-banner.png "Virtuoso Logo")
 * The header image is **Virtuoso**'s logo, released under the same terms as the comic and website itself.
 * title: Of Course Political Parties Don't Reflect You!
 * ![Politicians doing something right](/blog/assets/SigningUSConstitution.png "The first United States Congress signing the Constitution")
 * The header image is [Scene at the Signing of the Constitution of the United States](https://commons.wikimedia.org/wiki/File:Scene_at_the_Signing_of_the_Constitution_of_the_United_States.jpg) by [Howard Chandler Christy](https://en.wikipedia.org/wiki/Howard_Chandler_Christy), placed in the public domain as the work of the United States government.
 * title: Developer Journal, September Eve
 * ![Les Très Riches Heures du duc de Berry septembre](/blog/assets/Les_Très_Riches_Heures_du_duc_de_Berry_septembre.png "Les Très Riches Heures du duc de Berry septembre")
 * The header images is [Les Très Riches Heures du duc de Berry septembre](https://commons.wikimedia.org/wiki/File:Les_Tr%C3%A8s_Riches_Heures_du_duc_de_Berry_septembre.jpg) by the Limbourg brothers and others, long in the public domain.
 * title: Real Life in Star Trek, Mirror, Mirror
 * ![Mirror, Mirror, on the Wall...](/blog/assets/europaSnowWhiteQueen.png "Mirror, Mirror, on the Wall...")
 * The header image is an illustration from [Europa's fairy book](https://archive.org/details/europasfairybook00jaco/page/200/mode/2up) by Joseph Jacobs, illustrated by John Dickson Batten, long in the public domain.
 * title: Tweets from 08/31 to 09/04
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Endgame∶ Singularity
 * ![Main Screen](/blog/assets/endgame-singularity-screenshot.png "Main Screen")
 * The header image is a screenshot of **Endgame: Singularity**, original to me, which I release under the terms of the same CC-BY-SA 4.0 license as the rest of the blog, which *should* be legitimate, with the in-game artwork being compatible.
 * title: The Fight of the Century (and The Last Century)
 * ![A French salon](/blog/assets/Salon_de_Madame_Geoffrin.png "A French salon")
 * The header image is []().
 * title: Developer Journal, International Literacy Eve
 * ![Extreme Literacy!](/blog/assets/desk-male-office-student-room-preparation-858304-pxhere.com.png "Extreme Literacy!")
 * The header image is [Untitled](https://pxhere.com/en/photo/858304) by an unknown PxHere photographer, released under the terms of the [Creative Commons CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) Public Domain Dedication.
 * title: Real Life in Star Trek, The Apple
 * ![Vaal was unavailable](/blog/assets/lion-Ba-Tam-Pagoda-Hanoi.png "Vaal was unavailable...unaVAALable?")
 * ![Adam and Eve](/blog/assets/Peter_Paul_Rubens_004.png "Adam and Eve, by Peter Paul Reubens")
 * The header image is based on [Sculpture of a lion, Bà Tấm Pagoda, Hanoi (1115), Vietnam National Museum of Fine Arts, Hanoi, Vietnam - 20131030](https://commons.wikimedia.org/wiki/File:Sculpture_of_a_lion,_B%C3%A0_T%E1%BA%A5m_Pagoda,_Hanoi_(1115),_Vietnam_National_Museum_of_Fine_Arts,_Hanoi,_Vietnam_-_20131030.JPG) by the [SMU Constitutional and Administrative Law Wikipedia Project](https://commons.wikimedia.org/wiki/User:Sgconlaw), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](http://creativecommons.org/licenses/by-sa/3.0/) License.  [The Fall of Man, Adam and Eve](https://commons.wikimedia.org/wiki/File:Peter_Paul_Rubens_004.jpg) is the 1629 painting by Peter Paul Reubens, old enough to long be in the public domain.
 * title: Tweets from 09/07 to 09/11
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Book Dash Books, Part 1
 * ![My Special Hair](/blog/assets/BD_spread_Cover.png "My Special Hair's Cover")
 * The header image is cropped from **My Special Hair**, so released under the terms of the same CC-BY 4.0 license as the rest of the book.
 * title: Developer Journal, International Eve of Democracy
 * ![Voting box](/blog/assets/A_coloured_voting_box.png "Voting box")
 * The header image is [A coloured voting box](https://en.wikipedia.org/wiki/File:A_coloured_voting_box.svg) by [Anomie](https://en.wikipedia.org/wiki/User:Anomie), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Real Life in Star Trek, The Doomsday Machine
 * ![Probably not a doomsday machine](/blog/assets/kisscc0-camping-food-hiking-outdoor-recreation-campfire-food-5afd7eb593b647.952647071526562485605.png "Probably not a doomsday machine...unless there's a really terrible sandwich in the foil")
 * ![Moby-Dick attacking a whaling ship](/blog/assets/Moby_Dick_p510_illustration.png "Moby-Dick attacking a whaling ship")
 * The header image is [Water,Coal,Animal Source Foods](kisscc0-camping-food-hiking-outdoor-recreation-campfire-food-5afd7eb593b647.952647071526562485605.png) by an unknown photographer, released under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  The picture of Moby-Dick is from an 1892 illustration of the novel by Augustus Burnham Shute.
 * title: Tweets from 09/14 to 09/18
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Book Dash Books, Part 2
 * ![Springing over tall buildings](/blog/assets/springloaded-spring.png "Springing over tall buildings")
 * The header image is cropped from a page of **Springloaded**, so is under the same CC-BY 4.0 license as the rest of the book.
 * title: Personal Pronouns, Revisited
 * ![Labels](/blog/assets/kisscc0-label-paper-textile-printing-marketing-design-5afdb270c28a29.7801036915265757287968.png "Labels")
 * The header image is [Label Paper Textile Printing Marketing](https://www.kisscc0.com/photo/label-paper-textile-printing-marketing-design-cn2vdr/) by an anonymous/unknown [KissCC0](https://www.kisscc0.com/) photographer and is [made available](https://www.kisscc0.com/license.html) under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, International Day of Peace
 * ![Gold Olive Branch Left on the Moon by Neil Armstrong](/blog/assets/GoldOliveBranchMoon.png "Gold Olive Branch Left on the Moon by Neil Armstrong")
 * The header image is [Gold Olive Branch Left on the Moon by Neil Armstrong](https://commons.wikimedia.org/wiki/File:Gold_Olive_Branch_Left_on_the_Moon_by_Neil_Armstrong_-_GPN-2002-000070.jpg) photographed by an unknown NASA photographer, so released into the public domain by NASA policy.
 * title: Real Life in Star Trek, Catspaw
 * ![A black cat, annoyed at hurtful stereotypes](/blog/assets/cat-kitten-black-eyes-mustache-ears-1583599-pxhere.com.png "A black cat, annoyed at hurtful stereotypes")
 * ![The Weird Sisters](/blog/assets/Three_Witches_Macbeth_James_Henry_Nixon.png "The Weird Sisters")
 * ![Sylvia and Kirk](/blog/assets/Antoinette-Bower-William-Shatner-Star-Trek-1967.png "It's like a perp-walk, but for magic")
 * The header image is [кот котенок черный глаза усы уши взгляд](https://pxhere.com/en/photo/1583599) by [vasilisa.via](https://pxhere.com/en/photographer/309524), released under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  [Three Witches, MacBeth](https://commons.wikimedia.org/wiki/File:Three_Witches,_MacBeth,_by_James_Henry_Nixon,_British_Museum,_1831.png) by James Henry Nixon was created in 1831, so long in the public domain.  The image of [Antoinette Bower and William Shatner](https://commons.wikimedia.org/wiki/File:Antoinette_Bower_William_Shatner_Star_Trek_1967.jpg) was published without copyright notices---for the purposes of republication as publicity, even---and so is in the public domain.
 * title: Tweets from 09/21 to 09/25
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Where Are the Joneses? Part 1
 * ![Ian and his Globe](/blog/assets/534761975_6dcd139c42_o.png "Ian and his Globe")
 * The header image is [Ian Jones stows away in Dawn's S-Max](https://www.flickr.com/photos/wherearethejoneses/534761981/) by [The Joneses](https://www.flickr.com/photos/wherearethejoneses/), a publicity shot for the show released under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Short Fiction — Instant Podcast
 * ![An older-style microphone](/blog/assets/AstaticCrystalMic.png "An older-style microphone")
 * The header image is [Vintage Astatic crystal microphone](https://commons.wikimedia.org/wiki/File:Astatic_crystal_mic.jpg) by [LuckyLouie](https://commons.wikimedia.org/wiki/User:LuckyLouie), released under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.  The music credits follow for the audio version.
 * title: Developer Journal, International Right to Know Day
 * ![Knowledge](/blog/assets/Knowledge-Reid-Highsmith.png "Knowledge")
 * The header image is [Knowledge](https://commons.wikimedia.org/wiki/File:Knowledge-Reid-Highsmith.jpeg), painted by Robert Lewis Reid in 1896, and so long in the public domain.
 * title: Real Life in Star Trek, I, Mudd
 * ![The episode goes something like this](/blog/assets/taxi-kitten-cat-mammal-vertebrate-vacuum-143790-pxhere.com.png "The episode goes something like this")
 * The header image is [Finally, my own personal taxi!](https://pxhere.com/en/photo/143790) by an unknown PxHere photographer, released under the terms of the [Creative Commons 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  If a cat riding a robot vacuum cleaner describes any episode, this is that episode.
 * title: Tweets from 09/28 to 10/02
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Where Are the Joneses? Part 2
 * ![Survived an ordeal](/blog/assets/783059407_3e21ab4d3e_o.png "Survived an ordeal")
 * The header image is [Dawn and Ian painted faces](https://www.flickr.com/photos/wherearethejoneses/783059407/) by [The Joneses](https://www.flickr.com/photos/wherearethejoneses/), released under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Developer Journal, World Teachers Day
 * ![A primary school teacher in northern Laos](/blog/assets/TeacherInLaos.png "A primary school teacher in northern Laos")
 * The header image is [Teacher in Laos](https://commons.wikimedia.org/wiki/File:Teacher_in_Laos.jpg) by Masae, released into the public domain.
 * title: Real Life in Star Trek, Metamorphosis
 * ![A planetoid](/blog/assets/PIA20182~orig.png "A planetoid")
 * The header image is [Ceres Rotation and Occator Crater](https://images.nasa.gov/details-PIA20182) by NASA/JPL-Caltech/UCLA/MPS/DLR/IDA, released into the public domain by NASA policy.
 * title: Tweets from 10/05 to 10/09
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Where Are the Joneses? Part 3
 * ![Laundry Day](/blog/assets/1015868050_c0ec1fc8cb_o.png "Laundry Day")
 * The header image is [Car wash](https://www.flickr.com/photos/wherearethejoneses/1015868050/) by [The Joneses](https://www.flickr.com/photos/wherearethejoneses/) (and slightly modified by me), released under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Voting as a Communal Activity
 * ![Voting together](/blog/assets/DSCF8469_50104988827.png "Voting together")
 * The header image is [untitled](https://www.flickr.com/photos/studiokanu/50104988827/) by [Studio Incendo](https://www.flickr.com/people/29418416@N08), released under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Indigenous Peoples' Day
 * ![An Indigenous Peoples' Day celebration](/blog/assets/Indigenous-Peoples-Day-8084917906.png "An Indigenous Peoples' Day Celebration")
 * The header image is [Day 286: Indigenous Peoples Day](https://www.flickr.com/photos/quinnanya/8084917906/) by [Quinn Dombrowski](https://www.flickr.com/photos/quinnanya/), released under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Real Life in Star Trek, Journey to Babel
 * ![The Tower of Babel](/blog/assets/BruegelTheTowerOfBabel.png "The Tower of Babel")
 * ![Sarek, Spock, and Amanda](/blog/assets/Spock-and-Parents.png "Sarek, Spock, and Amanda")
 * The header image is [The Tower of Babel](https://commons.wikimedia.org/wiki/File:Pieter_Bruegel_the_Elder_-_The_Tower_of_Babel_(Vienna)_-_Google_Art_Project.jpg) by the [Pieter Bruegel the Elder](https://en.wikipedia.org/wiki/Pieter_Bruegel_the_Elder), long passed into the public domain.  [Spock and parents 1968](https://commons.wikimedia.org/wiki/File:Spock_and_parents_1968.jpg) (Mark Lenard, Leonard Nimoy, and Jane Wyatt) is also used, as a photographic work without a copyright notice, meant for republication as publicity.
 * title: Tweets from 10/12 to 10/16
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Where Are the Joneses? Part 4
 * ![A guitar divided](/blog/assets/1304578426_3f34d835a8_o.png "A guitar divided...")
 * The header image is [Guitar in 2](https://www.flickr.com/photos/wherearethejoneses/1304578426/) by [The Joneses](https://www.flickr.com/photos/wherearethejoneses/), released under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Politics in Art and Technology
 * ![Horse painting in Lascaux Cave](/blog/assets/Lascaux2.jpg "Horse painting in Lascaux Cave")
 * The header image is [Horse in Lascaux Cave](https://commons.wikimedia.org/wiki/File:Lascaux2.jpg), in the public domain since...forever, I guess, as one of the earliest known works of art in the world.
 * title: Developer Journal, Nanomonestotse
 * ![A Nanomonestotse village](/blog/assets/NanomonestotseVillage.png "A Nanomonestotse Village")
 * The header image is based on [Nanomonestotse Village](https://commons.wikimedia.org/wiki/File:NanomonestotseVillage.jpg) by [Paxsis](https://commons.wikimedia.org/w/index.php?title=User:Paxsis&action=edit&redlink=1), released under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Friday's Child
 * ![Vasquez Rocks](/blog/assets/Geocaching-at-Vasquez-Rocks-2427296234.png "Vasquez Rocks")
 * ![Kirk and Eleen](/blog/assets/William-Shatner-Julie-Newmar-Star-Trek-1967.png "🎶 I can threaten to show you the world... 🎶")
 * The header image is [Geocaching at Vasquez Rocks](https://www.flickr.com/photos/respres/2427296234/) by the [Jeff Turner](https://www.flickr.com/photos/respres/), available under a [Creative Commons Attribution 2.0 Generic](http://creativecommons.org/licenses/by/2.0/) License.  It's probably not the same location as in the episode, but I wasn't about to dig through the hundreds of images of the park for a closer match.
 * title: Tweets from 10/19 to 10/23
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Drakes
 * ![Front Cover, sideways](/blog/assets/DrakesFrontCover.png "Drakes")
 * The header image is the book's cover, made available under the same terms as the rest of the book.
 * title: Developer Journal, World Eve for Audiovisual Heritage
 * ![Audiovisual media](/blog/assets/Audiovisual.png "Audiovisual media")
 * The header image is [A home cinema satellite speaker provides high-quality audio for watching a concert on a flat screen television](https://commons.wikimedia.org/wiki/File:Audiovisual.jpg) by [Colin](https://commons.wikimedia.org/wiki/User:Colin) and [Mikey Hennessy](https://www.flickr.com/photos/mikeyh), released under the terms of the [Creative Commons Attribution 3.0 Unported](https://creativecommons.org/licenses/by/3.0/deed.en) license.
 * title: Writing a Twitter Bot
 * ![A bluebird](/blog/assets/nature-bird-wildlife-beak-blue-robin-915663-pxhere.com.png "A bluebird")
 * The header image is [untitled](https://pxhere.com/en/photo/915663) by an unnamed photographer, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, The Deadly Years
 * ![An aging woman](/blog/assets/Portrait-of-an-old-Walapai-Indian-woman-Kingman-Arizona-1902-CHS-3196.png "An aging woman")
 * The header image is [Portrait of an old Walapai Indian woman, Kingman, Arizona, 1902](http://digitallibrary.usc.edu/cdm/ref/collection/p15799coll65/id/14582) by Charles C. Pierce, long in the public domain.
 * title: Tweets from 10/26 to 10/30
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — MTF Sigma-5 "Pumpkin Punchers"
 * ![Jack O'Lanterns](/blog/assets/night-sweet-spooky-dark-food-produce-1324569-pxhere.com.png "Jack O'Lanterns")
 * The header image is [untitled](https://pxhere.com/en/photo/1324569) by an unnamed photographer, released under the terms of the [Creative Commons CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) public domain dedication.
 * title: Developer Journal, Election Eve
 * ![People waiting to vote](/blog/assets/El_mourouj_élection_1.png "People waiting to vote")
 * The header image is [wainting line at El mourouj](https://commons.wikimedia.org/wiki/File:El_mourouj_(%C3%A9lection)_1.jpg) (sic) by [Wael Ghabara](https://commons.wikimedia.org/wiki/User:Ghabara), made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Obsession
 * ![Dark Matter Poster](/blog/assets/darkmatter-000.png "Dark Matter Poster")
 * The header image is [Dark Matter](https://exoplanets.nasa.gov/resources/2248/dark-matter/?galaxy_horror) by [NASA-JPL/Caltech](https://www.nasa.gov), placed in the public domain by NASA policy.  The posters were released for Halloween and it was better than trying to find something that could pass as a vampire space-cloud...
 * title: Tweets from 11/02 to 11/06
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Nothing to Hide
 * ![Title Screen](/blog/assets/NothingToHide.png "Nowhere to Hide Title Screen")
 * ![Vote Gardner](/blog/assets/pickup_gardner.png "Vote Gardner")
 * ![Nobody messes with the Ministry](/blog/assets/nobody_messes.png "Nobody messes with the Ministry")
 * ![PanoptiCorp](/blog/assets/nth-footer.png "PanoptiCorp")
 * The header image is the game's title screen, made available under the terms of the same "unlicense" as the rest of the game.  Likewise, other images come directly from game assets.
 * title: Post-Election Reconciliation
 * ![Joe Biden touring a neighborhood in Youngston, Ohio](/blog/assets/biden-header-middleclass.png "Joe Biden touring a neighborhood in Youngston, Ohio")
 * The header image is [Vice President Joe Biden tours the Idora Neighborhood with Assistant Secretary of Commerce for Economic Development](https://obamawhitehouse.archives.gov/vp) by David Lienemann, released into the public domain as a work of the United States government.
 * title: Developer Journal, Benjamin Banneker's Birthday
 * ![Benjamin Banneker](/blog/assets/Benjamin_Banneker_LCCN2010641717.png "Benjamin Banneker")
 * The header image is [Benjamin Banneker: Surveyor-Inventor-Astronomer](https://loc.gov/pictures/resource/highsm.09905/) by Maxime Seelbinder (mural) and [Carol M. Highsmith](https://en.wikipedia.org/wiki/Carol_M._Highsmith) (photograph).  The mural is in the public domain as a work of the United States government, and Highsmith dedicated her collection of photographs at the Library of Congress to the public domain.
 * title: Real Life in Star Trek, Wolf in the Fold
 * ![The Ripper from Punch Magazine](/blog/assets/TennielRipper.png "The Ripper from Punch Magazine")
 * The header image is [John Tenniel - Punch - Ripper cartoon](https://commons.wikimedia.org/wiki/File:John_Tenniel_-_Punch_-_Ripper_cartoon.png) by John Tenniel, long passed into the public domain.
 * title: Tweets from 11/09 to 11/13
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Elvie
 * ![Introducing Elvie](/blog/assets/Elvie.png "Introducing Elvie")
 * ![Richard Lippman/Stallman?](/blog/assets/elvie-028.png "Richard Lippman/Stallman?")
 * The header image is the first strip in the series, introducing the main character.
 * title: Loving It and/or Leaving It and What “It” Might Be
 * ![Coming together](/blog/assets/hand-number-finger-community-together-handle-773437-pxhere.com.png "Coming together")
 * Untitled header photograph [from PxHere](https://pxhere.com/en/photo/773437), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, International Day for Tolerance
 * ![Handshake](/blog/assets/hand-rock-wood-floor-cobblestone-wall-652711-pxhere.com.png "Handshake")
 * The header image is [untitled](https://pxhere.com/en/photo/652711) by an uncredited photographer, who released it under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, The Trouble with Tribbles
 * ![Probably Not a Tribble](/blog/assets/nature-leather-countryside-animal-wildlife-fur-715615-pxhere.com.png "Probably Not a Tribble")
 * The header image is adapted from [untitled](https://pxhere.com/en/photo/715615) by an unlisted photographer, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  Based on the tags of the original, it was a close-up picture of highlander bull fur.
 * title: Tweets from 11/16 to 11/20
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — The Journey to Thelio
 * ![On the run](/blog/assets/vlcsnap-2020-11-11-14h41m11s716.png "On the run")
 * The header image is just a screen capture from the final episode.
 * title: Replybrary
 * ![It's a robot!](/blog/assets/4189390241_d6fef03f49_k.png "It's a robot!")
 * The header image is [Gabriel in robot costume](https://www.flickr.com/photos/21550937@N03/4189390241) by [Roy Luck](https://www.flickr.com/photos/royluck/), released under the terms of the Creative Commons [Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Mary Brewster Hazelton's Birthday
 * ![Two Sisters at a Piano](/blog/assets/MaryBrewsterHazelton,TwoSistersataPiano1894.png "Two Sisters at a Piano")
 * The header image is [*Two Sisters at a Piano*](https://upload.wikimedia.org/wikipedia/commons/f/f2/Mary_Brewster_Hazelton%2C_Two_Sisters_at_a_Piano%2C_1894.jpg) by Mary Brewster Hazelton, long since lapsed into the public domain.  Unable to find a copy of her award-winning *In a Studio*, this will have to suffice.
 * title: Real Life in Star Trek, The Gamesters of Triskelion
 * ![Triskelion with more literal legs](/blog/assets/IsleOfMannFlag.png "Triskelion with more literal legs")
 * ![Daniel in the Lions' Den](/blog/assets/Daniellion.png "Daniel in the Lion's Den")
 * The header image is [isle of man](https://openclipart.org/detail/119587/isle-of-man-by-anonymous) (sic) by an anonymous Open Clip Art contributor, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 11/23 to 11/27
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Pratham Books, Part 1
 * ![The Worst Haircut Idea](/blog/assets/AnnualHaircutday.png "The Worst Haircut Idea")
 * The header image is just an image from **Annual Haircut Day**.
 * title: Developer Journal, Jonathan Swift's Birthday
 * ![Jonathan Swift](/blog/assets/JonathanSwiftNoWig.png "Jonathan Swift")
 * The header image is [Jonathan Swift1745](https://commons.wikimedia.org/wiki/File:Jonathan_Swift_by_Rupert_Barber,_1745,_National_Portrait_Gallery,_London.JPG) by portrait painter Rupert Barber, and has been in the public domain for many years.
 * title: Real Life in Star Trek, A Piece of the Action
 * ![Tommy Gun in a violin case](/blog/assets/Thompson_in_violin_case.png "Tommy Gun in a violin case")
 * The header image is [Thompson Submachine Gun, Model 1928A1, stored in a violin case](https://commons.wikimedia.org/wiki/File:Thompson_in_violin_case.jpg) by  C. Corleis, available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](http://creativecommons.org/licenses/by-sa/3.0/) License.  The trope doesn't actually show up in the episode, of course, though many so-called Tommy Guns do.
 * title: Tweets from 11/30 to 12/04
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Pratham Books, Part 2
 * ![Fighting the Suns](/blog/assets/SeventhSunP04.png "Fighting the Suns")
 * The header image is a page from **The Seventh Sun**.
 * title: Developer Journal, Willa Cather's Birthday
 * ![Willa Cather](/blog/assets/VanVechten-WillaCather.png "Willa Cather")
 * The header image, unsurprisingly, is [Portrait of Willa Cather](https://www.loc.gov/pictures/resource/van.5a51808/?co=van) (cropped) by Carl Van Vechten, in the public domain as a donation to the Library of Congress where the [donor restrictions have expired](https://www.loc.gov/rr/print/res/079_vanv.html).
 * title: Real Life in Star Trek, The Immunity Syndrome
 * ![Space amoeba...technically](/blog/assets/Collection-Penard-MHNG-Specimen-05bis-1-1-Amoeba-proteus.png "Space amoeba...technically")
 * The header image is adapted from [Amoeba from the Collection Pénard MHNG](https://commons.wikimedia.org/wiki/File:Collection_Penard_MHNG_Specimen_05bis-1-1_Amoeba_proteus.tif) by Dalinda Bouraoui, available under a [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) License, with my alternations under the same terms.  It's a "space amoeba," to the extent that it was on Earth and the Earth is in space...
 * title: Tweets from 12/07 to 12/11
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Monkaa
 * ![Monkaa investigating the meteor](/blog/assets/vlcsnap-2020-12-07-15h29m47s895.png "Monkaa investigating the meteor")
 * The header image is a screenshot from **Monkaa**.
 * title: Developer Journal, Zamenhof Eve
 * ![L. L. Zamenhof](/blog/assets/1910Universop322llzdet.png "L. L. Zamenhof")
 * The header image is [Ludvík Lazar Zamenhof za svým stolem, Varšava, Polsko](https://commons.wikimedia.org/wiki/File:1910-Universo-p322-llzdet.jpg) by Ignacy Matuszewski, lapsed into the public domain.
 * title: Real Life in Star Trek, A Private Little War
 * ![Flintlock mechanism from a hunting rifle](/blog/assets/Musee-historique-lausanne-img-0086.png "Flintlock mechanism from a hunting rifle")
 * The header image is [Flintlock of an 18th-century hunting rifle, with flint missing](https://commons.wikimedia.org/wiki/File:Musee-historique-lausanne-img_0086.jpg) by an unknown photographer, made available under the terms of the [Creative Commons Attribution 2.0 France](https://creativecommons.org/licenses/by-sa/2.0/fr/deed.en) License.  It made some sense, after all, given how flintlock rifles are at the center of the episode.
 * title: Tweets from 12/14 to 12/18
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Pepper & Carrot, ep. 1–6
 * ![Pepper, Carrot, and their Snowman](/blog/assets/PepperCarrot5-1.png "Pepper, Carrot, and their Snowman")
 * ![Page 1](/blog/assets/en_Pepper-and-Carrot_by-David-Revoy_E01P01.png "Page 1")
 * ![Page 2](/blog/assets/en_Pepper-and-Carrot_by-David-Revoy_E01P02.png "Page 2")
 * ![Page 3](/blog/assets/en_Pepper-and-Carrot_by-David-Revoy_E01P03.png "Page 3")
 * The header image is an extracted panel from [episode 5](https://www.peppercarrot.com/en/article244/special-holiday-episode).
 * title: Developer Journal, Winter Solstice
 * ![Amaterasu emerging from a cave](/blog/assets/Amaterasu-cave-large-1856.png "Amaterasu emerging from a cave")
 * The header image is [Japanese Sun goddess Amaterasu emerging from a cave](https://www.yamada-shoten.com/onlinestore/detail.php?item_id=44830), one East Asian version of the rebirth symbolism of the day.
 * title: Real Life in Star Trek, Return to Tomorrow
 * ![Not Sargon](/blog/assets/light-sunrise-sunset-white-glass-city-1329238-pxhere.com.png "Sargon")
 * The header image is [untitled](https://pxhere.com/en/photo/1329238) by an unlisted photographer, available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 12/21 to 12/25
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Biodigital, ch 1–13
 * ![That Overmind Emergent, Maybe?](/blog/assets/biodigital-cover.png "That Overmind Emergent, Maybe?")
 * The header image has been extracted from the book's cover.
 * title: 🔭 Looking Back on 2020
 * ![Look, everybody else has gotten to use the joke...](/blog/assets/20200529-DSC8235-49950467713.png "Look, everybody else has gotten to use the joke...")
 * The header image is [5/29/20-5/30/20-Minneapolis](https://www.flickr.com/photos/188619396@N05/49950467713/) by [Hungryogrephotos](https://www.flickr.com/people/188619396@N05), made available under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  At least I waited until the *end* of the year to mumble about dumpster fires.
 * title: Developer Journal, End of 2020
 * ![Happy New Year](/blog/assets/liquid-black-and-white-wine-glass-motion-celebration-671028-pxhere.com.png "Happy New Year")
 * The header image is [untitled](https://pxhere.com/en/photo/671028) by an unidentified photographer, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Patterns of Force
 * ![Bunches of Nazis](/blog/assets/Bundesarchiv-Bild-146-2004-0014-Nürnberg-Reichsparteitag-HJ-Aufmarsch.png "Bunches of Nazis")
 * ![Daras and Kirk](/blog/assets/Star-Trek-Patterns-of-Force.png "An awkward scene on every level.")
 * The header image is [Nürnberg, Reichsparteitag, HJ-Aufmarsch ](https://www.bild.bundesarchiv.de/dba/de/search/?query=Bild+146-2004-0014) by August Priesack, made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Germany](https://creativecommons.org/licenses/by-sa/3.0/de/deed.en) License.  I tried to find *something* relevant that didn't include iconography banned for non-educational use in certain areas.  The image of [Valora Noland and William Shatner](https://commons.wikimedia.org/wiki/File:Star_Trek_Patterns_of_Force.JPG) was published without copyright notices---for the purposes of republication as publicity, even---and so is in the public domain.
 * title: Tweets from 12/28 to 01/01
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Biodigital, ch 14–26
 * ![That Overmind Emergent, Maybe?](/blog/assets/biodigital-cover.png "That Overmind Emergent, Maybe?")
 * The header image has been extracted from the book's cover.
 * title: 🍾 Happy Calendar-Changing Day (Belated), 2021 🎆
 * ![2021](/blog/assets/happy-newyear-2021-card-fireworks-new-years-day-sky-1601384-pxhere.com.jpg "2021")
 * ![House by the Railroad](/blog/assets/House-by-the-railroad-edward-hopper-1925.png "House by the Railroad")
 * The header image is adapted from [New Year 2020](https://pxhere.com/en/photo/1601384) by [Henk Adriaan Meijer](https://pxhere.com/en/photographer/244493), made available under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  Surprisingly, there aren't many Free Culture "2021" graphics available, yet, so I needed to do a bit of remixing.  Edward Hopper's [*House by the Railroad*](https://commons.wikimedia.org/wiki/File:House-by-the-railroad-edward-hopper-1925.jpg) is also used, and (as is the entire point) in the public domain.
 * title: Developer Journal, ⠠⠸⠺⠀⠠⠃⠗⠇⠀⠠⠐⠙
 * ![Braille](/blog/assets/Braille-closeup.png "Braille")
 * The header image is [Braille closeup](https://commons.wikimedia.org/wiki/File:Braille_closeup.jpg) by Lrcg2012, released under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, By Any Other Name
 * ![Just add salts](/blog/assets/Plato-20.png "Just add salts")
 * The header image is [Plato 20](https://commons.wikimedia.org/wiki/File:Plato-20.png) by [KoenB](https://commons.wikimedia.org/wiki/User:KoenB), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](http://creativecommons.org/licenses/by-sa/3.0/) License.
 * title: Tweets from 01/04 to 01/08
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Biodigital, ch 27–39
 * ![That Overmind Emergent, Maybe?](/blog/assets/biodigital-cover.png "That Overmind Emergent, Maybe?")
 * The header image has been extracted from the book's cover.
 * title: ...Perchance to Dream
 * ![A cat napping](/blog/assets/flower-wildlife-pet-fur-beak-cat-562078-pxhere.com.png "A cat napping")
 * The header image is [untitled](https://pxhere.com/en/photo/562078) by an unidentified photographer, released under the terms of the [Creative Commons CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) public domain dedication.
 * title: Developer Journal, Bread and Roses Strike
 * ![The Fight is on; Join The IWOW And STAy WITH US](/blog/assets/Lawrence-textile-strike-begins-1912.png "The Fight is on; Join The IWOW And STAy WITH US")
 * The header image is [Workers picket with signs around the textile mill as the Lawrence textile strike begins in 1912](https://dp.la/exhibitions/breadandroses/strike/strike-begins?item=74), long in the public domain.
 * title: Real Life in Star Trek, The Omega Glory
 * ![United States Constitution](/blog/assets/Constitution-of-the-United-States.png "United States Constitution")
 * The header image is the first page of the [Constitution of the United States](https://en.wikipedia.org/wiki/Constitution_of_the_United_States), long in the public domain...and must apply to everyone, or it means nothing, even if an anonymous person on the Internet claimed that they're Satanic cannibals or whatever.
 * title: Tweets from 01/11 to 01/15
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Biodigital, ch 40–52
 * ![That Overmind Emergent, Maybe?](/blog/assets/biodigital-cover.png "That Overmind Emergent, Maybe?")
 * The header image has been extracted from the book's cover.
 * title: The Parler Problem
 * ![Free speech](/blog/assets/136733958_d19086fe20_o.png "Free speech")
 * The header image is [Winter arguing with Summer about which is the best season](https://www.flickr.com/photos/56832361@N00/136733958) by [Karen](https://www.flickr.com/photos/56832361@N00/), released under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Martin Luther King Jr. Day
 * ![Martin Luther King Jr after his 1963 arrest](/blog/assets/MLK_mugshot_birmingham.png "Martin Luther King Jr after his 1963 arrest")
 * The header image is [Mugshot of Martin Luther King Jr following his 1963 arrest in Birmingham](https://commons.wikimedia.org/wiki/File:MLK_mugshot_birmingham.jpg) from the Birmingham, Alabama, police department, in the public domain due to the fussy nature of old photography copyrights.
 * title: Real Life in Star Trek, The Ultimate Computer
 * ![The M5's much older uncle](/blog/assets/Babbage-Difference-Engine-Detail-6224956233.png "The M5's much older uncle")
 * The header image is [Babbage Difference Engine Detail](https://www.flickr.com/photos/ddebold/6224956233/) by [Don DeBold](https://www.flickr.com/photos/ddebold/), available under a [Creative Commons Attribution 2.0 Generic](http://creativecommons.org/licenses/by/2.0/) License.
 * title: Tweets from 01/18 to 01/22
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Biodigital, ch 53–end
 * ![That Overmind Emergent, Maybe?](/blog/assets/biodigital-cover.png "That Overmind Emergent, Maybe?")
 * The header image has been extracted from the book's cover.
 * title: Developer Journal, Home for Nellie Bly
 * ![Round the World with Nellie Bly](/blog/assets/round-the-world-bly-1890.png "Round the World with Nellie Bly")
 * The header image is [Round the World with Nellie Bly](https://npg.si.edu/object/npg_NPG.2017.111) by J.A. Grozier, long lapsed into the public domain.
 * title: How Long Will That Project Take?
 * ![Preparing to Estimate](/blog/assets/writing-work-architecture-pen-train-architect-819794-pxhere.com.png "Preparing to Estimate")
 * The header image is [untitled](https://pxhere.com/en/photo/819794) by an unknown PxHere photographer, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Bread and Circuses
 * ![The Colosseum](/blog/assets/Colosseum2_11-7-2003.png "The Colosseum")
 * The header image is [The Colosseum in Rome, picture taken in the summer of 2003](https://commons.wikimedia.org/wiki/File:Colosseum2_11-7-2003.JPG) by Bjarki Sigursveinsson, released into the public domain.
 * title: Tweets from 01/25 to 01/29
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Continuity Drift
 * ![Birds](/blog/assets/c2e579f05ed23e538547eda4605e5c001796496915.jpeg "Birds")
 * ![This needs more plot and characters, right?](/blog/assets/continuity-drift-caption-need.png "This needs more plot and characters, right?")
 * The header image is a page (#25) from the comic.
 * title: Developer Journal, National Freedom Day
 * ![The 13th Amendment, Page 1](/blog/assets/13thAmendment.png "The 13th Amendment, Page 1")
 * The header image is [Amendment XIII in the National Archives, bearing the signature of Abraham Lincoln](https://www.archives.gov/files/founding-docs/downloads/13th_Amendment_Pg1of1_AC.jpg) from the National Archives.
 * title: Real Life in Star Trek, Assignment∶ Earth
 * ![A totally different black cat](/blog/assets/Black-cat-superstition.png "A totally different black cat")
 * The header image is [black cats are considered bad luck](https://commons.wikimedia.org/wiki/File:Black_cat_superstition.jpg) by ALY41980, available under a [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) License.
 * title: Tweets from 02/01 to 02/05
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Uncle Nagy's House
 * ![Episode Thumbnail](/blog/assets/497154190_1280x720.png "Episode Thumbnail")
 * The header image is the image used for the episode on <https://spoonfullofpoo.com/>, made available under the terms of the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license along with the rest of the website.
 * title: Developer Journal, Mendeleev's Birthday
 * ![Dmitri Mendeleev](/blog/assets/Medeleeff_by_repin.png "Dmitri Mendeleev")
 * The header image is [Medeleeff](https://commons.wikimedia.org/wiki/File:Medeleeff_by_repin.jpg) painted by Ilya Repin in 1885, and so long in the public domain.
 * title: Real Life in Star Trek, Season 2 Summary
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is [The planet, the galaxy and the laser](https://www.eso.org/public/images/eso0733a/) by the [ESO](https://www.eso.org) and [Yuri Beletsky](Yuri Beletsky), released under the terms of a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) license.
 * title: Tweets from 02/08 to 02/12
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — The Wanderers' Library
 * ![The Wanderers' Library Emblem](/blog/assets/wl_logo.png "The Wanderers' Library Emblem")
 * The header image extracted from the book's cover.
 * title: An Unexpected Case for Reparations
 * ![Depiction of a slave revolt](/blog/assets/DesperateConflictBarn.png "Depiction of a slave revolt")
 * The header image is [Desperate Conflict in a Barn](https://commons.wikimedia.org/wiki/File:Desperate_Conflict_in_a_Barn.png) (1872) by William Still, long in the public domain.
 * title: Developer Journal, Presidents' Day
 * ![No Business Transacted](/blog/assets/no-business-transacted.png "No Business Transacted")
 * The header image is [No Business Transacted poster](https://loc.gov/pictures/resource/cph.3g12934/), circa 1895---and so long in the public domain---courtesy of the Prints and Photographs collection at the Library of Congress.
 * title: Real Life in Star Trek, Spock's Brain
 * ![Spock's brain is strongest there is](/blog/assets/spock-brain-exercising.png "Spock's brain is strongest there is")
 * ![Kara arguing with Kirk over the titular brain](/blog/assets/Spocks-Brain.png "Kara arguing with Kirk over the titular brain")
 * The header image is based on [Brain Exercising](https://commons.wikimedia.org/wiki/File:Brain_Exercising.png) by Tumisu, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).  [Scene from the Star Trek episode "Spock's Brain"](https://commons.wikimedia.org/wiki/File:Leonard_Nimoy_William_Shatner_Spock%27s_Brain_Star_Trek_1968.JPG) (Marj Dusay, Leonard Nimoy, and William Shatner) is also used, passed into the public domain as a photographic work without a copyright notice, meant for republication as publicity.
 * title: Tweets from 02/15 to 02/19
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Phill from GCHQ
 * ![Caution: Phill at Work](/blog/assets/phill-p2.png "Caution: Phill at Work")
 * ![Can we get on with this?](/blog/assets/phillP58caption.png "Can we get on with this?")
 * The header image extracted from the first page.  The caption comes from page 58 of the comic.
 * title: Daily Nonogram
 * ![A solved nonogram](/blog/assets/daily-nonogram.png "A solved nonogram")
 * No actual credits; the header image is just a screenshot of a solved puzzle.
 * title: Developer Journal, Northwest States Anniversary
 * ![Constitutional Convention Parade](/blog/assets/1889ConstitutionalConventionParadeBismarkND.png "Constitutional Convention Parade")
 * The header image is [1889 Constitutional Convention parade in Bismark, North Dakota at Main and 4th streets](https://digital.denverlibrary.org/digital/collection/p15330coll22/id/68795) by David Francis Barry, long in the public domain.
 * title: Real Life in Star Trek, The Enterprise Incident
 * ![Organic cloaking](/blog/assets/PeacockInTheWoods.png "Organic cloaking")
 * The header image is [Peacock in the Woods](https://commons.wikimedia.org/wiki/File:PeacockInTheWoods.jpg) (1907) by Abbott Handerson Thayer, long in the public domain.
 * title: Tweets from 02/22 to 02/26
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — The Spiraling Web, Part 1
 * ![Both Webby and Spiraling](/blog/assets/ACfU3U18DzuB7v6SxdKOV6ajrMXysTqAmA.png "Both Webby and Spiraling")
 * The header image is extracted from the book's cover, credited to Wolfgang Beyer and released into the public domain.
 * title: Explaining Gender to the Biased
 * ![Transgender Pride Flag](/blog/assets/Transgender_Pride_flag.svg "Transgender Pride Flag")
 * The header image is [the Transgender Pride flag](https://commons.wikimedia.org/wiki/File:Transgender_Pride_flag.svg), functionally in the public domain---whether or not it has been explicitly donated---as a series of colored blocks.
 * title: Developer Journal, Self-Injury Awareness Day
 * ![Orange butterflies](/blog/assets/nature-photography-leaf-flower-petal-orange-867720-pxhere.com.png "Orange butterflies")
 * The header image is [untitled](https://pxhere.com/en/photo/867720) by an unlisted PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, The Paradise Syndrome
 * ![Algonquin Village](/blog/assets/Pomeioc.png "Algonquin Village")
 * ![Miramanee and Kirok](/blog/assets/The-Paradise-Syndrome.png "No, I get the shirt.  I actually meant your hair.")
 * The header image is [The village of Pomeioc, North Carolina, 1885](https://commons.wikimedia.org/wiki/File:The_village_of_Pomeioc,_North_Carolina,_1885,_color_-_NARA_-_535753.jpg) by an unknown artist, but long in the public domain.
 * title: Tweets from 03/01 to 03/05
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — The Spiraling Web, Part 2
 * ![Both Webby and Spiraling](/blog/assets/ACfU3U18DzuB7v6SxdKOV6ajrMXysTqAmA.png "Both Webby and Spiraling")
 * The header image is extracted from the book's cover, credited to Wolfgang Beyer and released into the public domain.
 * title: Superheroes and Race
 * ![Superman checking the hallway](/blog/assets/SupermanFleischer.png "Superman checking the hall")
 * ![Bronze Man, diving](/blog/assets/BronzeMan.png "Bronze Man, diving")
 * ![Samson Slaying the Lion](/blog/assets/Sansón-matando-al-león.png "Samson Slaying the Lion")
 * ![John Carter and Dejah Thoris](/blog/assets/Princess-of-Mars.png "John Carter and Dejah Thoris")
 * ![Amazons vs Greeks](/blog/assets/Amazons-vs-Greeks.png "Amazons vs Greeks")
 * ![Clark Kent](/blog/assets/Clark-Kent-winking.png "Clark Kent")
 * ![Waiting for a parade, Cottonwood Falls, Kansas, 1974](/blog/assets/NARA-557057.png "Waiting for a parade, Cottonwood Falls, Kansas, 1974")
 * ![A Black, off-brand superhero](/blog/assets/Minority-superhero-art-3.png "A Black, off-brand superhero")
 * ![Superman](/blog/assets/Superman-posing.png "Superman")
 * The header image and other images of Superman or Clark Kent are screen captures from [**Superman**](https://en.wikipedia.org/wiki/Superman_(1941_film)), the first of the animated films, which have lapsed into the public domain.  As mentioned, use of the image obviously does *not* represent any challenge to active copyrights and trademarks attached to the Superman character.  The image of Bronze Man comes from **Blue Beetle** #42.  The image of Green Turtle comes from **Blazing Comics** #1.  [Sansón matando al león](https://commons.wikimedia.org/wiki/File:Sans%C3%B3n_matando_al_le%C3%B3n_-_Pedro_Pablo_Rubens.jpg) by [Pedro Pablo Rubens](https://en.wikipedia.org/wiki/Peter_Paul_Rubens), the cover of the 1917 edition of **A Princess of Mars**, [Amazons vs Greeks](https://commons.wikimedia.org/wiki/File:Amazons_vs_Greeks.jpg) by an unknown artist, and the picture of Seh-Dong-Hong-Beh---leader of the [Dahomey Amazons](https://commons.wikimedia.org/wiki/File:Dahomey_amazon1.jpg) in her time, drawn by Frederick Forbes---have long been in the public domain.  The picture of [parade-goers](https://catalog.archives.gov/id/557057) by [Patricia D. Duncan](https://catalog.archives.gov/id/10614764) is in the public domain as a work of the United States government.  The "minority superhero" image comes from [a delightful article about diverse superheroes](https://share.america.gov/evildoers-beware-new-american-superheroes-enter-fray/) from the Bureau of Public Affairs, also in the public domain as a work of the United States government.  The copyright for **Gladiator** does not appear to have ever been renewed, so the cited text is in the public domain.
 * title: Developer Journal, International Women's Day
 * ![Women's International League](/blog/assets/WomensInternationalLeague.5.1.1922.png "Women's International League")
 * The header image is [Women's International League, 5/1/22](https://commons.wikimedia.org/wiki/File:Women%27s_International_League,_5._1._1922.png) by an unidentified photographer, in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, And the Children Shall Lead
 * ![Hail, Hail, Fire and Snow?](/blog/assets/snow-cold-winter-light-wood-night-822893-pxhere.com.png "Hail, Hail, Fire and Snow?")
 * The header image is [untitled](https://pxhere.com/en/photo/822893) by an unlisted PxHere photographer, released under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 03/08 to 03/12
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — The Spiraling Web, Part 3
 * ![Both Webby and Spiraling](/blog/assets/ACfU3U18DzuB7v6SxdKOV6ajrMXysTqAmA.png "Both Webby and Spiraling")
 * The header image is extracted from the book's cover, credited to Wolfgang Beyer and released into the public domain.
 * title: Developer Journal, Day against Police Brutality
 * ![Felix Baran's funeral](/blog/assets/Everett_Funeral.png "Felix Baran's funeral")
 * The header image is [Funeral of Felix Baran, held in Seattle, Washington, November 1916, by members of the Industrial Workers of the World](https://commons.wikimedia.org/wiki/File:Everett_Funeral.jpg) by an unknown photographer, long in the public domain.  The [Everett massacre](https://en.wikipedia.org/wiki/Everett_massacre) isn't the worst or more predominant form of police violence, today, but showing the people opposed to it isn't likely to result in many arrests...
 * title: Real Life in Star Trek, Is There in Truth No Beauty?
 * ![Head of Medusa](/blog/assets/HeadMedusaGodfriedMaes.png "Head of Medusa")
 * The header image is [Head of Medusa](https://commons.wikimedia.org/wiki/File:Head_of_Medusa_by_Godfried_Maes,_1680.jpg) by Godfried Maes, long in the public domain.
 * title: Tweets from 03/15 to 03/19
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — The Spiraling Web, Part 4
 * ![Both Webby and Spiraling](/blog/assets/ACfU3U18DzuB7v6SxdKOV6ajrMXysTqAmA.png "Both Webby and Spiraling")
 * The header image is extracted from the book's cover, credited to Wolfgang Beyer and released into the public domain.
 * title: Asian Women in Fiction
 * ![Norwuz 2017](/blog/assets/Nowruz_2017_in_Bisaran_Kurdistan_province.png "Norwuz 2017")
 * ![Anna May Wong in 1948](/blog/assets/anna-may-wong-1948.png "Anna May Wong in 1948")
 * ![The Conquering Hero...and the Protagonist](/blog/assets/flying-fool-wing-ding.png "The Conquering Hero...and the Protagonist")
 * ![Moon Girl fighting...someone else](/blog/assets/moongirl01_11.png "Moon Girl fighting...someone else")
 * The header image is [Nowruz 2017 in Bisaran, Kurdistan province](https://commons.wikimedia.org/wiki/File:Nowruz_2017_in_Bisaran,_Kurdistan_province.jpg) by [Salar Arkan — سالار ارکان](https://commons.wikimedia.org/wiki/User:Salar.arkan), made available under the terms of the [Creative Commons Attribution-Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.  The picture of Anna May Wong is a screenshot from [**Impact**](https://en.wikipedia.org/wiki/Impact_(1949_film)), seemingly in the public domain, and chosen for its proximity in date to when Wong played Liu-Tsong.  The picture of Wing Ding is a panel from **Airboy** volume 4, number 5, October 1945, which is in the public domain due to a failure to renew copyright.  The picture of Moon Girl is from **Moon Girl and the Prince** #1, Fall 1947, which is in the public domain due to a failure to renew copyright.  The picture of Marah Durimeh is from the cover of [**Ardistan und Dschinnistan**](https://de.wikipedia.org/wiki/Ardistan_und_Dschinnistan), long in the public domain.
 * title: Developer Journal, World Water Day
 * ![Choir singing for World Water Day](/blog/assets/4459460043_ae2899c018_o.png "Choir singing for World Water Day")
 * The header image is [World water day - choir singing](https://www.flickr.com/photos/gtzecosan/4459460043/) by [SuSanA Secretariat](https://www.flickr.com/photos/gtzecosan/), made available under a [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Assembling a Dark Mode
 * ![A man in darkness](/blog/assets/50030379698_5f948aa93e_o.png "A man in darkness")
 * The header image is [Dark](https://www.flickr.com/photos/71267357@N06/50030379698) by [Transformer18](https://www.flickr.com/photos/71267357@N06/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.  I don't recommend messing with the color balance of the image.  It's far less dramatic when it's just a pasty bald guy leaning against a door frame...
 * title: Real Life in Star Trek, Spectre of the Gun
 * ![Reenactment at the O.K. Corral](/blog/assets/GunfightAtTheOKCorral2.png "Reenactment at the O.K. Corral")
 * ![Titles for Mazeppa](/blog/assets/Mazeppa.png "Titles for Mazeppa")
 * The header image is [Gunfight at the OK Corral 2](https://commons.wikimedia.org/wiki/File:Gunfight_at_the_OK_Corral_2.jpg) by [James G. Howes](https://commons.wikimedia.org/wiki/User:JGHowes), released for use "for any purpose, provided that the copyright holder is properly attributed. Redistribution, derivative work, commercial use, and all other use."  The credits for **Mazeppa** were extracted from the title page of [the play](https://archive.org/details/mazeppa_00miln).
 * title: Tweets from 03/22 to 03/26
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Valkaama
 * ![Runa and Erik](/blog/assets/valkaama_00a.png "Runa and Erik")
 * The header image is a production still from the movie, released under the terms of the same license.
 * title: Calafia, Queen of California
 * ![The New Pacific frontispiece](/blog/assets/new-pacific-title-page.png "The New Pacific frontispiece")
 * The header image is the frontispiece of **The New Pacific** (1912 edition), long in the public domain due to copyright term expiration.
 * title: Developer Journal, National Doctors' Eve
 * ![Doctors](/blog/assets/people-technology-view-biology-asia-professional-1026135-pxhere.com.png "Doctors")
 * The header image is [untitled](https://pxhere.com/en/photo/1026135) by an unlisted PxHere photographer, made available under the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Day of the Dove
 * ![Doves with Bad Publicity](/blog/assets/ancient-animal-background-beautiful-bird-cappadocia-1603600-pxhere.com.png "Doves with Bad Publicity")
 * The header image is [untitled](https://pxhere.com/en/photo/1603600) by [Pijarn Jangsawang](https://pxhere.com/en/photographer/3012915), available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  I considered finding a more clever image to represent the episode, like creating something inspired by the creature, but being able to point out that there's no formal distinction between doves and pigeons was more interesting, especially when it's possible to mention [Uçhisar](https://en.wikipedia.org/wiki/U%C3%A7hisar), the Turkish "Pigeon Valley," and draw attention to the architecture down that way.
 * title: Tweets from 03/29 to 04/02
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — A Dark Room
 * ![A Dark Room's logo](/blog/assets/ADarkRoomLogo.png "A Dark Room's logo")
 * The header image is the logo image from **A Dark Room**'s Git repository, released under the terms of the same license.
 * title: (Finally) Cutting the Cord
 * ![Cutting a cord](/blog/assets/StockSnap_DXPPJOEVZP.png "Cutting a cord")
 * The header image is [Vintage Packaging](https://stocksnap.io/photo/vintage-packaging-DXPPJOEVZP) by [Eneida Nieves](https://stocksnap.io/author/eneidanieves), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, National Library Week
 * ![Main Reading Room of the New York Public Library](/blog/assets/Main-Reading-Room-of-the-New-York-City-Public-Library-on-5th-Avenue-ca-1910-1920.png "Main Reading Room of the New York Public Library")
 * the header image is [Main reading room, the New York Public Library](https://www.loc.gov/item/2016812243/) by the Detroit Publishing Co., sometime prior to 1920, and so long in the public domain.
 * title: Real Life in Star Trek, For the World Is Hollow (etc.)
 * ![Hypothetical alien base](/blog/assets/SpaceshipMoon.png "Hypothetical alien base")
 * The header image is [Spaceship Moon](https://commons.wikimedia.org/wiki/File:Spaceship_moon.jpg) by "Bromley86," made available under the terms of the [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.  Technically, it's meant to be an alien base hidden inside the Moon, but that's close enough, here.
 * title: Tweets from 04/05 to 04/09
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — The Fellowship of Heroes
 * ![Introducing Crusader](/blog/assets/FellowshipOfHeroesPanel.png "Introducing Crusader")
 * The header image is a cropped and lightly edited panel from the comic.
 * title: Developer Journal, Yuri's Night
 * ![Gagarin in 1961](/blog/assets/617_d2004_17_r1_r61-26810_1.png "Gagarin in 1961")
 * The header image is [Neuvostoliittolainen avaruuslentäjä, majuri Juri Gagarin Suomen-vierailunsa lehdistötilaisuudessa vuonna 1961](https://kuviakaikille.valokuvataiteenmuseo.fi/kuvat/Julkisuuden+henkil%C3%B6it%C3%A4/617_d2004_17_r1_r61-26810_1.jpg) by Arto Jousi, in the public domain, having passed into the public domain as a photograph published in Finland prior to 1966.
 * title: Real Life in Star Trek, The Tholian Web
 * ![Not a Tholian](/blog/assets/nature-stone-decoration-medicine-deco-transparent-541858-pxhere.com.png "Not a Tholian")
 * The header image is lightly adapted from [untitled](https://pxhere.com/en/photo/541858) by an unlisted PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 04/12 to 04/16
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Moses und Aron, ch 1-3
 * ![A different Aaron](/blog/assets/Aaron-Kirillo-Belozersk.png "A different Aron")
 * The header image is [Пророк Аарон](https://commons.wikimedia.org/wiki/File:Aaron_(Kirillo-Belozersk).jpg) by Terenty Fomin (photograph by shakko), long in the public domain.
 * title: Developer Journal, Patriots' Day
 * ![Minute Man](/blog/assets/Minute-Man-Statue-Lexington-Massachusetts.png "Minute Man")
 * The header image is [The Lexington Minuteman](https://commons.wikimedia.org/wiki/File:Minute_Man_Statue_Lexington_Massachusetts.jpg) by [Daderot](https://en.wikipedia.org/wiki/User:Daderot), who dedicated the image to the public domain.
 * title: Real Life in Star Trek, Plato's Stepchildren
 * ![Plato's Academy](/blog/assets/PlatosAcademy.png "Plato's Academy")
 * The header image is [Plato's Academy mosaic from Pompeii](https://commons.wikimedia.org/wiki/File:Plato%27s_Academy_mosaic_from_Pompeii.jpg), long in the public domain.
 * title: Tweets from 04/19 to 04/23
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Moses und Aron, ch 4-6
 * ![A different Aaron](/blog/assets/Aaron-Kirillo-Belozersk.png "A different Aron")
 * The header image is [Пророк Аарон](https://commons.wikimedia.org/wiki/File:Aaron_(Kirillo-Belozersk).jpg) by Terenty Fomin (photograph by shakko), long in the public domain.
 * title: Announcing All Around the News
 * [![All Around the News logo](/blog/assets/AllAroundTheNews.svg "All Around the News logo")](https://allaroundthe.news)
 * The header image is just the **All Around the News** logo, which I'm willing to release under the blog's license for the sake of consistency.
 * title: Developer Journal, Old Permic Alphabet Day
 * ![Abur komi inscription](/blog/assets/Abur-komi-inscription.png "Abur komi inscription")
 * The header image is an [Abur (old Permic) komi inscription](https://commons.wikimedia.org/wiki/File:Abur_komi_inscription.jpg), old enough to have long been in the public domain.
 * title: Real Life in Star Trek, Wink of an Eye
 * ![A vastly superior alien city](/blog/assets/fractal-city-1.png "A vastly superior alien city")
 * The header image is [Fractal city](https://opengameart.org/content/fractal-alien-landscape-pack) by [p0ss](https://opengameart.org/users/p0ss), made available under the terms of the [Creative Commons Attribution 3.0 Unported](http://creativecommons.org/licenses/by/3.0/) License.
 * title: Tweets from 04/26 to 04/30
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Moses und Aron, ch 7-9
 * ![A different Aaron](/blog/assets/Aaron-Kirillo-Belozersk.png "A different Aron")
 * The header image is [Пророк Аарон](https://commons.wikimedia.org/wiki/File:Aaron_(Kirillo-Belozersk).jpg) by Terenty Fomin (photograph by shakko), long in the public domain.
 * title: Media Websites Wanted...
 * ![Wanted: Media Websites; BYO Reward](/blog/assets/wanted-media-sign.png "Wanted: Media Websites; BYO Reward")
 * The header image was constructed from the [Cowboy Collective](https://cowboycollective.cc/)'s [Wanted Poster Resource Pack](https://github.com/CowboyCollective/Wanted-Posters), released under the terms of the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license.  It includes an adapted version of [Wikipedia Homepage, Chromium Web browser 36](https://commons.wikimedia.org/wiki/File:Wikipedia_Homepage_Chromium_Web_browser_36.png) by [Enock4seth](https://commons.wikimedia.org/wiki/User:Enock4seth), released under the terms of the [Creative Commons Attribution-Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license, with the user interface parts of Chromium released under the terms of the [3-Clause BSD](https://opensource.org/licenses/BSD-3-Clause) license and Wikipedia content released under the terms of the [Creative Commons Attribution-ShareAlike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.  If you want a vector image to pull the text from---but doesn't display well in browsers, otherwise I would've used it, instead, for accessibility purposes---you can [download it](/blog/assets/wanted-media-sign.svg).
 * title: Developer Journal, World Press Freedom Day
 * ![Press Freedom Day Poster](/blog/assets/World-Press-Freedom-Day-2017-Poster.png "Press Freedom Day Poster")
 * The header image is [World Press Freedom Day 2017 Poster](https://commons.wikimedia.org/wiki/File:World_Press_Freedom_Day_2017_Poster.jpg) by unknown [UNESCO](https://en.unesco.org/) staffers, made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 IGO](https://creativecommons.org/licenses/by-sa/3.0/igo/deed.en) license.
 * title: Real Life in Star Trek, The Empath
 * ![A 2012 solar flare](/blog/assets/Magnificent-CME-Erupts-on-the-Sun-August-31.png "A 2012 solar flare")
 * title: Tweets from 05/03 to 05/07
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Moses und Aron, ch 10-13
 * ![A different Aaron](/blog/assets/Aaron-Kirillo-Belozersk.png "A different Aron")
 * The header image is [Пророк Аарон](https://commons.wikimedia.org/wiki/File:Aaron_(Kirillo-Belozersk).jpg) by Terenty Fomin (photograph by shakko), long in the public domain.
 * title: Belated FSF Discussion
 * ![Agnother gnu](/blog/assets/gnu-head.png "Agnother gnu")
 * [![Richard Lippman/Stallman?](/blog/assets/elvie-028.png "Richard Lippman/Stallman")](http://127.0.0.1:4000/blog/2020/11/14/elvie.html)
 * The header image is [Gnu head](https://commons.wikimedia.org/wiki/File:Gnu_head.jpg) by Eberhard Riedel, dedicated to the public domain.  The [**Elvie**](http://peppertop.com/elvie/comic/elvie-028/) is by "Mark and Vince" from Peppertop Comics, made available under the terms of an unspecified Creative Commons Attribution Share-Alike license.
 * title: Developer Journal, Golden Spike Day
 * ![The Golden Spike](/blog/assets/Golden-Spike-7Oct2012.png "The Golden Spike")
 * The header image is [The Golden Spike](https://commons.wikimedia.org/wiki/File:The-Golden-Spike-7Oct2012.jpg) by "Wjenning," made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Elaan of Troyius
 * ![Katharine and the Music Master](/blog/assets/Katherine_hits_the_music_master.png "Katharine and the Music Master")
 * The header image is [Katharine & music master](https://luna.folger.edu/luna/servlet/detail/FOLGERCM1~6~6~364219~130920:Katharine-&-music-master--graphic-) by Louis Rhead, probably technically in the public domain given its age, but mad available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License by the [Folger Shakespeare Library](https://www.folger.edu/).
 * title: Tweets from 05/10 to 05/14
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — FSF Propaganda Videos
 * ![Superhero teleconference](/blog/assets/univ-cost-hero-conference.png "Superhero teleconference")
 * The header image is a screenshot from *The University of Costumed Heroes*.
 * title: Explaining Cryptocurrency
 * ![Bitcoin, artist's rendition](/blog/assets/bitcoin-money-decentralized-virtual-coin-currency-1435009-pxhere.com.png "Bitcoin, artist's rendition")
 * ![A linked list representation](/blog/assets/Singly-linked-list.svg "A linked list representation")
 * The header image is [untitled](https://pxhere.com/en/photo/1435009) by [Mohamed Hassan](https://pxhere.com/en/photographer/767067), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Day Against Homophobia (et al)
 * ![Ambassador Gidwitz celebrating IDAHOT 2020](/blog/assets/Ronald-Gidwitz-holding-a-LGBT-flag-for-2020-IDAHOT.png "Ambassador Gidwitz celebrating IDAHOT 2020")
 * The header image is [Ronald Gidwitz holding a LGBT flag for 2020 IDAHOT](https://commons.wikimedia.org/wiki/File:Ronald_Gidwitz_holding_a_LGBT_flag_for_2020_IDAHOT.jpg) by the Office of the U.S. Ambassador to Belgium, in the public domain as a work of employees of the United States Department of State.  [Gidwitz](https://en.wikipedia.org/wiki/Ronald_Gidwitz) is apparently exactly the sort of person you would expect Donald Trump to appoint in his last months in office.  So, if he can get it right for the day, so can you.  Plus, for once, I'm not concerned about just showing someone's face on the blog without explicit permission.
 * title: Real Life in Star Trek, Whom Gods Destroy
 * ![The Madhouse, Francisco de Goya](/blog/assets/Francisco-de-Goya-La-casa-de-locos-Google-Art-Project.png "The Madhouse, Francisco de Goya")
 * The header image is [La casa de locos](https://commons.wikimedia.org/wiki/File:Francisco_de_Goya_-_La_casa_de_locos_-_Google_Art_Project.jpg) by Francisco de Goya, which has long been in the public domain.  The Equal Pay public service announcement is a work of the United States Department of Labor and, as such, was released into the public domain on creation.
 * title: Tweets from 05/17 to 05/21
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Ficly
 * ![Once upon a time...](/blog/assets/writing-read-creative-white-time-old-738184-pxhere.com.png "Once upon a time...")
 * The header image is [untitled](https://pxhere.com/en/photo/738184) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Africa Eve
 * ![Mali's Modiba Keita and Egypt's Gamal Abdel Nasser](/blog/assets/Keita-and-Nasser-1966.png "Mali's Modiba Keita and Egypt's Gamal Abdel Nasser")
 * The header image is [Keita and Nasser, 1966](https://commons.wikimedia.org/wiki/File:Keita_and_Nasser,_1966.jpg), in the public domain by Egyptian law, after its copyright expired.
 * title: Real Life in Star Trek, Let That Be Your Last Battlefield
 * ![The Coalsack Nebula (and Friends)](/blog/assets/yb_southern_cross_cc.png "The Coalsack Nebula (and Friends)")
 * The header image is [The Southern Cross](https://www.eso.org/public/images/yb_southern_cross_cc/) by the [European Southern Observatory](https://www.eso.org) and [Yuri Beletsky](https://www.facebook.com/yuribeletskyphoto), available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Tweets from 05/24 to 05/28
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Wilber & Co.
 * ![Wilber Learning](/blog/assets/202010-wilber-and-co.png "Wilber Learning")
 * ![Wilber](/blog/assets/gimp-wilber.svg "Wilber")
 * The header image is [Wilber Learning never Stops](https://www.gimp.org/news/2020/10/07/gimp-2-10-22-released/) by Aryeom, made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.  Wilber's headshot is found throughout the GIMP Project's web assets, was created by Jakub Steiner, and has been made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Winning the Twenty-First Century
 * ![Flying past the Earth](/blog/assets/car-driving-travel-airplane-vehicle-moon-394728-pxhere.com.png "Spoiler, the future is pretty boring")
 * ![Same banner, slightly different context](/blog/assets/USS-Abraham-Lincoln-CVN-72-Mission-Accomplished.png "Same banner, slightly different context")
 * ![Three Worlds Model](/blog/assets/Cold_War_alliances_1975.svg "Three Worlds Model")
 * The header image is [Fly me to the moon](https://pxhere.com/en/photo/394728) by Robert Couse-Baker.  The "Mission Accomplished" image is [USS Abraham Lincoln (CVN-72) Mission Accomplished](https://commons.wikimedia.org/wiki/File:USS_Abraham_Lincoln_(CVN-72)_Mission_Accomplished.jpg) by United States Navy Photographer's Mate 3rd Class Juan E. Diaz, in the public domain as the work of the United States government.  The map is [Cold War Alliances mid-1975](https://commons.wikimedia.org/wiki/File:Cold_War_alliances_mid-1975.svg) by [Vorziblix](https://commons.wikimedia.org/wiki/User:Vorziblix), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).
 * title: Developer Journal, Memorial Day
 * ![1944 Memorial Day Parade, Washington DC](/blog/assets/8d05028v.png "1944 Memorial Day Parade, Washington DC")
 * ![Example paint chips](/blog/assets/miniboost-paintchip.png "Example paint chips")
 * The header image is [Washington, D.C. Parade on Memorial Day](https://www.loc.gov/resource/fsa.8d05028/) by Royden Dixon, released into the public domain as a work of the Library of Congress Farm Security Administration.
 * title: Real Life in Star Trek, The Mark of Gideon
 * ![Crowding](/blog/assets/structure-people-crowd-audience-stadium-arena-631798-pxhere.com.png "Crowding")
 * ![MAA](/blog/assets/maritime-flags-m-a-a.svg "MAA")
 * The header image is [untitled](https://pxhere.com/en/photo/631798) by an unspecified PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 05/31 to 06/04
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — The House
 * ![Introductory screen for The House](/blog/assets/house-intro-screen.png "Introductory screen for The House")
 * ![The House's train](/blog/assets/the-house-train.png "The House's train")
 * ![Billy boards the train](/blog/assets/whiz-2-train.png "Billy boards the train")
 * The header image was extracted from the game's title screen and the train comes directly from the game's assets, and so should be available under the terms of the [Creative Commons Attribution 3.0 Unported](https://creativecommons.org/licenses/by/3.0/) license.  The Captain Marvel panel is from **Whiz Comics** #2, in the public domain due to a failure to renew the copyright.
 * title: Do the Work
 * ![Graduation](/blog/assets/nature-plant-flower-garden-pink-red-rose-676881-pxhere.com.png "Graduation")
 * ![An elephant directing white votes to a ballot box and "colored" votes to a toilet](/blog/assets/Vote-Purity-Laws-scaled.png "Voter purity laws")
 * ![Bridge over a river](/blog/assets/tree-water-nature-forest-waterfall-wilderness-1279856-pxhere.com.png "Wildly inaccurate, but pretty")
 * The header image is [untitled](https://pxhere.com/en/photo/676881) by an unlisted PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  [The GOP’s ‘Voter Purity’ Laws](https://otherwords.org/the-gops-voter-purity-laws/) by [Khalil Bendib](https://otherwords.org/authors/khalil-bendib/) has been made available under the terms of the [Creative Commons Attribution No-Derivatives 3.0 Unported](https://creativecommons.org/licenses/by-nd/3.0/) license.  The photograph of an unrelated bridge is similarly [untitled](https://pxhere.com/en/photo/1279856) by another unlisted PxHere, made available under the terms of the CC0 dedication.
 * title: Developer Journal, Ludi Piscatorii
 * ![The Tiber River](/blog/assets/PonteSantAngeloRom.png "The Tiber River")
 * The header image is [The Ponte Sant'Angelo with a view of St. Peter's Square and St. Peter's Basilica](https://commons.wikimedia.org/wiki/File:PonteSantAngeloRom.jpg), made available under the terms of the [Creative Commons Attribution-Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, That Which Survives
 * ![Exoplanet](/blog/assets/Kepler186f-ArtistConcept-20140417.png "Exoplanet")
 * The header image is [Artist's Concept of Kepler-186f](https://commons.wikimedia.org/wiki/File:Kepler186f-ArtistConcept-20140417.jpg) by NASA Ames/SETI Institute/JPL-Caltech, placed into the public domain by NASA policy.
 * title: Tweets from 06/07 to 06/11
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Boris Munchausen
 * ![Screenshot from Episode 3](/blog/assets/boris-munchausen-team.png "Screenshot from Episode 3")
 * The header image was extracted from the third episode, and so should be available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by/3.0/) license.
 * title: Managing Priorities
 * ![Foggy roads](/blog/assets/tree-forest-snow-winter-girl-fog-819272-pxhere.com.png "Foggy roads")
 * The header image is [untitled](https://pxhere.com/en/photo/819272) by an unlisted PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/) license.
 * title: Developer Journal, World Blood Donor Day
 * ![Blood donation center](/blog/assets/blood-donation.png "Blood donation center")
 * The header image is [Saal Blutspendezentrum Basel 3](https://commons.wikimedia.org/wiki/File:Saal_Blutspendezentrum_Basel_3.jpg) by [Keimzelle](https://commons.wikimedia.org/wiki/User:Keimzelle), made available under the terms of the [Creative Commons Attribution-Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, The Lights of Zetar
 * ![Not Memory Alpha](/blog/assets/Inflatable_habitat_s89_20084.png "Not Memory Alpha")
 * The header image is [Inflatable module for lunar base](https://commons.wikimedia.org/wiki/File:Inflatable_habitat_s89_20084.jpg) by NASA, Kitmacher, Ciccora artists, in the public domain by NASA policy.
 * title: Tweets from 06/14 to 06/18
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Solitudes and Silence, ch 1-4
 * ![Solitudes and Silence cover](/blog/assets/solitudes.png "Solitudes and Silence cover")
 * The header image is the cover of **Solitudes and Silence** by Jeremy Thevonot, made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: First Official Juneteenth
 * ![Juneteenth flag](/blog/assets/Juneteenth_Flag.svg "Juneteenth flag")
 * ![Map of areas covered and not covered by the Emancipation proclamation](/blog/assets/Emancipation-Proclamation-map.png "Areas in red were covered, areas in cyan were not")
 * ![Gerrymandered TX-2 and AZ-4](/blog/assets/tx2-and-az4.png "Gerrymandered TX-2 and AZ-4")
 * ![Augusta Savage and Harp](/blog/assets/nypl.digitalcollections.5e66b3e8-80d0-d471-e040-e00a180654d7.001.w.png "Augusta Savage and Harp")
 * The header image is [Juneteenth Flag](https://commons.wikimedia.org/wiki/File:Juneteenth_Flag.svg) by [Nafsadh](https://commons.wikimedia.org/wiki/User:Nafsadh), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  The [map of areas affected by the Emancipation Proclamation](https://commons.wikimedia.org/wiki/File:Emancipation_Proclamation.PNG), by SFGiants, has been made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.  The [House Democrats' video](https://twitter.com/RepMarciaFudge/status/1273966498168680451) is in the public domain, as a work of employees of the United States Congress.  The maps of TX-2 and AZ-4 are adapted from the Representatives' Congressional website, and as such, should be in the public domain.  [Art — Sculpture — Harp (Augusta Savage) — Harp](https://digitalcollections.nypl.org/items/5e66b3e8-80d0-d471-e040-e00a180654d7#/?uuid=5e66b3e8-80d0-d471-e040-e00a180654d7), by an uncredited photographer, appears to be in the public domain.
 * title: Developer Journal, World Humanist Day
 * ![Solstice from space](/blog/assets/Himawari-8-Summer-Solstice-Midnight-2017.png "Solstice from space")
 * The header image is [Himawari-8 Summer Solstice Midnight 2017](https://commons.wikimedia.org/wiki/File:Himawari-8_Summer_Solstice_Midnight_2017.jpg) by the Japan Meteorological Agency’s Himawari-8 satellite, apparently released into the public domain, as it was released by [NOAA](https://www.noaa.gov/) with no disclaimer.
 * title: Real Life in Star Trek, Requiem for Methuselah
 * ![Methuselah, in stained-glass](/blog/assets/17841296346_0a3e1d9aac_o.png "Methuselah, in stained-glass")
 * The header image is (the upper half of) [Ancestors of Christ Window, Canterbury Cathedral](https://www.flickr.com/photos/jpguffogg/17841296346/) by [Jules & Jenny](https://www.flickr.com/photos/jpguffogg/), made available under the terms of a [Creative Commons Attribution 2.0 Generic](http://creativecommons.org/licenses/by/2.0/) License.  The Brahms Waltz is [Brahms waltz op. 39 no. 3](https://commons.wikimedia.org/wiki/File:Brahms-39-3.ogg), played by [Mathmensch](https://commons.wikimedia.org/wiki/User:Mathmensch), and made available under the terms of a [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Tweets from 06/21 to 06/25
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Solitudes and Silence, ch 5-9
 * ![Solitudes and Silence cover](/blog/assets/solitudes.png "Solitudes and Silence cover")
 * The header image is the cover of **Solitudes and Silence** by Jeremy Thevonot, made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Attracting Job Candidates
 * ![A casually dressed man being interviewed](/blog/assets/negative-space-man-job-interview-talk-discuss-yellow-chair-work-desk-office-linkedin.png "Interviewing a blockhead")
 * The header image is [Man Job Interview](https://negativespace.co/man-job-interview/) by "Linkedin" (possibly mis-attributed), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Stonewall Uprising
 * ![Nearby Christopher Park](/blog/assets/ChristopherPark3358.png "Nearby Christopher Park")
 * The header image is [Sheridan Square](https://commons.wikimedia.org/wiki/File:ChristopherPark3358.jpg) (despite the picture clearly being of Christopher Park) by an uncredited photographer, made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, The Way to Eden
 * ![The Garden of Eden](/blog/assets/Cole-Thomas-The-Garden-of-Eden-1828.png "The Garden of Eden")
 * The header image is [The Garden of Eden](https://commons.wikimedia.org/wiki/File:Cole_Thomas_The_Garden_of_Eden_1828.jpg) by [Thomas Cole](https://en.wikipedia.org/wiki/Thomas_Cole), long passed into the public domain.
 * title: Tweets from 06/28 to 07/02
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Solitudes and Silence, ch 10-14
 * ![Solitudes and Silence cover](/blog/assets/solitudes.png "Solitudes and Silence cover")
 * The header image is the cover of **Solitudes and Silence** by Jeremy Thevonot, made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Content Advisories
 * ![Warning: Brain Stuff Ahead](/blog/assets/warning-brain.svg "Warning: Brain Stuff Ahead"){:style="width: 100%;"}
 * The header image is my original work, combining modified versions of [Vector clip art of human brain in 2 colors](https://openclipart.org/detail/140683/brain-2colors-by-trubinial-guru) by [trubinial guru](https://openclipart.org/artist/trubinial%20guru) and [MUTCD W2-1.svg](https://commons.wikimedia.org/wiki/File:MUTCD_W2-1.svg), both released into the public domain.
 * title: Developer Journal, Early July
 * ![Independence Day, 1986](/blog/assets/Fourth-of-July-fireworks-behind-the-Washington-Monument-1986.png "Setting things on fire, near a body of water, to celebrate nationalism...")
 * The header image is [Fourth of July fireworks behind the Washington Monument, 1986](https://catalog.archives.gov/id/6413316) by Ssgt. Lono Kollars, placed into the public domain as the work of an employee of the United States Air Force as part of their duty.
 * title: Real Life in Star Trek, The Cloud Minders
 * ![Not the surface of Ardana](/blog/assets/41g-36-036~large.png "Not the surface of Ardana")
 * The header image is [STS-41G earth observations](https://images.nasa.gov/details-41g-36-036) by the [STS-41-G](https://en.wikipedia.org/wiki/STS-41-G) crew and the [National Aeronautics and Space Administration](https://www.nasa.gov/), placed in the public domain by NASA policy.  I selected it, because the episode uses a similar Gemini IV (1965) photograph of [Hadhramaut](https://en.wikipedia.org/wiki/Hadhramaut) as the surface, doctored in the remastered editions.
 * title: Tweets from 07/05 to 07/09
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — La Chute d'Une Plume
 * ![Movie frame](/blog/assets/mouse-on-scale.png "Mouse observing from the scale")
 * The header image is a frame from the film, under the same license as the other sources.
 * title: Media and Imposed Morality
 * ![Media in progress](/blog/assets/33282887826_557c9ef951_o.png "Media in progress")
 * The header image is [TELEVISION](https://www.flickr.com/photos/131601741@N05/33282887826/) by [Paulo Liberini](https://www.flickr.com/photos/131601741@N05/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Developer Journal, Bisbee Kidnapping Anniversary
 * ![Bisbee "Deportation"](/blog/assets/BisbeeDeportation.png "Bisbee 'Deportation'")
 * The header image is [Bisbee Deportation](https://commons.wikimedia.org/wiki/File:BisbeeDeportation.jpg) by an uncredited photographer, in the public domain due to a long-expired copyright.
 * title: Real Life in Star Trek, The Savage Curtain
 * ![Where It Rains Iron](/blog/assets/eso2005a.png "Where It Rains Iron")
 * The header image is [Artist’s impression of the night side of WASP-76b](https://www.eso.org/public/images/eso2005a/) by the ESO/M. Kornmesser, available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Tweets from 07/12 to 07/16
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Headshot, part 1
 * ![Helsinki apartments over a café](/blog/assets/209053882_b1ffe28ceb_o.png "Helsinki apartments over a café")
 * The header image is [helsinki](https://www.flickr.com/photos/flrnt/209053882/) by [Florent Le Gall](https://www.flickr.com/photos/flrnt/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: GitHub Copilot and Other Programming Doom
 * ![ENIAC Function Table](/blog/assets/ENIAC-function-table-at-Aberdeen.png "On the bright side, nobody argued about CamelCase and snake_case...")
 * The header image is [ENIAC function table at Aberdeen](https://commons.wikimedia.org/wiki/File:ENIAC_function_table_at_Aberdeen.jpg) by [Jud McCranie](https://commons.wikimedia.org/wiki/User:Bubba73), made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Developer Journal, GPS Anniversary
 * ![GPS Satellite, Artist's Conception](/blog/assets/GPS-Satellite-NASA-art-iif.png "GPS Satellite, Artist's Conception")
 * The header image is [GPS Satellite NASA art](https://commons.wikimedia.org/wiki/File:GPS_Satellite_NASA_art-iif.jpg), in the public domain by NASA policy.
 * title: Writing Jekyll Plugins
 * ![A door labeled Pull, in marker](/blog/assets/wood-furniture-room-lighting-door-interior-design-204256-pxhere.com.png "Pull, but not for quotes")
 * ![The old quote style, shaded background with dark bars to the left and right](/blog/assets/old-blog-quote-example.png "The old quote style")
 * The header image is [If a door know needs a label... well its function is not clear](https://pxhere.com/en/photo/204256) by Alan Levine, made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, All Our Yesterdays
 * ![Nova Remnant](/blog/assets/0401563~orig.png "Nova Remnant")
 * The header image is a [Chandra X-Ray Observatory (CXO) image of the supernova remnant Cassiopeia A](https://images.nasa.gov/details-0401563) by the National Aeronautics and Space Administration, placed into the public domain by NASA policy.
 * title: Tweets from 07/19 to 07/23
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://en.wikipedia.org/wiki/Week#/media/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Headshot, part 2
 * ![Row of Helsinki Apartments](/blog/assets/4724476163_18213d40af_o.png "Row of Helsinki Apartments")
 * The header image is [Helsinki boy](https://www.flickr.com/photos/andresfib/4724476163/) by [Andrés Moreno](https://www.flickr.com/photos/andresfib/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Stack Overflow's Utility
 * ![Stacks of books in front of shelves](/blog/assets/novel-antique-building-reading-narrow-pile-877721-pxhere.com.png "When I was your age, Stack Overflow was called 'books.'")
 * The header image is [untitled](https://pxhere.com/en/photo/877721) by an uncredited photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Korean Armistice Anniversary Eve
 * ![Signing the Armistice](/blog/assets/Korean-War-armistice-agreement-1953.png "Signing the Armistice")
 * The header image is [Korean War armistice agreement 1953](https://commons.wikimedia.org/wiki/File:Korean_War_armistice_agreement_1953.jpg) by F. Kazukaitis, U.S. Navy, in the public domain as a work of the United States government.
 * title: Real Life in Star Trek, Turnabout Intruder
 * ![Silhouettes of a man and woman arguing, with their outlines superimposed over each other](/blog/assets/32498689184_2d0c092796_o.png "I wish I had YOUR life...")
 * The header image is based on [arguing-1296392_960_720](https://www.flickr.com/photos/148525251@N02/32498689184/) by the [j t](https://www.flickr.com/photos/148525251@N02/), made available under the terms of a [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/); my version is licensed [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) like the rest of the blog.
 * title: Tweets from 07/26 to 07/30
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Eroticature
 * ![Salsa dancers (maybe)](/blog/assets/tango-performing-arts-entertainment-dance-event-performance-1509727-pxhere.com.png "Not accurate to any story...")
 * The header image is [untitled](https://pxhere.com/en/photo/1509727) by [alapileo](https://pxhere.com/en/photographer/1897607), made available under the terms of a [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Public Records and Privacy
 * ![A woman scanning a computer screen for information](/blog/assets/8230786746_f16e864208_o.png "Not actually stalking...")
 * The header image is [U.S.D.A. Deputy Press Secretary Stephanie Chan scans the computer screen in front of her for useful links to support people asking questions](https://www.flickr.com/photos/usdagov/8230786746/in/photolist-dxgX1B-dBVtTY-cDxmud-cDvGvb-dxgYyD-dxjLCQ-dxeDcP-dxgns4-dBVsrj-dxkdxw-dxnz5C-dxnrTd-cDwVWq-dBQ3QR-cDwMj7-dBQ4wz-dBQ3aK-cDvG2G-cDwt8C-dxjW9j-cDxZuW-cDxFjh-cDwny5-cDxEWw-cDvFDC-dxgHJv) by Lance Cheung, in the public domain as a work of the United States government.
 * title: Developer Journal, Pachamama Raymi
 * ![Piquillacta, 30 km SE from Cusco, Peru](/blog/assets/Piquillacta-view4.png "Not too far from where Pachamama Raymi is celebrated")
 * The header image is [Pikillaqta](https://commons.wikimedia.org/wiki/File:Piquillacta_view4.jpg) by Håkan Svensson (Xauxa), released under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Mudd's Angels
 * ![Three women attempting to strike the "Charlie's Angels" title card pose in silhouette](/blog/assets/504114713_9f6dfd6931_o.png "Probably the right reference, right?")
 * The header image is [shadow posing](https://www.flickr.com/photos/rocksee/504114713/) by [Rocksee](https://www.flickr.com/photos/rocksee/), available under a [Creative Commons Attribution 2.0 Generic](http://creativecommons.org/licenses/by/2.0/) License.
 * title: Tweets from 08/02 to 08/06
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Typhoon, Part 1
 * ![Dustrunners: Typhoon cover](/blog/assets/typhoon-cover.png "Dustrunners: Typhoon cover")
 * The header image is the cover of **Dustrunners: Typhoon** by ?, made available under the same terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/licenses/by-sa/3.0/) as the remainder of the book.
 * title: The Need for Modern Web Application Frameworks
 * ![A canopy frame](/blog/assets/4901169636_78abb669bc_o.png "These workers get it.")
 * The header image is [Framework](https://www.flickr.com/photos/26170836@N05/4901169636) by [Steven Lilley](https://www.flickr.com/photos/sk8geek/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Developer Journal, Indigenous Peoples Day
 * ![A Nama man playing a traditional greeting](/blog/assets/Nama-man-greeting-us-3693359757.png "A traditional Nama greeting")
 * The header image is [Nama man greeting us](https://commons.wikimedia.org/wiki/File:Nama_man_greeting_us_%283693359757%29.jpg) by Greg Willis, made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/deed.en) license.
 * title: Real Life in Star Trek, Season 3 Summary
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is [The planet, the galaxy and the laser](https://www.eso.org/public/images/eso0733a/) by the [ESO](https://www.eso.org) and [Yuri Beletsky](Yuri Beletsky), released under the terms of a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) license.
 * title: Tweets from 08/09 to 08/13
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Typhoon, Part 2
 * ![Dustrunners: Typhoon cover](/blog/assets/typhoon-cover.png "Dustrunners: Typhoon cover")
 * The header image is the cover of **Dustrunners: Typhoon** by an uncredited artist (presumably MCM), made available under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Age Cohorts, Ruining Things, and Apocalypses
 * ![Multiple generations of telephone](/blog/assets/technology-antique-old-phone-telephone-communication-494891-pxhere.com.png "All the pictures of generations of people had unfortunate racial connotations, so humor me on this one...")
 * ![Don Quixote knocked off Rocinante by a windmill](/blog/assets/Don-Quixote-6-windmill.png "He could have gotten milled grain instead of an injury")
 * ![The Wizard of Oz revealed as a "Humbug"](/blog/assets/Wizard-Oz-Humbug.png "He doesn't NOT look like Jeff Bezos, now that I think about it.")
 * ![Uncle Sam Army recruitment poster](/blog/assets/James-Montgomery-Flagg-I-want-you-for-US-Army-1917.png "I'm surprised that Strauss and Howe didn't try to imagine latch-key kids in 1910")
 * ![A blurred picture of a person wearing a sandwich board reading "The End Is Nigh"](/blog/assets/8029801761_08fca52fca_o.png "Don't believe everything that you read, especially on sandwich boards")
 * ![The Last Judgment, by Hans Memling](/blog/assets/Das-Jüngste-Gericht-Memling.png "I would think that the end of the world would involve either less nudity and/or less boredom...")
 * ![The Obama-in-a-tan-suit photograph, recolored to resemble Obama's "Hope" campaign posters](/blog/assets/P082814PS-0607-15043139586-hope.jpg "I couldn't think of anything more hopeful than recoloring the picture of Obama in his tan suit, because I lack imagination...")
 * The header image is [untitled](https://pxhere.com/en/photo/494891) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  The illustration of Don Quixote tilting and the windmill by [Gustave Doré](https://en.wikipedia.org/wiki/Gustave_Dor%C3%A9) is from the 1863 edition of [**Don Quixote**](https://gutenberg.org/ebooks/996), long in the public domain.  The illustration of the revelation of the Wizard of Oz as a fraud by [William Wallace Denslow](https://en.wikipedia.org/wiki/William_Wallace_Denslow) is from [**The Wonderful Wizard of Oz**](https://www.gutenberg.org/ebooks/43936), long in the public domain via an expired copyright.  [The End Is Nigh](https://www.flickr.com/photos/42420143@N02/8029801761) by [Alma Ayon](https://www.flickr.com/photos/almitaayon/) is made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.  [*Das Jüngste Gericht*](https://commons.wikimedia.org/wiki/File:Das_J%C3%BCngste_Gericht_%28Memling%29.jpg) by Hans Memling was created before 1473, and so has been in the public domain for a long time.  The [I Want You](https://commons.wikimedia.org/wiki/File:30b_Sammlung_Eybl_USA_James_Montgomery_Flagg_%281877-1960%29_I_want_you_for_U.S._Army._1917._101_x_76_cm._%28Coll..Nr._3116%29.jpg) by James Montgomery Flagg has been in the public domain as a work of the United States government *and* because it was published in the United States more than ninety-five years ago.  The image of Barack Obama is cropped and recolored from [President Barack Obama meets with John F. Tefft, U.S. Ambassador to Russia, in the Oval Office, Aug. 28, 2014](https://www.flickr.com/photos/obamawhitehouse/15043139586/) by [Pete Souza](https://en.wikipedia.org/wiki/Pete_Souza)---the origin of the inane [tan suit controversy](https://en.wikipedia.org/wiki/Obama_tan_suit_controversy)---which is in the public domain as the work of the United States government, with recoloring handled by the [GNU Image Manipulation Program](https://www.gimp.org/).
 * title: Developer Journal, Hugo Gernsback's Birthday
 * ![Hugo Gernsback](/blog/assets/blythe-hugo-gernsback.svg "Sadly, the picture of him wearing television-goggles is still under copyright...")
 * The header image is [Hugo Gernsback c.1929](https://commons.wikimedia.org/wiki/File:Hugo_Gernsback_SWS_2911.jpg) by William Edward Blythe, apparently in the public domain for failure to renew the copyright.
 * title: Real Life in Star Trek, Beyond the Farthest Star
 * ![A picture of seaweed standing in for the alien ship](/blog/assets/beautiful-reefs-ocean-reef-underwater-coral-reef-marine-biology-1633325-pxhere.com.png "Not an alien ship...probably")
 * The header image is [Beautiful reefs in the deep ocean](https://pxhere.com/en/photo/1633325) by [Tatyana2021](https://pxhere.com/en/photographer/3442559), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 08/16 to 08/20
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Typhoon, Part 3
 * ![Dustrunners: Typhoon cover](/blog/assets/typhoon-cover.png "Dustrunners: Typhoon cover")
 * The header image is the cover of **Dustrunners: Typhoon** by an uncredited artist (presumably MCM), made available under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Black Ribbon Day
 * ![Black ribbon](/blog/assets/Black_Ribbon.svg "A black ribbon for remembrance")
 * ![Fýlakas Onomáton's name list](/blog/assets/fylakas-onomaton-name-list.png "Anybody who knows Korean can laugh at me, but that's really the mobile app's problem...")
 * The header image is [Black Ribbon](https://commons.wikimedia.org/wiki/File:Black_Ribbon.svg), by [MarianSigler](https://commons.wikimedia.org/wiki/User:MarianSigler), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Yesteryear
 * ![Stone arch, with a blurred and recolored interior](/blog/assets/landscape-nature-rock-wilderness-mountain-architecture-746291-pxhere.com.png "Not the Guardian, part two")
 * The header image is based on [untitled](https://pxhere.com/en/photo/746291) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 08/23 to 08/27
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Typhoon, Part 4
 * ![Dustrunners: Typhoon cover](/blog/assets/typhoon-cover.png "Dustrunners: Typhoon cover")
 * The header image is the cover of **Dustrunners: Typhoon** by an uncredited artist (presumably MCM), made available under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Day of the Disappeared
 * ![Remembering the disappeared in Argentina](/blog/assets/Bandera-de-los-Desaparecidos-Dia-de-la-Memoria.png "Remembering the disappeared in Argentina")
 * The header image is [Bandera de los Desaparecidos — Día de la Memoria](https://commons.wikimedia.org/wiki/File:Bandera_de_los_Desaparecidos_-_D%C3%ADa_de_la_Memoria.jpg) by [Banfield](https://commons.wikimedia.org/wiki/User:Banfield), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, One of Our Planets Is Missing
 * ![A space-cloud](/blog/assets/eso1145a.png "Please don't eat the planets")
 * The header image is [The cool clouds of Carina](https://www.eso.org/public/images/eso1145a/) by ESO/APEX/T. Preibisch et al. (Submillimetre); N. Smith, University of Minnesota/NOAO/AURA/NSF, available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Tweets from 08/30 to 09/03
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Cataclysm — Dark Days Ahead
 * ![Screenshot from Cataclysm: Dark Days Ahead](/blog/assets/cdda-screenshot.png "Into the unknown...")
 * The header image is a screenshot of **Cataclysm:  Dark Days Ahead**, made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license, matching the rest of the game.
 * title: Women Deserve Better Than Roe, Anyway
 * ![An angry cat](/blog/assets/animal-cute-pet-fur-fluffy-kitten-1221758-pxhere.com.png "His ears are still raised, so it's probably not THAT bad, actually...")
 * The header image is [untitled](https://pxhere.com/en/photo/1221758) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Labor Day
 * ![Workers battering a wall marked "Axis" with a ram labeled "Free Labor Will Win"](/blog/assets/LABOR-DAY-1942-NARA-535654.png "Collective bargaining would help...")
 * The header image is [LABOR DAY 1942 — NARA — 535654](https://commons.wikimedia.org/wiki/File:LABOR_DAY_1942_-_NARA_-_535654.jpg) by Charles Henry Alston, placed into the public domain as the work of the United States Department of the Treasury.
 * title: Real Life in Star Trek, The Lorelei Signal
 * ![Lorelei Monument, Bronx, NY](/blog/assets/Heine-Bronx-1.png "Why is it in the Bronx? Anti-Semitic trash kept it out of everywhere else...")
 * ![The Lorelei, in 1900](/blog/assets/Loreley-LOC.png "It was a BIG rock...")
 * The header image is [The Loreli, the Rhine, Germany](https://www.loc.gov/pictures/collection/pgz/item/2002714142/) by an unknown photographer.  The picture of [Ernst Herter](https://en.wikipedia.org/wiki/Ernst_Herter)'s [the Lorelei Monument](https://commons.wikimedia.org/wiki/File:Heine_Bronx_1.jpg) is by [Schreibkraft](https://de.wikipedia.org/wiki/Benutzer:Schreibkraft), made available under the terms of the [Creative Commons Attribution-Share Alike 2.0 Germany](https://creativecommons.org/licenses/by-sa/2.0/de/deed.en) license.
 * title: Tweets from 09/06 to 09/10
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Affair, Part 1
 * ![Affair cover](/blog/assets/stokes-affair-cover.png "Affair cover")
 * The header image is the cover of **Affair** by Omar Willey,  made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Developer Journal, Day of the Programmer
 * ![A woman at a small table transferring code from her laptop to her phone](/blog/assets/25392390163_d6a5321bfb_o.png "Large windows don't make these multi-device moments any less tedious.")
 * The header image is [wocintech (microsoft) — 42](https://www.flickr.com/photos/wocintechchat/25392390163/in/photostream/) by [WOCinTech Chat](https://www.flickr.com/photos/wocintechchat/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, More Tribbles, More Troubles
 * ![A tribble-substitute on a tile floor](/blog/assets/4361933128_6691830072_o.png "A lot of tribble for a simple joke")
 * The header image is [Tribble on the floor](https://www.flickr.com/photos/83782921@N00/4361933128) by [Bethany](https://www.flickr.com/photos/andie712b/), made available under the terms of a [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Tweets from 09/13 to 09/17
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Affair, Part 2
 * ![Affair cover](/blog/assets/stokes-affair-cover.png "Affair cover")
 * The header image is the cover of **Affair** by Omar Willey,  made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Developer Journal, World Peace Eve
 * ![Iranian 1987 stamp celebrating World Peace Day](/blog/assets/1987-International-peace-day-stamp-of-Iran.png "May the Great Bird of the Galaxy bless your planet.")
 * The header image is [1987 "International peace day" stamp of Iran](https://commons.wikimedia.org/wiki/File:1987_%22International_peace_day%22_stamp_of_Iran.jpg) by Iran Post, lapsed into the public domain in Iran, and in the public domain in the United States as a non-party to major international copyright treaties.
 * title: Startup, but Not Really Startup
 * ![Gears](/blog/assets/work-technology-vintage-wheel-retro-clock-606288-pxhere.com.jpg "I'm bringing back the gears image, because I didn't feel like looking for metaphors for starting things...")
 * The header image is [work, technology, vintage, wheel, retro, clock](https://pxhere.com/en/photo/606288), by an uncredited PxHere photographer, is made available under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, The Survivor
 * ![A recolored whip-lash squid](/blog/assets/Grimalditeuthis-bonplandi-2.png "Not a Vendorian")
 * The header image is [Grimalditeuthis bonplandi](https://commons.wikimedia.org/wiki/File:Grimalditeuthis_bonplandi_%282%29.jpg) by Jeanne Le Roux & L. Joubin, copyrights lapsed into the public domain, having been published prior to 1909.
 * title: Tweets from 09/20 to 09/24
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Affair, Part 3
 * ![Affair cover](/blog/assets/stokes-affair-cover.png "Affair cover")
 * The header image is the cover of **Affair** by Omar Willey,  made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Developer Journal, Gay Men's HIV/AIDS Awareness
 * ![National Gay Men's HIV/AIDS Awareness Day](/blog/assets/ngmhaad-profile-logo.png "National Gay Men's HIV/AIDS Awareness Day")
 * The header image is [National Gay Men's HIV/AIDS Awareness Day](https://www.hiv.gov/events/awareness-days/gay-mens) by the United States Department of Health and Human Services, placed in the public domain as a work of the United States government.
 * title: Where Have All the Emoji Gone?
 * ![Hundreds of randomly selected OpenMoji images](/blog/assets/emoji-collage.png "Emoji, emoji everywhere, and all the boards did shrink...")
 * The header image is a random assortment of OpenMoji images, generated by me from images made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, The Infinite Vulcan
 * ![34 Squadron in ghillie suits](/blog/assets/34-Squadron-undertake-Live-Fire-Tactical-Training-at-Otterburn-Camp.png "Not Phylosians...or maybe that's just what they WANT us to think")
 * The header image is [34 Squadron undertake Live Fire Tactical Training at Otterburn Camp](https://commons.wikimedia.org/wiki/File:34_Squadron_undertake_Live_Fire_Tactical_Training_at_Otterburn_Camp._MOD_45159226.jpg) by SAC Phil Dye, made available under the terms of the [Open Government License version 1.0](https://nationalarchives.gov.uk/doc/open-government-licence/version/1/).
 * title: Tweets from 09/27 to 10/01
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Affair, Part 4
 * ![Affair cover](/blog/assets/stokes-affair-cover.png "Affair cover")
 * The header image is the cover of **Affair** by Omar Willey,  made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Developer Journal, World Space Week
 * ![World Space Week](/blog/assets/World-Space-Week–Stacked.png "Not to be confused with Word Space Week, where people discuss kerning...")
 * The header image is the logo for [World Space Week](https://commons.wikimedia.org/wiki/File:World_Space_Week_%E2%80%93_Stacked.png) by [QWqDk](https://commons.wikimedia.org/w/index.php?title=User:QWqDk&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: So You Need an Image of Random Images...
 * ![Another bunch of emoji](/blog/assets/emoji-collage-2.png "Another run of the script")
 * ![Female superhero with type-VI skin emoji](/blog/assets/1F9B8-1F3FF-200D-2640-FE0F.png "Save us, Noseless Woman with Fabulous Hair!")
 * The header image is a random assortment of OpenMoji images, generated by me from images made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/ license), as is the woman superhero image.
 * title: Real Life in Star Trek, The Magicks of Megas-Tu
 * ![Lucifer, the fallen angel](/blog/assets/Lucifer3.png "Serpent-Watch and Chill almost caught on, so that streaming service took over production of that show to stifle it...")
 * The header image is [Lucifer, the fallen angel](https://commons.wikimedia.org/wiki/File:Lucifer3.jpg) by Gustav Doré for **Paradise Lost**, long in the public domain.
 * title: Tweets from 10/04 to 10/08
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Lightbringer, Part 1
 * ![Our hero, psyching himself up for his origin story](/blog/assets/Carter-Grandholme.png "Our hero, fleeing from cries for help.")
 * The header image is a panel from the first issue of the comic, with writing by Lewis Lovhaug and art by Ted Sage...who appears to be a pseudonym for Lovhaug, dedicated to the public domain.
 * title: Developer Journal, National Coming Out Day
 * ![A walk-in closet with clothes hanging](/blog/assets/Walk-In-Closet-Expandable-Closet-Rod-and-Shelf.png "If you're in there, that's less room for clothes, right?")
 * The header image is [Walk In Closet — Expandable Closet Rod and Shelf](https://commons.wikimedia.org/wiki/File:Walk_In_Closet_-_Expandable_Closet_Rod_and_Shelf.jpg) by Wjablow, made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Using git to Count Changed Words
 * ![Chinese finger-counting system](/blog/assets/Chinese-counting.png "Hanging six doesn't sound as appealing, somehow.")
 * The header image is [Chinese finger-counting system](https://commons.wikimedia.org/wiki/File:Chinese_counting.jpg) by [Aljosa21](https://commons.wikimedia.org/w/index.php?title=User:Aljosa21), made available under the terms of the [Creative Commons Attribution-Share Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Once Upon a Planet
 * ![A man in a sweatsuit looking out over water and hills](/blog/assets/man-sea-coast-water-ocean-horizon-312-pxhere.com.png "This vacation could've been an e-mail...")
 * The header image is [untitled](https://pxhere.com/en/photo/312) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 10/11 to 10/15
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Lightbringer, Part 2
 * ![Partial cover to Lightbringer #13](/blog/assets/lightbringer-20081112.png "Red, White, and Osprey, I guess")
 * ![Introduction to Peacemaker](/blog/assets/peacemaker-intro-ff40.png "The white pants are really what sell this get-up...")
 * The header image is a cover from the comic---with writing by Fred Gouldart...who *also* appears to be a pseudonym for Lovhaug---dedicated to the public domain.  The image of Peacemaker is from **Fightin' 5** #40 (November 1966), and lapsed into the public domain due to a malformed copyright statement.
 * title: Let's Fix...Facebook
 * ![Mark Zuckerberg testifying in front of the House Energy and Commerce Committee, 2018 April 11](/blog/assets/92aae362-0167-4975-89cb-b9884b73f8bc_fullhd-02-09.png "Sir, how dare you suggest that you know more about laws than the people who plan to break them?  And does anybody here know how to tie a tie?  I only recently graduated from hoodies...")
 * The header image is a frame from [Facebook:  Transparency and Use of Consumer Data, Full Committee](https://energycommerce.house.gov/committee-activity/hearings/hearing-on-facebook-transparency-and-use-of-consumer-data-full-committee).
 * title: Developer Journal, Alaska Day
 * ![Alaskan flag](/blog/assets/Flag-of-Alaska.png "I'm still mystified by the whole ladle or bear thing...")
 * The header image is [Flag of Alaska](https://openclipart.org/detail/119155) by an anonymous Open Clip Art artist, released into the public domain.
 * title: Real Life in Star Trek, Mudd's Passion
 * ![CGI smoke rising from a crystal glass](/blog/assets/light-glass-smoke-drink-light-painting-wine-glass-1323728-pxhere.com.png "Dear Resident/Occupant:  I love you and can't live without you...")
 * The header image is [untitled](https://pxhere.com/en/photo/1323728) by an uncredited PxHere photographer, released under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 10/18 to 10/22
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Occupy This Novel!, Part 1
 * ![Occupy This Novel! cover](/blog/assets/occupy-novel-cover.png "Occupy This Novel! cover")
 * ![Occupy hand signals](/blog/assets/744px-Hands-signals-Occupy-A4.png "I assume that the colors were unnecessary, but I admittedly wasn't there...")
 * The header image is the cover of **Occupy This Novel!** by [Nicholas D. Ouellette](https://www.deviantart.com/ndouellette),  made available under the terms of the same license as the novel.  The image of the [Occupy hand signals](https://commons.wikimedia.org/wiki/File:OccupyHandSignals.pdf) by [Ruben de Haas](http://www.lekrmoi.com/) has been made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Developer Journal, Intersex Awareness Eve
 * ![Intersex flag](/blog/assets/Intersex_Pride_Flag.png "I used to go to a school with a logo that wasn't so different")
 * The header image is the [Intersex Pride Flag](https://ihra.org.au/22773/an-intersex-flag/) by Morgan Carpenter and AnonMoos, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).
 * title: Real Life in Star Trek, The Terratin Incident
 * ![Minimidget (sigh) and Ritty, chasing a normal-sized by climbing a broom](/blog/assets/amazing-06-p39-p8.png "Lifting the phone off the hook with a pencil was probably more appropriate, but (a) that panel had too much space filled with a caption, and (b) fewer of you would probably be able to use a rotary-dial phone for scale than the milk bottle...")
 * The header image is a panel from **Amazing Man** #6, published in 1939.  The copyright was never renewed, and is therefore in the public domain.
 * title: Tweets from 10/25 to 10/29
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Occupy This Novel!, Part 2
 * ![Occupy This Novel! cover](/blog/assets/occupy-novel-cover.png "Occupy This Novel! cover")
 * The header image is the cover of **Occupy This Novel!** by [Nicholas D. Ouellette](https://www.deviantart.com/ndouellette),  made available under the terms of the same license as the novel.
 * title: Short Fiction — The Gevkahahal
 * ![Monster pumpkin](/blog/assets/19405145_b1229af64f_o.png "Maybe don't sit up all night waiting for this guy to show up")
 * title: Developer Journal, Day of the Dead
 * ![La Leona Cemetery, Cuernavaca, Mexico](/blog/assets/10660470386_3a6ff27609_o.png "A Day of the Dead celebration")
 * The header image is the [Celebrating "Día de los Muertos"](https://www.flickr.com/photos/furphotos/10660470386/in/photostream/) by [Felicity Rainnie](https://www.flickr.com/photos/furphotos/), made available under the terms of the [Creative Commons Attribution No-Derivatives 2.0 Generic](https://creativecommons.org/licenses/by-nd/2.0/) license.
 * title: Artificial Stupidity with GitHub Copilot
 * ![A copilot](/blog/assets/7581301810_2fb22999a8_o.png "Bleep bloop?")
 * The header image is [Copilot](https://www.flickr.com/photos/basheertome/7581301810) by [Basheer Tome](https://www.flickr.com/photos/basheertome/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, The Time Trap
 * ![The Sargasso Sea, sideways to fit](/blog/assets/Lines-of-sargassum-Sargasso-Sea.png "I'm honestly not sure if I would have preferred the episode with lines of space-algae stretching out")
 * The header image is [Lines of sargassum Sargasso Sea](https://oceanexplorer.noaa.gov/explorations/02sab/logs/aug09/media/lines.html) by the [National Oceanic and Atmospheric Administration](https://www.noaa.gov/), albeit sideways, placed in the public domain as a work of the United States government.
 * title: Tweets from 11/01 to 11/05
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Sita Sings the Blues
 * ![Sita, Rama, and Hanuman flying away](/blog/assets/sita-and-company-flying.png "Not shown:  The massive war the narration built to for several minutes.")
 * The header image is a frame from **Sita Sings the Blues**,  made available under the terms of the same license as the film.
 * title: Developer Journal, Intersex Solidarity Day
 * ![Intersex flag](/blog/assets/Intersex_Pride_Flag.png "I see no reason not to recycle the flag from two weeks ago...")
 * The header image is the [Intersex Pride Flag](https://ihra.org.au/22773/an-intersex-flag/) by Morgan Carpenter and AnonMoos, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).
 * title: Real Life in Star Trek, The Ambergris Element
 * ![A reclining mer-man wearing a crown](/blog/assets/A-Crowned-Merman-Arthur-Rackham.png "Transformation to a fish-person can be so exhausting, you know?")
 * The header image is [A Crowned Merman](https://www.artrenewal.org/Artwork/Index/18817) by Arthur Rackham, lapsed into the public domain on the seventieth anniversary of Rackham's death.
 * title: Tweets from 11/08 to 11/12
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — if then else, part 1
 * ![Three Doors](/blog/assets/11825045214_28fbb799ff_o.png "If, Then, and Else? Sure.")
 * The header image is [Three Doors](https://www.flickr.com/photos/atoach/11825045214/) by [Tim Green](https://www.flickr.com/photos/atoach/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Geography Awareness Week
 * ![A physical world map](/blog/assets/world_phy.png "The Earth, presented in the famous UFO projection")
 * The header image is the [Physical Map of the World, February 2021](https://www.cia.gov/the-world-factbook/static/2ea375ba452947cfe77e5a3d6cd7c1c5/world_phy.pdf) by uncredited Central Intelligence Agency workers, in the public domain as a work of the United States Government.
 * title: Real Life in Star Trek, The Slaver Weapon, part 1
 * ![3D-Printed Puzzle Box](/blog/assets/featured_preview_IMG_20200829_223733.png "Not a stasis box...")
 * The header image is [Puzzle Box --- Easy Print, Hard Solution](https://www.thingiverse.com/thing:4583356) by [David Lybeck](https://www.thingiverse.com/dlybeck383/designs), made available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Tweets from 11/15 to 11/19
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — if then else, part 2
 * ![Three Doors](/blog/assets/11825045214_28fbb799ff_o.png "If, Then, and Else? Sure.")
 * The header image is [Three Doors](https://www.flickr.com/photos/atoach/11825045214/) by [Tim Green](https://www.flickr.com/photos/atoach/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Superheroes Behaving Badly
 * ![An annoyed child superhero](/blog/assets/2380481728_f2f8f668b9_o.png "I realize that we find a way to avoid killing dangerous people at least once a month, but hear me out:  Maybe this time, I should murder the dangerous person, and there's no other way.  Eh?")
 * The header image is based on [Pensive Superhero](https://www.flickr.com/photos/thenickster/2380481728/) by [Nicki Dugan Pogue](https://www.flickr.com/photos/thenickster/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Developer Journal, Albanian Alphabet Anniversary
 * ![The Congress of Manastir](/blog/assets/Komisioni-i-Alfabetit-Monastir-1908.png "If they didn't actually wear the numbers on their jackets and include a disembodied head among their ranks, why even show up...?")
 * The header image is [Komisioni i Alfabetit Monastir 1908](https://commons.wikimedia.org/wiki/File:Komisioni_i_Alfabetit_Monastir_1908.jpg) by an unknown photographer, long in the public domain.
 * title: Real Life in Star Trek, The Slaver Weapon, part 2
 * ![Five kittens on a lawn](/blog/assets/kitten-cat-mammal-fauna-vertebrate-rush-918620-pxhere.com.png "The story isn't NOT like this")
 * The header image is [Untitled](https://pxhere.com/en/photo/918620) by an uncredited PxHere photographer, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 11/22 to 11/26
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — if then else, part 3
 * ![Three Doors](/blog/assets/11825045214_28fbb799ff_o.png "If, Then, and Else? Sure.")
 * The header image is [Three Doors](https://www.flickr.com/photos/atoach/11825045214/) by [Tim Green](https://www.flickr.com/photos/atoach/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Amateur Stenography
 * ![A SOFT-HRUF Splitography Writer](/blog/assets/SOFT-HRUF.png "It's not mine, but close enough")
 * ![steno-keyboard](/blog/assets/steno-keyboard.svg "The generic stenotype keyboard")
 * The header image is [SOFT-HRUF](https://commons.wikimedia.org/wiki/File:SOFT-HRUF.jpg) by [GorillaWarfare](https://commons.wikimedia.org/wiki/User:GorillaWarfare), made available under the terms of the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/deed.en) license.  [Stenotype](https://www.flickr.com/photos/paille-fr/8637796793/) by [Paille](https://www.flickr.com/photos/paille-fr/) has been made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.  The stenotype keyboard layout is adapted from images used throughout [**Art of Chording**](https://www.artofchording.com/), available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Developer Journal, Byrd Antarctic Expedition
 * ![The Byrd expedition ship in heavy pack ice of the South Pole](/blog/assets/Bundesarchiv-Bild-102-09158-Expeditionsschiff-Byrds.png "Watch out for that icy patch.")
 * The header image is the [Byrd's South Pole expedition in the pack ice](https://en.wikipedia.org/wiki/File:Bundesarchiv_Bild_102-09158,_Expeditionsschiff_Byrds.jpg) by an unknown photographer, made available by the [German Federal Archive](http://www.bundesarchiv.de/) under the terms of the [Creative Commons Attribution Share-Alike 3.0 Germany](https://creativecommons.org/licenses/by-sa/3.0/de/deed.en) license.
 * title: Real Life in Star Trek, The Slaver Weapon, part 3
 * ![Arrival of the English ambassadors to the King of Brittany](/blog/assets/Accademia-Arrivo-degli-ambasciatori-inglesi-presso-il-re-di-Bretagna-di-Vittore-Carpaccio.png "Every delegation should have a random guy alone on a staircase")
 * The header image is [Accademia — Arrivo degli ambasciatori inglesi presso il re di Bretagna](https://commons.wikimedia.org/wiki/File:Accademia_-_Arrivo_degli_ambasciatori_inglesi_presso_il_re_di_Bretagna_di_Vittore_Carpaccio.jpg) by Vittore Carpaccio, long in the public domain.
 * title: Tweets from 11/08 to 11/12
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — if then else, part 4
 * ![Three Doors](/blog/assets/11825045214_28fbb799ff_o.png "If, Then, and Else? Sure.")
 * The header image is [Three Doors](https://www.flickr.com/photos/atoach/11825045214/) by [Tim Green](https://www.flickr.com/photos/atoach/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Short Fiction — All Quiet on the Vernal Front
 * ![Blurred holiday lights](/blog/assets/281963757_bee0898808_o.png "The holidays are mostly a blur for most people, anyway.")
 * The header image is adapted from [holiday blur](https://www.flickr.com/photos/elias_daniel/281963757/) by [Elias Gayles](https://www.flickr.com/photos/elias_daniel/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Saint Nicholas Day
 * ![Ukrainian Christmas stamp](/blog/assets/Christmas_Stamp_of_Ukraine_2006_2.png "How did Santa Claus NOT get angels who dump amorphous orange things out of horns?")
 * ![A hole drilled into the back of a laptop case, with a power cord dangling out](/blog/assets/laptop-cord-hole.png "More than anything, I'm impressed that the Ikea drill didn't blow up in my face, figuratively or literally.")
 * The header image is [Christmas Stamp of Ukraine 2006 2](https://commons.wikimedia.org/wiki/File:Christmas_Stamp_of_Ukraine_2006_2.jpg) by an unknown artist, in the public domain as a symbol of Ukrainian government authority.
 * title: Real Life in Star Trek, The Eye of the Beholder
 * ![A slug eating local vegetation](/blog/assets/Arion-vulgaris-eating.png "Not a Lactran...but he does look smarter than some people I've worked with")
 * The header image is [Arion vulgaris eating](https://commons.wikimedia.org/wiki/File:Arion_vulgaris_eating.jpg) by Håkan Svensson, made available under a [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Tweets from 12/06 to 12/10
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Space Company
 * ![Space Company's logo](/blog/assets/SpaceCompanyloadScreenPic.png "Graphic design is definitely not this game's primary draw...")
 * The header image is [Load Screen Pic](https://github.com/sparticle999/SpaceCompany/blob/gh-pages/loadScreenPic.png), released under the same terms as the game itself, as part of the repository.
 * title: Developer Journal, Acadian Remembrance Day
 * ![An image of the Expulsion of the Acadians](/blog/assets/A-View-of-the-Plundering-and-Burning-of-the-City-of-Grymross-by-Thomas-Davies.png "The expulsion of the Acadians and attack on Grymross")
 * The header image is [A View of the Plundering and Burning of the City of Grymross](https://commons.wikimedia.org/wiki/File:A_View_of_the_Plundering_and_Burning_of_the_City_of_Grymross,_by_Thomas_Davies,_1758.JPG) by Thomas Davies, long in the public domain.
 * title: Real Life in Star Trek, The Eye of the Beholder, part 2
 * ![A crystalline tree](/blog/assets/tree-branch-stone-macro-blue-lighting-1171262-pxhere.com.png "Not a Boqus")
 * The header image is [untitled](https://pxhere.com/en/photo/1171262) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 12/13 to 12/17
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Children of Wormwood, part 1
 * ![Children of Wormwood cover](/blog/assets/Children-of-Wormwood.png " Vulture Priest with Raycat")
 * The header image is **Children of Wormwood**'s cover, based on [* Vulture Priest with Raycat*](https://thefifthworld.com/art/giulianna-maria-lamanna/vulture-priest-with-raycat) by the author, released under the same license as the novel.
 * title: Developer Journal, Yaldā Eve
 * ![A Persian woman reading poetry for Yaldā](/blog/assets/Persian-Lady-recites-Hafez-Poems-in-Yalda-Night.png "The West needs more holidays involving baklava.  Somebody get on that project...")
 * ![An image of the icon next to the tags](/blog/assets/github-link-and-tags.png "An escape hatch to the online repository")
 * The header image is [Persian Lady recites Hafez Poems in Yalda Night](https://commons.wikimedia.org/wiki/File:Persian_Lady_recites_Hafez_Poems_in_Yalda_Night.jpg) by [PersianDutchNetwork](https://commons.wikimedia.org/w/index.php?title=User:PersianDutchNetwork), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, The Jihad
 * ![Men standing around an interactive table](/blog/assets/6359817133_12b0e0a715_o-2.png "Probably not actually planning a heist...")
 * The header image is [Heist! At The Tech Museum — Test Zone Opening](https://www.flickr.com/photos/27512715@N02/6359817133) by [Open Exhibits](https://www.flickr.com/photos/openexhibits/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) License.
 * title: Tweets from 12/20 to 12/24
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Children of Wormwood, part 2
 * ![Children of Wormwood cover](/blog/assets/Children-of-Wormwood.png " Vulture Priest with Raycat")
 * The header image is **Children of Wormwood**'s cover, based on [* Vulture Priest with Raycat*](https://thefifthworld.com/art/giulianna-maria-lamanna/vulture-priest-with-raycat) by the author, released under the same license as the novel.
 * title: 🔭 Looking Back on 2021
 * ![Joe Biden Pitching "Build Back Better"](/blog/assets/P20210331AS-1892-51131137370.png "Regardless of whether it goes forward, I'd argue that Build Back Better has been the defining story of the United States in 2021...")
 * The header image is [President Joe Biden delivers remarks on his economic vision](https://www.flickr.com/photos/whitehouse/51131137370/) by Adam Schultz, in the public domain as a work of the United States government.
 * title: Developer Journal, Junkanoo
 * ![A Junkanoo parade, from overhead](/blog/assets/23562387456_0538859fd2_o.png "Look, it's better than trying to make sense out of Boxing Day...")
 * The header image is [Bahamas 1988 (127) New Providence: Junkanoo](https://www.flickr.com/photos/rstehn/23562387456/) by [Rüdiger Stehn](https://www.flickr.com/photos/rstehn/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Real Life in Star Trek, The Pirates of Orion
 * ![An alien pirate](/blog/assets/9828691256_cbc14e0b82_o.png "How much do Orions pay for space-corn?  About a buck an ear...")
 * The header image is [Ahoy, lubbers! Cap'n Alien at yer service.](https://www.flickr.com/photos/8344964@N04/9828691256) by [Adam J. Manley](https://www.flickr.com/photos/adamthealien/), made available under the terms of the  [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) License.
 * title: Tweets from 12/27 to 12/31
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * ![An unofficial Vignelli-style MTA map](/blog/assets/NYC-subway-4D.png "Why yes, this format probably WOULD make for a fun urban planning game...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.  The subway map is [NYC subway-4D](https://en.wikipedia.org/wiki/File:NYC_subway-4D.svg) by [Jake Berman](https://en.wikipedia.org/wiki/User:CountZ).
 * title: Free Culture Book Club — Children of Wormwood, part 3
 * ![Children of Wormwood cover](/blog/assets/Children-of-Wormwood.png " Vulture Priest with Raycat")
 * The header image is **Children of Wormwood**'s cover, based on [* Vulture Priest with Raycat*](https://thefifthworld.com/art/giulianna-maria-lamanna/vulture-priest-with-raycat) by the author, released under the same license as the novel.
 * title: 🍾 Happy Calendar-Changing Day (Belated), 2022 🎆
 * ![Loading 2022](/blog/assets/49737049918_50cd20902c_o.png "Please stand by...")
 * The header image is adapted from [Handwriting Text 2022 Loading](https://www.flickr.com/photos/91261194@N06/49737049918) by [Jernej Furman](https://www.flickr.com/photos/91261194@N06/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, National Mentoring Month
 * ![Neo Ntsoma leading a workshop](/blog/assets/Neo-mentoring-students-001.png "Some mentoring happens in groups")
 * The header image is [Neo mentoring students 001](https://commons.wikimedia.org/wiki/File:Neo_mentoring_students_001.jpg) by [Bobby Shabangu](https://commons.wikimedia.org/wiki/User:Bobbyshabangu), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Real Life in Star Trek, Bem part 1
 * ![A young-looking alien](/blog/assets/night-star-atmosphere-space-human-darkness-813214-pxhere.com.png "Not Bem")
 * The header image is [untitled](https://www.eso.org/public/usa/images/eso1905a/) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 01/03 to 01/07
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Children of Wormwood, part 4
 * ![Children of Wormwood cover](/blog/assets/Children-of-Wormwood.png " Vulture Priest with Raycat")
 * The header image is **Children of Wormwood**'s cover, based on [* Vulture Priest with Raycat*](https://thefifthworld.com/art/giulianna-maria-lamanna/vulture-priest-with-raycat) by the author, released under the same license as the novel.
 * title: Developer Journal, Eugenio María de Hostos
 * ![De Hostos and some students](/blog/assets/HostosyStudents.png "¡Oh Capitán! ¡Mi capitán!")
 * The header image is [Hostosy Students](https://commons.wikimedia.org/wiki/File:HostosyStudents.jpg) by an unknown photographer, long in the public domain.
 * title: Ubuntu Scrolling Woes — Small Technology Notes
 * ![Gears](/blog/assets/work-technology-vintage-wheel-retro-clock-606288-pxhere.com.jpg "Fiddly technical things ahead")
 * The header image is [work, technology, vintage, wheel, retro, clock](https://pxhere.com/en/photo/606288), by an anonymous PxHere photographer, is made available under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Bem part 2
 * ![Sculpted head with a lizard crown](/blog/assets/3830119106_7351371907_o.png "Not the Tam Paupa")
 * The header image is [Lizards make an awesome hat](https://www.flickr.com/photos/quinnanya/3830119106/) by the [Quinn Dombrowski](https://www.flickr.com/photos/quinnanya/), made available under the terms of a [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) License.
 * title: Tweets from 01/10 to 01/14
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Dead Ascend
 * ![Screenshot of the game](/blog/assets/dead-ascend-screenshot.png "A storeroom with art on the walls?  Sure, let's assume that's what it is...")
 * The header image is a screenshot of **Dead Ascend**, stored in the repository, and so released under the same MIT license as the game.
 * title: Developer Journal, Martin Luther King, Jr. Day
 * ![Ronald Reagan signing the bill creating the national holiday, reluctantly, by many reports](/blog/assets/Reagan-signs-Martin-Luther-King-bill.png "I don't credit the actor with much good, but like they say, even stopped clocks get to be right occasionally.")
 * The header image is [Reagan signs Martin Luther King bill](https://commons.wikimedia.org/wiki/File:Reagan_signs_Martin_Luther_King_bill.jpg) by an unnamed White House photographer, in the public domain as a work of the United States government.
 * title: Real Life in Star Trek, The Practical Joker
 * ![Cress growing out of a keyboard](/blog/assets/265229897_38bf7bb9b5_o.png "A far more sophisticated prank than anything seen in the episode, sadly")
 * The header image is [Keyboard and Cress](https://www.flickr.com/photos/wetwebwork/265229897/) by [wetwebwork](https://www.flickr.com/photos/wetwebwork/), available under a [Creative Commons Attribution Share-Alike 2.0 Generic](http://creativecommons.org/licenses/by-sa/2.0/) License.
 * title: Tweets from 01/17 to 01/21
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — For the Fragile Muses...
 * ![For the Fragile Muses cover, a damaged piano with overlaid titles](/blog/assets/fragile-muses.png "Sometimes the ivories tickle back")
 * The header image is the [cover](https://www.deviantart.com/deejf/art/For-the-fragile-Muses-first-cover-650147531) of the e-book version of the story, by the author, released under the terms of the [Creative Commons Attribution 3.0 Unported](https://creativecommons.org/licenses/by/3.0/) license.
 * title: Developer Journal, International Day of Education
 * ![Reading lessons in Laus](/blog/assets/Khamla-Panyasouk-of-Big-Brother-Mouse-reads-to-children.png "It worries me that this is the first that I'm learning about outdoor reading festivals")
 * The header image is [Khamla Panyasouk of Big Brother Mouse reads to children](https://commons.wikimedia.org/wiki/File:Khamla_Panyasouk_of_Big_Brother_Mouse_reads_to_children.jpg) by [Blue Plover](https://commons.wikimedia.org/w/index.php?title=User:Blue_Plover&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Testing Jekyll Plugins — Small Technology Notes
 * ![Gears](/blog/assets/work-technology-vintage-wheel-retro-clock-606288-pxhere.com.jpg "Fiddly technical things ahead")
 * The header image is [work, technology, vintage, wheel, retro, clock](https://pxhere.com/en/photo/606288), by an anonymous PxHere photographer, is made available under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Albatross
 * ![An albatross's head](/blog/assets/Diomedea-epomophora-portrait-SE-Tasmania.png "Southern Royal Albatross giving McCoy the stink-eye")
 * The header image is [Diomedea epomophora portrait — SE Tasmania](https://commons.wikimedia.org/wiki/File:Diomedea_epomophora_portrait_-_SE_Tasmania.jpg) by [JJ Harrison](https://www.jjharrison.com.au/), available under a [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) License.
 * title: Tweets from 01/24 to 01/28
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Tex Montana Will Survive!
 * ![Tex on a rock, describing the project ahead](/blog/assets/tex-montana-narrating.png "Either Tex-splaining or Mon-splaining will be accepted.")
 * The header image is a frame extracted from the film, and so is under its license.
 * title: Developer Journal, Me-Dam-Me-Phi
 * ![A Me-Dam-Me-Phi service](/blog/assets/Mae-Dam-Mae-Phi.png "I know that it's unlikely, but I'm weirdly paranoid that the signs are either ethnic slurs and/or brand names...")
 * The header image is [Mae Dam Mae Phi](https://commons.wikimedia.org/wiki/File:Mae_Dam_Mae_Phi.jpg) by [Sairg](https://commons.wikimedia.org/wiki/User:Sairg), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, How Sharper Than a Serpent's Tooth
 * ![Feathered Serpent](/blog/assets/Quetzalcoatl-telleriano.png "Even serpent gods get hungry, sometimes...")
 * The header image is [Quetzalcoatl in the Codex Telleriano-Remensis](https://commons.wikimedia.org/wiki/File:Quetzalcoatl_telleriano.jpg), long in the public domain.
 * title: Tweets from 01/31 to 02/04
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Homem-Grilo
 * ![Homem-Grilo, waving to the audience](/blog/assets/homem-grilo-1-1.png "The bridge is famous enough that it's featured on Wikipedia's page for the real Osasco")
 * The header image is the first panel of the (current) first strip of the comic, introducing us to Homem-Grilo, and so under the same license as the rest of the work.
 * title: Developer Journal, Cripple Creek Miners' Strike
 * ![The town under martial law](/blog/assets/Cc_martiallaw.png "The modern world doesn't contain nearly enough of those small-wall-on-wheels gizmos...")
 * The header image is [Cripple Creek, Colo., under martial law, 1894](https://en.wikipedia.org/wiki/File:Cc_martiallaw.png) by Rastall, Benjamin McKie. University of Wisconsin, long in the public domain.
 * title: Real Life in Star Trek, The Counter-Clock Incident, pt 1
 * ![Firework Nova](/blog/assets/GSFC_20171208_Archive_e000693.png "Don't try this at home, folks...")
 * The header image is [Firework Nova](https://images.nasa.gov/details-GSFC_20171208_Archive_e000693) by NASA Goddard, in the public domain by NASA policy.
 * title: Tweets from 02/07 to 02/11
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Learn to Code RPG
 * ![College kids waiting for a hackathon](/blog/assets/ltc-rpg-college-kids.png "Every time I see the kid on the right, I think this has become a fantasy adventure and Techbro-Monk is going to give me an uncomfortable quest...")
 * The header image is a screenshot from the game, and so available under its license.
 * title: Developer Journal, Valentine's Day 💘
 * ![A painting of two women in dresses, one leaning on the other's shoulder, both staring at a flower in one's hands](/blog/assets/38_of_The_Quiver_of_Love.png "Would that yon bloom could be but a humble unmoored far-hearing device of some intelligence, we might use it to mark our relationship status as a thing less complicated on that venerable yet secular Book of Life.")
 * The header image is [38 of 'The Quiver of Love. A collection of Valentines ancient and modern. With illustrations, in colors, ... by W. Crane and Kate Greenaway](https://www.flickr.com/photos/britishlibrary/11112970976/), courtesy of the British Library, and long in the public domain.
 * title: Real Life in Star Trek, The Counter-Clock Incident, pt 2
 * ![A game of chess](/blog/assets/board-wood-white-game-play-recreation-602939-pxhere.com.png "It's hard not to get the sense that all non-human discourse in the Star Trek universe is about how humans are irritating and boring, but have you tried chess...?")
 * The header image is [untitled](https://pxhere.com/en/photo/602939) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 02/14 to 02/18
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Your Face Is a Saxophone
 * ![Blake pitching the YFIaS campaign](/blog/assets/your-face-is-a-saxophone-title.png "Do you look in the mirror often enough to prove him wrong?")
 * The header image is a screenshot from the game, and so available under its license.
 * title: The World's Slowest, Dumbest Botnet ⚠️
 * ![Three bristlebots](/blog/assets/2166476315_9b2f8cd945_o.png "Fluor-i-date!")
 * The header image is [Bristlebot version II](https://www.flickr.com/photos/21649179@N00/2166476315) by [fdecomite](https://www.flickr.com/photos/21649179@N00/2166476315), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Mother Language Day
 * ![Islamabad demonstration supporting the Punjabi language](/blog/assets/Mother-Tongue-Day-Islamabad.png "Without language, we would have no puns or wordplay")
 * The header image is [Mother Tongue Day, Islamabad](https://commons.wikimedia.org/wiki/File:Mother_Tongue_Day,_Islamabad.JPG) by [Khalid Mahmood](https://commons.wikimedia.org/wiki/User:Khalid_Mahmood), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, The Counter-Clock Incident, pt 3
 * ![Artist's conception of a rogue planet](/blog/assets/Alone-in-Space.png "Everything will probably not be coming up roses, here...")
 * The header image is [Alone in Space — Astronomers Find New Kind of Planet](https://www.nasa.gov/topics/universe/features/pia14093.html) by NASA/JPL-Caltech, in the public domain by NASA policy.
 * title: Tweets from 02/21 to 02/25
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Level 13
 * ![A typical Level 13 level map](/blog/assets/level-13-map.png "There's a lot of walking.  A lot.")
 * The header image is a screenshot from the game, and so available under its license.
 * title: Developer Journal, February 28 Incident
 * ![A woodcut representing the White Terror](/blog/assets/228-by-Li-Jun.png "")
 * The header image is [228 by Li Jun](https://commons.wikimedia.org/wiki/File:228_by_Li_Jun.jpg) by Huang Rong-can, in the public domain due to an expired copyright term.
 * title: Real Life in Star Trek, Animated Series Summary
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is [The planet, the galaxy and the laser](https://www.eso.org/public/images/eso0733a/) by the [ESO](https://www.eso.org) and [Yuri Beletsky](Yuri Beletsky), released under the terms of a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) license.
 * title: Tweets from 02/28 to 03/04
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Dudley's (New, Improved) Dungeon
 * ![The start of a comic](/blog/assets/mome-raths-outgrabe.png "Adventure...sort of.")
 * The header image is a (partial) screenshot of a comic, and so available under its license.
 * title: Announcing...Iungimoji
 * ![An Iungimoji game in progress](/blog/assets/iungimoji.png "Stage 2:  Force the player to write a story based on the emoji and the order they were revealed?")
 * The header image is a screenshot from the game.
 * title: Developer Journal, International Women's Eve
 * ![Rough painting of a 1910s German woman waving a large red flag](/blog/assets/Frauentag-1914-Heraus-mit-dem-Frauenwahlrecht.png "I've seen better rhythmic gymnastics routines...")
 * The header image is extracted from [Frauentag 1914 Heraus mit dem Frauenwahlrecht](https://commons.wikimedia.org/wiki/File:Frauentag_1914_Heraus_mit_dem_Frauenwahlrecht.jpg) by Karl Maria Stadler, in the public domain since 2014.
 * title: Real Life in Star Trek, The Motion Picture, pt 1
 * ![The cast with the Shuttle Enterprise](/blog/assets/The_Shuttle_Enterprise_-_GPN-2000-001363.png "This picture probably goes a long way towards explaining the uniforms...")
 * ![Minerva Terrace](/blog/assets/2881959846_8668bc2aab_o.png "Due to the sulfur, the stones are much yellower when the visual effects team isn't turning them red, hence Yellowstone.")
 * The header image is [The Shuttle Enterprise with Star Trek cast](https://www.flickr.com/photos/nasacommons/9467279838/in/album-72157634981463837/) by an unspecified NASA photographer, in the public domain as a work of NASA.  I normally avoid pictures of the cast as headers, but a picture of the full cast in front of an *Enterprise*, especially so close to when they would have been filming, was too appropriate to pass up.  The photograph of [Minerva Terrace](https://www.flickr.com/photos/67975030@N00/2881959846/), by [Bernt Rostad](https://www.flickr.com/photos/brostad/), has been made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Tweets from 03/07 to 03/11
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Dünnes Eis, part 1
 * ![The cover to Dünnes Eis](/blog/assets/duennes-eis-cover.png "'A long walk off a short pier' might be a decent description of the book...")
 * The header image is the book's cover, and so available under its license.
 * title: Announcing G.L.O.B.E.
 * ![A G.L.O.B.E. game in progress](/blog/assets/g-l-o-b-e.png "Gabon has a distinctive enough shape that it's easy to guess on the first try, but that makes for a bad demonstration...")
 * The header image is a screenshot from the game.
 * title: Developer Journal, White Day
 * ![A Japanese bride](/blog/assets/Bride-at-meiji-shrine.png "I kind of regret that the picture including this phenomenal white hat happens to be part of an entirely unrelated Japanese tradition; but it was her or a stoat, and the stoat didn't have that hat...")
 * The header image is [Bride at meiji shrine](https://www.flickr.com/photos/photocapy/44134137/) by [Photocapy](https://www.flickr.com/photos/photocapy/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Real Life in Star Trek, The Motion Picture, pt 2
 * ![The Voyager proof test model in the space simulator chamber, 1976](/blog/assets/PIA21735.png "Apparently every one of the probes has loose connections.")
 * ![An "album cover" poster for the Voyager mission's greatest hits](/blog/assets/voyager_disco_poster.png "I like to imagine that this poster advertises for the version of the film in the alternate universe where V'Ger replaces all life on Earth with robots")
 * The header image is [Voyager Test Model Configuration](https://voyager.jpl.nasa.gov/galleries/images-of-voyager/#gallery-17) by an unspecified NASA photographer.  The header for the adaptation is [The Voyagers Rock On](https://voyager.jpl.nasa.gov/downloads/#gallery-posters-4) by an unspecified NASA/JPL-Caltech artist (or artists).  Both are in the public domain as works of NASA.  Likewise, the [Outer planets mission of the successful pair of Voyager spacecraft](http://www.donaldedavis.com/PARTS/allyours.html) was painted by Don Davis, released into the public domain by NASA policy.
 * title: Tweets from 03/14 to 03/18
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Dünnes Eis, part 2
 * ![The cover to Dünnes Eis](/blog/assets/duennes-eis-cover.png "'A long walk off a short pier' might serve as a decent description of the book...")
 * The header image is the book's cover, and so available under its license.
 * title: Developer Journal, Elimination of Racial Discrimination
 * ![Islamabad demonstration supporting the Punjabi language](/blog/assets/Mother-Tongue-Day-Islamabad.png "Without language, we would have no puns or wordplay")
 * The header image is [Mother Tongue Day, Islamabad](https://commons.wikimedia.org/wiki/File:Mother_Tongue_Day,_Islamabad.JPG) by [Khalid Mahmood](https://commons.wikimedia.org/wiki/User:Khalid_Mahmood), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, The Wrath of Khan
 * ![The Little Gem Nebula](/blog/assets/little-gem.png "Not Mutara")
 * The header image is [Little Gem](https://images.nasa.gov/details-hubble-finds-a-little-gem_20185002499_o) by NASA/GSFC, released into the public domain by NASA policy.
 * title: Tweets from 03/21 to 03/25
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Josh Woodward
 * ![Woodward playing live](/blog/assets/josh-woodward-live.png "Not what I expected him to look like...")
 * The header image is a publicity still for Woodward, released under the same [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) license as the rest of his website.  Excerpt from [*The Folk Song Army*](https://tomlehrersongs.com/the-folk-song-army/) "should be treated as though they were in the public domain," as of the disclaimer on the main site.
 * title: Abandoned Concept — The Heralds of Justice 🎺
 * ![A Black woman with an open laptop, walking past a row of server racks](/blog/assets/25388931814_0e042c3bea_o.png "Basically every hacking scene in an action movie, right...?")
 * The header image is based on [wocintech (microsoft) — 189](https://www.flickr.com/photos/wocintechchat/25388931814/) by [WOCinTech Chat](https://www.flickr.com/photos/wocintechchat/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, World Theatre Day, belated
 * ![An Ancient Greek theater](/blog/assets/Taormina-BW2012-10-05-16-05-05.png "All the pit's a stage...or something like that")
 * The header image is [Taormina BW 2012-10-05 16-05-05](https://commons.wikimedia.org/wiki/File:Taormina_BW_2012-10-05_16-05-05.jpg) by [Berthold Werner](https://commons.wikimedia.org/wiki/User:Berthold_Werner), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, The Search for Spock
 * ![Artist's conception of Mars with exaggerated topography, rendered with multiple textures](/blog/assets/exaggerated-mars-preview-48.png "Not Genesis")
 * The header image is [Mars 3D model, exaggerated, multiple textures](https://opengameart.org/content/mars-3d-model-exaggerated-multiple-textures) by [Daniel Michel](https://opengameart.org/users/dizzy-crow), released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](http://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 03/28 to 04/01
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Fiction by David Turner
 * ![Woodward playing live](/blog/assets/12127974115_7c62484a2d_o.png "Not what I expected him to look like...")
 * The header image is adapted from [Jellyfish](https://www.flickr.com/photos/74542540@N00/12127974115) by [Amit Patel](https://www.flickr.com/photos/amitp/), released under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Tel Aviv's Anniversary
 * ![Panorama of Tel Aviv](/blog/assets/Tel-Aviv-Yafo.png "Yep, it's a city")
 * The header image is [Panorama of Tel Aviv (cropped)](https://commons.wikimedia.org/wiki/File:Panorama_of_Tel_Aviv_%28cropped%29.jpg) by [RaphaelQS](https://commons.wikimedia.org/w/index.php?title=User:RaphaelQS), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, The Voyage Home
 * ![A humpback whale breaching the surface](/blog/assets/humpback-whale-breaching.png "OK, OK, I'll call you back!")
 * The header image is [Humpback Whale Breaching](https://www.fisheries.noaa.gov/species/humpback-whale) by the NOAA Fisheries, in the public domain as a work of the United States government.  [Golden Gate Park aerial](https://commons.wikimedia.org/wiki/File:Golden_Gate_Park_aerial.jpg) by [Dick Lyon](https://en.wikipedia.org/wiki/User:Dicklyon) is available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.  The [humpback whale song](https://en.wikipedia.org/wiki/File:Humpbackwhale2.ogg), recorded by [Spyrogumas](https://commons.wikimedia.org/w/index.php?title=User:Spyrogumas&action=edit&redlink=1), is available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).
 * title: Tweets from 04/04 to 04/08
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — One Hour One Life
 * ![Screenshot from the game tutorial, with a fur-clad woman carrying a basket through a hall](/blog/assets/2-hours-one-life.png "Over the river and through the wood...except that I haven't seen a river")
 * The header image is adapted from [Jellyfish](https://www.flickr.com/photos/74542540@N00/12127974115) by [Amit Patel](https://www.flickr.com/photos/amitp/), released under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, First Paleontology Lecture
 * ![Reconstruction of an American mastodon](/blog/assets/Mammut-americanum.png "All the pit's a stage...or something like that")
 * The header image is [Mammut americanum Sergiodlarosa](https://commons.wikimedia.org/wiki/File:Mammut_americanum_Sergiodlarosa.jpg) by [Sergio De La Rosa](https://www.deviantart.com/serchio25), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, The Final Frontier
 * ![Michelangelo's Creation of Adam, but Adam holds out a spaceship](/blog/assets/Michelangelo-Creation-of-Adam-with-Spaceship.png "Gimme the spaceship, kid...")
 * I adapted the header image from [The Creation of Adam](https://en.wikipedia.org/wiki/The_Creation_of_Adam) by Michelangelo, long in the public domain, though I added [Futuristic Spaceship](https://www.flickr.com/photos/rivet1/14055064887/in/photostream/) by [Rivet](https://www.flickr.com/photos/rivet1/) available under a [Creative Commons Attribution 2.0 Generic](http://creativecommons.org/licenses/by/2.0/) License.  I don't consider my copying-and-pasting (and copying the thumb) to be transformative enough to add another license to the mix.
 * title: Tweets from 04/11 to 04/15
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Collectivity
 * ![The Bonaventure traveling through space](/blog/assets/collectivity-bonaventure.png "These are NOT the voyages of those other folks...")
 * The header image combines [Galaxy Packs Big Star-Making Punch](https://images.nasa.gov/details-PIA17005) by NASA/JPL-Caltech/STScI/IRAM and [ASC Bonaventure](https://opengameart.org/content/asc-bonaventure) by [ClaudeB](https://opengameart.org/users/claudeb) based on the story, the former in the public domain by NASA policy and the latter released under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license; I release the rendered, exported, and combined version under the latter license.
 * title: Developer Journal, World Heritage Day
 * ![The Great Wall of China](/blog/assets/Great-Wall-of-China.png "It feels like a grave historical injustice that Mongolia hasn't built The Awesome Ladder of the Eurasian Steppe as a tourist trap...")
 * ![A bubble sort flowchart](/blog/assets/assets/bubble_sort_mermaid.png "Don't use this sort in production")
 * The header image is [P1090119 Chiny](https://commons.wikimedia.org/wiki/File:P1090119_Chiny.JPG) by [Jsporysz](https://commons.wikimedia.org/w/index.php?title=User:Jsporysz), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, The Undiscovered Country
 * ![A horseshoe crab climbing over some rocks](/blog/assets/4035509350_5c1defa34b_o.png "To crab or not to crab, THAT is the question...")
 * The header image is [Horseshoe Crab](https://www.flickr.com/photos/usfwsnortheast/4035509350/) by the [U.S. Fish and Wildlife Service — Northeast Region](https://www.fws.gov/about/region/northeast), released into the public domain as a work of the United States government.  Horseshoe crabs probably inspired the modern look of Klingons and have a bluish blood---based on hemocyanin---which companies brutally harvest for applications in medical testing.
 * title: Tweets from 04/18 to 04/22
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Moose Lake Cartoons
 * ![A mugger explaining that his preferred term is "aggressive marketing"](/blog/assets/1427406268948-6H4R5KOQTGBJLXX207BC-eth6.png "Everybody loves muggings, but nobody wants to give any respect to the mugger, right?  OK, maybe not...")
 * ![Moose Lake's graduation ceremony](/blog/assets/moose-lake-graduation.png "Not inaccurate...")
 * The header image is one of the comics, as is the other comic.
 * title: Don't Feed the Invisible Authoritarians
 * ![A 2008 McCain/Palin rally](/blog/assets/3263755442_4babc75c9b_o.png "Those were the days...apparently.")
 * The header image is [mccainpalinrally](https://www.flickr.com/photos/34299679@N02/3263755442) by [Rachael Dickson](https://www.flickr.com/photos/34299679@N02/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, DNA Day 🧬
 * ![A truncated model of a DNA molecule](/blog/assets/DNA-model.png "🧬")
 * The header image is [DNA Furchen](https://commons.wikimedia.org/wiki/File:DNA_Furchen.png) by [Yikrazuul](https://commons.wikimedia.org/wiki/User:Yikrazuul), released into the public domain by the creator.
 * title: The Care and Feeding of Plain Text
 * ![Movable type](/blog/assets/wood-texture-number-symbol-alphabet-typography-672822-pxhere.com.png "Try not to organize your notes as depicted.")
 * ![The rendered bubble sort flowchart](/blog/assets/sort.mermaid.svg "Yes, there are better ways to sort, but few are this easy to describe.")
 * The header image is [untitled](https://pxhere.com/en/photo/672822) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Generations
 * ![Ukrainian 1992 ₴5 stamp, featuring a rhythmic gymnast with a ribbon](/blog/assets/ukraine-1992-stamp.png "Not the Nexus ribbon")
 * ![Dom Pérignon, 1928](/blog/assets/dom-perignon-1928.png "The label hasn't changed much")
 * The header image is [Stamp of Ukraine s25](https://commons.wikimedia.org/wiki/File:Stamp_of_Ukraine_s25.jpg) by the Post of Ukraine, in the public domain as a state symbol of the nation.  [Dom Perignon champagne 1928](https://www.flickr.com/photos/28585409@N04/11717863683) by [Sam Beebe](https://www.flickr.com/photos/sbeebe/) has been made available under the terms of a [Creative Commons Attribution 4.0 Generic](http://creativecommons.org/licenses/by/2.0/) License.
 * title: Tweets from 04/25 to 04/29
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Lunatics
 * !["The (animated) main cast standing in front of windows overlooking Earthrise on the Moon"](/blog/assets/lunatics-cover.png "Those of the inappropriate collective name...")
 * The header image is part of the art book's title, and so available under its license.
 * title: Twitter Troubles, or the 280-Character Blues
 * ![A bluebird with the disembodied head of Nikola Tesla poking in from above](/blog/assets/Mountain-Bluebird.png "Good blue-sir, might you originate from Venus?")
 * I adapted the header image---by adding a public domain head of Nikola Tesla---from [Mountain Bluebird](https://www.naturespicsonline.com/galleries/15#30) by Elaine R. Wilson, made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](http://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Developer Journal, International Workers' Day, belated
 * ![A Labor Day celebration in Beijing, 1952](/blog/assets/1952-05-1952年5月五一劳动节.png "Definitely a pre-COVID celebration style...")
 * The header image is [1952-05 1952年5月五一劳动节](https://commons.wikimedia.org/wiki/File:1952-05_1952%E5%B9%B45%E6%9C%88%E4%BA%94%E4%B8%80%E5%8A%B3%E5%8A%A8%E8%8A%82.png) by 《人民画报》(**The People's Pictorial**), in the public domain, since its copyright expired.  The growling sound effect is [Animal Dog Growl 01](https://freesound.org/people/abhisheky948/sounds/625500/) by [abhisheky948](https://freesound.org/people/abhisheky948/), released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](http://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Movies Summary
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is [The planet, the galaxy and the laser](https://www.eso.org/public/images/eso0733a/) by the [ESO](https://www.eso.org) and [Yuri Beletsky](Yuri Beletsky), released under the terms of a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) license.
 * title: Tweets from 05/02 to 05/06
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club --- Virtual Danger, part 1
 * ![The cover for Virtual Danger, showing a Grim Reaper silhouette in front of a "digital" spiral](/blog/assets/virtual-danger-cover.png "Having seen these tropes a million times, I'm going to try not to judge the book by this...")
 * The header image is the book's cover, released under the same license.
 * title: Supporting the Right to Choose
 * ![Abstract silhouettes of women against a sunrise or sunset](/blog/assets/nature-outdoor-horizon-silhouette-light-people-1096098-pxhere.com.png "Look, any header image that I could think of that might have some relevance to the post seemed like it would traumatize half my readers, so...")
 * The header image is [Untitled](https://pxhere.com/en/photo/1096098) by an uncredited PxHere photographer, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Europe Day
 * ![Signing the Treaty of Lisbon, under a large EU flag](/blog/assets/Tratado-de-Lisboa-13-12-2007-04.png "I appreciate that the people whose faces we can see look like they want to ignore the giant flag lurking behind them...")
 * The header image is [Tratado de Lisboa 13 12 2007 (04)](https://commons.wikimedia.org/wiki/File:Tratado_de_Lisboa_13_12_2007_%2804%29.jpg) by [Archiwum Kancelarii Prezydenta RP](https://www.prezydent.pl/), made available under the terms of the [GNU Free Documentation v1.2](https://www.gnu.org/licenses/old-licenses/fdl-1.2.html) license, which I take to be (roughly) compatible with the blog, but will change the image if someone provides a reasonable argument to the contrary.
 * title: Dragging and Dropping on the Web
 * ![A clown dragging a box through a city crosswalk](/blog/assets/4272263928_23ff17c092_o.png "Sorry about the clown, but it turns out that pictures of people dragging things make up a TINY minority of the pictures that turn up when you search for the word 'drag'...")
 * The header image is [1.12.10](https://www.flickr.com/photos/aprilzosia/4272263928/) by [aprilzosia](https://www.flickr.com/photos/aprilzosia/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Real Life in Star Trek, Final Summary
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is [The planet, the galaxy and the laser](https://www.eso.org/public/images/eso0733a/) by the [ESO](https://www.eso.org) and [Yuri Beletsky](Yuri Beletsky), released under the terms of a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) license.
 * title: Tweets from 05/09 to 05/13
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Virtual Danger, part 2
 * ![The cover for Virtual Danger, showing a Grim Reaper silhouette in front of a "digital" spiral](/blog/assets/virtual-danger-cover.png "Apparently, we absolutely judge this book by its cover...")
 * The header image is the book's cover, released under the same license.
 * title: Close Reading of Roe v Wade
 * ![A cat on a fence, looking at something intently out of frame](/blog/assets/50524187521_33d854425c_o.png "OK, now hold up the Ninth Amendment, again.  OK, and why did he allow people to call him Whizzer...?")
 * The header image is [Judging the height to jump up](https://www.flickr.com/photos/68166820@N08/50524187521) (with some color enhancement) by [Catherine Poh Huay Tan](https://www.flickr.com/photos/68166820@N08/), made available under the terms of the [Creative Common Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, The Nickel
 * ![The obverse face of the first nickel coin](/blog/assets/Shield-nickel-obverse-by-Howard-Spindel.png "Don't try to use a nickel as an actual shield; it's too small.")
 * The header image is [Shield nickel obverse by Howard Spindel](https://commons.wikimedia.org/wiki/File:Shield_nickel_obverse_by_Howard_Spindel.png) by Howard Spindel, made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Encounter at Farpoint, part 1
 * ![A market in Adalaj, Gujarat, India](/blog/assets/Market-in-Adalaj-01.png "A different open-air market")
 * The header image is [Market in Adalaj 01](https://commons.wikimedia.org/wiki/File:Market_in_Adalaj_01.jpg) by [Bernard Gagnon](https://commons.wikimedia.org/wiki/User:Bgag), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Tweets from 05/16 to 05/20
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Virtual Danger, part 3
 * ![The cover for Virtual Danger, showing a Grim Reaper silhouette in front of a "digital" spiral](/blog/assets/virtual-danger-cover.png "We're still here, I guess...")
 * The header image is the book's cover, released under the same license.
 * title: Developer Journal, New York Public Library
 * ![The front entrance of the NYPL's Main Branch](/blog/assets/51396225599_18974b261a_c.png "I wanted a picture with the lions, forgetting how small and far apart they sit, relative to the space...")
 * The header image is [New York Public Library- Main Branch](https://www.flickr.com/photos/ajay_suresh/51396225599/) by [Ajay Suresh](https://www.flickr.com/photos/ajay_suresh/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Encounter at Farpoint, part 2
 * ![Two jellyfish (recolored)](/blog/assets/44791540314_623f06b9f6_o.png "You are not to use either of these poor fish to build your house; only hire licensed contractors...")
 * I adapted the header image from [Jellyfish, Monterey Aquarium, California](https://www.flickr.com/photos/pedrosz/44791540314) by [Pedro Szekely](https://www.flickr.com/photos/pedrosz/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Tweets from 05/23 to 05/27
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Virtual Danger, part 4
 * ![The cover for Virtual Danger, showing a Grim Reaper silhouette in front of a "digital" spiral](/blog/assets/virtual-danger-cover.png "Having seen these tropes a million times, I'm going to try not to judge the book by this...")
 * The header image is the book's cover, released under the same license.
 * title: Developer Journal, Grace Andrews
 * ![Mathematicians working out problems on a glass board, as we see them through the board](/blog/assets/Culture-glasses-jesting-stained-glass-Fortepan-15581.png "Annoyingly, I couldn't find a picture of Andrews, so random mathematicians in black-and-white will have to suffice for now...")
 * ![A bowl of toasted oats, walnuts, banana, blackberries, and cottage cheese](/blog/assets/banana-toasted-oats.png "Look, if I wanted to do food photography at any level, I would have spent much less of my life writing code...")
 * The header image is [Culture, glasses, jesting, stained glass Fortepan 15581](https://commons.wikimedia.org/wiki/File:Culture,_glasses,_jesting,_stained_glass_Fortepan_15581.jpg) by [Fortepan](https://fortepan.hu/), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, The Naked Now
 * ![A table at a party with paste-on facial hair and novelty glasses](/blog/assets/8862531175_1d8781fd46_o.png "Probably a better party than the Tsiolkovsky's...")
 * The header image is [party](https://www.flickr.com/photos/gabianow/8862531175) by [gabla party](https://www.flickr.com/photos/gabianow/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Tweets from 05/30 to 06/03
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Counterfeit Monkey
 * ![The “box art” for Counterfeit Monkey](/blog/assets/counterfeit-monkey.png "I assume that a monkey is somewhere in there...")
 * The header image is the game's "box cover," released under the same license.
 * title: Developer Journal, D-Day
 * ![Photograph of US troops storming the beach](/blog/assets/Into-the-Jaws-of-Death.png "As the quote goes, there is no beach in Omaha...")
 * The header image is [Into the Jaws of Death](https://commons.wikimedia.org/wiki/File:WWII,_Europe,_France,_%22Into_the_Jaws_of_Death_-_U.S._Troops_wading_through_water_and_Nazi_gunfire%22_-_NARA_-_195515.tif) by Robert F. Sargent, U.S. Coast Guard, in the public domain as a work of the United States Armed Forces.
 * title: Real Life in Star Trek, Code Of Honor
 * ![Two kittens fighting in the grass](/blog/assets/play-kitten-cat-mammal-playful-fight-820025-pxhere.com.png "Let's not pretend that the writers thought of this episode in any other way...")
 * The header image is [Untitled](https://pxhere.com/en/photo/820025) by an uncredited PxHere photographer, in the public domain as a state symbol of the nation.  Released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 06/06 to 06/10
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Captain Quark, part 1
 * ![The cover for the book](/blog/assets/51D7F53YBIL.png "Beam up as many puns as we can, I guess...")
 * The header image is the book's cover, presumably released under the same license.
 * title: Developer Journal, James Clerk Maxwell
 * ![Photograph of Maxwell, sitting](/blog/assets/James-Clerk-Maxwell-sitting.png "Totally casual photograph of a man looking straight ahead with dead eyes while he pretends to write something.")
 * The header image is [James Clerk Maxwell sitting](https://commons.wikimedia.org/wiki/File:James_Clerk_Maxwell_sitting.jpg) by an unknown photographer, old enough to have surely lapsed into the public domain by any metric.
 * title: Real Life in Star Trek, The Last Outpost
 * ![The precursor the finger-trap, the "girl-catcher"](/blog/assets/Mädchenfänger.png "I do have to wonder what one tells their prospective victim to entice them to ram a finger down the gullet of this trap, but not hard enough to dislodge the hook...")
 * The header image is [Mädchenfänger](https://commons.wikimedia.org/wiki/File:M%C3%A4dchenf%C3%A4nger.png) by [Redlinux](https://commons.wikimedia.org/wiki/User:Redlinux), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by/3.0/deed.en) license.
 * title: Tweets from 06/13 to 06/17
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Captain Quark, part 2
 * ![The cover for the book](/blog/assets/51D7F53YBIL.png "Beam up as many puns as we can, I guess...")
 * The header image is the book's cover, presumably released under the same license.
 * title: Developer Journal, World Refugee Day
 * ![World Refugee Day poster](/blog/assets/WRDMultiling.png "It always worries me when I flat out can't even identify a language.")
 * The header image is adapted from [WRDMultiling](https://commons.wikimedia.org/wiki/File:WRDMultiling.png) by [MisterMashtots](https://commons.wikimedia.org/w/index.php?title=User:MisterMashtots&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Where No One Has Gone Before
 * ![A dolphin flying through a rainbow background with stars](/blog/assets/New-age-dolphin-rainbow.png "It only seems reasonable to assume that the Traveler has a blowhole.")
 * The header image is [New age dolphin rainbow](https://commons.wikimedia.org/wiki/File:New_age_dolphin_rainbow.svg) by [David Gerard](https://commons.wikimedia.org/wiki/User:David_Gerard), released into the public domain.
 * title: Tweets from 06/20 to 06/24
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Captain Quark, part 3
 * ![The cover for the book](/blog/assets/51D7F53YBIL.png "Beam up as many puns as we can, I guess...")
 * The header image is the book's cover, presumably released under the same license.
 * title: The Powder Keg
 * ![The U.S. Supreme Court Building](/blog/assets/7W9A9324_50138445142.png "I like that the statue on our left plays with action figures")
 * The header image is [7W9A9324 (50138445142)](https://commons.wikimedia.org/wiki/File:7W9A9324_%2850138445142%29.jpg) by [Senate Democrats](https://www.flickr.com/people/32619231@N02), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/deed.en) license.
 * title: Developer Journal, National PTSD Awareness Day
 * ![Regions of the brain affected by PTSD and stress](/blog/assets/PTSD-stress-brain.png "I imagine that this would feel highly traumatic if the depicted brain belonged to one of us...")
 * The header image is [PTSD stress brain](https://commons.wikimedia.org/wiki/File:PTSD_stress_brain.gif) by National Institutes of Health, in the public domain as the work of the United States government.
 * title: Real Life in Star Trek, Lonely Among Us
 * ![A nebula, NGC 6153, tinted blue](/blog/assets/GSFC-20171208-Archive-e000699.png "Nitrogen-Rich Nebula")
 * The header image is [Hubble View of a Nitrogen-Rich Nebula](https://images.nasa.gov/details-GSFC_20171208_Archive_e000699) by NASA Goddard, in the public domain by NASA policy.
 * title: Tweets from 06/27 to 07/01
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Captain Quark, part 4
 * ![The cover for the book](/blog/assets/51D7F53YBIL.png "Beam up as many puns as we can, I guess...")
 * The header image is the book's cover, presumably released under the same license.
 * title: A Heritage-Based Reading Schedule
 * ![A tangle of bookshelves surrounding a door](/blog/assets/wood-city-entrance-color-italy-furniture-496032-pxhere.com.png "Sadly, a part of me sees this and feels confident that shelves like this would dramatically improve on my storage system...")
 * The header image is adapted from [untitled](https://pxhere.com/en/photo/496032) by an uncredited PxHere photographer, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Independence Day
 * ![Children of various ethnicities from around the United States and then-territories celebrate Independence Day outside the Capitol, next to Uncle Sam and Columbia](/blog/assets/American-1902-Fourth-of-July-fireworks.png "I don't know if the Filipino kid mortifies me or impresses me.")
 * The header image is [A False Alarm on the Fourth](https://www.loc.gov/item/2010652033/) by Udo J. Keppler, long in the public domain.
 * title: Real Life in Star Trek, Justice
 * ![Hot springs at Aachen, Germany](/blog/assets/Aachen-Kaiserbad-1682.png "You don't even want to know the punishment if you have imperfect table manners at the floating wine bar...")
 * The header image is adapted from [Hot springs at Aachen, Germany](https://commons.wikimedia.org/wiki/File:Aachen_Kaiserbad_1682.jpg) by Jan Luyken or Cuyken, long in the public domain.
 * title: Tweets from 07/04 to 07/08
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Game Grab Bag
 * ![A burlap sack of Free Culture games](/blog/assets/8359243304_fb28cea9d3_o.png "Of course, none of the pictured games get a mention in this post, but a hearty congratulations to anyone who can name each of them WITHOUT looking up the games that we've previously discussed or checking the post's credits.")
 * The header image adapts [Olives in a sack](https://www.flickr.com/photos/katsommers/8359243304) by [Kat Sommers](https://www.flickr.com/photos/katsommers/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license, plus portions of the header images for [**Counterfeit Monkey**]({% post_url 2022-06-04-monkey %}), [**One Hour One Life**]({% post_url 2022-04-09-ohol %}), [**Learn to Code RPG**]({% post_url 2022-02-12-code-rpg %}), [**Dead Ascend**]({% post_url 2022-01-15-deadascend %}), [**Cataclysm:  Dark Days Ahead**]({% post_url 2021-09-04-cdda %}), [**The House**]({% post_url 2021-06-05-house %}), [**Nothing to Hide**]({% post_url 2020-11-07-hide %}), and [**Forgotten**]({% post_url 2020-07-04-forgotten %}).
 * title: The AllStore — Hypothetical E-Commerce
 * ![A generic warehouse with a robot carrying a package](/blog/assets/storage-shelf-shelving-publication-book-orange-1638666-pxhere.com.png "I hope that this space doesn't have any distinguishing features that I just don't have any awareness of; I modified the signal light colors, just in case they represent some company's trademark...")
 * The header image is based on [untitled](https://pxhere.com/en/photo/1638666) by [schilderenopnummer](https://pxhere.com/en/photographer/3531756), released under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, World Population Day
 * ![A large crowd seated at a theater, photographed from behind](/blog/assets/music-black-and-white-people-crowd-concert-audience-874486-pxhere.com.png "Notice how nobody complains when the population density comes from people paying top-dollar for cramped seats.")
 * ![A Mystic T-Square game with no winner](/blog/assets/mystic-t-square-demo.png "The only way to win is not to play?  No, wait, that makes a decent tagline for a movie, but a TERRIBLE tagline for a game.")
 * The header image is [untitled](https://pxhere.com/en/photo/874486) by an uncredited PxHere photographer, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, The Battle
 * ![A 1960s design for a ship to take a crew of three to Mars, resembling a simple kite with rockets attached at arbitrary points](/blog/assets/9902036~orig.png "Definitely not the Stargazer")
 * The header image is adapted from [Early Program Development — Martian Electric Spaceship](https://images.nasa.gov/details-9902036) by the [Marshall Space Flight Center](https://www.nasa.gov/centers/marshall/home/index.html), in the public domain by NASA policy.
 * title: Tweets from 07/11 to 07/15
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Banjo Players/Die, part 1
 * ![The cover for the book](/blog/assets/banjo-players-die.png "Beam up as many puns as we can, I guess...")
 * The header image is the book's cover, presumably released under the same license.
 * title: Developer Journal, Mandela Day
 * ![Nelson Mandela in Johannesburg, Gauteng, on 13 May 2008](/blog/assets/Nelson-Mandela-full-cropped.png "Tie-less mustache guy probably should have stayed home.")
 * The header image is [Nelson Mandela](https://www.flickr.com/photos/sagoodnews/3199012558/) by [South Africa The Good News](https://www.flickr.com/photos/sagoodnews/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Hide and Q
 * ![An amateur magician appearing to pull a series of playing cards out of his mouth](/blog/assets/finger-hat-clothing-outerwear-cards-gentleman-869956-pxhere.com.png "Can I interest you in your shallowest desire?")
 * The header image is [untitled](https://pxhere.com/en/photo/869956) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 07/18 to 07/22
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Banjo Players/Die, part 2
 * ![The cover for the book](/blog/assets/banjo-players-die.png "Beam up as many puns as we can, I guess...")
 * The header image is the book's cover, presumably released under the same license.
 * title: Dying Is Easy; Redemption Is Hard
 * ![A kitten in a tree, peering out from behind the trunk](/blog/assets/tree-branch-sweet-flower-animal-summer-498814-pxhere.com.png "All shall tremble and bow before the might of...Purrofessor Kitty von Mouser, Devourer of Kibble!  Muah-ha-ha-ha...")
 * title: Developer Journal, Founding of Caracas
 * ![The Caracas downtown skyline](/blog/assets/Edificios-de-Oficinas-Conjunto-Parque-Central.png "Very shiny.")
 * The header image is [Edificios de Oficinas Conjunto Parque Central](https://commons.wikimedia.org/wiki/File:Edificios_de_Oficinas_Conjunto_Parque_Central_-.jpg) by [Esclasans](https://commons.wikimedia.org/w/index.php?title=User:Esclasans&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Haven
 * ![A rocket launching from a red-tinted planet while a rover looks on](/blog/assets/PIA07187~orig.png "Probably not Tarellians.")
 * The header image is [Illustration of Launching Samples Home from Mars](https://images.nasa.gov/details-PIA07187) by [NASA/JPL](https://www.jpl.nasa.gov/), in the public domain by NASA policy, the choice inspired by the LibriVox cover for [**Plague Ship**](https://librivox.org/plague-ship-by-andre-norton/).
 * title: Tweets from 07/25 to 07/29
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Banjo Players/Die, part 3
 * ![The cover for the book](/blog/assets/banjo-players-die.png "Beam up as many puns as we can, I guess...")
 * The header image is the book's cover, presumably released under the same license.
 * title: Developer Journal, Lughnasadh
 * ![A Lughnasadh festival, maybe 1904](/blog/assets/Puck-Fair-Killorglin-Co-Kerry.png "I, for one, welcome our new goat overlords.")
 * The header image is [Puck Fair, Killorglin, Co. Kerry](https://www.flickr.com/photos/47290943@N03/5497503269) by an unknown photographer, long in the public domain.
 * title: Real Life in Star Trek, The Big Goodbye
 * ![A stylized man in a red scarf and black slouch hat cradles a distraught woman](/blog/assets/black-mask-1923-10-01.png "Not Dixon Hill...probably.")
 * The header image is extracted from the cover of the 1923 October 1 issue of **Black Mask** magazine, painted by John Decker, featuring the first *Continental Op* story by Dashiell Hammett, in the public domain since its copyright expired in 2019.
 * title: Tweets from 08/01 to 08/05
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Banjo Players/Die, part 4
 * ![The cover for the book](/blog/assets/banjo-players-die.png "Beam up as many puns as we can, I guess...")
 * The header image is the book's cover, presumably released under the same license.
 * title: Developer Journal, Indigenous Peoples Day
 * ![Naga people engaging in their Hornbill Festival](/blog/assets/Hornbill-Festival.png "A hornbill festival")
 * The header image is [Hornbill Festival](https://commons.wikimedia.org/wiki/File:Hornbill_Festival.jpg) by [Dhrubazaanphotography](https://commons.wikimedia.org/w/index.php?title=User:Dhrubazaanphotography&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Datalore
 * ![A magnified snowflake](/blog/assets/Snowflake-Detail.png "I started to write a joke about how dangerous this would seem if we lived on a tiny, tiny planet, but that just seems obvious...")
 * The header image is [Snowflake Detail](https://commons.wikimedia.org/wiki/File:Snowflake_Detail.jpg) by [Charles Schmitt](https://commons.wikimedia.org/w/index.php?title=User:RShepHorse&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Tweets from 08/08 to 08/12
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Tom Lehrer
 * ![Lehrer playing the piano c1960](/blog/assets/Tom-Lehrer-Southern-Campus-1960.png "Not Victor Borge, not Mark Russell, and not whatever other piano-playing comedian who came to mind...")
 * The header image is [Tom Lehrer --- Southern Campus 1960](https://commons.wikimedia.org/wiki/File:Tom_Lehrer_-_Southern_Campus_1960.jpg) by Associated Students, University of California, Los Angeles, in the public domain due to a lack of copyright renewal.
 * title: Returning to the Fold
 * ![A file cabinet, approved by the GSA as Class 6 for storing classified documents, on its side](/blog/assets/GSAclass6SecurityContainer.png "Not found at Mar-A-Lago.  Unrelated, of all of these that I've seen, it never occurred to me that they have those same two stickers on them.")
 * The header image is [GSA Class 6 Security Container](https://en.wikipedia.org/wiki/File:GSAclass6SecurityContainer.jpg) by an uncredited U.S. Navy photographer, in the public domain as a work of the federal government.
 * title: Developer Journal, Tivoli Gardens
 * ![Tivoli Gardens](/blog/assets/Tivoli-Gardens-4.png "Where do they hide the people in giant mascot costumes, though?")
 * The header image is [Tivoli Gardens 4](https://audiovisual.ec.europa.eu/en/reportage/P-051259) by a European Commission photographer, released into the public domain by [European Commission law](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32011D0833).
 * title: Simple Build Scripts
 * ![The original version of Zork](/blog/assets/zork-original.png "You might fall into bottomless pits, here; no grues will get belly-aches from eating you.")
 * The header image is a screenshot of a session of the [original (mainframe) version of Zork](https://github.com/MITDDC/zork), released under the [MIT No Attribution](https://github.com/MITDDC/zork/blob/master/LICENSE.md) license.
 * title: Real Life in Star Trek, Angel One
 * ![Barricade at the university on May 26, 1848, in Vienna](/blog/assets/Barricade-bei-der-Universität-am-26ten-Mai-1848-in-Wien.png "Amateur productions of Les Mis are the worst.  They set them in the wrong revolution and in the wrong country.  And they staged this one 140 years before they even wrote the thing and twenty years before Hugo published the novel.  Tsk, tsk...")
 * The header image is [Barricade bei der Universität am 26ten Mai 1848 in Wien](https://repository.library.brown.edu/studio/item/bdr:225996/) by Franz Werner, long in the public domain.
 * title: Tweets from 08/15 to 08/19
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Starborn
 * ![The cover for the game](/blog/assets/starborn-cover.png "This cover would have flopped on the shelves, but for modern interactive fiction, it seems decent enough.")
 * The header image is the game's cover, released under the same license.
 * title: Public Records, Privacy, People-Search Antics, One Year Later
 * ![A photograph of a man taking a picture of a woman taking a picture of something](/blog/assets/computer-work-light-abstract-people-woman-819322-pxhere.com.png "Spoiler:  The real stalker rides overhead in his invisible airship.")
 * The header image is [untitled](https://pxhere.com/en/photo/819322) by an uncredited PxHere photographer, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Haitian Revolution
 * ![The Battle of Crête-à-Pierrot](/blog/assets/Haitian-Revolution.png "I like how, opposite of the repulsive caricatures of Black people in media, this painting portrays white people with sickly skin, tiny mouths, and identical mustaches.")
 * The header image is [Combat et prise de la Crête-à-Pierrot](https://commons.wikimedia.org/wiki/File:Haitian_Revolution.jpg) by [Auguste Raffet](https://en.wikipedia.org/wiki/Auguste_Raffet), long in the public domain.
 * title: Real Life in Star Trek, 11001001
 * ![An artist's conception of a supernova](/blog/assets/Supernova_CGI.png "Have you tried turning it off and on again?")
 * The header image is [Supernova (CGI)](https://commons.wikimedia.org/wiki/File:Supernova_%28CGI%29.jpg) by MTV International (apparently), made available under the terms of the [Creative Commons Attribution 3.0 Unported](https://creativecommons.org/licenses/by/3.0/deed.en) license.
 * title: Tweets from 08/22 to 08/26
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Ke'Aun Charles
 * ![One poem's header image](/blog/assets/0WmneJYGlPpmVgWNBfQRJA.png "I've listened to men talk about their lives in less comfortable spots than that, in all fairness...")
 * The header image comes from [Sale](https://www.flickr.com/photos/pagedooley/6575053747) by [Kevin Dooley](https://www.flickr.com/photos/pagedooley/), with text from *priceless wisdom for $40*, made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Day against Nuclear Tests
 * ![A nuclear test crater in Semipalatinsk](/blog/assets/CTBTO-Crater.png "Over the years, nothing has surprised me more than the number of tourist attractions that look suspiciously like this, without any justification.")
 * The header image is [Crater](https://www.flickr.com/photos/ctbto/3861359465/) by [The Official CTBTO Photostream](https://www.flickr.com/photos/ctbto/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Too Short A Season
 * ![The Return of Persephone](/blog/assets/DP-21913-001.png "Would it have KILLED you to bring back some pomegranate?  Too soon...?")
 * The header image is [The Return of Persephone](https://www.metmuseum.org/art/collection/search/847023) by Lord Leighton Frederic, long in the public domain.
 * title: Tweets from 08/29 to 09/02
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Quand manigancent les haricots
 * ![Beans spilling down an incline](/blog/assets/86406496_56cd763de7_o.png "Those beans are revolting...")
 * The header image is [Haricot beans](https://www.flickr.com/photos/wordridden/86406496/) by [Jessica Spengler](https://www.flickr.com/photos/wordridden/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: The Social Media Site of Record
 * ![A pixelated screenshot of a Twitter-like screen, using some of my own tweets](/blog/assets/twitter-mock-2022-09.png "Blurred to protect the timeliness of the post, not anybody's identity...")
 * The header image is a quick mock-up of part of my Twitter account, by me, which I hereby release under the blog's license.  The [puppies](https://pxhere.com/en/photo/1080738) come from an untitled, uncredited photograph from PxHere, released under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, International Day of Charity
 * ![Sandstone vestige of a Jewish gravestone depicting a Tzedakah box](/blog/assets/Jewish-cemetery-Otwock-Karczew-Anielin-IMGP6721.png "The spell-checker will have an absolute field day with the Hebrew vocabulary, and I do not look forward to seeing what mildly offensive conclusions that it draws.")
 * The header image is [Jewish cemetery in Otwock (Karczew-Anielin)](https://commons.wikimedia.org/wiki/File:Jewish_cemetery_Otwock_Karczew_Anielin_IMGP6721.jpg) by [Nikodem Nijaki](https://commons.wikimedia.org/wiki/User:Nikodem_Nijaki), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, When The Bough Breaks
 * ![Artist's conception of Atlantis](/blog/assets/Grie-Lost-City-of-Atlantis.png "I couldn't find a polite image to suggest kidnappings, so have some Atlantean ruins.")
 * The header image is [Mystery Legend Atlántida](https://commons.wikimedia.org/wiki/File:Lost_City_of_Atlantis.jpg) by [George Grie](https://www.wikidata.org/wiki/Q4160484), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Tweets from 09/05 to 09/09
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Quand manigancent les haricots pt 2
 * ![Beans spilling down an incline](/blog/assets/86406496_56cd763de7_o.png "Those beans are revolting...")
 * The header image is [Haricot beans](https://www.flickr.com/photos/wordridden/86406496/) by [Jessica Spengler](https://www.flickr.com/photos/wordridden/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, María de Zayas
 * ![A sketch of María de Zayas from del Siglo de Oro](/blog/assets/MariadeZayas.png "Anybody who riffed on Cervantes qualifies as a friend in my book...")
 * The header image is [María de Zayas y Sotomayor](https://commons.wikimedia.org/wiki/File:MariadeZayas.jpg), long in the public domain.
 * title: Real Life in Star Trek, Home Soil
 * ![An artist's impression of a terraformed Mars centered over Valles Marineris](/blog/assets/TerraformedMarsGlobeRealistic.png "In today's Silicon Valley culture, Ugly Bags of Mostly Water would probably secure even more investment money than Juicero did...")
 * The header image is [Terraformed Mars Globe Realistic](https://commons.wikimedia.org/wiki/File:TerraformedMarsGlobeRealistic.jpg) by [Daein Ballard](https://commons.wikimedia.org/w/index.php?title=User:Ittiz), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Tweets from 09/12 to 09/16
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Quand manigancent les haricots pt 3
 * ![Beans spilling down an incline](/blog/assets/86406496_56cd763de7_o.png "Those beans are revolting...")
 * The header image is [Haricot beans](https://www.flickr.com/photos/wordridden/86406496/) by [Jessica Spengler](https://www.flickr.com/photos/wordridden/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Washington's Farewell Address
 * ![Washington's prayer at Valley Forge](/blog/assets/Valley-Forge-prayer.png "...and I would love another tweed scarf like this one, and what do YOU want, kid-in-the-hat?")
 * The header image is [An engraving of George Washington praying at Valley Forge](https://commons.wikimedia.org/wiki/File:Valley_Forge_prayer.jpg) by John C. McRae, 1866, based on a painting by Henry Brueckner, long in the public domain.
 * title: Real Life in Star Trek, Coming Of Age
 * ![People in a classroom-like setup, with some intent on some activity, and others likely completed](/blog/assets/etug-storybox-seminar-205002-pxhere.com.png "Hey, everyone, look under the tables.  When we move, they move...")
 * The header image is based on [untitled](https://pxhere.com/en/photo/205002) by Alan Levine, made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Tweets from 09/19 to 09/23
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Bulletproof Blues, part 1
 * ![The third edition cover, prominently featuring Manticore, Vulcan, and Widow in Manhattan](/blog/assets/bulletproof-blues-3e.png "It definitely doesn't hide its inspirations...")
 * The header image is the game's book cover, under the same license as the game.
 * title: Developer Journal, European Day of Languages
 * ![Place des Abbesse, tourists examining a plaque with "I love you" written in 311 languages](/blog/assets/je-t-aime-311-laguages.png "Love is love, then disorder, it says.")
 * The header image is [Place des Abbesse (the plaque with the je t'aime=te iubesc in 311 laguages)](https://commons.wikimedia.org/wiki/File:Place_des_Abbesse_(the_plaque_with_the_je_t%27aime%3Dte_iubesc_in_311_laguages).jpg) by [Britchi Mirela](https://commons.wikimedia.org/w/index.php?title=User:Britchi_Mirela&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Heart of Glory
 * ![Tea and a crumpet](/blog/assets/6283771859_ac3f742e3d_o.png "Yes, what stuck with me is that the Klingons order something that sounds suspiciously like tea and crumpets, not any of the plot...")
 * The header image is [Yorkshire Gold tea & crumpets](https://www.flickr.com/photos/dianamoon/6283771859/) by [Diana Moon](https://www.flickr.com/photos/dianamoon/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Tweets from 09/26 to 09/30
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Bulletproof Blues, part 2
 * ![The cover to The Evangelists of Mars, featuring Avalon blasting one of the Martians](/blog/assets/evangelists-mars.png "They keep saying that Mars needs women, but really, what would they even do with them but this...?")
 * The header image is the cover to **Evangelists of Mars**, under the same license as the game.
 * title: Developer Journal, World Habitat Day
 * ![A row of probably suburban houses](/blog/assets/Dr-Landreys-office-on-the-Schofield.png "🎶 Gonna shake hands around the world, down by the riverside...")
 * The header image borrows from [Dr. Landrey's office on the Schofield - panoramio](https://commons.wikimedia.org/wiki/File:Dr._Landrey%27s_office_on_the_Schofield_-_panoramio.jpg) by [A'eron Blackman](https://web.archive.org/web/20161023015031/http://www.panoramio.com/user/5954608?with_photo_id=55385505), made available under the terms of the [Creative Commons Attribution 3.0 Unported](https://creativecommons.org/licenses/by/3.0/deed.en) license.
 * title: Real Life in Star Trek, The Arsenal Of Freedom
 * ![A cannon outside a stone wall, partly covered in snow](/blog/assets/vintage-antique-steel-military-army-tourism-1349531-pxhere.com.png "But is the cannon canon...?")
 * The header image is [untitled](https://pxhere.com/en/photo/1349531) by an uncredited PxHere photographer, released under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 10/03 to 10/07
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — American Dream, part 1
 * ![The cover to American Dream](/blog/assets/american-dream-cover.png "Unfortunately not something that I'd pick off the shelf, but still catches my interest")
 * The header image is the book's cover, under the same license as the novel.
 * title: Developer Journal, Indigenous Peoples' Day
 * ![A celebration of Indigenous Peoples' Day, including people in a circle, watching a dance](/blog/assets/New-Mexico-Indigenous-Peoples-Day-2019-03.png "If white people had participated in this in the '90s, I can promise that they'd all go home focused on the grass-less circle and arguing about crop circles...")
 * The header image borrows from [New Mexico Indigenous Peoples' Day 2019](https://twitter.com/RepDebHaaland/status/1183894892214398977) by [Deb Haaland](https://twitter.com/RepDebHaaland), in the public domain as the work of an employee of the United States government, Representative from the state of New Mexico, in Haaland's case.
 * title: Real Life in Star Trek, Symbiosis
 * ![A pile of raw opium](/blog/assets/raw-opium.png "Not felicium")
 * The header image is [Raw opium](https://commons.wikimedia.org/wiki/File:Raw_opium.jpg) by Erik Fenderson, released into the public domain by the photographer.
 * title: Tweets from 10/10 to 10/14
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — American Dream, part 2
 * ![The cover to American Dream](/blog/assets/american-dream-cover.png "Unfortunately not something that I'd pick off the shelf, but still catches my interest")
 * The header image is the book's cover, under the same license as the novel.
 * title: Developer Journal, Eradication of Poverty
 * ![A replica of the stone commemorating the International Day for the Eradication of Poverty](/blog/assets/Lapide-in-onore-delle-vittime-della-miseria.png "We have a plaque for this one!")
 * The header image is [Lapide in onore delle vittime della miseria](https://commons.wikimedia.org/wiki/File:Lapide_in_onore_delle_vittime_della_miseria.jpg) by [Giovanni T](https://web.archive.org/web/20161023015031/http://www.panoramio.com/user/https://commons.wikimedia.org/w/index.php?title=User:Giovanni_T), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Skin of Evil
 * ![A surf scooter duck covered in oil from the 2007 San Francisco Bay oil spill](/blog/assets/Oiled_bird_3.png "Not Armus, but when they inevitably reboot this show...🤞")
 * The header image is [Oiled bird 3](https://commons.wikimedia.org/wiki/File:Oiled_bird_3.jpg) by [Brocken Inaglory](https://sites.google.com/site/thebrockeninglory/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.5 Generic](https://creativecommons.org/licenses/by-sa/2.5/deed.en) license.
 * title: Tweets from 10/17 to 10/21
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — American Dream, part 3
 * ![The cover to American Dream](/blog/assets/american-dream-cover.png "Unfortunately not something that I'd pick off the shelf, but still catches my interest")
 * The header image is the book's cover, under the same license as the novel.
 * title: Developer Journal, United Nations Day
 * ![The flag of the United Nations](/blog/assets/Flag-of-the-United-Nations.svg "While every studio churns out multiverse stories, why haven't we gotten the alternate reality where the United Nations keeps creating new flags, trying to find the map projection that will finally please everyone?")
 * The header image is [Flag of the United Nations](https://openclipart.org/detail/23722) by an anonymous artist, who released their work into the public domain.  Likewise, ignoring trademark issues, we can say with some certainty that the design falls into the public domain, originally published prior to September 1987 by the United Nations in a document not in their list of exceptions.
 * title: Real Life in Star Trek, We'll Always Have Paris
 * ![A view of the Eiffel Tower from the streets, near Le Recuitement Café](/blog/assets/cafe-light-sunrise-sunset-skyline-night-1397604-pxhere.com.png "Of all the cafés, in all the towns, in all the world, he doesn't walk into hers.  Wait...")
 * The header image is [untitled](https://pxhere.com/en/photo/1397604) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Tweets from 10/24 to 10/28
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — American Dream, part 4
 * ![The cover to American Dream](/blog/assets/american-dream-cover.png "Unfortunately not something that I'd pick off the shelf, but still catches my interest")
 * The header image is the book's cover, under the same license as the novel.
 * title: Fearing Fear Itself
 * ![A man in a white room crouching behind a desk, so that we only see his fingers and upper face](/blog/assets/hand-man-person-people-male-finger-1341681-pxhere.com.png "Kilroy was scared")
 * The header image is [untitled](https://pxhere.com/en/photo/1341681) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Halloween 🎃
 * ![A saci](/blog/assets/1991631014_bea0fb9612_o.png "That symmetrical foot will haunt my dreams for years to come...")
 * ![A famous U.S. President making music with explosions](/blog/assets/pyrophone-tj.png "Faces, admittedly, don't always get the attention that you might expect")
 * ![A study for "Un dimanche après-midi à l'Île de la Grande Jatte"](/blog/assets/Georges_Seurat_034.png "The funny thing about the project?  Our art teacher struggled to explain to a television-immersed generation how dots can form a picture from a distance, without ever using the word PIXEL or referring to televisions at all.")
 * The header image borrows from [saci](https://www.flickr.com/photos/ars351/1991631014/) by [a.](https://www.flickr.com/photos/ars351/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/deed.en) license.  You can find [more on Seurat's famous painting](https://www.metmuseum.org/art/collection/search/110002111) at the Metropolitan Museum of Art, where I believe that the study of the painting shown above currently sits on display.
 * title: Styles and Fonts in SVGs
 * ![Gears](/blog/assets/work-technology-vintage-wheel-retro-clock-606288-pxhere.com.jpg "No, in fact I could NOT find a sensible header image...")
 * The header image is [work, technology, vintage, wheel, retro, clock](https://pxhere.com/en/photo/606288) by an anonymous PxHere photographer, made available under the terms of the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Conspiracy
 * ![A sculpture of the Eye of Providence in Wayside chapel Geretsberg, with six insect legs added](/blog/assets/st-tng-conspiracy.png "The conspiracy has no patience for your issues.")
 * The header image combines [Kapelle in Brunn (1) 02](https://commons.wikimedia.org/wiki/File:Kapelle_in_Brunn_(1)_02.jpg) by [Werner100359](https://commons.wikimedia.org/wiki/User:Werner100359), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license, and [Coenagrion puella hind leg](https://commons.wikimedia.org/wiki/File:Coenagrion_puella_hind_leg.jpg) by [Siga](https://commons.wikimedia.org/wiki/User:Siga), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Tweets from 10/31 to 11/04
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — American Dream, part 5
 * ![The cover to American Dream](/blog/assets/american-dream-cover.png "Unfortunately not something that I'd pick off the shelf, but still catches my interest")
 * The header image is the book's cover, under the same license as the novel.
 * title: The Chains of Fear
 * ![Rusty chains](/blog/assets/chain-old-steel-rust-food-produce-1128315-pxhere.com.png "Not the most innovative metaphor, admittedly")
 * The header image is [untitled](https://pxhere.com/en/photo/1128315) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Ensisheim Meteorite
 * ![A woodcut from the Chronicles of Nuremberg (1493) showing the meteorite landing in Ensisheim](/blog/assets/chronicles-nuremberg-ensisheim.png "We won't go into detail on the bizarre chain of custody on this rock...")
 * The header image is a woodcut from **The Chronicles of Nuremberg** (1493) by Hartmann Schedel, illustrating the meteorite, long in the public domain.
 * title: (Desktop) Night Mode at...Well, Night
 * ![Sundown](/blog/assets/horizon-sky-sun-sunrise-sunset-sunlight-19628-pxhere.com.png "Light's out, everyone...")
 * The header image is [untitled](https://pxhere.com/en/photo/19628) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, The Neutral Zone
 * ![An augur buzzard, taking flight from a tree](/blog/assets/Augurbussard-Serengeti.png "You might say that they've taken this plot-line...down to the buzzard")
 * The header image is [Augurbussard-Serengeti](https://commons.wikimedia.org/wiki/File:Augurbussard-Serengeti.jpg) by [Tobi 87](https://commons.wikimedia.org/wiki/User:Tobi_87), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Tweets from 11/07 to 11/11
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — The Command Line Murders
 * ![A drawing of a person with their back towards us, dressed in a trench coat and slouch hat, examining a "murder board"](/blog/assets/174092-clue-illustration.png "Murder, She INSERTed")
 * The header image(s) are the game header images, under the same license as the respective games.
 * title: Fiction — Whatever Happened to Social Media?
 * ![Dev Boulder introducing Sqwoogy in 2011 (animation)](/blog/assets/sqwoogy-talk.png "Let's sqwoog some sqwoots into our sqweams.")
 * ![The interior of a sales cart, with golden eggs hovering around the goods](/blog/assets/wall-metaverse.png "The metaverse's ultimate form?")
 *  > ![Simulated headshot for David Hepler](/blog/assets/avatars/david-hepler.png)
 *  > ![Simulated headshot for David Hepler](/blog/assets/avatars/david-hepler.png)
 *  > ![Simulated headshot for David Hepler](/blog/assets/avatars/david-hepler.png)
 *  > ![Simulated headshot for David Hepler](/blog/assets/avatars/david-hepler.png)
 *  > ![Simulated headshot for David Hepler](/blog/assets/avatars/david-hepler.png)
 *  > ![Simulated headshot for David Hepler](/blog/assets/avatars/david-hepler.png)
 *  > ![Simulated headshot for David Hepler](/blog/assets/avatars/david-hepler.png)
 *  > ![Simulated headshot for David Hepler](/blog/assets/avatars/david-hepler.png)
 *  > ![Simulated headshot for David Hepler](/blog/assets/avatars/david-hepler.png)
 *  > ![Simulated headshot for David Hepler](/blog/assets/avatars/david-hepler.png)
 *  > ![Simulated headshot for the Great White Plague](/blog/assets/avatars/great-white-plague.png)
 *  > ![Simulated headshot of a random user](/blog/assets/avatars/generic-avatar-1.png)
 *  > ![Simulated headshot of a random user](/blog/assets/avatars/generic-avatar-9.png)
 *  > ![Simulated headshot of a random user](/blog/assets/avatars/generic-avatar-6.png)
 *  > ![Simulated headshot of a random user](/blog/assets/avatars/generic-avatar-4.png)
 *  > ![Simulated headshot of a random user](/blog/assets/avatars/generic-avatar-3.png)
 *  > ![Simulated headshot of a random user](/blog/assets/avatars/generic-avatar-7.png)
 *  > ![Simulated headshot for the Great White Plague](/blog/assets/avatars/great-white-plague.png)
 *  > ![Simulated headshot of a random user](/blog/assets/avatars/generic-avatar-5.png)
 *  > ![Simulated headshot of a random user](/blog/assets/avatars/generic-avatar-8.png)
 *  > ![Simulated headshot of a random user](/blog/assets/avatars/generic-avatar-2.png)
 *  > ![Simulated headshot of a random user](/blog/assets/avatars/generic-avatar-0.png)
 *  > ![Simulated headshot for Elton Moosek](/blog/assets/avatars/elton-moosek.png)
 *  > ![Simulated headshot for Elton Moosek](/blog/assets/avatars/elton-moosek.png)
 *  > ![Simulated headshot for Elton Moosek](/blog/assets/avatars/elton-moosek.png)
 *  > ![Simulated headshot for Elton Moosek](/blog/assets/avatars/elton-moosek.png)
 *  > ![Simulated headshot for Elton Moosek](/blog/assets/avatars/elton-moosek.png)
 *  > ![Simulated headshot for Elton Moosek](/blog/assets/avatars/elton-moosek.png)
 *  > ![Simulated headshot for David Hepler](/blog/assets/avatars/david-hepler.png)
 *  > ![Simulated headshot for the Great White Plague](/blog/assets/avatars/great-white-plague.png)
 * Other than in-story media, the header image is a frame from the first episode of **Your Face Is a Saxophone**, still by Zacqary Adam Green and Plankhead, still made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Moby-Dick
 * ![An illustration of Moby-Dick destroying a whaling boat](/blog/assets/moby-dick-illustration.png "Both jaws, like enormous shears, bit the craft completely in twain.")
 * The header image is one of Augustus B. Shute's illustrations for the 1892 edition of **Moby-Dick**, long in the public domain.
 * title: Using Linux
 * ![The GNU gnu and Linux Tux mascots as flying superheroes](/blog/assets/Gnu-and-penguin-color.png "...Why does the gnu have hands on his fore-legs, but hoofs on his hind-legs?")
 * The header image is [*The Dynamic Duo: The Gnu and the Penguin in flight*](https://www.gnu.org/graphics/bwcartoon.html) by Lissanne Lake, made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.  Originally released under the [GNU Free Documentation License, version 1.1](https://www.gnu.org/licenses/old-licenses/fdl-1.1.html) or later, but Wikipedia took advantage of a [limited negotiated period](https://meta.wikimedia.org/wiki/Licensing_update) to relicense important works for consistency.
 * title: Real Life in Star Trek, Season 1, TNG
 * ![The Hubble Space Telescope](/blog/assets/GSFC-20171208-Archive-e002151.png "Still scanning the galaxy...in the next generation")
 * The header image is [Hubble Space Telescope](https://images.nasa.gov/details-GSFC_20171208_Archive_e002151) by NASA Goddard, in the public domain by NASA policy.
 * title: Tweets from 11/14 to 11/18
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Redmine, part 1
 * ![An uncut ruby in a larger stone](/blog/assets/Ruby-Winza-Tanzania.png "I got a rock...")
 * The header image is [Ruby --- Winza, Tanzania](https://commons.wikimedia.org/wiki/File:Ruby_-_Winza,_Tanzania.jpg) by [StrangerThanKindness](https://commons.wikimedia.org/w/index.php?title=User:StrangerThanKindness), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: A Gentle (and Biased) Guide to Mastodon
 * ![A reconstruction (painting) of an American mastodon](/blog/assets/Knight-Mastodon.png "Let's talk about the elephant in the room...")
 * The header image is [Knight Mastodon](https://commons.wikimedia.org/wiki/File:Knight_Mastodon.jpg) by Charles R. Knight, long in the public domain.
 * title: Developer Journal, Mayflower Compact
 * ![J.L.G. Ferris's painting, The Mayflower Compact, depicting Pilgrims signing the document](/blog/assets/The-Mayflower-Compact-1620-cph.png "Excuse me, but elementary school promised me buckles on hats, not a bunch of hippies and...maybe Conquistadors?")
 * The header image is [*The Mayflower Compact 1620*](https://loc.gov/pictures/resource/cph.3g07155/) by Jean Leon Gerome Ferris, seemingly in the public domain due to expired copyright.
 * title: Real Life in Star Trek, The Child
 * ![A mother cradling her child](/blog/assets/234720408_e422677153_o.png "Wait, haven't hundreds of thousands of years of humanity shown that this makes a terrible way to understand what life means...?")
 * The header image is [mother child](https://www.flickr.com/photos/delicategenius/234720408/) by [Michael Kordahi](https://www.flickr.com/photos/delicategenius/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Tweets from 11/21 to 11/25
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Redmine, part 2
 * ![An uncut ruby in a larger stone](/blog/assets/Ruby-Winza-Tanzania.png "I got a rock...")
 * The header image is [Ruby --- Winza, Tanzania](https://commons.wikimedia.org/wiki/File:Ruby_-_Winza,_Tanzania.jpg) by [StrangerThanKindness](https://commons.wikimedia.org/w/index.php?title=User:StrangerThanKindness), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Things That Worked (and Didn't Work) in 2022
 * ![The silhouette of a woman seemingly vaulting across a valley](/blog/assets/beach-sea-coast-outdoor-rock-ocean-1271114-pxhere.com.png "Don't panic.  I don't have anything this dramatic...")
 * The header image is [untitled](https://pxhere.com/en/photo/1271114) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Lā Kūʻokoʻa
 * ![Satellite image of the windward Hawaiian islands](/blog/assets/HawaiiNoRedLine.png "I honestly had no idea that we had satellites in 2003 that took color pictures with enough detail that you can ALMOST make out individual trees on some islands.")
 * The header image is [Hawaiian Islands](https://web.archive.org/web/20110608065020/http://visibleearth.nasa.gov/view_rec.php?id=16470) by the MODIS Land Rapid Response Team, in the public domain as work produced by NASA.
 * title: Real Life in Star Trek, Where Silence Has Lease
 * ![An artist's rendition of colliding black holes](/blog/assets/31209212418_910d33ddf8_o.png "No less empty than their special effects, right...?")
 * The header image is [New Simulation Sheds Light on Spiraling Supermassive Black Holes](https://www.flickr.com/photos/24662369@N07/31209212418) by [NASA Goddard Space Flight Center](https://www.flickr.com/photos/gsfc/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Tweets from 11/28 to 12/02
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Superflu
 * ![A poster for the imaginary Superflu film, "The Fuchsia Knight"](/blog/assets/fuschia-knight.png "I hear that the critics have already given it better scores than 2003's Catwoman, despite the typo.")
 * The header image is the "cover" of part two of the comic, made available under the same terms as the rest.
 * title: Pejoratives and Specificity
 * ![A cat's face, the cat lying on a sheet of paper where someone has written "a mongrel rogue" in red ink](/blog/assets/cat-mammal-black-cat-black-close-up-nose-284906-pxhere.com.png "Some serious judgment going on, there...")
 * The header image is [untitled](https://pxhere.com/en/photo/284906) by Robert Couse-Baker, made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Repeal Day
 * ![A police officer confiscating alcohol from a 1922 automotive accident](/blog/assets/master-pnp-cph-3b40000-3b42000-3b42800-3b42859u.png "Possibly the oddest choice of poses that he could have made...")
 * The header image is [Policeman standing alongside wrecked car and cases of moonshine](https://www.loc.gov/pictures/item/89709481/) by an unknown photographer, in the public domain with either an expired copyright or lack of original copyright.
 * title: Real Life in Star Trek, Elementary, Dear Data
 * ![An overhead diagram of 221B Baker Street](/blog/assets/221B_Baker_Street.png "He doesn't collect anything that doesn't have some critical significance, therefore it looks like a hoarder lives there...")
 * The header image is [Plan of 221B Baker Street](https://commons.wikimedia.org/wiki/File:221B_Baker_Street.JPEG) by [Russ Stutler](http://www.stutler.cc/), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Tweets from 12/05 to 12/09
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Colossal Cave Adventure
 * ![The Travertine speleothem in Great Onyx Cave](/blog/assets/8313139575_cebf70ac5d_o.png "The graphics don't live up to real life...")
 * The header image is [Travertine speleothem in Great Onyx Cave](https://www.flickr.com/photos/47445767@N05/8313139575) by [James St. John](https://www.flickr.com/photos/jsjgeology/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, Kanji Day
 * ![One scroll of the ninth century Chronicles of Japan](/blog/assets/Nihonshoki-tanaka-version.png "Partly as an experiment to see if anybody reads these, and partly because I'll never do the work, I'll scrounge up and mail an interesting prize to the first person who leaves a valid translation of the scroll in the comments...")
 * The header image is [Chronicles of Japan, Volume 10](https://web.archive.org/web/20070419170047/http://www.emuseum.jp/cgi/pkihon.cgi?SyoID=4&ID=w012&SubID=s000) by some seemingly unknown---either anonymous or long-forgotten---court scribe, and much too old to have a valid copyright.
 * title: Real Life in Star Trek, The Outrageous Okona
 * ![Stand-up comic Max Amini performing, with the face blurred to not call him out specifically](/blog/assets/Max-Amini-Persian-American-Stand-Up-Comedian-2015.png "What's the deal with replicators...?")
 * The header image is [Max Amini Persian American Stand Up Comedian 2015](https://commons.wikimedia.org/w/index.php?curid=40213566) by [PersianDutchNetwork](https://commons.wikimedia.org/w/index.php?title=User:PersianDutchNetwork&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Tweets from 12/12 to 12/16
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — R. James Gavreau
 * ![Filament Eruption Creates 'Canyon of Fire' on the Sun, but used to represent a more general explosion...](/blog/assets/47422682251_33843e992b_o.png "Boom...")
 * The header image is [Space](https://www.flickr.com/photos/127744844@N06/47422682251) by the [GPA Photo Archive](https://www.flickr.com/photos/127744844@N06/47422682251), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Journal, International Human Solidarity Eve
 * ![A dance at a Wikipedia event on the unity-in-diversity theme](/blog/assets/Unity-In-Diversity-Opening-Ceremony-Wiki-Conference-India-CGC-Mohali-2016-08-05-6656.png "Somewhere, there exists a United-Nations-Backing-a-Dance-Dance-Revolution joke in this, but I don't have it...")
 * The header image is [Unity In Diversity - Opening Ceremony - Wiki Conference India - CGC - Mohali](https://commons.wikimedia.org/wiki/File:Unity_In_Diversity_-_Opening_Ceremony_-_Wiki_Conference_India_-_CGC_-_Mohali_2016-08-05_6656.JPG) by [Biswarup Ganguly](https://commons.wikimedia.org/wiki/User:Gangulybiswarup), made available under the terms of the [Creative Commons Attribution 3.0 Unported](https://creativecommons.org/licenses/by/3.0/deed.en) license.
 * title: Real Life in Star Trek, Loud As A Whisper
 * ![Deaf artist Suliphone teaches children words from Lao sign language](/blog/assets/Lao-sign-language-025.png "I feel like the adults shouldn't look nearly so mortified, or the dog quite as disinterested, in this scene.")
 * The header image is [Lao sign language 025](https://commons.wikimedia.org/wiki/File:Lao_sign_language_025.jpg) by [Big Brother Mouse](https://commons.wikimedia.org/wiki/User:BigBrotherMouse), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Tweets from 12/19 to 12/23
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Quantum Flux
 * ![The adventure's cover, featuring a woman in a science fiction helmet and armor firing a rifle to the side](/blog/assets/QuantumFlux-Digital-P2.png "I give it credit for getting through this entire story-line without once bringing up a single cargo-loading exoskeleton.")
 * The header image is the book's cover, made available under the same terms as the book itself, though if you can track down the original---the referenced page no longer exists---you might find slightly different terms.
 * title: 🔭 Looking Back on 2022
 * ![An owl with its head turned to look to the left](/blog/assets/StockSnap_CMDGZMPI28.png "Looking back...")
 * The header image is adapted from [Eye Bird](https://stocksnap.io/photo/eye-bird-CMDGZMPI28) by [Bob Richards](https://stocksnap.io/author/bobrichards), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Journal, Boxing Day
 * ![A wooden box with a metal handle and latches](/blog/assets/Låda-Livrustkammaren-107142.png "🎶 On the second day of Christmas, my true love gave to me, a box.")
 * ![The salmon-and-potato pie with the first slice removed](/blog/assets/salmon-pie.png "Some greenery really would have made this look much better.")
 * The header image is [Arkitektmodell Fredrikshof](https://commons.wikimedia.org/wiki/File:L%C3%A5da_-_Livrustkammaren_-_107142.tif) by an unknown artisan, photographed by [Jens Mohr](https://commons.wikimedia.org/wiki/Creator:Jens_Mohr), in the public domain due to age.
 * title: Real Life in Star Trek, The Schizoid Man
 * ![A circuit-like pattern laid out over a human brain](/blog/assets/circuit-board-brain.png "I enjoyed making this more than watching the episode...")
 * The header image is based on [Human brain NIH](https://commons.wikimedia.org/wiki/File:Human_brain_NIH.png) by the [National Institutes of Health](http://www.nih.gov).  The original lies in the public domain as a work by an employee of the United States government, and I release my modified version under the terms of the blog's license.
 * title: Tweets from 12/26 to 12/30
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "diagrams showing the division of the day and of the week")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Life Blood, chapters 1 – 3
 * ![The book's cover, featuring an abstract woman's face pointing up](/blog/assets/life-blood-cover.png "Out of the...something or other.")
 * The header image is the book's cover, made available under the same terms as the book itself.
 * title: 🍾 Happy Calendar-Changing Day, 2023 🎆
 * ![2023 rising from the sand](/blog/assets/5660944477_baaef7ca2a_o.png "Hey, was that there yesterday...?")
 * The header image is adapted from [Sand Dunes, Huacachina, Peru](https://www.flickr.com/photos/22526939@N03/5660944477) by [nwhitford](https://www.flickr.com/photos/22526939@N03/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.  For anybody who looks closely, I apologize for not building a boundary between the various layers of the image that actually has the same depth from the camera.
 * title: Developer Diary, National Science Fiction Day
 * ![A man standing on a staircase in aerial traffic on a public telephone playing advertisements...and a fishbowl across the way](/blog/assets/talk-to-me-color-pass.png "The top hat really sells this...")
 * The header image is from [Talk to Me (Color Pass)](https://archive.org/details/Wootha_Public_Domain) by [Stephane (Wooltha) Richard](https://t.me/atelierdarts), released into the public domain in early 2021.
 * title: Real Life in Star Trek, Unnatural Selection
 * ![CRISPR-Cas9 as a Molecular Tool Introduces Targeted Double Strand DNA Breaks](/blog/assets/15-Hegasy-Cas9-DNA-Tool-Wiki-E-CCBYSA.png "Mean CRISPR makes people old, I guess")
 * The header image is [15 Hegasy Cas9 DNA Tool](https://commons.wikimedia.org/wiki/File:15_Hegasy_Cas9_DNA_Tool_Wiki_E_CCBYSA.png) by [Guido4](https://commons.wikimedia.org/w/index.php?title=User:Guido4), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🐘 from 01/02 to 01/06
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Some champion eye-rolling, there.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Life Blood, chapters 4 – 5
 * ![The book's cover, featuring an abstract woman's face pointing up](/blog/assets/life-blood-cover.png "Out of the...something or other.")
 * The header image is the book's cover, made available under the same terms as the book itself.
 * title: Developer Diary, Hōonkō
 * ![A portrait of Shinran Shōnin](/blog/assets/Shinran-Shonin.png "I prefer to believe that this shows a fantasy-franchise dwarf, rather than an older Japanese man on his knees...")
 * The header image is [Shinran Shonin](https://commons.wikimedia.org/wiki/File:Shinran_Shonin.jpg) by an unknown thirteenth century artist, long in the public domain.
 * title: Real Life in Star Trek, A Matter of Honor
 * ![A group of people in Klingon costumes toasting something around a table](/blog/assets/5188365617_e4817d73f5_o.png "Why do we all sit on this side of the table?  Err...that side has no honor.  Sure.  Let's go with that.")
 * The header image is [A Klingon Christmas Carol --- Commedia Beauregard](https://flickr.com/photos/39884518@N08/5188365617) by [Guy F. Wicke](https://flickr.com/photos/guyfwicke/) and Scott Pakudaitis, made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.  I assume that the "chain of custody" on copyrights works out, here, in the sense of the actors posing for a releasable picture and not wearing commercially produced prosthetics.
 * title: Toots 🐘 from 01/09 to 01/13
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Some champion eye-rolling, there.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Life Blood, chapters 6 – 9
 * ![The book's cover, featuring an abstract woman's face pointing up](/blog/assets/life-blood-cover.png "Out of the...something or other.")
 * The header image is the book's cover, made available under the same terms as the book itself.
 * title: Developer Diary, Martin Luther King Jr. Day
 * ![44th Original MLK Jr. Parade, downtown Houston, 2022](/blog/assets/51837281921_ced7053865_o.png "I don't care for parades, but I'll make an exception, this once.")
 * The header image is [Original MLK parade Houston TX 2022 11722-39](https://www.flickr.com/photos/2cheap2keep/51837281921/) by [2C2K Photgraphy](https://www.flickr.com/photos/2cheap2keep/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, The Measure of a Man
 * ![A tailor measuring an adult male customer for a suit](/blog/assets/36804045581_14ac148b2e_o.png "Look, either I used something like this or make a reference to Dred Scott v Sandford.  Which would you really rather I make a snarky comment about...?")
 * The header image is [Tailor Measuring Man for Suit](https://www.flickr.com/photos/141761303@N08/36804045581) by [Amtec Photos](https://www.flickr.com/photos/141761303@N08/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🐘 from 01/16 to 01/20
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Some champion eye-rolling, there.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Life Blood, chapters 10 – 13
 * ![The book's cover, featuring an abstract woman's face pointing up](/blog/assets/life-blood-cover.png "Out of the...something or other.")
 * The header image is the book's cover, made available under the same terms as the book itself.
 * title: Developer Diary, World Freedom Day
 * ![A fictional protest march by a multi-ethnic crowd wearing blue t-shirts and holding sky blue and white flags, marching down a wide city street possibly modeled on someplace in Europe](/blog/assets/FH7GzVl946prbqReMfuG--1--43fgr.png "A more generic World Freedom Day")
 * The header image is *A New World Freedom Day*, generated by me via the NightCafe AI art generator, used in place of traumatizing people with pictures of Prisoners of War or anti-communist attacks.  I hereby release it under the same terms as the blog, though with the caveat that using it entails responsibility for making sure that no faces or logos adapt anything in the real world.
 * title: Real Life in Star Trek, The Dauphin
 * ![A reconstruction of the coat of arms of the Dauphin of Viennois](/blog/assets/Dauphin-of-Viennois-Arms.png "Every time I read about heraldry, I feel half-convinced that somebody replaced the actual text with a hoax...")
 * The header image is slightly cropped from [Dauphin of Viennois Arms](https://commons.wikimedia.org/wiki/File:Dauphin_of_Viennois_Arms.svg) by [Odejea](https://commons.wikimedia.org/wiki/User:Odejea), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Toots 🐘 from 01/23 to 01/27
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I hate Venn diagram memes, especially when they don't make any sense")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Life Blood, chapters 14 – 16
 * ![The book's cover, featuring an abstract woman's face pointing up](/blog/assets/life-blood-cover.png "Out of the...something or other.")
 * The header image is the book's cover, made available under the same terms as the book itself.
 * title: Developer Diary, Fred Korematsu Day
 * ![Baggage held at an internment camp](/blog/assets/Luggage-Japanese-American-internment.png "A photographer somehow saw an internment camp and decided that the world needed to see the baggage claim.  Well, it saves us the trauma of looking at prisoners, so I guess I owe that lousy photographer some modest thanks...")
 * The header image is [Baggage of Japanese-Americans evacuated from certain West coast areas under United States Army war emergency order](https://loc.gov/pictures/resource/fsa.8c24660/) by an unknown photographer, in the public domain as the work of an employee o the United States government.
 * title: Real Life in Star Trek, Contagion
 * ![A cartoon of malware locking a smartphone](/blog/assets/Al-menos-35-periodistas-y-activistas-en-El-Salvador-fueron-espiados-con-Pegasus.png "Demons of air, darkness, and backwards baseball caps")
 * The header image is [Al menos 35 periodistas y activistas en El Salvador fueron espiados con Pegasus - 1](https://r3d.mx/2022/01/17/al-menos-35-periodistas-y-activistas-en-el-salvador-fueron-espiados-con-pegasus/) by Gibrán Aquino, made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🐘 from 01/30 to 02/03
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Life Blood, chapters 17 – 19
 * ![The book's cover, featuring an abstract woman's face pointing up](/blog/assets/life-blood-cover.png "Out of the...something or other.")
 * The header image is the book's cover, made available under the same terms as the book itself.
 * title: Developer Diary, Day against FGM
 * ![A worker conducting an awareness session in Mogadishu](/blog/assets/12456298255_a68305cc1d_o.png "Teaching the men to not do it seems like a more important step than telling women to avoid letting it happen, but at least we've started moving.")
 * The header image is [Anti-FGM Campaign at Walala Biyotey](https://www.flickr.com/photos/au_unistphotostream/12456298255/) by David Mutua for [AMISOM Public Information](https://www.flickr.com/photos/au_unistphotostream/), released into the public domain.
 * title: Real Life in Star Trek, The Royale
 * ![An ace and king of diamonds laid out in front of a pile of chips on a green felt surface](/blog/assets/5857823720_a69a8d1f9e_o.png "They create the ultimate no-win scenario in this episode...")
 * The header image is [A blackjack hand at the casino](https://www.flickr.com/photos/59937401@N07/5857823720) by [Images Money](https://www.flickr.com/photos/59937401@N07/), placed into the public domain, according to the description.
 * title: Toots 🐘 from 02/06 to 02/10
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Life Blood, chapters 20 – 22
 * ![The book's cover, featuring an abstract woman's face pointing up](/blog/assets/life-blood-cover.png "Out of the...something or other.")
 * The header image is the book's cover, made available under the same terms as the book itself.
 * title: Developer Diary, World Radio Day
 * ![A foxhole radio](/blog/assets/Foxhole-radio-from-WW2.png "Don't underestimate a technology that allows someone to get a message out to anybody with access to literal junk.")
 * The header image is [Foxhole radio from WW2](https://commons.wikimedia.org/wiki/File:Foxhole_radio_from_WW2.jpg) by  John W. Campbell Jr., in the public domain due to a failure to renew its copyright.
 * title: Real Life in Star Trek, Time Squared
 * ![People lined up in Times Square for sandwiches during the Great Depression](/blog/assets/FSHFYQYACNFN5ML2INUYJFFVMI.png "What a difference a century and a million billboards make...")
 * The header image is [People line in Times Square and 43rd Street to receive sandwiches and a cup of coffee](https://www.pennlive.com/business/2019/10/stock-market-crash-of-1929-left-people-hysterical-with-fear.html) by the Associated Press, in the public domain due to a lack of copyright notice and (if a notice existed) failure to renew the copyright.
 * title: Toots 🐘 from 02/13 to 02/17
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Life Blood, chapters 23 – 25
 * ![The book's cover, featuring an abstract woman's face pointing up](/blog/assets/life-blood-cover.png "Out of the...something or other.")
 * The header image is the book's cover, made available under the same terms as the book itself.
 * title: Developer Diary, World Day of Social Justice
 * ![Eleanor Roosevelt reading the Universal Declaration of Human Rights](/blog/assets/27758131387_302d739453_o.png "I should send all my correspondence on paper like that...")
 * The header image is [64-165](https://www.flickr.com/photos/fdrlibrary/27758131387/) by the [FDR Presidential Library & Museum](https://www.flickr.com/photos/fdrlibrary/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, The Icarus Factor
 * ![A relief showing The Fall of Icarus, including the labyrinth, a head as the Sun, and Daedalus looking irritated at his son](/blog/assets/The-Fall-of-Icarus.png "Didn't the Sun star in Planet of the Apes?")
 * The header image is [The Fall of Icarus, 17th century, Musée Antoine Vivenel](https://commons.wikimedia.org/wiki/File:%27The_Fall_of_Icarus%27,_17th_century,_Mus%C3%A9e_Antoine_Vivenel.JPG), photograph by [Wmpearl](https://commons.wikimedia.org/wiki/User:Wmpearl), released into the public domain.
 * title: Toots 🐘 from 02/20 to 02/24
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Life Blood, chapters 26 – 29
 * ![The book's cover, featuring an abstract woman's face pointing up](/blog/assets/life-blood-cover.png "Out of the...something or other.")
 * The header image is the book's cover, made available under the same terms as the book itself.
 * title: Five Phases of AI Grief
 * ![A cyberpunk-style picture of a trench-coated person on a pink-lighted dock looking out at a misty skyline](/blog/assets/EnDRyeSZhtFDcDz0QBYm--1--l9vnq.png "Wanderer above Cheap LED Lighting and Fog")
 * The header image is adapted from creations generated (by John) on [NightCafe Studio](https://creator.nightcafe.studio/studio), hereby released under the same CC BY-SA 4.0 terms as the blog.
 * title: Developer Diary, Marathi Language Day
 * ![Ancient Marathi writing, sideways](/blog/assets/Ancient-scriptures-on-the-walls-in-Big-Temple-Thanjavur.png "You act like they carved the language in stone...")
 * The header image is [Ancient scriptures on the walls in Big Temple, Thanjavur - 2](https://commons.wikimedia.org/wiki/File:Ancient_scriptures_on_the_walls_in_Big_Temple,_Thanjavur_-_2.JPG) by [Junykwilfred](https://commons.wikimedia.org/w/index.php?title=User:Junykwilfred), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Pen Pals
 * ![Betty Ford speaking into a CB radio microphone from the backseat of a car](/blog/assets/betty-ford-cb.png "Look, there are plenty of low blows to make, here, but despite failures, the Fords admittedly worked hard to stand on the right side of history, right until their deaths.")
 * The header image is [First Lady Betty Ford Speaking Into a Citizens Band (CB) Radio](https://catalog.archives.gov/id/45644255) by an unlisted White House photographer, in the public domain as a work of the United States government.
 * title: Toots 🐘 from 02/27 to 03/03
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Golem, part 1
 * ![A reproduction of the Prague Golem](/blog/assets/Prague-golem-reproduction.png "How has this fellow never become a stuffed animal?")
 * The header image [Prague-golem-reproduction](https://commons.wikimedia.org/wiki/File:Prague-golem-reproduction.jpg) by "user Thander," released into the public domain.
 * title: Commenting Code
 * ![Seen from behind, two people, identified as women of color, review Ruby code on a laptop](/blog/assets/25392428253_26315dc3f0_o.png "Oh, A = B + Cc adds B and C, then assigns the result to A?  Well, now that you say it, that changes everything...")
 * The header image is [wocintech (microsoft) — 62](https://www.flickr.com/photos/wocintechchat/25392428253/in/dateposted/) by [WOCinTech Chat](https://www.flickr.com/photos/wocintechchat/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Diary, European Day of the Righteous
 * ![All in blue, the European circle of stars logo to the left of the words "European Day of the Righteous"](/blog/assets/European-Day-of-the-Righteous-logo.png "I don't think that I've ever seen a holiday with its own logo")
 * ![A bar chart representing, for the past ninety-one days, the sentiment as height and the length as opacity, most prominently featuring a spike in sentiment around two months ago and a precipitous dip about a week ago](/blog/assets/morning-dashboard-journal-sentiment.png "I expected less stability than that, honestly.")
 * ![A bar chart representing, for the past ninety-one days, the length of sleep as height and my morning reaction time as opacity, most prominently featuring a sudden jump up around two months ago and a sudden dip about a week ago](/blog/assets/morning-dashboard-sleep-time.png "I agree that I need to put more effort into consistent sleep patterns.")
 * The header image is [European Day of the Righteous' logo](https://commons.wikimedia.org/wiki/File:European_Day_of_the_Righteous%27_logo.png) by [Ruggibrante](https://commons.wikimedia.org/wiki/User:Ruggibrante), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Q Who?
 * ![A person with human arms and face, but with the rest of the head made from an anvil and horseshoes, and a forge running through their torso, bellows in back and a work area in front.](/blog/assets/Habit-de-Marechal.png "Hey, not every cyborg gets to work with computers.")
 * The header image is [Habit de Marêchal](https://commons.wikimedia.org/wiki/File:Habit_de_Mar%C3%AAchal.jpg) by Nicolas de Larmessin, long in the public domain.
 * title: Toots 🐘 from 03/06 to 03/10
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Golem, part 2
 * ![A reproduction of the Prague Golem](/blog/assets/Prague-golem-reproduction.png "How has this fellow never become a stuffed animal?")
 * The header image [Prague-golem-reproduction](https://commons.wikimedia.org/wiki/File:Prague-golem-reproduction.jpg) by "user Thander," released into the public domain.
 * title: Developer Diary, Aztec New Year (Belated)
 * ![A colored drawing of Huitzilopotchli in feathered regalia, seemingly riding a ladder across a field](/blog/assets/Huitzilopochtli-WDL6725.png "I can't quite figure out where, but Huitzilopotchli seems to figure in to the holiday, somehow, and I couldn't find any pictures of Nahua people celebrating the new year")
 * The header image is [Huitzilopochtli, the Principal Aztec God](https://commons.wikimedia.org/wiki/File:Huitzilopochtli,_the_Principal_Aztec_God_WDL6725.png) by (presumably) Juan de Tovar, long in the public domain.
 * title: Real Life in Star Trek, Samaritan Snare
 * ![Doctors operating, presumably on a patient, circa 1965](/blog/assets/14356056541_d9e22ef6c7_o.png "We could improve this scene dramatically with blood-red robes and hats")
 * The header image is cropped from [Heart Valve surgery at the Clinical Center](https://www.flickr.com/photos/124413887@N04/14356056541) by [NIH History Office](https://www.flickr.com/photos/historyatnih/), released into the public domain.
 * title: Toots 🐘 from 03/13 to 03/17
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * ![An AI-generated image of an enclosed deck surrounded by foliage, with two outdoor couches surrounding a low table, either flowers or food on the table; it is drawn in the style of an anime scene](/blog/assets/AAfu7T44guw8qJWGEVh4--1--4tqc4.jpg "Subsequent images didn't even show the same porch.  How can I use this to watch my porch...?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Delilah H. Smith
 * ![A sketch of Jenny Everywhere, standing with her thumb out, hitchhiking with a sign reading "Reality or Bust!"  She carries a duffel bag with stickers implying travel to various fictional universes](/blog/assets/Jenny-Everywhere-by-Diana-Nock.png "I only recognize a couple of the references, so if you need that, fingers crossed that someone will fill us in, down in the comments...")
 * The header image [Jenny Everywhere](https://commons.wikimedia.org/w/index.php?curid=10709730) by Diana Nock, made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 United States](https://creativecommons.org/licenses/by-sa/3.0/us/deed.en) license.
 * title: Fiction — Bank on It
 * ![The entrance to a café from a brick street, the doorway overgrown with foliage, and a handful of empty folding chairs along the wall](/blog/assets/2481692525_828cb92490_o.png "I gave up on trying to get AI to illustrate anything in this story...")
 * The header image adapts [Cafe](https://www.flickr.com/photos/59099750@N00/2481692525) by [Andrew Garton](https://www.flickr.com/photos/andrew-garton/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Developer Diary, International Day of Happiness
 * ![A smiling 95-year-old man from Pichilemu, Chile](/blog/assets/Mario-Canete-Farias-in-2007.png "I think that I might like the flannel/sweater/sport-coat combination...")
 * The header image is [Mario Cañete Farías in 2007](https://commons.wikimedia.org/wiki/File:My_Grandfather_Photo_from_January_17.JPG) by [Diego Grez Cañete](https://www.wikidata.org/wiki/Q15304738), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Up The Long Ladder
 * ![A line of Irish dancers on a stage, all dressed in green, dancing in front of a band](/blog/assets/Irish-Dancers.png "Correct.  I could not find an image as stereotypical as what we see in the episode...")
 * The header image is [Irish Dancers](https://commons.wikimedia.org/wiki/File:Irish_Dancers.jpg) by [Jonathan Baker](https://commons.wikimedia.org/wiki/User:Jonjobaker), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🐘 from 03/20 to 03/24
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Snowbound Blood part 1
 * ![A cropped screenshot of the visual novel, showing a feminine troll character in a business suit, standing in a conference room](/blog/assets/snowbound-blood-hamiki.png "I've had secret meetings in dark conference rooms that felt FAR more awkward than this one did...")
 * The header image comes from the game, and so should fall under the Creative Commons license of the rest of the art.
 * title: Modern Social Media Etiquette
 * ![A brick wall, with the command "Like me!" written as graffiti on the left, with a thumbs-up, opposite a superimposed (probably) feminine eye looking at it](/blog/assets/woman-wall-internet-red-communication-friendship-923229-pxhere.com.png "I'd sign up for a service like this, at least to try it out...")
 * The header image is based on [untitled](https://pxhere.com/en/photo/923229) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Diary, World Theatre Day
 * ![An Ancient Greek theater](/blog/assets/Taormina-BW2012-10-05-16-05-05.png "Five minutes to curtain...")
 * The header image is [Taormina BW 2012-10-05 16-05-05](https://commons.wikimedia.org/wiki/File:Taormina_BW_2012-10-05_16-05-05.jpg) by [Berthold Werner](https://commons.wikimedia.org/wiki/User:Berthold_Werner), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Mastodon's API (The Pitfalls)
 * ![A mastodon skeleton in a museum](/blog/assets/37079091691_7f7b4e82eb_o.png "The API has good bones, you see, but...")
 * ![A table from Mastodon's app developer configuration, showing a (fake, spelling out a warning not to use them) client key, client secret, and access token](/blog/assets/Mastodon-dev-application-keys.png "I don't know why it bothers with the client key and secret")
 * The header image is [DSC09097 - Mastodon](https://www.flickr.com/photos/22490717@N02/37079091691) by [Dennis Jarvis](https://www.flickr.com/photos/archer10/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.  It replaces a cheap-looking AI-generated image of a robot-mastodon-thing that I thought better of...
 * title: Real Life in Star Trek, Manhunt
 * ![A two-page advertisement for 1919's silent Western drama "The Man Hunter" from Fox Film Corporation, billed as The Greatest Photoplay Since "Les Misérables"](/blog/assets/The-Man-Hunter-1919.png "Other than the similar titles, no, it has nothing to do with the episode; all things considered, I'd rather watch the lost film...")
 * The header image is [Advertisement in Moving Picture World for the American film The Man Hunter (1919) with William Farnum and Louise Lovely](https://archive.org/details/movingwor39chal?view=theater#page/724/mode/2up) by the film's crew, long in the public domain due to an expired copyright.
 * title: Toots 🐘 from 03/27 to 03/31
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Snowbound Blood part 2
 * ![A screenshot of the visual novel, showing the moonlit "trash dunes" in the foreground, a cave entrance that resembles an open animal mouth in the background, and a dark ocean beyond that](/blog/assets/sbb-trash-dunes.png "If I had played this as a child, I would almost certainly have spent hours fantasizing about the trash dunes...")
 * The header image comes from the game, and so should fall under the Creative Commons license of the rest of the art.
 * title: Developer Diary, Yuknoom Yich'aak K'ahk'
 * ![The pyramid at Calakmul, emerging from the tree canopy](/blog/assets/Piramides-de-Calakmul.png "Like I said, never a bad picture.")
 * ![A crusty roll, about four inches by three inches, with visible sesame, sunflower, and hemp seeds peeking through a glossy brown surface](/blog/assets/stovetop_roll.png "Yes, I could only find an ancient tape measure for scale.")
 * The header image is [Pirámides de Calakmul](https://commons.wikimedia.org/wiki/File:Pir%C3%A1mides_de_Calakmul.JPG) by [PashiX](https://commons.wikimedia.org/w/index.php?title=User:PashiX&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Normalizing Image Type and Size
 * ![A black and white photograph of a bearded person in jeans and a leather jacket, drawing a complex image on a cobblestone surface](/blog/assets/man-sand-people-street-artist-material-1338169-pxhere.com.png "Sadly, you can't hire this guy to make things happen...")
 * The header image is adapted from [untitled](https://pxhere.com/en/photo/1338169) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, The Emissary
 * ![Portrait of Dom Miguel de Castro, Emissary of Congo](/blog/assets/cHJpdmF0ZS9sci9pbWFnZXMvd2Vic2l0ZS8yMDIyLTExL3Nta2ttczctaW1hZ2UuanBn.png "He looks thrilled to be here, but at least I found an emissary...")
 * The header image is [Portrait of Dom Miguel de Castro, Emissary of Congo](https://www.rawpixel.com/image/8920800/don-miguel-castro-emissary-congo) by Jasper or Jeronimus Becx, long in the public domain.
 * title: Toots 🐘 from 04/03 to 04/07
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Snowbound Blood part 3
 * ![A screenshot of the visual novel, showing our protagonist Secily on her motorcycle, looking into a forest](/blog/assets/sbb-forest-biking.png "I imagine that some seeing-the-forest-for-the-trees joke feels appropriate, here, but nothing useful comes to mind...")
 * The header image comes from the game, and so should fall under the Creative Commons license of the rest of the art.
 * title: Great Ideas from Terrible Jobs
 * ![A small pile of mildly distressed, square orange sheets of note paper, the top one having "Measure good stuff" scrawled on it in marker](/blog/assets/writing-orange-yellow-brand-product-font-197089-pxhere.com.png "Also valid advice, but not where I plan to take this post.")
 * The header image is [PostIt Advice](https://pxhere.com/en/photo/197089) by Alan Levine, made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Diary, Siblings Day
 * ![A photograph of three similar-looking young children running through a field holding hands, with a sketched-in question "vamos celebrar?" beneath them](/blog/assets/Cartaz-Dia-dos-Irmaos-2016.png "This technically celebrates the wrong Siblings Day, but I call that close enough...")
 * The header image is [Cartaz Dia dos Irmãos 2016](https://commons.wikimedia.org/wiki/File:Cartaz_Dia_dos_Irm%C3%A3os_2016.jpg) by Associação Portuguesa de Famílias Numerosas, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).
 * title: Real Life in Star Trek, Peak Performance
 * ![The later moves of a game of Go between a human and an artificial intelligence; the credits describe which game, for those who want a more thorough description of the action, since I don't know the game well enough to describe it meaningfully](/blog/assets/AlphaGo-Fan-Huiren-aurka.png "If I cared less about copyright, I would've gone with a picture of A24's hotdog-finger gloves...")
 * The header image is [Part 2 of Game 4 (October 8, 2015) against Go player Fan Hui by the artificial intelligence program AlphaGo](https://commons.wikimedia.org/wiki/File:AlphaGo_Fan_Huiren_aurka.png) by [Xabi22](https://commons.wikimedia.org/wiki/User:Xabi22), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🐘 from 04/10 to 04/14
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Snowbound Blood part 4
 * ![A screenshot of the visual novel, depicting downtown in "Stronghold 21," complete with widespread advertising](/blog/assets/sbb-s21-normal.png "I sort of enjoy the implication, here, that the news has carefully covered the bogus awards ceremony for the past two months or blinks or whatever they call them")
 * The header image comes from the game, and so should fall under the Creative Commons license of the rest of the art.
 * title: Developer Diary, Lena Massacre
 * ![A view of the Lone Maiden rock formation (far right) above the Lena River, Yakutsk](/blog/assets/Lone-maiden-formation.png "I admit it. The view makes me feel a bit jealous. And I like this picture better than the massacre victims...")
 * The header image is [Lone maiden formation](https://commons.wikimedia.org/wiki/File:Lone-maiden-formation.jpg) by [Linjye](https://commons.wikimedia.org/w/index.php?title=User:Linjye&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Shades of Gray
 * ![A stilt-root mangrove in Cilintang, Taman Nasional Ujung Kulon, Banten](/blog/assets/Rhizopora-stylosa-Found-in-Cilintang.png "See, this guy knows how to rhizome")
 * The header image is [Rhizopora stylosa Found in Mangrove of Cilintang, Taman Nasional Ujung Kulon, Banten](https://commons.wikimedia.org/wiki/File:Rhizopora_stylosa_Found_in_Mangrove_of_Cilintang,_Taman_Nasional_Ujung_Kulon,_Banten.jpg) by [Putra Mahanaim Tampubolon](https://commons.wikimedia.org/wiki/User:Ptrmtb), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🐘 from 04/17 to 04/21
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Expedition Sasquatch, part 1
 * ![The Expedition Sasquatch logo, featuring a sketch of a Sasquatch-like creature from the nose down, walking from right to left in front of hills.  The name of the podcast overlays the lower third, written in a Comic Sans-like font](/blog/assets/expedition-sasquatch.png "Squatch the skies?")
 * The header image comes from the podcast art, licensed under the same terms as the podcast.
 * title: Why Federate?
 * ![A collage of various mascot characters from the Fediverse applications](/blog/assets/fediverse-mascots.png "Honestly, I didn't plan on putting in nearly this much work for this post's image")
 * The header image combines [pleroma and sepia](https://www.peppercarrot.com/en/viewer/comissions-src__2023-03-03_sketches_pleroma-and-sepia_by-David-Revoy.html), [mobilizon and sepia](https://www.peppercarrot.com/en/viewer/comissions-src__2023-03-03_sketches_mobilizon-and-sepia_by-David-Revoy.html), [misskey and mastodon](https://www.peppercarrot.com/en/viewer/comissions-src__2023-03-03_sketches_misskey-and-mastodon_by-David-Revoy.html), [funkwhale and pixelfed](https://www.peppercarrot.com/en/viewer/comissions-src__2023-03-03_sketches_funkwhale-and-pixelfed_by-David-Revoy.html), and [Joinfediverse wiki mascot](https://www.peppercarrot.com/en/viewer/comissions-src__2022-03-28_joinfediverse-wiki_mascot_by-David-Revoy.html), all by [David Revoy](https://www.davidrevoy.com/), all made available under the terms of the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license.
 * title: Developer Diary, Fashion Revolution Day
 * ![Photograph displaying a diverse-looking group of people protesting fast fashion with Fashion Revolution's slogan of "Who Made My Clothes?"](/blog/assets/Who-Made-My-Clothes-Protest.png "Generally speaking, if you can't name the people who made what you own, unfortunately, you probably enabled exploitation")
 * The header image is [Rana Plaza commemoration - Who Made My Clothes, April 22nd 2015](https://www.flickr.com/photos/greensefa/17055641890/) (faces blurred by me) by [greensefa](https://www.flickr.com/photos/greensefa/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/deed.en) license.
 * title: Real Life in Star Trek, Season 2, TNG
 * ![The Hubble Space Telescope](/blog/assets/GSFC-20171208-Archive-e002151.png "Still scanning the galaxy...in the next generation")
 * The header image is [Hubble Space Telescope](https://images.nasa.gov/details-GSFC_20171208_Archive_e002151) by NASA Goddard, in the public domain by NASA policy.
 * title: Toots 🐘 from 04/24 to 04/28
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Expedition Sasquatch, part 2
 * ![The Expedition Sasquatch logo, featuring a sketch of a Sasquatch-like creature from the nose down, walking from right to left in front of hills.  The name of the podcast overlays the lower third, written in a Comic Sans-like font](/blog/assets/expedition-sasquatch.png "Squatch the skies?")
 * The header image comes from the podcast art, licensed under the same terms as the podcast.
 * title: Developer Diary, International Workers' Day
 * ![A large audience, some members holding signs saying such slogans as "Agitate, Educate, Organize," "One Big Union," "The Workers Fight the Wars While Bosses Reap the Profits," and so forth](/blog/assets/Socialists-in-Union-Square-NYC.png "I appreciate that, in this large crowd, a handful of people spotted the camera and decided to pose...")
 * The header image is [Socialists in Union Square, N.Y.C.](https://loc.gov/pictures/resource/ggbain.10505/) by the Bain News Service, long in the public domain.
 * title: Real Life in Star Trek, Evolution
 * ![An army of tiny robots crawling across a futuristic circuit board](/blog/assets/Yvbw6lszIPQkXU795ipu--2--lxjyk.png "No kill I?")
 * I assembled the header image from components generated by [NightCafé Studio](https://nightcafe.studio/), hereby released under the same CC BY-SA 4.0 terms as the blog.
 * title: Toots 🐘 from 05/01 to 05/05
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Expedition Sasquatch, part 3
 * ![The Expedition Sasquatch logo, featuring a sketch of a Sasquatch-like creature from the nose down, walking from right to left in front of hills.  The name of the podcast overlays the lower third, written in a Comic Sans-like font](/blog/assets/expedition-sasquatch.png "Squatch the skies?")
 * The header image comes from the podcast art, licensed under the same terms as the podcast.
 * title: Developer Diary, WWII Remembrance
 * ![Field Marshall Wilhelm Keitel, signing the ratified surrender terms for the German Army at Russian Headquarters in Berlin. Germany, May 7, 1945](/blog/assets/keitel-111-SC-206292.png "I always assumed that pop culture exaggerated the monocle affectation...")
 * The header image is [Field Marshall Wilhelm Keitel, signing the ratified surrender terms for the German Army at Russian Headquarters in Berlin. Germany, May 7, 1945](https://catalog.archives.gov/id/531290) by a United States Army Lt. Moore, in the public domain.
 * title: Graphs with Chart.js
 * ![A hand with a pen touches up some charts](/blog/assets/journey-achievement-arms-business-businessman-calculating-1575617-pxhere.com.png "The best grocery list ever?")
 * The header image is adapted from [Hand business woman pointing paper data analyze chart on desk at office](https://pxhere.com/en/photo/1575617) by [asawin](https://pxhere.com/en/photographer/2102671), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, The Ensigns of Command
 * ![Ensign of the United States Coast Guard, with the Seal of the United States (the eagle with shield, arrows, olive branch, etc.) in the upper right, vertical red stripes everywhere except the upper right, and the Coast Guard logo near center-right](/blog/assets/Ensign_of_the_United_States_Coast_Guard.svg "The lettering on the seal seems a bit too fiddly, no?")
 * The header image is [Ensign of the United States Coast Guard](https://commons.wikimedia.org/wiki/File:Ensign_of_the_United_States_Coast_Guard.svg) by [Sagredo](https://commons.wikimedia.org/wiki/User:Sagredo) and [Zscout370](https://commons.wikimedia.org/wiki/User:Zscout370), released into the public domain, based on work by the United States Coast Guard.
 * title: Toots 🐘 from 05/08 to 05/12
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Jectoons Random Vignettes
 * ![A comic of the artist, holding up a comic of a frog for the audience to see, with a pile of other comics at his feet](/blog/assets/jectoons-season-1-cover.png "It seems like a bad sign that I want to read the comic about the foot, especially as the lead character of a series...")
 * ![A comic featuring a "copyright" bird, silent in a cage, watching Creative Commons birds flying and singing out the window](/blog/assets/random-vignettes-18-768x768.png "I don't want to group this with Maya Angelou, mind you, but a motivated student could probably pull a PhD thesis out of why this caged bird does NOT sing...")
 * The header image comes from the game, and so should fall under the Creative Commons license of the rest of the art.
 * title: Developer Diary, Nabka Day
 * ![Looking south at a demonstration for Nakba Day on Broadway at 42nd Street in Times Square, Midtown Manhattan](/blog/assets/42nd-St-Bway-7th-Av-2018-05-18-06-Nakba-Day-Protest.png "I realize that one protests in Midtown because you find people there, but it also has so much visual noise that we can barely identify the protesters in pictures")
 * The header image is [42nd St Bway 7th Av td (2018-05-18) 06 - Nakba Day Protest](https://commons.wikimedia.org/wiki/File:42nd_St_Bway_7th_Av_td_(2018-05-18)_06_-_Nakba_Day_Protest.jpg) (advertising blurred by me) by [Tdorante10](https://commons.wikimedia.org/wiki/User:Tdorante10), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, The Survivors
 * ![Artist's impression of an exoplanet around beta Pictoris, showing a disc permeated with carbon monoxide around the star, and cometary collisions in the distance](/blog/assets/eso1408a.png "One day, the episode will refer to an exoplanet that exists and an artist has tried to recreate...")
 * The header image is [Artist's impression of Beta Pictoris](https://www.eso.org/public/images/eso1408a/) bythe [ESO](https://www.eso.org), available under a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) License.
 * title: Toots 🐘 from 05/15 to 05/19
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Wait a second, are these all the same day of the week?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Death off the Cuff
 * ![A (fake) computer game with a balding man wearing glasses, examining a glass as he walks through a hotel lobby](/blog/assets/0T7Xq2n9mQexs502W3ow--2--hh4hb.png "From this detail, I can deduce, as can any simpleton, that I need a refill...and that the blogger could not find an appropriately licensed image.")
 * I adapted the header image from something that I created for this post---since the game's assets don't fall under the code's license---using [NightCafé Studio](https://nightcafe.studio/), hereby released under the same CC BY-SA 4.0 terms as the blog.
 * title: Developer Diary, World Biodiversity Day
 * ![A sampling of fungi collected during summer 2008 in Northern Saskatchewan mixed woods, near LaRonge. This photograph also includes leaf lichens and mosses](/blog/assets/Fungi-of-Saskatchewan.png "Tired pun overload...")
 * The header image is [Fungi of Saskatchewan](https://commons.wikimedia.org/wiki/File:Fungi_of_Saskatchewan.JPG) by [Sasata](https://commons.wikimedia.org/w/index.php?title=User:Sasata&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Who Watches the Watchers
 * ![A photograph of the oddly angled peaks of Vasquez Rocks](/blog/assets/Vasquez_Rocks_2.png "I don't even want to get into how pointless it seems to dig the outpost into rocks nowhere in sight of the village...")
 * The header image is [Vasquez Rocks 2](https://commons.wikimedia.org/wiki/File:Vasquez_Rocks_2.jpg) by [Ayleen Dority](https://www.flickr.com/people/23978325@N05), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🐘 from 05/22 to 05/26
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Just what the world needed:  A calendar that flips the bird.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Distress Beacon
 * ![The cover to Distress Beacon, featuring an industrial-looking spaceship in a light-colored, gaseous background, viewed through shutters.  The titles refer to the Morse code for SOS, as well as the Saga Guide series and the Sword of Odin publisher](/blog/assets/distress-beacon.png "That poor ship looks broken to me, like someone made it out of old (upright) pianos and didn't bother to clean the knickknacks off the top.")
 * The header image is the book's cover, by Seth Rutledge, under the same license as the rest of the book.
 * title: Developer Diary, Day of UN Peacekeepers
 * ![The UN Peacekeepers force´s helmet](/blog/assets/Blue-helmet.png "Do they pad them on the outside?  That seems irresponsible")
 * The header image is [Blue helmet](https://commons.wikimedia.org/wiki/File:Blue_helmet.JPG) by [Daniel Košinár](https://commons.wikimedia.org/w/index.php?title=Gravedigger88&action=edit&redlink=1), released into the public domain.
 * title: Real Life in Star Trek, The Bonding
 * ![An eroded sandstone-like background with "In living Memory" carved into its face](/blog/assets/in-loving-memory.png "I find it funny that the episode focuses on grief over the death of...someone who I'd bet that almost none of us could name without looking it up.")
 * The header image is [In loving Memory](https://pxhere.com/en/photo/822525) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🐘 from 05/29 to 06/02
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I hate Venn diagram memes, especially when they don't make any sense")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Green Comet, part 1
 * ![The cover to Green Comet, featuring a green-tinted structure that looks like stone](/blog/assets/green-comet.png "Seems fairly literal, but sure.")
 * The header image is the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, World Environment Day
 * ![The helmet worn by the UN Peacekeepers force](/blog/assets/15636978156_5313675756_o.png "Do they pad them on the outside?  That seems irresponsible")
 * The header image is [Marine Debris Removal in the Northwestern Hawaiian Islands](https://www.flickr.com/photos/40322276@N04/15636978156/) by [NOAA's National Ocean Service](https://www.flickr.com/photos/usoceangov/), in the public domain as a work of the United States government.
 * title: Real Life in Star Trek, Booby Trap
 * ![A person with their finger caught in an old-style, spring-loaded mousetrap, baited with a paper heart](/blog/assets/5265260921_15c339b2a9_o.png "Does this relate to the A-plot or the B-plot?  The world will ever wonder.")
 * The header image is [Self Trapped](https://www.flickr.com/photos/49970491@N07/5265260921) by [Nicu Buculei](https://www.flickr.com/photos/nicubunuphotos/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🐘 from 06/05 to 06/09
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I hate Venn diagram memes, especially when they don't make any sense")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Green Comet, part 2
 * ![The cover to Green Comet, featuring a green-tinted structure that looks like stone](/blog/assets/green-comet.png "Seems fairly literal, but sure.")
 * The header image is the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, World Day Against Child Labor
 * ![Child coal miners — drivers and mules, in Gary, West Virginia, mine](/blog/assets/Child-coal-miners-1908-crop.png "Learning productive skills, such as how to cover up black lung disease...")
 * The header image is [Drivers and Mules, Gary, W. Va., Mine, Where much of the mining and carrying is done by machinery](https://loc.gov/pictures/resource/nclc.01052/) by Lewis Wickes Hine, long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, The Enemy
 * ![A storm scattering many lightning strikes across (probably) a city or suburban skyline at night](/blog/assets/night-evening-weather-storm-darkness-electricity-1356023-pxhere.com.png "I won't even bring up the Velma-can't-see-without-her-glasses vibe, to this episode...")
 * The header image is [untitled](https://pxhere.com/en/photo/1356023) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🐘 from 06/12 to 06/16
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Green Comet, part 3
 * ![The cover to Green Comet, featuring a green-tinted structure that looks like stone](/blog/assets/green-comet.png "Seems fairly literal, but sure.")
 * The header image is the book's cover, under the same license as the rest of the book.
 * title: Unraveling Universality
 * ![Runcorn Spiritualist Church in the UK, built awkwardly under an arch of the Queen Ethelfleda Viaduct](/blog/assets/1244305_ba5b074f.png "Perfectly inconspicuous and normal...")
 * The header image is [Square Peg in a Round Hole](https://www.geograph.org.uk/photo/1244305) by [Sue Adair](https://www.geograph.org.uk/profile/1657), made available under the terms of the [Creative Commons Attribution 2.0 Generic](http://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Developer Diary, Juneteenth
 * ![A 1905 Emancipation Day celebration in Virginia, the precursor to Juneteenth](/blog/assets/Emancipation-Day-in-Richmond-Virginia-1905.png "Seriously, compare this holiday to Memorial Day, which we sanitize almost completely so that we don't need to think about the horrors of war on out day off")
 * The header image is [Emancipation Day in Richmond, Virginia, 1905](https://commons.wikimedia.org/wiki/File:Emancipation_Day_in_Richmond,_Virginia,_1905.jpg) formerly curated by VCU Libraries, long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, The Price
 * ![A dark tunnel angling up in the medium range to a bright light](/blog/assets/light-sunlight-hole-skate-bike-atmosphere-670187-pxhere.com.png "Not a wormhole...probably")
 * The header image is [untitled](https://pxhere.com/en/photo/670187) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🐘 from 06/19 to 06/23
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Let's Move Forward
 * ![The cover to Let's Move Forward's prologue, featuring a family portrait of the Fairstones and a newspaper featuring one of them, set on a desk in front of a window.  Titles proclaim the prologue "The Last Day of Normal," and indicates the credits and license](/blog/assets/lets-move-forward-ch000-p000.png "I can't think of a single scenario where the newspaper ends up UNDER the standing picture, honestly.  It will fall...")
 * The header image is the book's cover, under the same license as the rest of the book.
 * title: Why Care about Free Culture?
 * ![A logo representing Free Culture, featuring a sketch of three definitely generic modular brick toys stacked as a corner](/blog/assets/117878252_3c00463c07_o.png "The original organization appears to have abandoned their use of the logo, so I might as well use it here...")
 * I adapted the header image from [FC.o logo](https://www.flickr.com/photos/mllerustad/117878252/in/set-72057594090576065/) by [Karen Rustad Tölva](https://www.flickr.com/photos/mllerustad/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.  They created the original version for [FreeCulture.org](http://freeculture.org/), but you can see as well as I can that the site no longer exists in any meaningful way, so I decided to appropriate it...
 * title: Developer Diary, World Refrigeration Day
 * ![William Thomson, 1st Baron Kelvin, writing](/blog/assets/Baron-Kelvin.png "Thank goodness he didn't go into interior design.")
 * The header image is [Baron Kelvin](https://commons.wikimedia.org/wiki/File:Baron_Kelvin.jpg) by an unknown photographer, long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, The Vengeance Factor
 * ![The Hatfields in 1897](/blog/assets/HatfieldClan.png "Wait. How did the Mirror Universe not change the doctor's name to Leonard Hatfield...?")
 * The header image is [The Hatfield Clan of the Hatfield-McCoy-feud](https://commons.wikimedia.org/wiki/File:HatfieldClan.jpg), long in the public domain.
 * title: Toots 🐘 from 06/26 to 06/30
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Wait a second, are these all the same day of the week?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — kiki the nano bot
 * ![A screenshot of a level of the game, in shades of magenta and purple, blocks protruding from the floor and ceiling](/blog/assets/kiki_005.png "Stalagblocks and stalacubes?")
 * The header image is a screenshot of the game, under the same license as the rest of the game.
 * title: Developer Diary, Pickett's Charge
 * ![An engraving of Pickett's charge at Gettysburg](/blog/assets/1903-14596104027.png "The man in the center, presumably Pickett based on the beard, who took the time to put a hat on his sword, pretty much tells the whole story.  White supremacists:  Not the sharpest tools in the shed.")
 * The header image is [General Pickett's famous charge at Gettysburg](https://commons.wikimedia.org/wiki/File:Makers_of_the_world%27s_history_and_their_grand_achievements_%281903%29_%2814596104027%29.jpg) by Alfred Swinton after Alfred Waud, long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, The Defector
 * ![The Battle of the Little Bighorn, showing Native Americans on horseback in foreground](/blog/assets/3g07160v.png "A petard in the process of hoisting someone")
 * The header image is [The Custer fight](https://www.loc.gov/pictures/item/99472670/) by 	C.M. Russell, long in the public domain.
 * title: Toots 🐘 from 07/03 to 07/07
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * ![A partially abstract scene at the Glider Hackerspace, with silhouettes working and moving around](/blog/assets/hackerspace_mike_liuzzi.png "I hope the giant orange bin gets a line.")
 * The header image is the book's cover, under the same license as the rest of the book.
 * title: Affirmative Action in Gentle Terms
 * ![Formal group photograph of the Supreme Court as comprised on June 30, 2022, after Justice Ketanji Brown Jackson joined the Court. The Justices pose in front of red velvet drapes and arranged by seniority, with five seated and four standing.](/blog/assets/Supreme-Court-of-the-United-States-Roberts-Court-2022.png "Some day, people will wonder how we had a Supreme Court known for baby drop-boxes, Black separatism, boofing, theocracy, disdain for civil rights, and disdain for governance.  OK, yes, that day started years ago and hasn't ended.")
 * The header image is [The Supreme Court as composed June 30, 2022 to present](https://commons.wikimedia.org/wiki/File:Supreme_Court_of_the_United_States_-_Roberts_Court_2022.jpg) by Fred Schilling, in the public domain as a work of the United States government.
 * title: Developer Diary, Wyoming Statehood
 * ![The state flag of Wyoming, in the shape of Wyoming](/blog/assets/State-Flag-States-4-2015072945.svg "Bison bison bison bison bison bison bison bison?")
 * The header image is [Wyoming](https://openclipart.org/detail/223880/State-Flag-States-4) by [Andrew Rohletter](https://openclipart.org/artist/Arohletter1), contributed to the public domain by the creator.
 * title: Real Life in Star Trek, The Hunted
 * ![A black-and-white abstract painting, resembling both foliage and shattered, repetitive scenes](/blog/assets/37595029996_94bc3e66ef_o.png "I didn't expect to find anything this imposing.")
 * The header image is [PTSD](https://www.flickr.com/photos/28469941@N02/37595029996) by [m o n c h o o h c n o m](https://www.flickr.com/photos/monchotoronto/), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/mark/1.0/).
 * title: Toots 🐘 from 07/10 to 07/14
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Just what the world needed:  A calendar that flips the bird.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Archive of Our Own, part 1
 * ![A city with strange architecture lit in bold colors, and an abstract sculpture off the center](/blog/assets/Cq2zGP7sXKeXwAT8GtAY--1--k2y6h.png "I couldn't find any images that actually made sense.")
 * I adapted the header image from something that I created for this post---since the game's assets don't fall under the code's license---using [NightCafé Studio](https://nightcafe.studio/), hereby released under the same CC BY-SA 4.0 terms as the blog.
 * title: Developer Diary, Yongle Emperor
 * ![Seated portrait of Emperor Ming Chengzu](/blog/assets/Portrait-assis-de-lempereur-Ming-Chengzu.png "He also apparently looks like he stepped out of a 1970s Saturday morning cartoon, where he and his team of meddling street urchins solve mysteries and play street concerts while operating from their Porcelain Tower.")
 * The header image is [Seated portrait of Emperor Ming Chengzu](https://new.shuge.org/view/li_dai_di_hou_xiang_zhou/) by an unknown artist, long in the public domain by virtue of creation before copyright law.
 * title: Real Life in Star Trek, The High Ground
 * ![An 1867 Punch cartoon commenting on the Fenian Rising, featuring a sloppily dressed man sitting on a barrel labeled gunpowder while waving a torch around, and a family around him](/blog/assets/Fenian-guy-fawkesr1867reduced.png "The least-biased image that I could find representing terrorism, sadly...")
 * The header image is [The Fenian Guy Fawkes](https://commons.wikimedia.org/wiki/File:Fenian_guy_fawkesr1867reduced.png) by an unknown **Punch** Magazine artist, long in the public domain.
 * title: Toots 🐘 from 07/17 to 07/21
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Some champion eye-rolling, there.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Archive of Our Own, part 2
 * ![A computer-generated image of four men standing near a stone railing in a Spanish-style scene, dressed casually, with one standing apart from the rest holding a pistol](/blog/assets/CjdzpFLJFbTewMjvhdaF--4--si28y.png "I did warn you that I have no experience with these games...")
 * I adapted the header image from something that I created for this post---since the game's assets don't fall under the code's license---using [NightCafé Studio](https://nightcafe.studio/), hereby released under the same CC BY-SA 4.0 terms as the blog.
 * title: Developer Diary, Simón Bolívar Day
 * ![Simón Bolívar, 1º Presidente de Bolivia](/blog/assets/Simon-Bolivar-Toro-Moreno-Jose-1922-Legislative-Palace-La-Paz.png "See, I would've called epaulettes and a cloak too much, but he makes them work...")
 * The header image is [Simón Bolívar. Toro Moreno, José. 1922, Legislative Palace, La Paz](https://commons.wikimedia.org/wiki/File:Sim%C3%B3n_Bol%C3%ADvar._Toro_Moreno,_Jos%C3%A9._1922,_Legislative_Palace,_La_Paz.png) by José Toro Moreno, passed into the public domain fifty years after the death of the artist.
 * title: Real Life in Star Trek, Déjà Q
 * ![Wispy tendrils of hot dust and gas glow brightly in this ultraviolet image of the Cygnus Loop nebula, taken by NASA Galaxy Evolution Explorer](/blog/assets/PIA15415-orig.png "Not the Calamarains")
 * The header image is [Cygnus Loop Nebula](https://images.nasa.gov/details/PIA15415) by [NASA/JPL-Caltech](https://www.jpl.nasa.gov/), released to the public domain by NASA policy.
 * title: Toots 🐘 from 07/24 to 07/28
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Some champion eye-rolling, there.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Project Ballad, part 1
 * ![The main cast of the comic, milling around in costume](/blog/assets/project-ballad-cover.png "William Ware Theiss does Eurovision?")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, First U.S. Patent
 * ![U.S. patent X000001](/blog/assets/FirstUSpatent.png "Wait, now I want to know if people and offices used to hire a clerk to draw letterhead in on their stationery...")
 * The header image is [US Patent X000001](https://commons.wikimedia.org/wiki/File:FirstUSpatent.jpg), in the public domain as a work of the United States government and because any copyright would have expired by now.
 * title: Real Life in Star Trek, A Matter of Perspective
 * ![Two abstract figures (farmers?) standing on opposite sides of a rectalinear figure, which one interprets as a number six and the other interprets as a number nine, due to their positions](/blog/assets/Point_of_view_bias.png "Yes, I see it.  No, I won't make the joke.  Take it to social media, if you must...")
 * The header image is [Point of view bias](https://commons.wikimedia.org/wiki/File:Point_of_view_bias.jpg) by [Mushki Brichta](https://commons.wikimedia.org/w/index.php?title=User:Mushki_Brichta&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🐘 from 07/31 to 08/04
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Some champion eye-rolling, there.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Project Ballad, part 2
 * ![The main cast of the comic, milling around in costume](/blog/assets/project-ballad-cover.png "William Ware Theiss does Eurovision?")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Software as a Haunting (SaaH?)
 * ![An old, run-down house with a "haunted" look to it, with JavaScript code pushing through the structure](/blog/assets/black-and-white-house-window-building-chateau-old-1264128-pxhere-com-pdf-js.png "Would the property values rise or sink, I wonder, if we changed the code to Oberon or Leda...")
 * The header image is based on [Untitled](https://pxhere.com/en/photo/1264128), by an uncredited PxHere photographer who made it available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/) and the [message handler](https://github.com/mozilla/pdf.js/blob/master/src/shared/message_handler.js) from the PDF Reader in JavaScript by the [Mozilla Corporation](https://wiki.mozilla.org/Github), released under the terms of the [Apache 2.0](https://apache.org/licenses/LICENSE-2.0) license, formatted using [Carbon](https://carbon.now.sh/).
 * title: Developer Diary, Purple Heart
 * ![A Badge of Military Merit](/blog/assets/badge-of-military-merit.png "Not to derail the topic, but it vaguely surprises that, at least to my knowledge, nobody produces a military-themed parody of Lucky Charms cereal...")
 * The header image is adapted from [Badge of Military Merit, an award in the United States](https://commons.wikimedia.org/wiki/File:MeritBadge.jpg) by [Husnock](https://en.wikipedia.org/wiki/User:Husnock), placed in the public domain by the photographer.  The badge itself would have no copyright, both as a work of the United States government and its age.
 * title: Real Life in Star Trek, Yesterday's Enterprise
 * ![An 1861 drawing of Professor Thaddeus S.C, Lowe's areostat Enterprise, arguably the ancestor of air and space programs](/blog/assets/Enterprise-Balloon-Corps-1861.png "Apologies for the crummy line-work; I couldn't find a decent-sized scan")
 * The header image is [Balloon Corps](https://commons.wikimedia.org/wiki/File:Balloon_Corps.jpg) by a **Harper's Weekly** artist in 1861, long in the public domain.
 * title: Toots 🐘 from 08/07 to 08/11
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Project Ballad, part 3
 * ![The main cast of the comic, milling around in costume](/blog/assets/project-ballad-cover.png "William Ware Theiss does Eurovision?")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Announcing Kabang!
 * ![Hypothetical "cabinet" art for the Kabang! arcade game, featuring a red, white, and blue astronaut holding a glowing yellow orb, which sends out a cyan tendril to connect other globes, in a field of debris](/blog/assets/kabang-cabinet-art.png "Best not to think about the spaghettification of the astronaut's left leg.")
 * ![The title screen for Kabang!, showing no high scores, an enlarged astronaut sprite, and a gray circle intended to represent a mine, rendered as if on a tube television](/blog/assets/kabang-splash-screen.png "Honestly, I'd almost consider it worth the splurge for this one visual.  Supporting the developer and saving to text helped the decision far more, though...")
 * I created the game---itself released under the terms of the [GNU Affero General Public License, version 3](https://www.gnu.org/licenses/agpl-3.0.html)---with all assets made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/) license.  The TIC-80 project has released its player code under the terms of the [MIT license](https://github.com/nesbox/TIC-80/blob/main/LICENSE).
 * title: Developer Diary, Gerakan Pramuka
 * ![Indonesian Scouts at the 8th Indonesian National Rover Moot July 8-17 2003](/blog/assets/Gerakan-Pramuka-Indonesia-Scouts-8th-Indonesian-National-Rover-Moot-2003.png "See, I would've called epaulets and a cloak too much, but he makes them work...")
 * The header image is slightly adapted from [Gerakan Pramuka Indonesia Scouts 8th Indonesian National Rover Moot 2003](https://commons.wikimedia.org/wiki/File:Gerakan_Pramuka_Indonesia_Scouts_8th_Indonesian_National_Rover_Moot_2003.jpg) by [Anakmadiun](https://en.wikipedia.org/wiki/User:Anakmadiun), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, The Offspring
 * ![A frog swimming through frogspawn-filled water](/blog/assets/Frog_in_frogspawn.png "Sixteenth from the left, seven rows down, we can call Lal, if you insist")
 * The header image is [Frog in frogspawn](https://commons.wikimedia.org/wiki/File:Frog_in_frogspawn.jpg) by [Salimfadhley](https://commons.wikimedia.org/wiki/User:Salimfadhley), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Toots 🐘 from 08/14 to 08/18
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Just what the world needed:  A calendar that flips the bird.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Poles, part 1
 * ![A pink chair sitting in front of a laptop saying "cinnamon(mint)" and a blue chair sitting in front of a workstation saying "i3wm(btw)" in an abstract office space with a visualized music score drifting through the background](/blog/assets/dhuru-vangal-cover.png "I have no idea what any of this means, but I now feel invested...")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, Haitian Revolution
 * ![Portrait of J. B. Belley, Deputy for Saint-Domingue](/blog/assets/WGA09508.png "From an ocean away, Baldie (the statue's subject) wrote that no state can ever legitimately authorize slavery, and that anyone trying to justify such a system deserves a dagger to the back.  Five stars, Monsieur Raynal, five stars.")
 * The header image is (part of) [Portrait of J. B. Belley, Deputy for Saint-Domingue](https://commons.wikimedia.org/wikihttps://commons.wikimedia.org/wiki/File:Anne-Louis_Girodet_De_Roucy-Trioson_-_Portrait_of_J._B._Belley,_Deputy_for_Saint-Domingue_-_WGA09508.jpg) by Anne-Louis Girodet de Roussy-Trioson, long in the public domain through expired copyright.
 * title: Real Life in Star Trek, Sins of the Father
 * ![A gavel resting on an open book](/blog/assets/book-read-wood-guitar-hammer-brown-641052-pxhere.com.png "I feel like using these header images to recontextualize the episode into our own culture exposes them as somewhat silly, behind the solid set designs and decent acting...")
 * ![An aerial photograph showing Pier 86 with the Intrepid Museum at bottom center, and the Enterprise carried above](/blog/assets/6973294496_b57b9dcbff_o.png "No, really.")
 * The header image is [Untitled](https://pxhere.com/en/photo/641052) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  [Shuttle Enterprise Flight to New York](https://commons.wikimedia.org/wiki/File:Shuttle_Enterprise_Flight_to_New_York_%28201204270023HQ%29.jpg) by NASA/Robert Markowitz *should* be released into the public domain by NASA policy.
 * title: Toots 🐘 from 08/21 to 08/25
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "And over here, we have...the Roman numeral for four, I guess.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Poles, part 2
 * ![A pink chair sitting in front of a laptop saying "cinnamon(mint)" and a blue chair sitting in front of a workstation saying "i3wm(btw)" in an abstract office space with a visualized music score drifting through the background](/blog/assets/dhuru-vangal-cover.png "I have no idea what any of this means, but I now feel invested...")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, Kaqchikel Rebellion
 * ![Ruins of Iximché, the capital of the Kaqchikel at the time](/blog/assets/Iximche.png "I have images of angry Spaniards coming here, reaching that steep staircase at the center of the frame, and becoming even more irate that anybody would flout building codes like this...")
 * The header image is [Iximché](https://commons.wikimedia.org/wiki/File:Iximch%C3%A9.JPG) by [Colocho](https://commons.wikimedia.org/wiki/User:Colocho), released into the public domain by the photographer.
 * title: Real Life in Star Trek, Allegiance
 * ![Ducklings follow a parent](/blog/assets/Ducks-in-a-row-following-leader.png "Ironically, the parent duck also plans on parking next to a random star to see how everybody reacts")
 * The header image is [Ducks in a row following leader ](https://pickupimage.com/free-photos/Ducks-in-a-row-following-leader/2351996) by [flickrpd](https://pickupimage.com/portfolio.cfm?uname=flickrpd), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🐘 from 08/28 to 09/01
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Poles, part 3
 * ![A pink chair sitting in front of a laptop saying "cinnamon(mint)" and a blue chair sitting in front of a workstation saying "i3wm(btw)" in an abstract office space with a visualized music score drifting through the background](/blog/assets/dhuru-vangal-cover.png "I have no idea what any of this means, but I now feel invested...")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, Labor Day
 * ![1882 Labor Day parade in Union Square, New York City](/blog/assets/First-United-States-Labor-Day-Parade-September-5-1882-in-New-York-City.png "I desperately want to complain about the Labor-Pays-All-Taxes sign when everybody else has aspirational signs, but somebody drew this.")
 * The header image is [First United States Labor Day Parade, September 5, 1882, in New York City](https://commons.wikimedia.org/wiki/File:First_United_States_Labor_Day_Parade,_September_5,_1882_in_New_York_City.jpg) by [Colocho](https://commons.wikimedia.org/wiki/User:Colocho) by an uncredited staff illustrator of **Frank Leslie's Illustrated Newspaper**, long in the public domain due to expired copyright.
 * title: Real Life in Star Trek, Captain's Holiday
 * ![Three people in beach-wear playing a Twister-like game on a pad in the sand](/blog/assets/beach-people-summer-female-leg-clothing-764741-pxhere.com.png "Jamaharon seemed less interesting than presented on television...")
 * The header image is [Untitled](https://pxhere.com/en/photo/764741) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal public domain dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🐘 from 09/04 to 09/08
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Just what the world needed:  A calendar that flips the bird.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Poles, part 4
 * ![A pink chair sitting in front of a laptop saying "cinnamon(mint)" and a blue chair sitting in front of a workstation saying "i3wm(btw)" in an abstract office space with a visualized music score drifting through the background](/blog/assets/dhuru-vangal-cover.png "I have no idea what any of this means, but I now feel invested...")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, Nayrouz
 * ![Painting of procession with flags in street, where participants and spectators mostly wear either turbans or fezzes](/blog/assets/Procession-of-the-Holy-Carpet.png "Maybe I would dislike parades less if I could watch them seated in front of my house...or maybe I'd go inside to check on something and never come back.")
 * The header image is [Procession of the Holy Carpet, Cairo](https://commons.wikimedia.org/wiki/File:Procession_of_the_Holy_Carpet,_Cairo._%28n.d.%29_-_front_-_TIMEA.jpg) by Rafael Tuck & Sons, Ltd, made available under the terms of the [Creative Commons Attribution Share-Alike 2.5 Generic](https://creativecommons.org/licenses/by-sa/2.5/deed.en) license.
 * title: Real Life in Star Trek, Tin Man
 * ![A person dressed as Baum's Tin Woodman, against a red disc](/blog/assets/Tin-Man-poster-Hamlin.png "Close enough...")
 * The header image is adapted from [The Tin Man. Poster for Fred R. Hamlin's musical extravaganza](https://loc.gov/pictures/resource/var.1422/) as portrayed by Dave Montgomery (photographer unknown to me), long in the public domain from an expired copyright.
 * title: Toots 🐘 from 09/11 to 09/15
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I hate Venn diagram memes, especially when they don't make any sense")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Poles, part 5
 * ![A pink chair sitting in front of a laptop saying "cinnamon(mint)" and a blue chair sitting in front of a workstation saying "i3wm(btw)" in an abstract office space with a visualized music score drifting through the background](/blog/assets/dhuru-vangal-cover.png "I have no idea what any of this means, but I now feel invested...")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, World Water Monitoring Day
 * ![Volunteers from the United States Environmental Protection Agency geared up in their official water sampling gear to show students how they do their jobs](/blog/assets/World-Water-Monitoring-Day-4049999633.png "I could have cropped this to only the students, but honestly, the random tourist, early (or late) lunch, and the anti-littering action felt artistically necessary...I definitely didn't fail to notice their presence until after I resized the image and then felt too lazy to bother.")
 * The header image is [World Water Monitoring Day](https://www.flickr.com/photos/usepagov/4049999633/) by the [United States Environmental Protection Agency](https://www.flickr.com/photos/usepagov/), in the public domain as a work of the United States government.
 * title: Real Life in Star Trek, Hollow Pursuits
 * ![A person sitting a bench engrossed in a video game in a dark space with red lighting](/blog/assets/game-games-electronics-screen-electronic-device-technology-1608777-pxhere.com.png "If I had the same colleagues, I'd probably spend more time playing video games, too...")
 * The header image is [Untitled](https://pxhere.com/en/photo/1608777) by [Tmaximumge](https://pxhere.com/en/photographer/3129712), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🐘 from 09/18 to 09/22
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — C-Man
 * ![C-Man determines the direction of a copyright violation from his secret fire escape](/blog/assets/c-man-piracy-in-progress.png "Time for a gritty, multiverse-spanning reboot, yet?")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, Unification of Nepal
 * ![A panoramic view of Thaha municipality-6, Bajrabarahi, Makawanpur, Nepal](/blog/assets/Bajrabarahi-Valley.png "I don't even know where to begin...")
 * The header image is [Bajrabarahi Valley](https://commons.wikimedia.org/wiki/File:Iximch%C3%A9.JPG) by [Raaznewa](https://commons.wikimedia.org/w/index.php?title=User:Raaznewa&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, The Most Toys
 * ![A large mass of short plastic tubes, presumably meant as minimalist beads](/blog/assets/plastic-pattern-food-color-small-colorful-888029-pxhere.com.png "Quantity over quality, everyone.  You can have an arbitrarily large number of toys, if you broaden your definition of the term...")
 * The header image is [untitled](https://pxhere.com/en/photo/888029) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🦣 from 09/25 to 09/29
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Full Bloom
 * ![A field of pink-to-purple flowers blooming in a grassy field](/blog/assets/nature-grass-plant-white-field-lawn-586790-pxhere.com.png "Pardon the shallow reading...")
 * The header image is [untitled](https://pxhere.com/en/photo/586790) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Diary, Batik Day
 * ![A craftsperson dying batik](/blog/assets/Women-Making-Batik-Ketelan-crop.png "I like how the shelf hides the eyes of the background workers, as if someone wanted to hide their identities.")
 * The header image is (part of) [Women Making Batik, Ketelan crop](https://www.flickr.com/photos/shkizzle/5981170339/) by [Stephen Kennedy](https://www.flickr.com/photos/shkizzle/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Sarek
 * ![Achilles pulling Agamemnon's hair, in Tiepolo's fresco](/blog/assets/The-Rage-of-Achilles-by-Giovanni-Battista-Tiepolo.png "Egad, you look mildly perturbed")
 * The header image is [The Rage of Achilles](https://www.wga.hu/frames-e.html?/html/t/tiepolo/gianbatt/6vicenza/2homer4.html) by Giovanni Battista Tiepolo, long in the public domain due to expired copyright, assuming that it ever had one.
 * title: Toots 🦣 from 10/02 to 10/06
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — ½
 * ![A paper cut-out of the artist's face, shaded in gold to the left and teal to the right](/blog/assets/Cullah-a3460696269-10.png "Quick, spot the metaphor...")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, Hangul Day
 * ![A photograph of Hunminjeongeum Changjebon, reading from right to left](/blog/assets/Hunmin-jeong-eum.png "Because the speech of this country is different from that of China, it [the spoken language] does not match the [Chinese] letters. Therefore, even if the ignorant want to communicate, many of them, in the end, cannot successfully express themselves. Saddened by this, I have [had] 28 letters newly made. It is my wish that all the people may easily learn these letters and that [they] be convenient for daily use.")
 * The header image is [Hunmin jeong-eum](https://web.archive.org/web/20030415094216/http://www.hangul.or.kr/M2-4-1.htm) by Sejong, long in the public domain.
 * title: Real Life in Star Trek, Ménage à Troi
 * ![A close-up of a glass chess set](/blog/assets/2338077903_051cc0bf04_o.png "Sorry, I couldn't find any pictures representing the crew's fear of middle-aged women who enjoy sex")
 * The header image is [POTD 16/3/2008](https://www.flickr.com/photos/ptc24/2338077903) by [Peter Corbett](https://www.flickr.com/photos/ptc24/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 10/09 to 10/13
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I hate Venn diagram memes, especially when they don't make any sense")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Sugar the Robot, part 1
 * ![A broken robot head resting on an apparent canister, wearing a housecoat, while sparks fly between its antennae](/blog/assets/sugar-frontcover.png "If we don't get a take-me-to-your-liter joke out of this...")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, World Food Day
 * ![The World Food Program's social media for World Food Day 2015, featuring a young person eating porridge with the hashtag #ZeroHunger](/blog/assets/One-Future-ZeroHunger.png "I always feel uncomfortable with ad campaigns like this, after decades of sitting through exploitative commercials...")
 * The header image is [One Future Zero Hunger](https://commons.wikimedia.org/wiki/File:One_Future_ZeroHunger.jpg) by the [World Food Program](https://www.wfp.org/), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Transfigurations
 * ![A view of a moodily lit bar with three bartenders working and the hand of one patron in the foreground](/blog/assets/person-restaurant-bar-meal-beverage-nightlife-705958-pxhere.com.png "🎶 Sometimes you want to go where nobody knows your name unless you have a spot in the main credits...")
 * The header image is [untitled](https://pxhere.com/en/photo/705958) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🦣 from 10/16 to 10/20
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "And over here, we have...the Roman numeral for four, I guess.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Sugar the Robot, part 2
 * ![A broken robot head resting on an apparent canister, wearing a housecoat, while sparks fly between its antennae](/blog/assets/sugar-frontcover.png "If we don't get a take-me-to-your-liter joke out of this...")
 * The header image comes from the book's cover, under the same license as the rest of the book.
 * title: Developer Diary, Emmanuel de Grouchy
 * ![Manny le Grouch or whatever](/blog/assets/Emmanuel-de-Grouchy.png "No, really, I have no interest in him beyond his name...")
 * The header image is [Marchal Grouchy](https://commons.wikimedia.org/wiki/File:EMMANUEL_DE_GROUCHY(1766-1847).jpg) by Georges Rouget, long in the public domain due to expired copyright *if* it ever had a copyright.
 * title: Real Life in Star Trek, The Best of Both Worlds, Part One
 * ![A person wearing headphones and holding a small, old mobile phone in front of their left eye showing a photograph of their left eye, while looking surprised at something](/blog/assets/2421386153_472cdbcb56_o.png "Oh, like he looks any sillier...")
 * The header image is [Cyborg 2.0](https://www.flickr.com/photos/16995443@N03/2421386153) by [Andres Moreno](https://www.flickr.com/photos/1080p/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 10/23 to 10/27
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Duelyst
 * ![A figure in shadow with crackling hands, in a strange landscape of natural arches with rings in them](/blog/assets/duelyst-chapter4-background-2x.jpg "Do you see what Free Culture could look like if we paid artists for things...?")
 * The header image is a chapter heading from the game, released under the same license.
 * title: Developer Diary, ...Victims of Political Repressions
 * ![The Return of the Names ceremony in Moscow, 2016](/blog/assets/Return-of-the-Names-29-10-2016-Moscow-A-01.png "The person to the far left, with a completely different style of jacket and attention drawn out of frame, seems wildly out of place...")
 * The header image is [Return of the Names 29.10.2016 Moscow](https://commons.wikimedia.org/wiki/File:Return_of_the_Names_29.10.2016_Moscow_(A)_01.jpg) by David Krikheli, made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Season 3, TNG
 * ![The Hubble Space Telescope](/blog/assets/GSFC-20171208-Archive-e002151.png "Still scanning the galaxy...in the next generation")
 * The header image is [Hubble Space Telescope](https://images.nasa.gov/details-GSFC_20171208_Archive_e002151) by NASA Goddard, in the public domain by NASA policy.
 * title: Toots 🦣 from 10/30 to 11/03
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Homestuck, part 1
 * ![The word "sorry" scrawled on a red metal door](/blog/assets/28399865610_a0a40d80d0_o.png "What the door said.")
 * The header image is [sorry](https://www.flickr.com/photos/pacifico/28399865610/) by [Etienne Girardet](https://www.flickr.com/photos/pacifico/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Developer Diary, IDPEEWAC
 * ![Timber at Donnelly Mills, Australia](/blog/assets/Timber-Donnelly-Mills-2005-Sean-McClean.png "I apologize if this triggers that condition where people feel uncomfortable looking at arrays of holes")
 * The header image is [Timber Donnelly Mills 2005](https://commons.wikimedia.org/wiki/File:Timber_DonnellyMills2005_SeanMcClean.jpg) by [Sean McClean](https://en.wikipedia.org/wiki/User:SeanMack), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, The Best of Both Worlds Part 2
 * ![A hairy arm with a QWERTY keyboard "embedded" into it](/blog/assets/8589339766_d438da8fff_o.png "Never trust a keyboard where touch-typing requires inviting a friend...")
 * The header image is [Dial U for Unterarm](https://www.flickr.com/photos/genista/8589339766) by [Kai Schreiber](https://www.flickr.com/photos/genista/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 11/06 to 11/10
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Homestuck, part 2
 * ![The word "sorry" scrawled on a red metal door](/blog/assets/28399865610_a0a40d80d0_o.png "What the door said.")
 * The header image is [sorry](https://www.flickr.com/photos/pacifico/28399865610/) by [Etienne Girardet](https://www.flickr.com/photos/pacifico/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Developer Diary, World Kindness Day
 * ![Placard for kindness, at the People's Climate March 2017, in Washington DC](/blog/assets/Peoples-Climate-March-2017-in-Washington-DC-35.png "Now that I look at the painting, it bothers me more than a little that all the detail in the flower didn't extend to the Earth...")
 * The header image is [Placard for kindness, at the People's Climate March 2017, in Washington DC](https://commons.wikimedia.org/wiki/File:People%27s_Climate_March_2017_in_Washington_DC_35.jpg) by [Dcpeopleandeventsof2017](https://commons.wikimedia.org/w/index.php?title=User:Dcpeopleandeventsof2017&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Family
 * ![Small vineyard row spacing in Burgundy](/blog/assets/2759766553_703a10d13a_o.png "I heard about the episode through the...well, you know.")
 * The header image is [DSC01971](https://www.flickr.com/photos/22098403@N00/2759766553) by [Philip Larson](https://www.flickr.com/photos/philiplarson/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 11/13 to 11/17
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Homestuck, part 3
 * ![The word "sorry" scrawled on a red metal door](/blog/assets/28399865610_a0a40d80d0_o.png "What the door said.")
 * The header image is [sorry](https://www.flickr.com/photos/pacifico/28399865610/) by [Etienne Girardet](https://www.flickr.com/photos/pacifico/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Developer Diary, Día de la Revolución
 * ![Children from the Montessori Kindergarten singing "La Cucaracha"](/blog/assets/Feliz-Dia-de-la-Revolucion-Mexico.png "The caption tells me what they sang; I couldn't confirm it beyond that, if anybody needs authentication")
 * The header image is [¡Feliz Dia de la Revolucion Mexico!](https://www.flickr.com/photos/uteart/4119501357/) by [Ute](https://www.flickr.com/photos/uteart/), made available under the terms of the [Creative Commons Attribution 2.0 International](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Brothers
 * ![Two similar-looking young men standing in a natural setting](/blog/assets/person-people-male-youth-stripe-teenager-562551-pxhere.com.png "A rather shocking percentage of images in this vein feature blonde kids...")
 * The header image is [Untitled](https://pxhere.com/en/photo/562551) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🦣 from 11/20 to 11/24
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Some champion eye-rolling, there.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — A Vessel for Offering, part 1
 * ![A plain green book cover with an abstract crown glyph](/blog/assets/vessel-offering.png "Trying not to judge...well, you know")
 * The header image comes from the book's (bland) cover.
 * title: Developer Diary, Berners Street
 * ![A comic characterizing the hoax in action](/blog/assets/Berners-Street-Hoax-caricature.png "That seems like the wrong way to carry a coffin, honestly")
 * The header image is [Berners Street Hoax caricature](https://commons.wikimedia.org/wiki/File:Berners_Street_Hoax_caricature.jpg) by William Heath, long in the public domain due to expired copyright.
 * title: Real Life in Star Trek, Suddenly Human
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🦣 from 11/27 to 12/01
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — A Vessel for Offering, part 2
 * ![A plain green book cover with an abstract crown glyph](/blog/assets/vessel-offering.png "Trying not to judge...well, you know")
 * The header image comes from the book's (bland) cover.
 * title: Developer Diary, Impeachment of Samuel Chase (Intro)
 * ![Samuel Chase face and shoulders](/blog/assets/Samuel-Chase.png "I've seen worse headshots on LinkedIn, honestly")
 * The header image is [Samuel Chase](https://digitalcollections.nypl.org/items/510d47db-9275-a3d9-e040-e00a18064a99) by John Wesley Jarvis and Henry Bryant Hall, long in the public domain due to expired copyright and as a probable work of the United States government.
 * title: Real Life in Star Trek, Remember Me
 * ![A length of string tied around an index finger](/blog/assets/String-around-finger.png "I need to...remember to buy more string, maybe?")
 * The header image is [String around finger](https://commons.wikimedia.org/w/index.php?curid=89217238) by [Rickyukon](https://commons.wikimedia.org/w/index.php?title=User:Rickyukon&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🦣 from 12/04 to 12/08
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "And over here, we have...the Roman numeral for four, I guess.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — A Vessel for Offering, part 3
 * ![A plain green book cover with an abstract crown glyph](/blog/assets/vessel-offering.png "Trying not to judge...well, you know")
 * The header image comes from the book's (bland) cover.
 * title: Developer Diary, Tango Day
 * ![Julio and his orchestra](/blog/assets/Orquesta-tipica-julio-de-caro.png "Only three de Caros in the picture makes for lazy nepotism")
 * The header image is [Orquesta tipica julio de caro](https://commons.wikimedia.org/wiki/File:Orquesta_tipica_julio_de_caro.jpg) by an unknown photographer, in the public domain due to expired copyright.
 * title: Real Life in Star Trek, Legacy
 * ![Two people against a yellow indoor wall awkwardly flashing hand-signals](/blog/assets/2993081172_cc23acb5f0_o.png "I can't take the in-story gangs any more seriously than these two...")
 * The header image is [Gang Signs](https://www.flickr.com/photos/pedestrianrex/2993081172) by [Ian T. McFarland](https://www.flickr.com/photos/pedestrianrex/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 12/11 to 12/15
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "And over here, we have...the Roman numeral for four, I guess.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — A Vessel for Offering, part 4
 * ![A plain green book cover with an abstract crown glyph](/blog/assets/vessel-offering.png "Trying not to judge...well, you know")
 * The header image comes from the book's (bland) cover.
 * title: Developer Diary, International Migrants Day
 * ![Celebrations of the 2017 International Migrants Day in Usera, a neighborhood of Madrid, Spain](/blog/assets/Usera-celebra-el-dia-del-Migrante.png "One day, I'll run a company and make this the dress code...")
 * The header image is [Usera celebra el día del Migrante](https://diario.madrid.es/blog/notas-de-prensa/24352/) by Ayuntamiento de Madrid, made available under the terms of the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license.
 * title: Real Life in Star Trek, Reunion
 * ![Two similarly dressed people sitting closely at a table](/blog/assets/7023667063_f1da940d95_o.png "If you guessed that this episode would have so few specific ideas that I'd have trouble finding a reasonable image, give yourself a gold star...")
 * The header image is [Reunion](https://www.flickr.com/photos/tamaiyuya/7023667063/) by [Yuya Tamai](https://www.flickr.com/photos/tamaiyuya/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 12/18 to 12/22
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Only One
 * ![A panel from the comic, featuring our protagonist walking Daniel past an open closet, from where Mrs. Whisper watches](/blog/assets/only-one-panel.png "Some people keep skeletons in their closets, so...")
 * The header image comes from the book's (bland) cover.
 * title: Ernest Hemingway's Visit from Saint Nicholas
 * ![A black-and-white photograph of a family gathered around a decorated Christmas tree](/blog/assets/11316098253_b94790f70b_k.png "Thematically appropriate, even if I don't know anybody named Keech")
 * The header image is [Christmas at the Keech house, Santa Ana, circa 1920s](https://www.flickr.com/photos/30346812@N07/11316098253) by an unknown (though presumably Keech-related) photographer, made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license, courtesy of the [Orange County Archives](https://www.flickr.com/photos/ocarchives/).  The **New Yorker** published the original version of this story in its December 16, 1927, issue, to which I made a single edit to modernize the spelling; 1927 copyrights expired at the end of 2022.  The *soundtrack* is [Winter Night](https://soundcloud.com/sakuragirl_official/winter-night)---apologies for the SoundCloud link, given what they've had going on---by [Sakura Girl](https://soundcloud.com/sakuragirl_official), made available under the terms of the [Creative Commons Attribution 3.0 Unported](http://creativecommons.org/licenses/by/3.0/) license.
 * title: Developer Diary, Christmas 🎄
 * ![A girl in a red cloak and hat carrying white ice skates over her shoulder in a snowy outdoor area](/blog/assets/outdoor-snow-cold-winter-girl-skate-669822-pxhere.com.png "Smart kid, finding a way to avoid the family fighting over who brought the best dessert")
 * The header image is [untitled](https://pxhere.com/en/photo/669822) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Scheduling Reminders, but Not Too Late
 * ![An analog alarm clock on an end table](/blog/assets/2054250017_3e12f12ae9_o.png "Wake up!")
 * The header image is adapted from [First photo](https://www.flickr.com/photos/nickstenning/2054250017) by [Nick Stenning](https://www.flickr.com/photos/nickstenning/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Real Life in Star Trek, Future Imperfect
 * ![A misty field, tinted orange](/blog/assets/21413914314_963cde50f0_o.png "This covers the song and the gas, I suppose...")
 * The header image is adapted from [Misty morning](https://www.flickr.com/photos/infomastern/21413914314) by [Susanne Nilsson](https://www.flickr.com/photos/infomastern/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 12/25 to 12/29
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Some champion eye-rolling, there.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Admin
 * ![Pixel art of a modern office, where two people look to the left](/blog/assets/admin-screenshot.png "My offices have historically had much less space and lighting, hence people refusing to let go of remote work...")
 * The header image comes from a scene in the game, and so made available under the same license.
 * title: 🔭 Looking Back on 2023
 * ![An emu looking directly at the camera](/blog/assets/bird-animal-wildlife-bouquet-portrait-beak-1202506-pxhere.com.png "Looking back...")
 * The header image is [Untitled](https://pxhere.com/en/photo/1202506) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Diary, New Year's Day
 * ![New Year fireworks behind Mexico City's Angel of Independence](/blog/assets/Mexico-City-2013.png "I don't love fireworks, but I can get behind this view...")
 * The header image is [¡2013!](https://www.flickr.com/photos/eneas/8333128248/) by [Eneas De Troya](https://www.flickr.com/photos/eneas/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Final Mission
 * ![A cave-like space with periodic lights along the left wall](/blog/assets/formation-cave-soil-caving-landform-conduit-762307-pxhere.com.png "Sometimes, a cave doesn't have any metaphorical value...")
 * The header image is [untitled](https://pxhere.com/en/photo/762307) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🦣 from 01/01 to 01/05
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Ada & Zangemann
 * ![A stereotypical older tech CEO bends to peer down at a young girl on a skateboard](/blog/assets/ada-a00.png "If your back does that, see a doctor instead of using it to menace children")
 * The header image is the book's cover, made available under the same terms as the rest of the book.
 * title: 🍾 Happy Belated Calendar-Changing Day, 2024 🎆
 * ![The ebbing tide revealing 2024 carved into the beach](/blog/assets/new-years-eve-party-resolutions-cheers-champagne-midnight-1680604-pxhere.com.png "Hey, did the water do that...?")
 * The header image is adapted from [Untitled](https://pxhere.com/en/photo/1680604) by [sathisht1](https://pxhere.com/en/photographer/4112928), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Diary, The Eighth
 * ![1840s color engraving depicting the Battle of New Orleans at Chalmette](/blog/assets/Battle-of-New-Orleans-Fought-Jan-8th-Currier-Ives.png "I like the two at front-right, who mostly seem politely annoyed that someone has started a war at their place of employment")
 * The header image is [Battle of New Orleans, Fought Jan 8th](https://loc.gov/pictures/resource/cph.3b52587/) by N. Currier, long in the public domain due to expired copyright.
 * title: Real Life in Star Trek, The Loss
 * ![Fingers held in a heart shape in front of a body of water](/blog/assets/9606474355_75ca2b9917_o.png "In essence, Troi loses the flexibility in her fingers to do this, you see...")
 * The header image is [Heart](https://www.flickr.com/photos/winterpearl/9606474355) by [Nature Therapy](https://www.flickr.com/photos/winterpearl/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 01/08 to 01/12
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Space Rover, part 1
 * ![Concept art for the podcast, featuring a Jeep-like vehicle with bright orange pontoons instead of wheels, and a satellite dish mounted to the hood](/blog/assets/SpaceRover-2016ConceptArt-Colourized.png "I've seen worse designs, honestly...")
 * The header image comes from concept art for the podcast, made available---as you can see in the image itself---under both the same GFDL and CC-BY-SA licenses as the podcast itself.
 * title: Diversifying the "Business" of Free Culture
 * ![A logo representing Free Culture, featuring a sketch of three definitely generic modular brick toys stacked as a corner](/blog/assets/117878252_3c00463c07_o.png "Recycled from a previous Free Culture-centric post...")
 * ![An empty room, Superflu, Sophie, and the Flumobile, outlined as vectors](/blog/assets/superflu-playset.svg "You can probably already see where I plan to go with this...")
 * ![Superflu and Sophie pointing at things in the empty study](/blog/assets/superflu-playset-indoor.png "Animators spend so much time working on shadows, and look, we got these free...")
 * ![Superflu and Sophie pointing at the front seat of the overturned Flumobile](/blog/assets/superflu-playset-outdoor.png "Blue ground and sky provided courtesy of my couch")
 * ![The Bonaventure traveling through space](/blog/assets/collectivity-bonaventure.png "A battery and a couple of LEDs could do a lot, here, too.")
 * ![A pattern for an Onyx one-piece garment](/blog/assets/freesewing-onyx.svg "I don't recommend actually using this pattern unless you happen to have the same measurements as Free Sewing sample model Vanessa")
 * I adapted the header image from [FC.o logo](https://www.flickr.com/photos/mllerustad/117878252/in/set-72057594090576065/) by [Karen Rustad Tölva](https://www.flickr.com/photos/mllerustad/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license; they created the original version for [FreeCulture.org](http://freeculture.org/), but you can see as well as I can that the site no longer exists in any meaningful way, so I decided to appropriate it.  The Superflu playset elements come from panels in [**Superflu**, season 1](https://grisebouille.net/superflu-saison-1/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 France](https://creativecommons.org/licenses/by-sa/2.0/fr/) license.  The image of the *Bonaventure* [Galaxy Packs Big Star-Making Punch](https://images.nasa.gov/details-PIA17005) by NASA/JPL-Caltech/STScI/IRAM and [ASC Bonaventure](https://opengameart.org/content/asc-bonaventure) by [ClaudeB](https://opengameart.org/users/claudeb) based on the story, the former in the public domain by NASA policy and the latter released under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license; I release the rendered, exported, and combined version under the latter license.  The "Collectivity music" is [*Alien Spaceship Atmosphere*](https://freepd.com/horror.php) by [Kevin MacLeod](https://incompetech.com/), ceded into the public domain.  The Asendowian fleet uniform comes courtesy of the [Onyx one-piece](https://freesewing.org/docs/designs/onyx) by Thrunic, made available under the terms of the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license.
 * title: Developer Diary, Wikipedia
 * ![Wikipedia's logo, an incomplete sphere made of large, white jigsaw puzzle pieces. Each puzzle piece contains one glyph from a different writing system, with each glyph written in black](/blog/assets/Wikipedia-logo-version-2.png "Has the Wikimedia Foundation manufactured and sold stuffed toys of this globe?  I feel like that'd work far better than Jimbo randomly begging for money every so often...")
 * The header image is [New Wikipedia’s logo](https://commons.wikimedia.org/wiki/File:Wikipedia-logo-v2.svg) by [Nohat](https://en.wikipedia.org/wiki/User:Nohat), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Data's Day
 * ![A wedding in Japan](/blog/assets/18531013834_c3c8426475_o.png "I hesitated on picking this image, but...the episode really does focus on the wedding.")
 * The header image is [JAPAN WEDDING](https://www.flickr.com/photos/sop220/18531013834/) by [古 天熱](https://www.flickr.com/photos/sop220/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 01/15 to 01/19
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Just what the world needed:  A calendar that flips the bird.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Space Rover, part 2
 * ![Concept art for the podcast, featuring a Jeep-like vehicle with bright orange pontoons instead of wheels, and a satellite dish mounted to the hood](/blog/assets/SpaceRover-2016ConceptArt-Colourized.png "I've seen worse designs, honestly...")
 * The header image comes from concept art for the podcast, made available---as you can see in the image itself---under both the same GFDL and CC-BY-SA licenses as the podcast itself.
 * title: The Return of AI Antics
 * ![A clip-art-style image of a robot sitting at a desk writing, with a clock on the wall next to it](/blog/assets/robot-artificial-intelligence-writing-report-table-business-1682522-pxhere.com.png "And it can't even hold a pen properly...")
 * The header image is [robot artificial intelligence](https://pxhere.com/en/photo/1682522) by [Mohamed Hassan](https://pxhere.com/en/photographer/767067), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Diary, January Uprising
 * ![The 1863 painting "Poland Enchained"](/blog/assets/Jan-Matejko-Polonia-1863-Poland-Enchained-MNK-XII-453-National-Museum-Kraków.png "The poster in the background looks like a prop from Doctor Who so that the characters can gasp at their new locale...")
 * The header image is [Polonia 1863 (Poland Enchained)](https://zbiory.mnk.pl/en/search-result/advance/catalog/325087) by Jan Matejko, long in the public domain due to expired copyright.
 * title: Real Life in Star Trek, The Wounded
 * ![Peering through the thick dust clouds of the galactic bulge an international team of astronomers has revealed the unusual mix of stars in the stellar cluster known as Terzan 5](/blog/assets/eso1630b.png "Probably not Cardassian space")
 * The header image is [The unusual cluster Terzan 5](https://www.eso.org/public/images/eso1630b/) by ESO/F. Ferraro, made available under the terms of the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license.
 * title: Toots 🦣 from 01/22 to 01/26
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Gedichte
 * ![A grassy area cut by water](/blog/assets/gedichte-cover.png "Alas, I don't actually know what poem this picture goes with, if any")
 * The header image comes from the book's cover, under the same license as the poems.
 * title: Slipped a Mickey
 * ![A still from Steamboat Willie, featuring Mickey Mouse playing music on household items](/blog/assets/steamboat-willie.png "How extremely 1920s...")
 * ![A poster reading "Celebrity Productions Inc. present a Mickey Mouse Sound Cartoon, A Walt Disney Comic drawn by 'Ub' Iwerks, the World's funniest Cartoon Character, A Sensation in Sound and Synchrony," with Mickey posing to the side in yellow gloves, red shorts, and brown shoes](/blog/assets/mickey-mouse-poster.png "You can find SVG versions of the rodent, too, if you click the link in the credits, below")
 * The header image comes from a frame from *Steamboat Willie*, now in the public domain.  United States Patent #D70,840 expired long ago.  The [Mickey Mouse poster by Ub Iwerks](https://commons.wikimedia.org/wiki/File:Mickey_Mouse_Color_Stock_Poster_%28Celebrity_Productions_era,_1928%29.jpg) either had no copyright protection to begin with or saw its copyright expire at the start of 2024.
 * title: Developer Diary, The Raven
 * ![A person looking at an open window as a raven flies in](/blog/assets/Raven-Manet-C2.png "The story seems significantly less uncanny now that we think of corvids as among the more intelligent animals, no...?")
 * The header image is [an 1875 illustration for a French edition of The Raven](https://memory.loc.gov/cgi-bin/ampage?collId=rbc3&fileName=rbc0001_2003gen33816page.db&recNum=17) by Édouard Manet, long in the domain due to expired copyright.
 * title: Real Life in Star Trek, Devil's Due
 * ![A woman dressed in a devil costume](/blog/assets/283681860_051f3fc865_o.png "Somehow no less credible than a popular show with a significant budget managed...")
 * The header image is [little devil...](https://www.flickr.com/photos/kiry/283681860) by [kirybabe](https://www.flickr.com/photos/kiry/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 01/29 to 02/02
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Pointy Chances
 * ![A retractable ballpoint pen in rainbow colors](/blog/assets/pointy-pen.png "Probably not actually Pointy, but I couldn't find anything more appropriate...")
 * I used [Trajectum white pen, Oude Pekela (2020) 03](https://commons.wikimedia.org/w/index.php?curid=88548975) as the basis for the image, released under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.  I then removed the logo and merged the plastic with a color gradient, and release that result under the same license.
 * title: Developer Diary, Hermitage Museum
 * ![The New Hermitage from the street entrance](/blog/assets/NewHermitage.png "I can fault the Russians for many things in their history, but careful attention to subtle ornamentation does not appear on that list...")
 * The header image is [New Hermitage](https://commons.wikimedia.org/wiki/File:NewHermitage.jpg) by [A. Sdobnikov](https://commons.wikimedia.org/wiki/User:%D0%A1%D0%B4%D0%BE%D0%B1%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2_%D0%90.), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Clues
 * ![Young people dressed in formal clothing with accents in thematic bright colors](/blog/assets/51034384736_edf0c42bdb_o.png "Okay, fine. One plus two plus one...Shut up!  Oh, wrong clues.")
 * The header image is [Clue](https://www.flickr.com/photos/cantanima/51034384736/) by [John Perry](https://www.flickr.com/photos/cantanima/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 02/05 to 02/09
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Moria's Race
 * ![Two cars jockeying for position in a race](/blog/assets/morias-race-screenshot.png "Almost there")
 * The header image comes from a frame of the film, under the same license.
 * title: SUREs — Silly Unsolicited Requests for Exposure
 * ![A pen writing the phrase "I am a writer" on plain paper](/blog/assets/writing-work-hand-white-pen-letter-1085357-pxhere.com.png "I don't even want to get into the probably-written-by-ChatGPT aspect of any of this...")
 * The header image is [untitled](https://pxhere.com/en/photo/1085357) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Diary, Red Hand Day
 * ![A sketch of a child soldier in the Ivory Coast](/blog/assets/Child-soldier-afrika.png "I never really believed the picture-worth-a-thousand-words cliché until seeing this, honestly")
 * The header image is [Child Soldier Afrika](https://commons.wikimedia.org/wiki/File:Child-soldier-afrika.jpg) by [ Gilbert G. Groud](https://commons.wikimedia.org/w/index.php?title=User:GroudGilbert&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Germany](https://creativecommons.org/licenses/by-sa/2.0/de/deed.en) license.
 * title: Real Life in Star Trek, First Contact
 * ![Martial arts training, where a student seems to knock over an instructor with a staff](/blog/assets/black-and-white-white-training-black-monochrome-sports-1101019-pxhere.com.png "Probably not their first contact, per se, but...")
 * The header image is [Untitled](https://pxhere.com/en/photo/1101019) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🦣 from 02/12 to 02/16
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — WNV Universe — Woethief 1
 * ![The book's cover, featuring silhouettes of spiders, bat-wings, a woman, and characters in various action poses](/blog/assets/Woethief-Cover.png "Will this cover prove itself as recounting the plot faster than the book itself?  Time will tell...")
 * The header image is the book's cover, by Autumn Patterson, released under the same terms as the book.
 * title: My Linux Story
 * ![Paper cranes made from paper with the old Ubuntu logo on it](/blog/assets/3671554921_2a5a24d6ac_o.png "How has nobody used this for an autonomous vehicle post?")
 * The header image is [Ubuntu Cranes](https://www.flickr.com/photos/duststorm01/3671554921) by [Duststorm](https://www.flickr.com/photos/duststorm01/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Diary, Eruption of Huaynaputina
 * ![Colca Canyon, the remnants of Huaynaputina](/blog/assets/3587376222_a401a77c8f_o.png "What a difference four centuries makes, right?")
 * The header image is [Hualca Hualca](https://www.flickr.com/photos/33037982@N04/3587376222/) by [Leonora (Ellie) Enking](https://www.flickr.com/photos/33037982@N04/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/deed.en) license.
 * title: Real Life in Star Trek, Galaxy's Child
 * ![A holographic arm hovering in an empty space](/blog/assets/10936215935_cbe5009802_o.png "Don't touch it; you don't know where that finger has been...")
 * The header image is [Hologram](https://www.flickr.com/photos/57460471@N06/10936215935) by [m_hweldon](https://www.flickr.com/photos/57460471@N06/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 02/19 to 02/23
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Just what the world needed:  A calendar that flips the bird.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — WNV Universe — Woethief 2
 * ![The book's cover, featuring silhouettes of spiders, bat-wings, a woman, and characters in various action poses](/blog/assets/Woethief-Cover.png "Will this cover prove itself as recounting the plot faster than the book itself?  Time will tell...")
 * The header image is the book's cover, by Autumn Patterson, released under the same terms as the book.
 * title: AI Doom Sounds So Familiar
 * ![Melting snowmen in front of a house](/blog/assets/52657413123_c3b64e2a59_o.png "As an artificial water-crystal hominid, I don't have the ability to access real-time information about the weather...")
 * The header image is [Melting snowman](https://www.flickr.com/photos/58411470@N00/52657413123) by [Tristan Schmurr](https://www.flickr.com/photos/kewl/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Diary, Beach Pneumatic Transit
 * ![Illustrated description of the Broadway underground railway](/blog/assets/beach-pneumatic-transit.png "Granted, it does look nicer than any MTA subway car that I've ridden on.")
 * The header image is [Illustrated description of the Broadway underground railway](https://digitalcollections.nypl.org/items/510d47dc-9aad-a3d9-e040-e00a18064a99) by the New York Parcel Dispatch Company, long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, Night Terrors
 * ![A shadowy figure standing in a foggy field](/blog/assets/man-landscape-tree-nature-silhouette-snow-1332197-pxhere.com.png "And then he starts screaming out the Fibonacci sequence...")
 * The header image is [untitled](https://pxhere.com/en/photo/1332197) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🦣 from 02/26 to 03/01
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — WNV Universe — Woethief 3
 * ![The book's cover, featuring silhouettes of spiders, bat-wings, a woman, and characters in various action poses](/blog/assets/Woethief-Cover.png "Will this cover prove itself as recounting the plot faster than the book itself?  Time will tell...")
 * The header image is the book's cover, by Autumn Patterson, released under the same terms as the book.
 * title: The Rise and (Likely) Fall of Anti-AI Licenses
 * ![People milling around and talking, behind a projected glass screen that obscures them](/blog/assets/40466252685_3701e77720_o.png "Something about the light fixtures and air vent delights me, for some reason...")
 * The header image is [Symposium Cisco Ecole Polytechnique 9-10 April 2018 Artificial Intelligence & Cybersecurity](https://www.flickr.com/photos/117994717@N06/40466252685) by [Ecole polytechnique](https://www.flickr.com/photos/117994717@N06/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Developer Diary, United States Constitution
 * ![The famous painting showing the signing of the Constitution](/blog/assets/us-constitution-signing.png "Also shown:  Too many flags on the rear wall.")
 * The header image is [Scene at the Signing of the Constitution of the United States](https://www.aoc.gov/explore-capitol-campus/art/signing-constitution) by Howard Chandler Christy, long in the public domain due to an expired copyright and/or as a work of the United States government.
 * title: Real Life in Star Trek, Identity Crisis
 * ![A chameleon lying on a branch](/blog/assets/3260943249_eafed154f1_o.png "You had to know that you'd either get this or a black-light poster...")
 * The header image is [Chameleon](https://www.flickr.com/photos/abolotnov/3260943249) by [Alexander Bolotnov](https://www.flickr.com/photos/abolotnov/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 03/04 to 03/08
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "And over here, we have...the Roman numeral for four, I guess.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — WNV Universe — Other
 * ![The book's covering, featuring a female silhouette set against a larger tusked silhouette, in a grassy field](/blog/assets/true-loves-kiss-cover.png "The series definitely has a specific aesthetic")
 * The header image is the book's cover, by Autumn Patterson, released under the same terms as the book.
 * title: Developer Diary, Roxy Theater
 * ![The cover of a weekly magazine dedicated to the Roxy, declaring it "the Cathedral of the Motion Picture"](/blog/assets/roxy-theater-magazine.png "...None of the scenes look like things that would work particularly well in silent film, do they?")
 * The header image is [Roxy Theater NY Weekly Review March 10, 1928](https://commons.wikimedia.org/wiki/File:Roxy_Theater_NY_Weekly_Review_March_10,_1928.jpg), long in the public domain due to an expired copyright.
 * title: Trying on the Indie Web, Part 1
 * ![A spider web with dew, in front of presumed concrete blocks](/blog/assets/5395977052_5e9f442ece_o.png "We named the SPIDER Indiana...")
 *  > It frustrates me to see social media constantly destroyed by capitalism. Anyway, come follow me at ██████@bsky.social![^1]
 * ![The setup-login page for Selfauth](/blog/assets/selfauth-setup-login.png "I think that I know these...")
 * The header image is [Web](https://www.flickr.com/photos/35521221@N05/5395977052) by [Martyn Wright](https://www.flickr.com/photos/martyn404/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, The Nth Degree
 * ![The "Cosmic Cliffs" in the Carina Nebula](/blog/assets/cosmic-cliffs.png "Nothing to do with the episode, other than...space")
 * The header image is [James Webb Space Telescope NIRCam Image of the “Cosmic Cliffs” in Carina Nebula](https://images.nasa.gov/details/carina_nebula) by NASA ESA CSA STScI, in the public domain by NASA policy.
 * title: Toots 🦣 from 03/11 to 03/15
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "And over here, we have...the Roman numeral for four, I guess.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Cauac Ox
 * ![The album art, featuring various intertwined fingers](/blog/assets/harmonique-cauac-ox.png "It always feels awkward when we cover an old enough project that I need to significantly increase the size of the header image...")
 * The header image is the album's cover, seemingly released under the same terms as the music.
 * title: Ruining Chivalry and Other Fun Diversions
 * ![A knight riding on a horse, with sword drawn](/blog/assets/grass-horse-stallion-mane-helmet-knight-1413030-pxhere.com.png "Why won't you let me open that door for you?")
 * The header image is [untitled](https://pxhere.com/en/photo/1413030) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Diary, Paris Commune
 * ![A barricade protecting the Paris Commune](/blog/assets/Paris-Commune-barricade.png "Do you hear the people---no?  Another different aborted revolution...?")
 * The header image is [Barricade 18 March 1871](https://commons.wikimedia.org/wiki/File:Barricade18March1871.jpg) by an unknown photographer, long in the public domain due to an expired copyright.
 * title: Deeper in the Indie Web
 * ![A spider web with dew, in front of presumed concrete blocks](/blog/assets/5395977052_5e9f442ece_o.png "I seriously thought about getting a different web image for every post, but this will do fine until I get tired of it...")
 * The header image is [Web](https://www.flickr.com/photos/35521221@N05/5395977052) by [Martyn Wright](https://www.flickr.com/photos/martyn404/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Qpid
 * ![The cast of an 1885 school production of a Robin Hood opera](/blog/assets/Robin-Hood-Opera-Wellingborough.png "Also not Merry Men")
 * The header image is [Robin Hood Opera by Colin McAlpin at Wellingborough School cast photo](https://commons.wikimedia.org/wiki/File:Robin_Hood_Opera_by_Colin_McAlpin_at_Wellingborough_School_cast_photo.jpg) by an unknown photographer, long in the public domain from an expired copyright.
 * title: Toots 🦣 from 03/18 to 03/22
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Some champion eye-rolling, there.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Airlock Bound, part 1
 * ![The book's cover, featuring a person with their head engulfed in blue flames, wielding a sword](/blog/assets/airlock-bound.png "That jacket looks inconvenient for fighting, no...?")
 * The header image is the book's cover, released under the same terms as the book.
 * title: Developer Diary, Transatlantic Slave Trade Remembrance
 * ![A painting of a slave market in Brazil](/blog/assets/slave-market-Johann-Moritz-Rugendas.png "Oh, I suppose that you would've gone with a United Nations official under arrest?")
 * The header image is [Marché aux Negres by Johann Moritz Rugendas](https://commons.wikimedia.org/wiki/File:March%C3%A9_aux_Negres_by_Johann_Moritz_Rugendas_2.jpg), long in the public domain due to an expired copyright.
 * title: Caught in the Indie Web
 * ![A spider web with dew, in front of presumed concrete blocks](/blog/assets/5395977052_5e9f442ece_o.png "I seriously thought about getting a different web image for every post, but this will do fine until I get tired of it...")
 * title: The Title
 * title: The Title of the Responded-to Post
 * The header image is [Web](https://www.flickr.com/photos/35521221@N05/5395977052) by [Martyn Wright](https://www.flickr.com/photos/martyn404/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, The Drumhead
 * ![A field court-martial and execution during the Thirty Years' War, with a drum used as a table](/blog/assets/Jacques-callot-miseres-guerre.png "I expected a more figurative drum, honestly")
 * The header image is [Les Misères de la guerre. 11. Les Pendus](https://www.wga.hu/frames-e.html?/html/c/callot/misere.html) by Jacques Callot, long in the public domain due to expired copyrights.
 * title: Toots 🦣 from 03/25 to 03/29
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Airlock Bound, part 2
 * ![A panel from the comic, featuring Gale repairing a domed roof](/blog/assets/airlock-bound-e4-p5-p6.png "OSHA would not permit this use of a ladder...")
 * The header image is a panel from the comic.
 * title: Developer Diary, (Ugh) April Fool's Day
 * ![Dutch officials pretending that a Moai statue washed up on shore](/blog/assets/Aprilgrap-1962.png "I guess that we laugh, because usually Europeans end up with artifacts like that by pillaging foreign holy sites...")
 * The header image is [Beeld aangespoeld op Zandvoortse strand](https://www.nationaalarchief.nl/onderzoeken/fotocollectie/a9fffa62-d0b4-102d-bcf8-003048976d84), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Spinning(?) the Indie Web
 * ![A spider web with dew, in front of presumed concrete blocks](/blog/assets/5395977052_5e9f442ece_o.png "I seriously thought about getting a different web image for every post, but this will do fine until I get tired of it...")
 * The header image is [Web](https://www.flickr.com/photos/35521221@N05/5395977052) by [Martyn Wright](https://www.flickr.com/photos/martyn404/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Half A Life
 * ![An older person, largely ignored](/blog/assets/52434193738_080ba34b7b_o.png "Death comes for us all...after it buys some new sneakers, though, because these have all but worn out.")
 * The header image is [Through the ages](https://www.flickr.com/photos/131814204@N04/52434193738) by [Gauthier DELECROIX - 郭天](https://www.flickr.com/photos/gauthierdelecroix/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 04/01 to 04/05
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — In Unexpected Places
 * ![Two drone-like objects showing different perspectives on a computer-generated eyeball as they circle it](/blog/assets/in-unexpected-places.png "Having something else look at the eye makes a mildly interesting twist")
 * The header image is a frame from the film, under its same license.
 * title: Topical Grab Bag
 * ![The characters Pepper and Carrot standing at an eight-way intersection of paths, trying to decide which way to go](/blog/assets/p-c-crossroads.png "Not only does it come from one of the items mentioned below, but the scene makes a great metaphor for the meandering post, here...")
 * The header image comes from the untitled header image of [*The end of Pepper & Carrot and my next project*](https://www.davidrevoy.com/article1020/the-end-of-peppercarrot-and-my-next-project) by David Revoy, made available under the terms of the [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) license.
 * title: Developer Diary, International Romani Day
 * ![The flag of the Romani people](/blog/assets/Flag-of-the-Romani-people.svg "I don't love the clashing colors, but the flower/wheel design actually impresses me")
 * The header image is [Flag of the Romani people](https://en.wikipedia.org/wiki/File:Flag_of_the_Romani_people.svg) by [AdiJapan](https://commons.wikimedia.org/wiki/User:AdiJapan), based on the design (allegedly) by [Gheorghe A. Lăzăreanu-Lăzurică](https://en.wikipedia.org/wiki/Gheorghe_A._L%C4%83z%C4%83reanu-L%C4%83zuric%C4%83), and released into the public domain.
 * title: Real Life in Star Trek, The Host
 * ![Picture of a slug in the genus Ambigolimax. Photo taken in Fremont, CA, USA](/blog/assets/Ambigolimax.png "Do you think that I could hitch a ride to a meeting?  Maybe you knew my father...")
 * The header image is [Ambigolimax](https://commons.wikimedia.org/wiki/File:Ambigolimax.jpg) by [Sanjay Acharya](https://commons.wikimedia.org/wiki/User:Sanjay_ach), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Toots 🦣 from 04/08 to 04/12
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Just what the world needed:  A calendar that flips the bird.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Noir & Blanc, part 1
 * ![The book's cover, featuring the white silhouette of a pistol against a black background](/blog/assets/blanc-noir.png "Maybe not the most auspicious start, but we've definitely had worse...")
 * The header image is the book's cover, released under the same terms as the book.
 * title: Short Fiction — 'Neath a Sunless Sky
 * ![The April 2024 solar eclipse from the Indianapolis Speedway](/blog/assets/NHQ202404080308.png "Wait until the animals start acting weird, the wine-guzzling neighbor covering their face with a colander tells me...")
 * The header image is [2024 Total Solar Eclipse](https://images.nasa.gov/details/NHQ202404080308) by NASA/Joel Kowsky, placed in the public domain by NASA policy.
 * title: Developer Diary, Universal Day of Culture
 * ![The "Pax Cultura" emblem defined by the Roerich Pact, consisting of three red balls in a circle](/blog/assets/Roerich-symbol-bold-red.svg "It feels like a design like this should see more use, no?")
 * The header image is [Roerich symbol](https://commons.wikimedia.org/wiki/File:Roerich_symbol_(bold,_red).svg) by [Kwamikagami](https://commons.wikimedia.org/wiki/User:Kwamikagami), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, The Mind's Eye
 * ![Part of the movie poster for 1962's The Manchurian Candidate, featuring sketches of the main character's arc, with text to the left warning against coming in late](/blog/assets/The-Manchurian-Candidate-1962-poster.png "I'd actually love to see an animated Star Trek show with this sketchy kind of style, now that I think about it")
 * The header image is [The Manchurian Candidate, United Artists, 1962, One Sheet](https://movieposters.ha.com/itm/thriller/the-manchurian-candidate-united-artists-1962-one-sheet-27-x-41-thriller/a/161831-51209.s) by an uncredited artist from United Artists, in the public domain due to lapsed and un-renewed copyright.
 * title: Toots 🦣 from 04/15 to 04/19
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Wait a second, are these all the same day of the week?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Noir & Blanc, part 2
 * ![The book's cover, featuring the white silhouette of a pistol against a black background](/blog/assets/blanc-noir.png "Maybe not the most auspicious start, but we've definitely had worse...")
 * The header image is the book's cover, released under the same terms as the book.
 * title: Developer Diary, Earth Day
 * ![The unofficial Earth Day flag, a blue rectangle with a satellite picture of the Earth centered, focused on Southern Africa](/blog/assets/Earth-Day-flag.png "Could you imagine walking up to a modern Betsy Ross and asking them to embroider this for you?")
 * The header image is [Earth flag](https://commons.wikimedia.org/wiki/File:Earth_flag_PD.jpg) by John McConnell (sort of), in the public domain for its utilitarian design, per *Earth Flag Ltd. v. Alamo Flag Co*.  No, really, we have a court case to say that you don't get a copyright for slapping NASA images onto a background, because the people who think of themselves as owning Earth Day tried to sue a company for making flags.
 * title: Real Life in Star Trek, In Theory
 * ![Two people sitting in a restaurant booth near the window, one lazily looking at their phone](/blog/assets/14579988255_9c24a7a3db_o.png "I feel you, lefty.  This episode gives me that exact I-could-have-done-anything-else-with-my-hour feeling...")
 * The header image is [An exiting Date](https://www.flickr.com/photos/96106108@N07/14579988255) by [Dragan](https://www.flickr.com/photos/draganbrankovic/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 04/22 to 04/26
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Noir & Blanc, part 3
 * ![The book's cover, featuring the white silhouette of a pistol against a black background](/blog/assets/blanc-noir.png "Maybe not the most auspicious start, but we've definitely had worse...")
 * The header image is the book's cover, released under the same terms as the book.
 * title: Developer Diary, International Dance Day
 * ![Amoke Project (modern dance) at Trout Lake on a not too hot not too humid sunny day](/blog/assets/52175397426_1b9542b090_o.png "If you follow the credits link over to Flickr, you'll find enough pictures of the scene over time that you could probably reconstruct a lot of the routine")
 * The header image is [RT509423](https://www.flickr.com/photos/roland/52174363417/in/photostream/) by [Ronald Tanglao](https://www.flickr.com/photos/roland/), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Real Life in Star Trek, Redemption
 * ![An American Civil War reenactor sitting against a wood-pile to read a letter](/blog/assets/37270938511_c1d1650f0e_o.png "Dear Mom, while they call this a civil war, we seem to spend a lot of attention on the pretentious bald fellow telling us that he may not involve himself in our affairs, while lecturing us about our culture and how the Romulans may have created Poland Spring water as a trap.")
 * The header image is [Glass Half-Plates Civil War, Kelham Hall.2017](https://www.flickr.com/photos/84313222@N03/37270938511) by [Ikonta Bloke](https://www.flickr.com/photos/84313222@N03/), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🦣 from 04/29 to 05/03
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — The Lost Universe
 * ![A serpentine dragon enveloping the Hubble Space Telescope as its mouth catches up to its tail](/blog/assets/lost-universe-cover.png "Starting with images like this, it seems like a massive missed opportunity to not tie this in with world-serpent myths from around the world, but NASA rarely consults me on the important issues...")
 * The header image is the book's cover, released under the same terms as the book.
 * title: Developer Diary, Hıdırellez
 * ![Celebration of Hydyrlez in the Bakhchisarai region of the Republic of Crimea](/blog/assets/5ccd9a194a62b5-33270460-IMG-0735.png "Yeah, I brought Crimea into this.  What of it...?")
 * The header image is [Hıdırellez in Crimea](https://glava.rk.gov.ru/articles/f66d6f79-9571-4d35-8ca8-8be5da1153fc) by Правительство Республики Крым (the Crimean government), made available under the terms of the [Creative Commons Attribution 4.0 Universal](https://creativecommons.org/licenses/by/4.0/).
 * title: Real Life in Star Trek, Season 4, TNG
 * ![The Hubble Space Telescope](/blog/assets/GSFC-20171208-Archive-e002151.png "Still scanning the galaxy...in the next generation")
 * The header image is [Hubble Space Telescope](https://images.nasa.gov/details-GSFC_20171208_Archive_e002151) by NASA Goddard, in the public domain by NASA policy.
 * title: Toots 🦣 from 05/06 to 05/10
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Wait a second, are these all the same day of the week?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Cistemfailure
 * ![The album art, featuring a banjo crossed with bolt-cutters, in front of the band's name in a rough script](/blog/assets/a2784444150_10.png "This seems like only the second project that centers a banjo, and I don't know whether than qualifies as weird because it seems like too few or too many...")
 * The header image is the album art, presumably released under the same terms as the book.
 * title: Developer Diary, Amerigo Vespucci
 * ![A bizarre propaganda piece, showing Vespucci waking a sleeping America from her hammock](/blog/assets/Stradanus-America.png "As a reminder, the Americas at this time included the largest and most populous cities in the world, but sure, depict them as someone taking a nap who needs a white guy with a padded résumé to rouse her...")
 * The header image is [Allegory of America, from New Inventions of Modern Times](https://www.metmuseum.org/art/collection/search/659655) by Jan "Stradanus" van der Straet, long in the public domain, assuming that it ever had a copyright.
 * title: Real Life in Star Trek, Redemption Part 2
 * ![An American Civil War reenactor sitting against a wood-pile to read a letter](/blog/assets/37270938511_c1d1650f0e_o.png "Dear Mom, while they call this a civil war, we seem to spend a lot of attention on the pretentious bald fellow telling us that he may not involve himself in our affairs, while lecturing us about our culture and how the Romulans may have created Poland Spring water as a trap.")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🦣 from 05/13 to 05/17
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Wait a second, are these all the same day of the week?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Catburglar
 * ![Cynth, twirling a keychain in a hallway](/blog/assets/catburglar.png "You can snag the full animation of the key-twirl in the repository")
 * The header image comes from the title screen, made available under the same terms as the rest of the game.
 * title: Developer Diary, World Metrology Day
 * ![Le Grand-K, which you shouldn't confuse with breakfast cereal Special K, the International Prototype Kilogram](/blog/assets/le-grand-k.png "Tonight, we have platinum-iridium kilogram under glass...under more glass...under even more glass")
 * The header image is [The International Prototype Kilogram, aka Le Grand K](https://www.nist.gov/si-redefinition/kilogram-introduction) by the [International Bureau of Weights and Measures](https://www.bipm.org/en/about-us/), made available under the terms of the [Creative Commons Attribution 3.0 IGO](https://creativecommons.org/licenses/by/3.0/igo/) license.
 * title: Real Life in Star Trek, Darmok
 * ![A sign for Tanagra Esthetique](/blog/assets/28148581579_4a749ac4ac_o.png "Jalad, his credit card maxed-out.")
 * The header image is [Darmok and Jalad...](https://www.flickr.com/photos/30571787@N00/28148581579) by [Luigi Rosa](https://www.flickr.com/photos/lrosa/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 05/20 to 05/24
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Unprintable
 * ![A line of ramshackle buildings in silhouette against a vividly colored sky dotted with ambiguous black and white flecks. A large black anomaly looms overhead](/blog/assets/Skyline.png "OK, a bit more than the two poems...")
 * The header image is [Falling Skyline](https://free.mortalityplays.com/falling-skyline-art/) by the same author, similarly released into the public domain.
 * title: An Army of Roger Cormans
 * ![A shoddy-looking science fiction boat sailing through caves](/blog/assets/VoyagetothePlanetofPrehistoricWomen.webm.480p.vp9.png "Do you really think that what you have in mind will turn out worse than this...?")
 * The header image comes from [**Voyage to the Planet of Prehistoric Women**](https://commons.wikimedia.org/wiki/File:VoyagetothePlanetofPrehistoricWomen.webm) directed by Roger Corman, in the public domain due to an unrenewed copyright.
 * title: Developer Diary, Memorial Day
 * ![1944 Memorial Day Parade, Washington DC](/blog/assets/8d05028v.png "I actually used this image for the 2021 Memorial Day post, but like it mostly because the description makes it sound like it exists accidentally, from an intermediary roll of film")
 * The header image is [Washington, D.C. Parade on Memorial Day](https://www.loc.gov/resource/fsa.8d05028/) by Royden Dixon, released into the public domain as a work of the Library of Congress Farm Security Administration.
 * title: Real Life in Star Trek, Ensign Ro
 * ![Mae La Refugee Camp](/blog/assets/440523699_c075bc61f8_o.png "Unfortunately, this sort of sight probably looks increasingly familiar.")
 * The header image is [Mae La Refugee Camp](https://www.flickr.com/photos/44124329962@N01/440523699) by [Mikhail Esteves](https://www.flickr.com/photos/jackol/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 05/27 to 05/31
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — There Is No Antimemetics Division
 * ![Marion preparing to give El an injection](/blog/assets/anti-memetics-ep2-00-45.png "The quieter scenes really make this")
 * The header image is [Falling Skyline](https://free.mortalityplays.com/falling-skyline-art/) by the same author, similarly released into the public domain.
 * title: The Free Culture Movement
 * ![A logo representing Free Culture, featuring a sketch of three definitely generic modular brick toys stacked as a corner](/blog/assets/117878252_3c00463c07_o.png "One day, I'll try to come up with something of my own to illustrate these sorts of posts, but for now, I'll keep hammering the logo for a defunct student organization...")
 * I adapted the header image from [FC.o logo](https://www.flickr.com/photos/mllerustad/117878252/in/set-72057594090576065/) by [Karen Rustad Tölva](https://www.flickr.com/photos/mllerustad/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.  They created the original version for [FreeCulture.org](http://freeculture.org/), but you can see as well as I can that the site no longer exists in any meaningful way, so I decided to appropriate it...
 * title: Developer Diary, World Bicycle Day
 * ![The Vice President, Shri M. Venkaiah Naidu and other officials interacting with the students participating in the Bicycle Rally, on the occasion of World Bicycle Day 2018, in New Delhi on June 03, 2018](/blog/assets/World-Bicycle-Day-2018-New-Delhi.png "Note the large gap pushed through between the two sets of riders, so that the official photographers could completely ignore the children and get pictures of the officials...")
 * The header image is [The Vice President, Shri M. Venkaiah Naidu interacting with the students participating in the Bicycle Rally, on the occasion of World Bicycle Day 2018, in New Delhi](https://commons.wikimedia.org/wiki/File:The_Vice_President,_Shri_M._Venkaiah_Naidu_interacting_with_the_students_participating_in_the_Bicycle_Rally,_on_the_occasion_of_World_Bicycle_Day_2018,_in_New_Delhi.JPG) by the Indian Vice President's Secretariat, made available under the terms of the [Government Open Data License - India](https://data.gov.in/sites/default/files/Gazette_Notification_OGDL.pdf).
 * title: Real Life in Star Trek, Silicon Avatar
 * ![A magnified snowflake](/blog/assets/Snowflake-Detail.png "Yes, I used the same image the last time the thing showed up.")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🦣 from 06/03 to 06/07
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "And over here, we have...the Roman numeral for four, I guess.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Nevada, part 1
 * ![Pink flowers](/blog/assets/5895557002_5a15b12c10_o.png "If this looks like a placeholder image...well, you've probably made worse assessments.")
 * The header image is [FLOWER 03 - PAINTBRUSH, sp (6-16-11) northeast Nevada](https://www.flickr.com/photos/8101022@N05/5895557002) by [Alan Schmierer](https://www.flickr.com/photos/sloalan/), released into the public domain by the photographer.  The artist released the actual cover under a non-commercial license.
 * title: Free Culture News
 * ![A news set with the logo representing Free Culture, featuring a sketch of three definitely generic modular brick toys stacked as a corner, showing on screens behind the anchors](/blog/assets/free-culture-news.png "I may have put more work into this than warranted, and still did a mediocre job on the screen edges...")
 * ![A stereotypical older tech CEO bends to peer down at a young girl on a skateboard](/blog/assets/ada-a00.png "I'll say it again:  If your back does that, go see a doctor, instead of using it to menace children")
 * ![A screenshot of the game, showing the player character fighting zombies in a park](/blog/assets/cdda-0.h.png "Same cheating as in my original post on the game, so that I don't need to worry about the actual hazards in exploring")
 * ![The Fodongo logo](/blog/assets/Fodongo-title.png "If it differs at all between issues, this particular version comes from the sixth.")
 * ![The second episode of Mini Fantasy Theater, featuring someone summoning a djinn to gain an amoral monster with precision, night vision, and claws, and receiving a kitten](/blog/assets/2024-04-26-monster.png "I always felt like cats had a strong moral sense, personally, but most of it ties to getting attention...")
 * ![The mostly full desktop splash screen of Snowbound Blood](/blog/assets/sb-main-menu-v12.png "Secily has had a lot to do...")
 * The header image combines [AFN Berlin TV News Set](https://commons.wikimedia.org/w/index.php?curid=48561756) by Peter Dollé---made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license---with my existing adaptation of [FC.o logo](https://www.flickr.com/photos/mllerustad/117878252/in/set-72057594090576065/) by [Karen Rustad Tölva](https://www.flickr.com/photos/mllerustad/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.  The cover of **Ada & Zangemann** has been made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.  I took the screenshot for **Cataclysm:  Dark Days Ahead**, and presume it to fall under the same terms as the game.  The Fodongo logo comes from the [sixth zine issue's](https://fodongo.jectoons.net/2024/04/22/issue-6-out-now/) cover, made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.  The comic is [*Monster*](https://www.davidrevoy.com/article1025/monster), by [David Revoy](https://www.davidrevoy.com/) made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.  The **Snowbound Blood** splash screen comes from the game assets, made available under the terms of the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license.
 * title: Developer Diary, Art Nouveau Day
 * ![Gran Vitral Tiffany del Hotel Ciudad de Mexico](/blog/assets/80130778-panoramio.png "I spent far too long deciding which appropriately licensed picture I wanted to use to represent the Art Nouveau movement, suggesting that maybe I like the style a bit...")
 * The header image is [Gran Vitral Tiffany del Hotel Ciudad de Mexico](https://web.archive.org/web/20161024202755/http://www.panoramio.com/photo/80130778) by [Octavio Alonso Maya Castro](https://web.archive.org/web/20161024202757/http://www.panoramio.com/user/5358119?with_photo_id=80130778), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Disaster
 * ![An old elevator shaft](/blog/assets/6866056517_713988db34_o.png "I'll go Picard one better and sing Alouette as I climb...")
 * The header image is [Old Elevator Shaft](https://www.flickr.com/photos/54765780@N07/6866056517) by [Brian Pirie](https://www.flickr.com/photos/sensinct/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 06/10 to 06/14
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Nevada, part 2
 * ![Pink flowers](/blog/assets/5895557002_5a15b12c10_o.png "If this looks like a placeholder image...well, you've probably made worse assessments.")
 * The header image is [FLOWER 03 - PAINTBRUSH, sp (6-16-11) northeast Nevada](https://www.flickr.com/photos/8101022@N05/5895557002) by [Alan Schmierer](https://www.flickr.com/photos/sloalan/), released into the public domain by the photographer.  The artist released the actual cover under a non-commercial license.
 * title: Developer Diary, Desertification and Drought
 * ![Corn shows the effect of drought in Texas on Aug. 20, 2013](/blog/assets/12308593123_c55785d458_h.png "As high as an extremely tired elephant's eye, I guess")
 * The header image is [20130821-OC-RBN-2764](https://www.flickr.com/photos/usdagov/12308593123) by Bob Nichols, released into the public domain as a work of the United States Department of Agriculture.
 * title: Real Life in Star Trek, The Game
 * ![An auditorium of people, most of them holding boxes in front of their faces with eyes painted on the outer wall](/blog/assets/51640224895_8e548ce176_o.png "Fear not, Wesley shall save us from this scourge... 🙄")
 * The header image is [Shuffling through the METAVERSE](https://www.flickr.com/photos/44124348109@N01/51640224895) by [Steve Jurvetson](https://www.flickr.com/photos/jurvetson/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 06/17 to 06/21
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Just what the world needed:  A calendar that flips the bird.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Nevada, part 3
 * ![Pink flowers](/blog/assets/5895557002_5a15b12c10_o.png "If this looks like a placeholder image...well, you've probably made worse assessments.")
 * The header image is [FLOWER 03 - PAINTBRUSH, sp (6-16-11) northeast Nevada](https://www.flickr.com/photos/8101022@N05/5895557002) by [Alan Schmierer](https://www.flickr.com/photos/sloalan/), released into the public domain by the photographer.  The artist released the actual cover under a non-commercial license.
 * title: Please Come Back...
 * ![A person facing a parted curtain revealing a wall of candid photographs](/blog/assets/4545786174_cca8d2de24_o.png "Would our going into a jealous rage win you back? [Yes] [Maybe Later]")
 * ![A tablet-like computer at an odd angle](/blog/assets/cutiepi-enclosure.png "Your computer similarly does not have any of our content playing")
 * ![A tablet-like computer at an odd angle](/blog/assets/cutiepi-enclosure.png "Our marketing team assured us that this image would make you feel regret")
 * ![Bobby Make-Believe as a Cowhand](/blog/assets/bobby-make-believe-cowboy.png "Bobby has no fear of missing out, because he still subscribes...")
 * ![A photograph of the Falcon-9 rocket rolling out, where someone has vandalized it with the phrase Bad Streaming Service and a frowning face, along the forward capsule](/blog/assets/Falcon-9-rollout-with-TurkmenAlem52E-MonacoSAT-to-SLC-40-17108097439.png "Who could have done this?  We don't even know where to buy or rent scaffolding large enough to take on a job like that, unless that receipt for 25,000₷ sitting on top of the garbage, this morning, suggests any clues.  Nah...")
 * The header image is [Stalkers](https://www.flickr.com/photos/37218489@N06/4545786174) by [Danielle Helm](https://www.flickr.com/photos/daniellehelm/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.  The tablet image comes from the [Cutie Pi Enclosure](https://github.com/cutiepi-io/cutiepi-enclosure), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.  The vandalized rocket is based on [Falcon 9 rollout with TurkmenAlem52E-MonacoSAT to SLC-40](https://commons.wikimedia.org/wiki/File:Falcon_9_rollout_with_TurkmenAlem52E-MonacoSAT_to_SLC-40_%2817108097439%29.jpg) by SpaceX, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).
 * title: Developer Diary, Inti Raymi
 * ![A 2009 Inti Raymi celebration at Sacsayhuamán, outside Cusco](/blog/assets/83-Cuzco-Juin-2009.png "I imagine that the Incas didn't wear quite as many baseball caps, but otherwise...")
 * The header image is [83 - Cuzco - Juin 2009](https://commons.wikimedia.org/wiki/File:83_-_Cuzco_-_Juin_2009.jpg) by [Martin St-Amant](https://commons.wikimedia.org/wiki/User:S23678), made available under the terms of the [Creative Commons Attribution 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Real Life in Star Trek, Unification Part 1
 * ![A monument featuring children pushing two hemispheres into a globe](/blog/assets/3377523551_a20576f4ef_o.png "I like that the artist realized that the exertion would make more sense if the subjects needed to climb steps as they did the work...")
 * The header image is [unification statue 01](https://www.flickr.com/photos/friarsbalsam/3377523551/)[^2] by [Christopher John SSF](https://www.flickr.com/photos/friarsbalsam/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 06/24 to 06/28
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Nevada, part 4
 * ![Pink flowers](/blog/assets/5895557002_5a15b12c10_o.png "If this looks like a placeholder image...well, you've probably made worse assessments.")
 * The header image is [FLOWER 03 - PAINTBRUSH, sp (6-16-11) northeast Nevada](https://www.flickr.com/photos/8101022@N05/5895557002) by [Alan Schmierer](https://www.flickr.com/photos/sloalan/), released into the public domain by the photographer.  The artist released the actual cover under a non-commercial license.
 * title: Developer Diary, Canada Day
 * ![Canada Day fireworks on Parliament Hill, Ottawa](/blog/assets/27768773120_7f78300786_b.png "I still don't enjoy fireworks, Canada sits so far away from me that, if I need to hear them as I go to sleep, then somebody has probably declared war...")
 * ![Fictopedia's logo, the name of the site in brackets, resting at an angle](/blog/assets/Fictopedia-Logo.gif "Actual size.  I couldn't find a larger version, and have no idea if they produced one.  However, I do know through painful experience that scaling the image up will only make it worse.")
 * The header image is [Ottawa fireworks 49](https://www.flickr.com/photos/theharv58/27768773120/) by [Harvey K](https://www.flickr.com/photos/theharv58/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Unification Part 2
 * ![A monument featuring children pushing two hemispheres into a globe](/blog/assets/3377523551_a20576f4ef_o.png "I like that the artist realized that the exertion would make more sense if the subjects needed to climb steps as they did the work...")
 * The header image is [unification statue 01](https://www.flickr.com/photos/friarsbalsam/3377523551/)[^1] by [Christopher John SSF](https://www.flickr.com/photos/friarsbalsam/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 07/01 to 07/05
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Nevada, part 5
 * ![Pink flowers](/blog/assets/5895557002_5a15b12c10_o.png "If this looks like a placeholder image...well, you've probably made worse assessments.")
 * The header image is [FLOWER 03 - PAINTBRUSH, sp (6-16-11) northeast Nevada](https://www.flickr.com/photos/8101022@N05/5895557002) by [Alan Schmierer](https://www.flickr.com/photos/sloalan/), released into the public domain by the photographer.  The artist released the actual cover under a non-commercial license.
 * title: Developer Diary, Emancipation Day (observed)
 * ![Henry Highland Garnet](/blog/assets/Henry-Highland-Garnet.png "You have no idea how much work it took to find ONE picture of somebody who participated in the 1827 festivities or a place where something relevant took place...")
 * The header image is [Henry Highland Garnet](https://commons.wikimedia.org/wiki/File:Henry_Highland_Garnet_by_James_U._Stead.jpg) by James U. Stead, long in the public domain due to expired copyright, or no copyright at all.
 * title: Real Life in Star Trek, A Matter of Time
 * ![A free-floating button in deep space showing a clock with an arrow pointing counter-clockwise around it](/blog/assets/240086966_1f04572a86_o.png "Do not click.  You'll smudge your screen.")
 * The header image is [DIY Time Machine Wallpaper](https://www.flickr.com/photos/16226024@N00/240086966) by [FHKE](https://www.flickr.com/photos/fhke/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 07/08 to 07/12
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Raiders of the Unix Seas
 * ![The Black Pearl, Classic Edition](/blog/assets/black-pearl.png "Why yes, I DID run ASCII art through software to generate a rasterized image of that art, because the SVG always came out empty...")
 * The header image derives from in-game art, via [Carbon](https://carbon.now.sh/).
 * title: Sleep, Addendum
 * ![A cat with its foreleg covering its eyes](/blog/assets/white-sweet-animal-cute-pet-kitten-720060-pxhere.com.png "I feel you, kitty...")
 * The header image is [untitled](https://pxhere.com/en/photo/720060) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).
 * title: Developer Diary, Queen Yun
 * ![Gyeongbokgung Palace at night, and built up over six hundred years](/blog/assets/Gyeongbokgung-Palace-Night-View.png "I honestly have no idea if I cropped out important parts of the facility or random nearby neighborhoods, but I decided to focus on the palace-like part instead of the traffic out front...")
 * The header image is [2016 Gyeongbokgung Palace Night View](https://commons.wikimedia.org/wiki/File:%EC%97%AD%EC%82%AC%EC%86%8D%EC%9C%BC%EB%A1%9C.jpg) by [LEESUNGWOO](https://commons.wikimedia.org/w/index.php?title=User:LEESUNGWOO&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Real Life in Star Trek, New Ground
 * ![Solio Ranch, White Baby Rhino](/blog/assets/baby-white-rhino.png "Maintaining the theme of child stars...")
 * The header image is [White Baby Rhino](https://commons.wikimedia.org/wiki/File:White_Baby_Rhino.jpg) by [Valentina Storti](https://www.flickr.com/people/44846648@N06), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 07/15 to 07/19
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Aether Age Codex - Helios, part 1
 * ![A person in loosely fitting wraps and pieces of golden armor, carrying something like an early firearm, under multiple moons](/blog/assets/helios-cover.png "I could pull off wearing an outfit like that...")
 * The header image is the book's cover image, by M.S. Corley, made available under the same terms as the rest of the book.
 * title: Developer Diary, Ratcatcher's Day
 * ![A Japanese statue of a rat-catcher upset at the rodent crawling over his neck instead of into his trap](/blog/assets/ma-131907.png "I've had programming projects that worked like this...")
 * The header image is [Frustrated Rat Catcher / 鼠捕り](https://collections.lacma.org/node/188349) by an unknown artist, long in the public domain due to either an expired copyright or no original copyright.
 * title: Real Life in Star Trek, Hero Worship
 * ![A child wearing a homemade robot costume](/blog/assets/4189390241_d6fef03f49_k.png "In the 1950s, Timothy would have needed to dress like this to make the point")
 * The header image is [Gabriel in robot costume](https://www.flickr.com/photos/21550937@N03/4189390241) by [Roy Luck](https://www.flickr.com/photos/royluck/), released under the terms of the Creative Commons [Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 07/22 to 07/26
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Aether Age Codex - Helios, part 2
 * ![A person in loosely fitting wraps and pieces of golden armor, carrying something like an early firearm, under multiple moons](/blog/assets/helios-cover.png "I could pull off wearing an outfit like that...")
 * The header image is the book's cover image, by M.S. Corley, made available under the same terms as the rest of the book.
 * title: Developer Diary, Thai Language Day
 * ![A white tiger walking to the left of the camera](/blog/assets/3865790598_859d87aa13_o.png "Must catch the glowing red dot and then sit adorably in a too-small box...")
 * The header image is [White Tiger 6](https://www.flickr.com/photos/hisgett/3865790598/) by [Tony Hisgett](https://www.flickr.com/photos/hisgett/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Violations
 * ![An abstract representation of memory, with a convoluted space containing tiny human figures wandering](/blog/assets/8665657878_9a2021378b_o.png "You didn't seriously think that I'd focus on the episode's MAIN element up here, did you?")
 * The header image is [Memory](https://www.flickr.com/photos/12836528@N00/8665657878) by [Kevin Dooley](https://www.flickr.com/photos/pagedooley/), made available under the terms of the [Creative Commons Attribution 2.0 International](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 07/29 to 08/02
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "And over here, we have...the Roman numeral for four, I guess.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Aether Age Codex - Helios, part 3
 * ![A person in loosely fitting wraps and pieces of golden armor, carrying something like an early firearm, under multiple moons](/blog/assets/helios-cover.png "I could pull off wearing an outfit like that...")
 * The header image is the book's cover image, by M.S. Corley, made available under the same terms as the rest of the book.
 * title: Developer Diary, Coast Guard Day (belated)
 * ![Drawing of a Guards-person in an old uniform looking across a divide at an officer in a modern uniform, with the claim of honoring the past, forging the present, and charting the future](/blog/assets/Coast_Guard_Day_2013_by_Cory_Mendenhall.png "Yes, I did briefly consider updating the end-year on that span...")
 * The header image is [Coast Guard Day 2013](https://web.archive.org/web/20151006021308/http://allhands.coastguard.dodlive.mil/files/2013/08/Coast-Guard-Day-2013.jpg) by [Petty Officer Third Class Corey Mendenhall, United States Coast Guard](https://web.archive.org/web/20210416102556/http://allhands.coastguard.dodlive.mil/2013/11/22/the-drawing-board-remember-those-standing-the-watch/), in the public domain as a work of a United States employee made as part of their duties.
 * title: Real Life in Star Trek, The Masterpiece Society
 * ![Cookies iced with A-T and C-G, with a handful of single-letters](/blog/assets/6217337528_36331bbb08_o.png "In an episode that plays with the idea of hospitality, they seriously lack cookie trays...")
 * The header image is [DNA biscuits](https://www.flickr.com/photos/24413864@N05/6217337528) by [Natasha de Vere](https://www.flickr.com/photos/col_and_tasha/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 08/05 to 08/09
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Viaje a la Tierra del Quebracho
 * ![The film's title appearing in front of trees](/blog/assets/quebracho.png "Well, ")
 * The header image is a few frames prior to the film's title screen, to get a more interesting visual than fully rendered text.
 * title: Open Source Characters
 * ![Line art and colored versions of Jenny](/blog/assets/Jenny-Everywhere-by-PencilInPain.png "We definitely need the scarf...")
 * The header image [Jenny Everywhere by PencilInPain](https://commons.wikimedia.org/wiki/File:Jenny_Everywhere_by_PencilInPain.jpg) by [Alberto J "Pencil in Pain" Silva](http://www.steelraining.albertosilva.es/), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Developer Diary, World Elephant Day
 * ![Elephants sharing water](/blog/assets/Kissing_Elephants.png "Setting a boundary now, in case it ever comes up:  No matter how dehydrated I get, you do not have permission to stuff your nose in my mouth...")
 *         "![#{alt_text}](#{avif_src} \"#{title_text}\")"
 * The header image is [Kissing Elephants](https://commons.wikimedia.org/wiki/File:Kissing_Elephants.JPG) by [Nicolas M. Perrault](https://commons.wikimedia.org/wiki/User:Nicolas_Perrault_III), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/deed.en).
 * title: Yet Another Dark Mode Post
 * ![A tropical green area at night](/blog/assets/6028828966_0046918665_o.png "Admittedly, nicer darkness than I pulled together...")
 * The header image is [night](https://www.flickr.com/photos/36307913@N00/6028828966) by [Fredrik Rubensson](https://www.flickr.com/photos/froderik/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Real Life in Star Trek, Conundrum
 * ![Starburst Cluster](/blog/assets/GSFC_20171208_Archive_e002049.png "Nothing about the episode stood out as illustration-worthy, so we revert to the random space picture...")
 * The header image is [Starburst Cluster Shows Celestial Fireworks](https://images.nasa.gov/details/GSFC_20171208_Archive_e002049) by NASA Goddard, in the public domain by NASA policy.
 * title: Toots 🦣 from 08/12 to 08/16
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I hate Venn diagram memes, especially when they don't make any sense")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Antumbra, chapter 0
 * ![The comic's logo, as it appears on the title page, showing the letters increasing, then decreasing in opposing arcs to a high point in the center, in front of an antumbra shadow](/blog/assets/antumbra.png "Definitely the fanciest logo that we've seen")
 * The header image is a few frames prior to the film's title screen, to get a more interesting visual than fully rendered text.
 * title: Developer Diary, World Humanitarian Day
 * ![Workers passing out nutritional supplements](/blog/assets/2995064256_853fd34d7c_o.png "Not the point, but I've often wondered why NGOs didn't sell the sorts of foods depicted, both to subsidize the projects and because low-income people everywhere could presumably benefit as well")
 * ![The LLL-1 action figure approaching a polished pseudo-monolith](/blog/assets/lights-edge-lll-1-a.png "Curious...")
 * ![Closer in on the LLL-1 action figure](/blog/assets/lights-edge-lll-1-b.png "Pondering the investigation...and yes, I put a tiny nose on the head so that it could have a front")
 * The header image is [Distributing high energy biscuits](https://www.flickr.com/photos/julien_harneis/2995064256/) by [Julien Harneis](https://www.flickr.com/photos/julien_harneis/), made available under the terms of the [Creative Commons Attribution Share-Alike](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Real Life in Star Trek, Power Play
 * ![People waiting at a bus stop with clothing over their heads, looking (vaguely) like ghosts](/blog/assets/52061548861_95273ca7ef_o.png "We've had to slash the budget, here at Real Life in Star Trek.  With your monthly donation, we could get real ghosts, and they could take faster transportation...")
 * The header image is [Ghosts waiting for the bus](https://www.flickr.com/photos/47568542@N03/52061548861) by [Jan Vlugt](Ghosts waiting for the bus), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/mark/1.0/).
 * title: Toots 🦣 from 08/19 to 08/23
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Morrisa Jeanine
 * ![A person on a bench facing away from the other person and the scenery to look into the middle distance](/blog/assets/nature-outdoor-people-girl-woman-bench-1335874-pxhere.com.png "It doesn't look like the artist released the album covers under a public license, so you get a random thematic picture instead...")
 * The header image is [untitled](https://pxhere.com/en/photo/1335874) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/mark/1.0/).
 * title: Developer Diary, Women's Equality Day
 * ![Nancy Pelosi, Anna Eshoo, Barbara Lee, and Jackie Speier speaking at a UCSF Womens Equality Day event](/blog/assets/30004393205_1c983b741d_o.png "The lab coat assures accuracy...")
 * ![LLL-1 considering the exploration of a crystalline cavern](/blog/assets/lights-edge-lll-2-a.png "Pardon the dust. I've had that geode on various shelves for over thirty years and never figured out how to clean it, but at least I finally found a use for it...")
 * ![LLL-1 piloting a toilet paper tube in space](/blog/assets/lights-edge-lll-2-b.png "I couldn't resist hamming it up in the lower-right.")
 * The header image is [Women’s Equality Day (30004393205)](https://www.flickr.com/photos/speakerpelosi/30004393205/) by [Nancy Pelosi's Office](https://www.flickr.com/photos/speakerpelosi/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license...but probably also technically in the public domain, since that account comes from her official office.
 * title: Real Life in Star Trek, Ethics
 * ![A group of oak barrels, one of which has a curled mustache, an eye patch, and a reddish slat reminiscent of a stereotypical facial scar](/blog/assets/Oak-wine-barrel-at-toneleria-nacional-chile.png "Barrel's log. I've never trusted Klingons, and I never will...")
 * The header image is based on [Wine barrels at the storage room at Tonelería Nacional, Chile](https://commons.wikimedia.org/w/index.php?curid=14955249) by [Gerard Prins](https://commons.wikimedia.org/wiki/User:Gerard_Prins), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.  It adds an element of the [mustache pack](https://openclipart.org/detail/302697/mustache-pack) by [ktb007](https://openclipart.org/artist/ktb007), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/mark/1.0/) and some manual additions.
 * title: Toots 🦣 from 08/26 to 08/30
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Geiko Eien Ni
 * ![A maiko (senior) and geisha dancing, facing away from the audience and camera](/blog/assets/2916308593_fa428042e9_o.png "The dance felt like watching paint dry, but without the paint...")
 * The header image is [Maiko and Geisha dance](https://www.flickr.com/photos/joi/2916308593/) by [Joi Ito](https://www.flickr.com/photos/joi/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Weird Enough?
 * ![A light-colored cat scrunching down to fit their head and forelegs under a tiny step-stool, while the rest of their body spills out](/blog/assets/3316028491_0a55965485_o.png "Not my cat, not my problem...")
 * The header image is [Cats are so weird.](https://www.flickr.com/photos/63791648@N00/3316028491) by [Mary-Frances O'Dea](https://www.flickr.com/photos/rexandsharkey/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Diary, Emma Nutt Day, Belated
 * ![Emma and Sheila Nutt operating the Edwin Holmes's telephone exchange in 1878](/blog/assets/Emma-and-shiela-nutt-1878.png "When someone says that they'll patch you through to someone else, that refers to the patch cables that you see linking one slot to another")
 * ![Caption](:storage/image-file-name.png "Title text")
 * The header image is [Emma and Sheila Nutt 1878](https://commons.wikimedia.org/wiki/File:Emma_and_shiela_nutt_1878-264x201.jpg) by an unknown photographer, long in the public domain from an expired copyright, assuming that it ever had one.
 * title: Real Life in Star Trek, The Outcast
 * ![The Coalsack Nebula, bottom center](/blog/assets/iss006e28016.png "Not null space...probably")
 * The header image is [Crew Earth Observations (CEO) taken during Expedition Six](https://images.nasa.gov/details/iss006e28016) by [JSC](https://www.nasa.gov/centers/johnson/home/index.html), in the public domain by NASA policy.
 * title: Toots 🦣 from 09/02 to 09/06
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Aumyr, part 1
 * ![The Aumyr logo](/blog/assets/aumyr.png "Yes, I'd call that a logo...")
 * The header image is the cover logo, (presumably) released under the same terms as the book and world.
 * title: Holding Universal Access to All Knowledge Hostage
 * ![The current Internet Archive headquarters, before they moved in](/blog/assets/Christian-science-church122908-02.png "Did they shop around for a building that matched their logo...?")
 * The header image is [Christian science church 122908 02](https://commons.wikimedia.org/wiki/File:Christian_science_church122908_02.jpg) by [Girl2k](https://en.wikipedia.org/wiki/User:Girl2k), released into the public domain by the photographer.
 * title: Developer Diary, Chrysanthemum Day
 * ![A display of chrysanthemums in Lahore, Pakistan](/blog/assets/Chrysanthemum.show.png "In the United States, our system of units and measurements would clock this as Way Too Many Mums")
 * ![A QR code pointing people to my Pinkary account](/blog/assets/pinkary_jcolag.png "It took me multiple tries to find software that would decode this, even after changing the pink to white and adding different borders, so beware.  It does, at least, only carry the link to my profile, as far as I can tell.")
 * ![LLL-1 in es Rail Runner prototype](/blog/assets/lights-edge-rail-runner-1.png "I decided not to go for subtlety in taping it, but the toothpick faux-harness, you might not spot.")
 * The header image is [Chrysanthemum show](https://commons.wikimedia.org/wiki/File:Chrysanthemum.show.jpg) by [Jugni](https://commons.wikimedia.org/wiki/User:Jugni), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Real Life in Star Trek, Cause and Effect
 * ![SpaceX Falcon9 CRS7 Explosion, 28 June 2015, 10:23](/blog/assets/SpaceX-Falcon9-CRS7-Explosion-19236222595.png "Not the Enterprise, but somebody involved probably thinks so...")
 * The header image is [SpaceX Falcon9 CRS7 Explosion (19236222595)](https://www.flickr.com/photos/mseeley1/19236222595/) by [Michael Seeley](https://www.flickr.com/people/76093456@N04), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🦣 from 09/09 to 09/13
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "And over here, we have...the Roman numeral for four, I guess.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Aumyr, part 2
 * ![The Aumyr logo](/blog/assets/aumyr.png "Yes, I'd call that a logo...")
 * The header image is the cover logo, (presumably) released under the same terms as the book and world.
 * title: Chosen
 * ![Sunset from cliffs...though not the Cliffs of Nubejmep](/blog/assets/38668565135_5d7c797e52_o.png "From page 538...")
 * The header image is [Winter Sunset over the Cliffs of Moher](https://www.flickr.com/photos/114717511@N02/38668565135) by [Sean O Riordan](https://www.flickr.com/photos/114717511@N02/), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Diary, Ozone Day
 * ![Animation of the ozone layer over the Southern Hemisphere, 1957-2001](/blog/assets/Ozone-over-southern-hemisphere-Sep11-1957-2001.gif "I once pitched raising money by selling ozone shovels...")
 * The header image is [Ozone over Southern Hemisphere Sep11 1957-2001](https://commons.wikimedia.org/wiki/File:Ozone_over_southern_hemisphere_Sep11_1957-2001.gif) by [RedAndr](https://commons.wikimedia.org/wiki/User:RedAndr), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Real Life in Star Trek, The First Duty
 * ![A sextet of stunt planes passing (with some editing) Titan, Rhea and Mimas](/blog/assets/4935102522_12f5ec6bcd_o.png "I apologize for the extra plane...")
 * The header image combines [Stunt Planes](https://www.flickr.com/photos/75264768@N00/4935102522) by [Chad Kainz](https://www.flickr.com/photos/smaedli/), made available under the terms of the [Creative Commons Attribution 2.0 International](https://creativecommons.org/licenses/by/2.0/) license, with [Saturn Triple Crescent](https://www.nasa.gov/image-article/triple-crescents/) by NASA/JPL-Caltech/Space Science Institute, in the public domain by NASA policy.
 * title: Toots 🦣 from 09/16 to 09/20
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Aumyr, part 3
 * ![The Aumyr logo](/blog/assets/aumyr.png "Yes, I'd call that a logo...")
 * The header image is the cover logo, (presumably) released under the same terms as the book and world.
 * title: Developer Diary, Celebrate Bisexuality Day
 * ![The bisexual pride flag](/blog/assets/Bisexual_Pride_Flag.svg "I see you, or at least some of you.  Actually, could you shimmy to the left a little so that the tree doesn't block you...?")
 * The header image is [Bisexual Pride Flag](https://web.archive.org/web/20120204071024/http://www.biflag.com/ActGraphics.asp) by Michael Page, presumed in the public domain as ineligible for copyright, based on its utilitarian nature.
 * title: Real Life in Star Trek, Cost of Living
 * ![A front stoop covered in glitter with an orange-colored poop emoji in the background](/blog/assets/6999881107_627e807ff3_o.png "Your bicycle tasted great, thanks...")
 * The header image (lazily) combines [glitter bomb](https://www.flickr.com/photos/14421410@N03/6999881107) by [Kate Millet](https://www.flickr.com/photos/kate28/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license, with a recolored version of [OpenMoji](https://openmoji.org/)'s [pile of poo](https://openmoji.org/library/emoji-1F4A9/) representation, made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🦣 from 09/23 to 09/27
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Aumyr, part 4
 * ![The Aumyr logo](/blog/assets/aumyr.png "Yes, I'd call that a logo...")
 * The header image is the cover logo, (presumably) released under the same terms as the book and world.
 * title: Developer Diary, International Translation Day
 * ![The Rosetta Stone](/blog/assets/Rosetta-Stone-front-face.png "It belongs in a museum...but not that one")
 * The header image is [Rosetta Stone](https://commons.wikimedia.org/wiki/File:Rosetta_Stone.JPG) by [Hans Hillewaert](https://www.flickr.com/photos/81858878@N00/), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, The Perfect Mate
 * ![An archeologist filling out paperwork at a dig site](/blog/assets/research-archaeology-digging-digger-archaeologist-human-action-1145298-pxhere.com.png "Dear diary, they still won't let me wear my fedora or use my whip...")
 * The header image is [Untitled](https://pxhere.com/en/photo/1145298) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🦣 from 09/30 to 10/04
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I hate Venn diagram memes, especially when they don't make any sense")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Ancient Beast
 * ![A screenshot from the game](/blog/assets/ancient-beast.png "Sadly, you can't play the skull embedded in the back wall")
 * The header image is a screenshot from the game, released under the same terms as the assets that make it up, presumably.  (But if anybody from the project objects, I could pull something together from the game assets.)
 * title: Developer Diary, Child Health Day
 * ![International children dressed in their native costumes participate in the 100th anniversary of the Statue of Liberty](/blog/assets/DPLA-4461efafd5c4ba7fde8eedb1f13f439e.png "I now live in fear that I might have actually known some of these kids...")
 * The header image is [International children dressed in their native costumes participate in the 100th anniversary of the Statue of Liberty](https://dp.la/item/4461efafd5c4ba7fde8eedb1f13f439e) by a Department of Defense photographer, in the public domain as a work of the United States government.
 * title: Real Life in Star Trek, Imaginary Friend
 * ![An adult standing next to a large stuffed animal, probably a panda](/blog/assets/2408033451_18206191bc_o.png "A bear and his boy")
 * The header image is [Leafar is my imaginary friend](https://www.flickr.com/photos/89941312@N00/2408033451) by [Raphaël Labbé](https://www.flickr.com/photos/ulikleafar/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 10/07 to 10/11
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Wait a second, are these all the same day of the week?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Restoration Day, part 1
 * ![The book's cover, prominently featuring a white chess pawn on a reflective surface, with darker pieces in the background](/blog/assets/restoration-day.png "Never make a promise or plan; take a little love where you can?")
 * The header image is the book's cover by George Becker, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Diary, World Standards Day
 * ![ISA founding plaque in Prague](/blog/assets/Memory-plaque-of-founding-ISA-in-Prague.png "Oddly, not manufactured to any standard...")
 * ![LLL-1 on his new skateboard, against a heavily edited domestic background](/blog/assets/lll-1-skateboard.png "I spent far too much time concealing that he rides on a wood floor past a cardboard wall, but I do like the results...")
 * The header image is [Memory plaque of founding ISA in Prague cropped](https://commons.wikimedia.org/wiki/File:Memory_plaque_of_founding_ISA_in_Prague_cropped.jpg) by [Luděk Kovář](https://cs.wikipedia.org/wiki/Wikipedista:Gumruch), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, I, Borg
 * ![A person holding a camera-phone in front of their eye, with a picture of the covered eye on the screen](/blog/assets/27650391_6fbad85e73_o.png "When you assimilate, you make an...no, wait")
 * The header image is [cyborg: merdi](https://www.flickr.com/photos/42805979@N00/27650391) by [Niv Singer](https://www.flickr.com/photos/antichrist/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 10/14 to 10/18
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Just what the world needed:  A calendar that flips the bird.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Restoration Day, part 2
 * ![The book's cover, prominently featuring a white chess pawn on a reflective surface, with darker pieces in the background](/blog/assets/restoration-day.png "Never make a promise or plan; take a little love where you can?")
 * The header image is the book's cover by George Becker, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Getting the Most from Mastodon (and Friends)
 * ![A mastodon skeleton from above](/blog/assets/31796917134_49240d896d_o.png "You must toot, because she can't...")
 * The header image is [Mammut americanum, Pleistocene; Aucilla River, Jefferson County, Florida, USA 8](https://www.flickr.com/photos/47445767@N05/31796917134) by [James St. John](https://www.flickr.com/photos/jsjgeology/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Diary, World Statistics Day, Belated
 * ![A graph of the normal distribution](/blog/assets/Standard-Normal-Distribution.png "Perfectly normal, nothing to see here...")
 * The header image is [Standard Normal Distribution](https://commons.wikimedia.org/wiki/File:Standard_Normal_Distribution.png) by [D Wells](https://commons.wikimedia.org/wiki/User:D_Wells), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, The Next Phase
 * ![A sculpture of a dinosaur head bursting through a hole in a wall](/blog/assets/nature-wall-portrait-museum-predator-reptile-813932-pxhere.com.png "You invert your phase your way...")
 * The header image is [Untitled](https://pxhere.com/en/photo/813932) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🦣 from 10/21 to 10/25
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Restoration Day, part 3
 * ![The book's cover, prominently featuring a white chess pawn on a reflective surface, with darker pieces in the background](/blog/assets/restoration-day.png "Never make a promise or plan; take a little love where you can?")
 * The header image is the book's cover by George Becker, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Some (US) Election Comments
 * ![An old-style voting booth, with a large red lever used to submit your votes with a satisfying sound](/blog/assets/292259730_5037c2d747_o.png "Honestly, half my decision to vote absentee in elections probably comes from how the lower budgets of the privacy screens make the actual process of voting indistinguishable from filling out the oversized Scantron test at home...")
 * The header image is [The Sound Of Democracy](https://www.flickr.com/photos/40646519@N00/292259730) by [Joe Shlabotnik](https://www.flickr.com/photos/40646519@N00/292259730), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Diary, International Animation Day
 * ![Six frames that (could) loop to show a bouncing ball](/blog/assets/Animexample3.png "Boing")
 * The header image is [Third in a series of three example images about animation](https://commons.wikimedia.org/wiki/File:Animexample3.png) by [Branko](https://en.wikipedia.org/wiki/User:Branko), released into the public domain by the artist.
 * title: Real Life in Star Trek, The Inner Light
 * ![A child with a necktie wrapped around their head playing a penny whistle](/blog/assets/38304600821_0aac6e602b_o.png "I dare you to find a superior image to represent this episode...")
 * The header image is [Will Playing Penny Whistle](https://www.flickr.com/photos/59993138@N00/38304600821) by [Sam Saunders](https://www.flickr.com/photos/samsaunders/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 10/28 to 11/01
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Just what the world needed:  A calendar that flips the bird.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Everybody Loves Eric Raymond
 * ![Cave art of the GNU mascot jumping over binary code, possibly for Chinese characters reading I'm worried about you](/blog/assets/eler-ep033.png "One of the least-objectionable panels in the comic...")
 * The header image is episode #33 of the comic, under the same license as the rest of it.
 * title: Developer Diary, Bank Transfer Eve
 * ![Playbill for a stage melodrama about a bank run](/blog/assets/war-of-wealth.png "It only now occurs to me how many images from this era include a newsie standing like he needs extra stability to hand somebody a newspaper, like the kid that the telegraph pole points to...")
 * The header image is [The war of wealth](https://loc.gov/pictures/resource/var.0760/) by Strobridge & Co. Lith., long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, Time's Arrow, part 1
 * ![Samuel Clemens in Nikola Tesla's lab, 1894](/blog/assets/Mark-Twain-in-the-lab-of-Nikola-Tesla-1894.png "This episode probably missed a huge opportunity to introduce Clemens through his interest in Tesla's work that resembles Data's gadgets, but also realize that the breathless Tesla biographies hadn't started circulating, yet")
 * The header image is [Mark Twain in the lab of Nikola Tesla; 1894](https://commons.wikimedia.org/wiki/File:Mark_Twain_in_the_lab_of_Nikola_Tesla;_1894.jpg) by an unidentified photographer, long in the public domain due to an expired copyright.
 * title: Toots 🦣 from 11/04 to 11/08
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Some champion eye-rolling, there.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Route 66, An American (bad) Dream
 * ![The film title](/blog/assets/route-66-american-bad-dream.png "One of the least-objectionable panels in the comic...")
 * The header image is the title screen of the film.
 * title: Some Further (US) Election Comments
 * ![A Black Lives Matter protest, including someone holding up a sign with a raised fist, and another sign asserting that we can't stay silent about things that actually matter](/blog/assets/50115137476_a23d59e537_o.png "I actually picked this for the raised-fist sign, but the other sign also has a valid point.")
 * ![The 1984 electoral map, showing a near-sweep for Republicans](/blog/assets/ElectoralCollege1984.svg "😱")
 * The header image is [Black Lives Matter activist holding a sign with the raised fist resistance logo](https://www.flickr.com/photos/26344495@N05/50115137476) by [Ivan Radic](https://www.flickr.com/photos/26344495@N05/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.  The map is [Electoral College 1984](https://en.wikipedia.org/wiki/File:ElectoralCollege1984.svg) by [Steve Sims ~ commons wiki](https://commons.wikimedia.org/wiki/User:SteveSims~commonswiki), released into the public domain by the creator.
 * title: Developer Diary, Veterans Day
 * ![Sailors in Toulon, France, commemorating Armistice Day 2022](/blog/assets/2211011-N-HD110-0118_7509291.png "Thank you for your service, BUT ALSO, nobody can look both absolutely formal and completely slacking like sailors...")
 * The header image is [2211011-N-HD110-0118 - 7509291](https://commons.wikimedia.org/wiki/File:2211011-N-HD110-0118_(7509291).jpg) (catchy name, I know) by Petty Officer 2nd Class Danielle Serocki, in the public domain as a work by the United States government.
 * title: Real Life in Star Trek, Season 5, TNG
 * ![The Hubble Space Telescope](/blog/assets/GSFC-20171208-Archive-e002151.png "Still scanning the galaxy...in the next generation")
 * The header image is [Hubble Space Telescope](https://images.nasa.gov/details-GSFC_20171208_Archive_e002151) by NASA Goddard, in the public domain by NASA policy.
 * title: Toots 🦣 from 11/11 to 11/15
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Wait a second, are these all the same day of the week?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Fully Automated! part 1
 * ![The book's cover, featuring a cat-lady with sunglasses vaulting backwards over two armed people on small flying machines over a disrupted market](/blog/assets/fully-automated-cover.png "I feel like the cover promises us body-modification furries that the text doesn't bother to pay off")
 * The header image is the title screen of the film.
 * title: Read Some (Other) Blogs
 * ![RSS icon in melon slices in a dish](/blog/assets/5066159552_b48d5080c9_o.png "Every time that I write about blogging in some respect, I feel like a failure in my relative inability to find anything resembling a useful header image...")
 * The header image is [RSS Melon](https://www.flickr.com/photos/35042256@N00/5066159552) by [BY-YOUR-⌘](https://www.flickr.com/photos/aparejador/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Diary, Susan B. Anthony
 * ![A caricature of Susan B. Anthony on the front page of the New York Daily Graphic](/blog/assets/Daily-Graphic-Woman-Who-Dared.png "Why has nobody turned this sketch into a Saturday morning cartoon?")
 * The header image is [Daily Graphic - Woman Who Dared - caricature of Susan B. Anthony](https://commons.wikimedia.org/wiki/File:Daily_Graphic-Woman_Who_Dared-caricature_of_Susan_B._Anthony.jpg) by an unidentified artist (MW?) for the **New York Daily Graphic**, long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, Time's Arrow, part 2
 * ![San Francisco's Chinatown roughly contemporary to the incidents](/blog/assets/Chinatown-San-Francisco-California-ca-1895.png "It seems kind of suspicious that these HIGHLY EVOLVED humans didn't actually bother to visit Chinatown or deal with any of the city's Asian population except off-screen, doesn't it...?")
 * The header image is [Chinatown, San Francisco, California, ca 1895](https://digitalcollections.lib.washington.edu/digital/collection/hester/id/287) by Wilhelm Hester, long in the public domain due to an expired copyright.
 * title: Toots 🦣 from 11/18 to 11/22
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I hate Venn diagram memes, especially when they don't make any sense")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Fully Automated! part 2
 * ![The book's cover, featuring a cat-lady with sunglasses vaulting backwards over two armed people on small flying machines over a disrupted market](/blog/assets/fully-automated-cover.png "I feel like the cover promises us body-modification furries that the text doesn't bother to pay off")
 * The header image is the title screen of the film.
 * title: Developer Diary, Elimination of Violence against Women
 * ![March of Non Una Di Meno (Not One Woman Less) on the occasion of the International Day for the Elimination of Violence Against Women in Rome, 24 November 2018](/blog/assets/March-for-Elimination-of-Violence-Against-Women-in-Rome-2018.png "See, they know how to demonstrate")
 * The header image is [March for Elimination of Violence Against Women in Rome](https://commons.wikimedia.org/wiki/File:WDG_-_March_for_Elimination_of_Violence_Against_Women_in_Rome_(2018)_18.jpg) by [Camelia.boban](https://commons.wikimedia.org/wiki/User:Camelia.boban), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Realm of Fear
 * ![Shimmering, out of focus lights](/blog/assets/5184961419_3f137bc06b_o.png "Try not to grab anything in there unless you have your monitor well-insured")
 * The header image is [Autumn bokeh](https://www.flickr.com/photos/26224875@N02/5184961419) by [Duncan Johnston](https://www.flickr.com/photos/duncanjohnston/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 11/25 to 11/29
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Wait a second, are these all the same day of the week?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Ancient Fire
 * ![The book's cover featuring ancient ruins](/blog/assets/ancient-fire-cover.png "I wonder if poetry book covers all tend more to the aspirational than illustrative or symbolic...")
 * The header image is the title screen of the film.
 * title: Developer Diary, Abolition of Slavery
 * ![Young people carrying American flags and wearing sashes calling to abolish child slavery, in English and Yiddish, during a 1909 May 1 labor parade in New York City](/blog/assets/06500-06591v.png "Not at all the point, but I love how the narrow focus on the two kids makes the rest of the picture look like a clumsy collage...")
 * The header image is [Protest against child labor in a labor parade](https://www.loc.gov/pictures/collection/ggbain/item/97519062/) by a Bain News Service photographer, long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, Man of The People
 * ![A person in fancy, old-fashioned clothing seated in front of a large framed painting of a corpse in similar dress](/blog/assets/53159150369_f7ed65aac4_o.png "Why did they act so uncomfortable when I called the painting my mother...?")
 * The header image is [The Picture of Dorian Gray](https://www.flickr.com/photos/91994044@N00/53159150369) by [Thomas Quine](https://www.flickr.com/photos/quinet/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 12/02 to 12/06
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — 2048, Enkidu, and Plastic
 * ![A terracotta sculpture of Enkidu](/blog/assets/enkidu.png "You don't see that style of beard anymore, or the hat")
 * The header image is [Enkidu, Gilgamesh's friend. From Ur, Iraq, 2027-1763 BCE. Iraq Museum](https://commons.wikimedia.org/wiki/File:Enkidu,_Gilgamesh%27s_friend._From_Ur,_Iraq,_2027-1763_BCE._Iraq_Museum.jpg) by [Osama Shukir Muhammed Amin FRCP(Glasg)](https://commons.wikimedia.org/wiki/User:Neuroforever), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Five Years of Entropy Arbitrage
 * ![A fifth birthday party](/blog/assets/5122978902_81d43e375c_o.png "I don't actually do birthdays, other than as a pretext to make a big deal about friends, but...")
 * ![A world map, with most countries shaded in light blue, representing visitors to the blog from that nation](/blog/assets/audience-map-2024-opt.svg "Apparently, I need to talk about middle-Africa more...")
 * The header image is [Stefan's fifth birthday school party](https://www.flickr.com/photos/35261188@N00/5122978902) by [Upsilon Andromedae](https://www.flickr.com/photos/upsand/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Developer Diary, Anti-Corruption Day
 * ![A student carrying a protest sign defining corruption as an unworthy practice in democracies](/blog/assets/student-in-Uvira-spreads-anti-corruption-message.png "I can't disagree with the assertion")
 * ![A screenshot of a recent Social Media Roundup post title on the index page, with tag bubbles centered beneath it](/blog/assets/ea-title-with-tags.png "Not exactly stunning, but functional...")
 * The header image is [A student in Uvira spreads anti-corruption message](https://commons.wikimedia.org/wiki/File:A_student_in_Uvira_spreads_anti-corruption_message.jpg) by [Bitamala](https://commons.wikimedia.org/w/index.php?title=User:Bitamala&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Relics
 * ![A more likely presentation of a Dyson sphere](/blog/assets/dyson-sphere.png "A star-in-a-box")
 * The header image is [Dyson Swarm realistic representation cropped](https://commons.wikimedia.org/wiki/File:Dyson_Swarm_realistic_representation_cropped.jpg) by [Archibald Tuttle](https://fr.wikipedia.org/wiki/Utilisateur:Archibald_Tuttle), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.  The image of Montgomery Scott is [James Doohan Scotty Star Trek](https://commons.wikimedia.org/wiki/File:James_Doohan_Scotty_Star_Trek.JPG) by NBC's publicity team, in the public domain due to pre-1978 publication with no copyright notice.
 * title: Toots 🦣 from 12/09 to 12/13
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — floodlight up. liquid god
 * ![The album's cover, featuring high-tension wires and markings over a wooded area](/blog/assets/floodlight-up-liquid-god.png "Study the diagrams in case somebody quizzes us later")
 * The header image is the album's cover art, presumed available as the same terms as the album itself.
 * title: Devils, in Detail
 * ![The Wicked Witch of the West, original version](/blog/assets/wwoo-xii-wicked-witch-west.png "I've worn outfits that matched less...")
 * The header image is the Chapter XII title illustration of [**The Wonderful Wizard of Oz**](https://www.gutenberg.org/ebooks/43936) by W.W. Denslow, long in the public domain due to an expired copyright.
 * title: Developer Diary, Day of Reconciliation
 * ![The South African flag](/blog/assets/Flag-of-South-Africa.svg "No, I don't have snarky things to say about somebody's flag...")
 * The header image is [Protest against child labor in a labor parade](https://www.loc.gov/pictures/collection/ggbain/item/97519062/) by a Bain News Service photographer, long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, Schisms
 * ![A pair of scissors lying on a rough sheet of paper](/blog/assets/3708293822_638e928859_o.png "Not Safe for Worf")
 * The header image is [Scissors](https://www.flickr.com/photos/29848680@N08/3708293822) by [James Bowe](https://www.flickr.com/photos/jamesrbowe/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 12/16 to 12/20
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Speciare Lunar Research Facility
 * ![The opening screen of the game, featuring an astronaut looking forward at an industrial building and a satellite antenna](/blog/assets/specaire-screenshot.png "No, we never discover why the Moon has no craters")
 * The header image is a screenshot from the game, borrowed from the game's repository, so under the same license.
 * title: Christmas Every Day
 * ![Small people in regal-looking outfits throwing items onto a bonfire near a column](/blog/assets/Christmas-every-day-frontispiece.png "Delivery of pre-trash every day...")
 * The header image is the frontispiece, *Having Bonfires in the Back Yard of the Palace*, of the book **Christmas Every Day and Other Stories Told for Children**, long in the public domain due to expired copyrights.  The music is [*Christmas Waltz*](https://www.jamendo.com/track/687776/christmas-waltz) by Nébur Maik, made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/) license.
 * title: Developer Diary, Treaty of Ghent
 * ![The Signing of the Treaty of Ghent, Christmas Eve, 1814](/blog/assets/SAAM-1922.5.2_1.png "Apparently, one side went for frilly militarism and the other opted for business casual and stifled giggles")
 * The header image is [The Signing of the Treaty of Ghent, Christmas Eve, 1814](https://collections.si.edu/search/detail/edanmdm:saam_1922.5.2) by Sir Amédée Forestier, long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, True Q
 * ![Puppies playing with an adult dog](/blog/assets/5025650088_7e57e46aa3_o.png "Amanda would have seemed much more interesting as a cat person...")
 * The header image is [shih-poo puppies playing with border collie](https://www.flickr.com/photos/49353456@N03/5025650088) by [westkeasman69](https://www.flickr.com/photos/49353456@N03/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 12/23 to 12/27
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Just what the world needed:  A calendar that flips the bird.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Trans Girl Project, part 1
 * ![The comic's cover, featuring a scantily clad, female-presenting person sitting with knees up to block most of the body](/blog/assets/000-trans-girl-project-cover-by-legendarya-da8funa.png "Nothing about this worries me half as much as the science fiction-y title font mixed with the word Project...")
 * The header image is the comic's cover.
 * title: 🔭 Looking Back on 2024
 * ![A lemur looking off to the left](/blog/assets/Coquerels-sifaka-head.png "Looking back...")
 * The header image is [Coquerel's sifaka (Propithecus coquereli) head](https://commons.wikimedia.org/wiki/File:Coquerel%27s_sifaka_%28Propithecus_coquereli%29_head.jpg) by [Charles J. Sharp](https://www.sharpphotography.co.uk/), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Developer Diary, Rizal Day
 * ![President Benigno S. Aquino III offering a wreath during the 2015 commemoration of Dr. Jose Rizal at the Rizal Monument in Rizal Park, Manila](/blog/assets/119th-Rizal-Day-commemoration.png "I appear to have invented the slogan Sizzle with Rizal, and it bothers me a bit that I can't find anybody using it, even though I suppose that it technically doesn't rhyme...")
 * The header image is [119th Rizal Day commemoration](https://commons.wikimedia.org/wiki/File:119th_Rizal_Day_commemoration.jpg) by Joseph Vidal, in the public domain as a work created by the Government of the Philippines.
 * title: Real Life in Star Trek, Rascals
 * ![Crumbling brick](/blog/assets/51349431101_ac5360bb24_o.png "Please accept my apologies for not sourcing a picture of tweens engaging in a heist")
 * The header image is [Chunks of crumbled brick in a person's hand close-up with blurry background](https://www.flickr.com/photos/26344495@N05/51349431101) by [Ivan Radic](https://www.flickr.com/photos/26344495@N05/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 12/30 to 01/03
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Carmen Miranda presents the Days of the Week.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Trans Girl Project, part 2
 * ![The comic's cover, featuring a scantily clad, female-presenting person sitting with knees up to block most of the body](/blog/assets/000-trans-girl-project-cover-by-legendarya-da8funa.png "Nothing about this worries me half as much as the science fiction-y title font mixed with the word Project...")
 * The header image is the comic's cover.
 * title: 🍾 Happy Belated Calendar-Changing Day, 2025 🎆
 * ![A comic-like scene of a person with white fluffy hair wearing a black sweater reading Happy Year 2025 and a person with long gray hair in a red sweater, in an outdoor scene of mostly reds and browns](/blog/assets/54227303764_d19fc65f36_o.png "I spent a long time rigging up an ugly 2025 image until finding this one that demanded attention...")
 * ![Edward Hopper's painting Chop Suey](/blog/assets/Edward-hopper-chop-suey.png "Check out discount Alfred Hitchcock pretending that social media exists while he avoids his companion...")
 * The header image is [¡Feliz Año Nuevo!](https://www.flickr.com/photos/martius/54227303764/) by [manuel m. v.](https://www.flickr.com/photos/martius/), made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).  Edward Hopper's [*Chop Suey*](https://www.christies.com/lot/lot-edward-hopper-chop-suey-6168966/) saw its copyright expire Wednesday morning.
 * title: Developer Diary, Epiphany
 * ![Christmas Carols in Little Russia by Trutovsky](/blog/assets/Trutovsky-Kolyadki.png "Thank goodness that we abandoned the pretend-that-you-run-a-drive-through part of Christmas...")
 * The header image is [Колядки в Малороссии](https://commons.wikimedia.org/wiki/File:Trutovsky_Kolyadki.jpg) by K. Trutovsky, long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, A Fistful of Datas
 * ![An old town outside Las Vegas](/blog/assets/109451610_aecf4a652e_o.png "For horse opera, this has too few horses, much as for space opera, this features too little space...")
 * The header image is [old west](https://www.flickr.com/photos/95505871@N00/109451610) by [Michael Rehfeldt](https://www.flickr.com/photos/ipjmike/), made available under the terms of the [Creative Commons Attribution 2.0 International](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 01/06 to 01/10
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "And over here, we have...the Roman numeral for four, I guess.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Blood of the Ancient Star, part 1
 * ![The book's cover, featuring the title in three-dimensional lettering between a star-like glow above and an Earth-like planet below](/blog/assets/bloodstar.png "Out of this...no, never mind")
 * The header image is the book's cover.
 * title: Exercise
 * ![A three-panel Jon comic strip, featuring the pre-reboot version of Garfield, who says: I'm getting lazy. It would do me good to get some exercise. In the second panel, he yawns and stretches a paw. For the punchline, he declares it much better.](/blog/assets/Jon-1976-06-03.png "That sounds about right...")
 * The header image is the [June 3<sup>rd</sup>, 1976, **Jon** comic strip](https://commons.wikimedia.org/wiki/File:Jon_-_1976-06-03.png) by Jim Davis---yes, the famous cartoonist who still works on (a version of) the same character---in the public domain due to lacking a copyright notice on the strips and periodicals.
 * title: Developer Diary, Old New Year's Eve
 * ![A person (face blurred by John) in an elaborate bear costume, related to Orthodox New Year celebrations](/blog/assets/Ukraine-bear-costume.png "You know what I know...")
 * The header image is [Bear Costume 1](https://commons.wikimedia.org/w/index.php?curid=87004487) by [Alin Rus](https://commons.wikimedia.org/w/index.php?title=User:Rusalin445&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, The Quality of Life
 * ![An Arduino-controlled robot](/blog/assets/3049090875_2d2cc9123a_o.png "Not an exocomp")
 * The header image is [An Arduino Controlled Servo Robot - (SERB)](https://www.flickr.com/photos/33504192@N00/3049090875) by [oomlout](https://www.flickr.com/photos/snazzyguy/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 01/13 to 01/17
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Blood of the Ancient Star, part 2
 * ![The book's cover, featuring the title in three-dimensional lettering between a star-like glow above and an Earth-like planet below](/blog/assets/bloodstar.png "Out of this...no, never mind")
 * The header image is the book's cover.
 * title: Grappling with AI Usage
 * ![A human hand and a robotic hand reaching from opposite sides of the image to approach near the center](/blog/assets/125905185-human-ai.png "Doot.")
 * The header image is [HUMAN-AI](https://commons.wikimedia.org/w/index.php?curid=125905185) by [JSkourr](https://commons.wikimedia.org/w/index.php?title=User:JSkourr&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Developer Diary, Turkish Constitution of 1921
 * ![A comic satirizing Atatürk's rule in Turkey](/blog/assets/Ataturk-satire-cartoon.png "An imperfect leader, but dismantling empires doesn't always come easy...")
 * The header image is [The Multiple Dictator - British propaganda against Mustafa Kemal Atatürk](https://commons.wikimedia.org/wiki/File:The_Multiple_Dictator_-_British_propaganda_against_Mustafa_Kemal_Atat%C3%BCrk_(7_November_1923).jpg) by Ernest Howard Shepard, in the public domain due to an expired British copyright.
 * title: Real Life in Star Trek, Chain of Command, Part 1
 * ![A military drill with officers attempting to sneak around a building](/blog/assets/DF-SD-06-14259.png "You almost got a picture of cats, so that I could pretend to have misheard the character's name as Jellicle...")
 * The header image is [Opposition Forces (OPFOR) sneak around a building during the Korean Peninsula Combat Evaluation Readiness Exercise (PENCERE) at Kunsan Air Base (AB), South Korea (ROK)](https://catalog.archives.gov/id/6663576) by SSGT Michael R. Holzworth, USAF, placed into the public domain as a work by the United States government.
 * title: Toots 🦣 from 01/20 to 01/24
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "What do you think, too much rouge for a monk?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Blood of the Ancient Star, part 3
 * ![The book's cover, featuring the title in three-dimensional lettering between a star-like glow above and an Earth-like planet below](/blog/assets/bloodstar.png "Out of this...no, never mind")
 * The header image is the book's cover.
 * title: Developer Diary, Liberation of Auschwitz
 * ![Prisoners of the Auschwitz Concentration Camp after their release](/blog/assets/Auschwitz-Liberation-1945.png "I can't help liking the fellow in front, the only person who seems to realize that somebody set up a camera because of the symbolic importance of the event")
 * The header image is [Auschwitz Liberation 1945](https://en.wikipedia.org/wiki/File:Auschwitz_Liberation_1945.jpg) by an unknown photographer, in the public domain due to an expired Russian copyright.
 * title: Real Life in Star Trek, Chain of Command, Part 2
 * ![Four tea-light candles in amorphous holders on a platform](/blog/assets/45272805025_f1be87f926_o.png "You can make the lights arbitrarily numbered, if you can source them cheaply enough...")
 * The header image is [Four Lights](https://www.flickr.com/photos/37996646802@N01/45272805025) by [Alan Levine](https://www.flickr.com/photos/cogdog/), made available under the terms of the [Creative Commons CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🦣 from 01/27 to 01/31
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I hate Venn diagram memes, especially when they don't make any sense")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — The Man Behind the Machine
 * ![The film's title screen, the text in white overlaid on a series of advertisements on a wall](/blog/assets/man-behind-machine.png "The world needs more fictional ads...")
 * The header image is the film's title screen.
 * title: Developer Diary, Sámi National Day
 * ![The Sámi flag](/blog/assets/saami-flag.png "I've finally found a flag that hurts my head a bit")
 * The header image is [Sámi flag](https://commons.wikimedia.org/wiki/File:Sami_flag.svg) by [Jeltz](https://commons.wikimedia.org/w/index.php?title=User:Jeltz&action=edit&redlink=1), released into the public domain by the creator.
 * title: Real Life in Star Trek, Ship in a Bottle
 * ![A model ship in an actual bottle](/blog/assets/Ship-in-a-bottle-Gaff-schooner1.png "Let's see what's out there...oh, nothing")
 * The header image is [Ship in a bottle Gaff schooner 1](https://commons.wikimedia.org/wiki/File:Ship_in_a_bottle_Gaff_schooner1.jpg) by [Vitavia](https://commons.wikimedia.org/wiki/User:Vitavia), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Toots 🦣 from 02/03 to 02/07
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I like to assume that the bust on the right, wearing a crown, blouse, and jacket, then shows up on the left, pointing with arms clad in sleeves made from woven wood slats...")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Cairn Players Guide and Background
 * ![The book's cover, with a script-like title over thorny vegetation, with small hands poking up near the bottom of the image](/blog/assets/cairn.png "Not the most exciting beginning, but we'll see...")
 * The header image is the game's cover.
 * title: Stop Doing Their Work for Them
 * ![A painting of Washington, DC, mostly blue with yellow structures/trees and a reflection](/blog/assets/15269344268_ca316e9f6f_o.png "I haven't gotten down to the city since junior high school, and recent events have not convinced me to do so any time soon...")
 * The header image is [Acrylic Waves of Washington DC](https://www.flickr.com/photos/82955120@N05/15269344268) by [Nicolas Raymond](https://www.flickr.com/photos/82955120@N05/), made available under the terms of the [Creative Commons Attribution 3.0 Unported](https://creativecommons.org/licenses/by/3.0/deed.en) license.
 * title: Developer Diary, Freedom to Marry Day
 * ![A wedding between two men, surrounded by the wedding party](/blog/assets/Wedding-in-New-Orleans-November-11-2017.png "Jazz Hands seems to have missed the point...")
 * The header image is [Wedding in New Orleans, November 11, 2017](https://commons.wikimedia.org/wiki/File:Wedding_in_New_Orleans,_November_11,_2017.jpg) by [Jami430](https://commons.wikimedia.org/wiki/User:Jami430), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Real Life in Star Trek, Aquiel
 * ![A (recolored) pug wrapped in a blanket in a field](/blog/assets/warm-leaf-flower-puppy-dog-animal-718747-pxhere.com.png "You want to give me a treat...")
 * The header image is [untitled](https://pxhere.com/en/photo/718747) by an uncredited PxHere photographer, made available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Toots 🦣 from 02/10 to 02/14
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I hate Venn diagram memes, especially when they don't make any sense")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Cairn Wardens Guide, part 1
 * ![The book's cover, with a script-like title over thorny vegetation, with small hands poking up near the bottom of the image](/blog/assets/cairn.png "Not the most exciting beginning, but we'll see...")
 * The header image is the game's cover.
 * title: Who Belongs
 * ![Shirley Chisholm reviewing a data table posted on a wall](/blog/assets/39254053445_5b1b5aa9fd_o.png "I may have spent an embarrassing amount of time trying, unsuccessfully, to work out the relevance of the data")
 * The header image is [Shirley Chisholm, head-and-shoulders portrait, facing left, standing with right arm raised, looking at list of numbers posted on a wall](https://www.loc.gov/pictures/item/2005676944/) by Roger Higgins, [gifted](https://guides.loc.gov/p-and-p-rights-and-restrictions/rights#076_nyw.html) to the Library of Congress and the public domain by **The New York World-Telegram & Sun**.
 * title: Developer Diary, World Eve of Social Justice
 * ![Eleanor Roosevelt holding poster of the Universal Declaration of Human Rights](/blog/assets/Eleanor-Roosevelt-UDHR.png "I actually thought that I had used this picture before, but apparently not")
 * The header image is [64-165](https://www.flickr.com/photos/fdrlibrary/27758131387/) by the [FDR Presidential Library & Museum](https://www.flickr.com/photos/fdrlibrary/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Face of the Enemy
 * ![A person holding up a funny-nose-and-glasses disguise made out of crumpled aluminum foil](/blog/assets/325700186_9288d03ce8_o.png "I choose to believe that the Romulans solved their identity problem in the same way")
 * The header image is [Jordan constructs his aluminum foil disguise](https://www.flickr.com/photos/38102495@N00/325700186) by [Jason Eppink](https://www.flickr.com/photos/jasoneppink/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Toots 🦣 from 02/17 to 02/21
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Some champion eye-rolling, there.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Cairn Wardens Guide, part 2
 * ![The book's cover, with a script-like title over thorny vegetation, with small hands poking up near the bottom of the image](/blog/assets/cairn.png "Not the most exciting beginning, but we'll see...")
 * The header image is the game's cover.
 * title: The Comet
 * ![A meteor arcing downward over trees, with the head of Du Bois in the stars looking to the right](/blog/assets/Leonid-meteor.png "Hang on to your old-timey hats")
 * I based the header image on [Leonid Meteor](https://commons.wikimedia.org/w/index.php?curid=8736621) by [Navicore](https://www.flickr.com/photos/edsweeney/sets/72157622894962452/), made available under the terms of the [Creative Commons Attribution 3.0 Unported](https://creativecommons.org/licenses/by/3.0/) license, combining it with [W.E.B. Du Bois, 1907](https://npg.si.edu/object/npg_NPG.80.25) by James E. Purdy, long in the public domain due to an expired copyright, but also kindly made explicitly available under the terms of the [Creative Commons CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
 * title: Developer Diary, Kingdom of Ava
 * ![Ruins at Innwa, capital of the Kingdom of Ava](/blog/assets/Innwa-ruins-Myanmar.png "I'd move in...")
 * The header image is [Ruins, Innwa, Mandalay Division, Burma](https://www.flickr.com/photos/sarahdepper/3873551357/) by [Sarah Depper](https://www.flickr.com/photos/86657422@N00), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Tapestry
 * ![Iran's Amir Sarkhosh in his gold medal-winning Snooker Single 15-Red event of ACBS 1st Asian Billiards Sports Championship](/blog/assets/139409081858534356611034.png "I actually didn't manipulate the eyes; the blue glow shows up in the original picture, too")
 * The header image is [Iran’s Sarkhosh Wins Gold at Asian Billiards Sports C’ship](https://www.tasnimnews.com/en/news/2016/09/30/1200708/iran-s-sarkhosh-wins-gold-at-asian-billiards-sports-c-ship) by Mohammad Hossein Taaghi, made available under the terms of the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license.
 * title: Toots 🦣 from 02/24 to 02/28
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "Wait a second, are these all the same day of the week?")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — Stories by Matthew DeBlock
 * ![The cosmic web](/blog/assets/cosmic-web.png "We need a cosmic spider...")
 * The header image [The cosmic web](https://commons.wikimedia.org/w/index.php?curid=84870202) by  Volker Springel / Max Planck Institute For Astrophysics, made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.en) license.
 * title: Developer Diary, World Wildlife Day
 * ![A painting of Australian, New Zealander, and Papuan fauna](/blog/assets/Australisk-fauna-Nordisk-familjebok.png "We have offended the kangaroo...")
 * The header image is [Australisk fauna](https://commons.wikimedia.org/wiki/File:Australisk_fauna,_Nordisk_familjebok.jpg) by an unnamed contributor to the [**Nordisk familjebok**](https://en.wikipedia.org/wiki/Nordisk_familjebok), long in the public domain due to an expired copyright.
 * title: Real Life in Star Trek, Birthright, part 1
 * ![A crow flying into the distance](/blog/assets/190062220_508ef7df5a_o.png "What, you wanted someone walking through the jungle...?")
 * The header image is [crow](https://www.flickr.com/photos/52473526@N00/190062220) by [Heather Katsoulis](https://www.flickr.com/photos/hlkljgk/), made available under the terms of the [Creative Commons Attribution Share-Alike 3.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Toots 🦣 from 03/03 to 03/07
 * ![diagrams showing the division of the day and of the week](/blog/assets/CLM_14456_71r_detail.png "I don't know, it seems to have turned into all cowlicks.")
 * Header image is [Circular diagrams showing the division of the day and of the week](https://commons.wikimedia.org/wiki/File:CLM_14456_71r_detail.jpg) from a manuscript drafted during the Carolingian Dynasty.
 * title: Free Culture Book Club — acoustic (yin)
 * ![The album's cover, with a person playing an acoustic guitar](/blog/assets/acoustic-yin.png "Consistent theming so far, if nothing else...")
 * The header image is the album's cover.
 * title: Developer Diary, Harriet Tubman Day
 * ![A younger Tubman, seated](/blog/assets/Harriet-Tubman-c1868.png "She apparently only stood at about five feet tall, to boot")
 * The header image derives from [Harriet Tubman c1868-69](https://catalogue.swanngalleries.com/Lots/auction-lot/(SLAVERY-AND-ABOLITION)-PHOTOGRAPHY-Civil-War-period-carte-d?saleno=2441&lotNo=75&refNo=718098) by Benjamin F. Powelson, long in the public domain due to expired copyright.
 * title: Real Life in Star Trek, Birthright part 2
 * ![A small child in a pose resembling a tai chi form](/blog/assets/1957280781_d37d7a4e18_o.png "Later, he'll do the throwing-a-spear-at-a-hula-hoop thing")
 * The header image is [Bram (1) does Tai Chi near Volterra, Italy](https://www.flickr.com/photos/11076844@N00/1957280781) by [Piano Piano!](https://www.flickr.com/photos/hansthijs/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Free Culture Book Club — electric (yang)
 * ![The album's cover, with a person playing an electric guitar](/blog/assets/electric-yang.png "Consistent theming so far, if nothing else...")
 * The header image is the album's cover.
 * title: Developer Diary, Saint Patrick's Day
 * ![A leprechaun smoking a pipe](/blog/assets/Leprechaun.png "I apologize if this association offends any actual Irish Christian folks, but we get a lot of this nonsense in the United States, and pictures of beer seemed even less kind...")
 * The header image is [Leprechaun](https://commons.wikimedia.org/wiki/File:Leprechaun_ill_artlibre_jnl.png) by Jean-noël Lafargue, made available under the terms of the [Free Art License 1.3](http://artlibre.org/licence/lal/en/).
 * title: Real Life in Star Trek, Starship Mine
 * ![Two people making small talk in an airy room](/blog/assets/3573744364_b8ea6c7803_o.png "Hey, look, a casual conversation with nobody lurking to mimic people or whining about mandatory attendance.")
 * The header image is [Smalltalk unter Schwangeren](https://www.flickr.com/photos/34515407@N03/3573744364) by [Thomas Pompernigg](https://www.flickr.com/photos/newlifehotels/), made available under the terms of the [Creative Commons Attribution Share-Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/) license.
 * title: Free Culture Book Club — Sulphur Nimbus
 * ![The game's loading screen, featuring the protagonist flying away from a black-furred unicorn standing on a castle tower](/blog/assets/sulfur-nimbus.png "My Action/Adventure Pony")
 * The header image is the album's cover.
 * title: Developer Diary, World Tuberculosis Day
 * ![A poster asking to prevent disease, warning that careless spitting, coughing, and sneezing spread influenza and tuberculosis, showing a person covering their mouth with a handkerchief](/blog/assets/TB-poster.png ""Disappointingly still relevant...)
 * The header image is [TB Poster](https://commons.wikimedia.org/wiki/File:TB_poster.jpg) by the  Rensselaer County Tuberculosis Association, in the public domain due to an expired copyright.  The page noted a possible copyright entanglement in 2008, but seventeen years later, the public domain has expanded to include the range when RCTA might have published it.
 * title: Real Life in Star Trek, Lessons
 * ![A small kindergarten class playing various percussion instruments](/blog/assets/Logan-Campbell-Kindergarten-music-lesson-1965.png "Granted, this lesson seems far less fun")
 * The header image is [Maori Series Publicity Caption: At the Logen Campbell Kindergarten in Auckland, the teacher leads her class in a music lesson](https://commons.wikimedia.org/w/index.php?curid=89350089) by Mr. Reithmaier, made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Free Culture Book Club — Nose Ears, part 1
 * ![Comic #20, Proprietary Social Media. The first panel has Mimi ask Eunice why they don't join proprietary social media, since it has much more polish, followed by a later panel where Mimi screams that evil corporations have taken over the Internet](/blog/assets/NE_en_20_hu15806520806758061560.png "A comic that knows how to play to its audience")
 * ![Comic #26, Sweeping Generalizations.  Eunice asserts that you can't make sweeping generalizations about trans people.  Mimi counters that all trans people are valid.  Eunice rethinks their position](/blog/assets/NE_en_26_hu4703116701650382992.png "A vastly superior handling of the existence of transgender folks")
 * The header image is the album's cover.
 * title: Developer Diary, Transgender Day of Visibility
 * ![Transgender Day of Visibility celebration in Cartagena, Colombia, 2019, featuring concentric circles of people in a stone area with LGBT+ symbols under yellow light](/blog/assets/trans-visibility.png "🏳️‍⚧️")
 * The header image is [Dia de la visibilidad Trans 10](https://commons.wikimedia.org/wiki/File:Dia_de_la_visibilidad_Trans_10.png) by [Juan Pajaro Velasquez](https://commons.wikimedia.org/w/index.php?title=User:Juanrapave&action=edit&redlink=1), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, The Chase
 * ![A bowl of biscuits, re-colored to have a purple hue](/blog/assets/11496462405_ce8c746c5a_o.png "I prefer a good drop biscuit, myself, but I'd give these a shot...")
 * I adapted the header image from [biscuits](https://www.flickr.com/photos/23119666@N03/11496462405) by [Mark Bonica](https://www.flickr.com/photos/23119666@N03/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Free Culture Book Club — Nose Ears, part 2
 * ![Comic #83, The Least Oppressed. Eunice worries that they lost the Opression Olympics. Mimi offers the consolation prize of a mountain of cash](/blog/assets/NE_en_83.png "A bit disappointing that we don't see the mountain of cash built by shaking down all the winners")
 * The header image is the album's cover.
 * title: Real Life in Star Trek, Frame of Mind
 * ![A person lying in the corner of a padded room](/blog/assets/35883810924_4ea73e0307_o.png "Highly inaccurate, but it gives the general sense")
 * The header image is [a visitor to the padded cell](https://www.flickr.com/photos/7510444@N06/35883810924) by [Saffia Widdershins](https://www.flickr.com/photos/primperfect/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Free Culture Book Club — Nose Ears, part 3
 * ![Comic #132, Apolitical.  Keno calls himself apolitical, prompting Poppy to worry that the fascists want to kill people like her, so Keno demands that she stop indoctrinating him with a political agenda](/blog/assets/NE_en_132.png "A bit too real, but...")
 * The header image is [Comic #132: Apolitical](https://wuzzy.neocities.org/comic/132/).
 * title: Real Life in Star Trek, Suspicions
 * ![The Sun's corona forming eruptions](/blog/assets/16139252568_c7675dc4e4_o.png "If we had their magic shield, we'd probably start carving this up as real estate...")
 * The header image is [SDO Reveals Star-Forming Eruptions](https://www.flickr.com/photos/24662369@N07/16139252568) by NASA/SDO/AIA/LMSAL, made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license...but probably also in the public domain by NASA policy.
 * title: Free Culture Book Club — Secrets in the Static
 * ![A CRT television, noise on the screen, sitting on a hexagon-tiled sidewalk](/blog/assets/Sony_tv_set_0001.png "I spent too much time designing the pseudo-static on the screen instead of overlaying a picture of actual static...")
 * The header image is [Sony tv set 0001](https://commons.wikimedia.org/wiki/File:Sony_tv_set_0001.JPG) by [Joxemai](https://commons.wikimedia.org/wiki/User:Joxemai), made available under the terms of the [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license.
 * title: Real Life in Star Trek, Rightful Heir
 * ![Two people standing on a raised area in a cave](/blog/assets/6772546493_40b57f8ea1_o.png "Not Kahless, either one")
 * The header image is [The animal flower cave, Barbados](https://www.flickr.com/photos/9298216@N08/6772546493) by [Berit Watkin](https://www.flickr.com/photos/ben124/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Free Culture Book Club — Pilogy, part 1
 * ![A collage of various images overlaid with the book's title in three rows of two letters, each in a different color](/blog/assets/pilogy.png "Well, that seems like a mess")
 * The header image is the book's cover, presumably under the same license, *but* appears to have some material under copyright.
 * title: Real Life in Star Trek, Second Chances
 * ![Twin babies amusing each other](/blog/assets/9477327336_ed83bbe95a_o.png "I know, this picture doesn't feel representative, because these kids seem much more mature")
 * The header image is [Happy Twin Girls](https://www.flickr.com/photos/11946169@N00/9477327336) by [Donnie Ray Jones](https://www.flickr.com/photos/donnieray/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Free Culture Book Club — Pilogy, part 2
 * ![A collage of various images overlaid with the book's title in three rows of two letters, each in a different color](/blog/assets/pilogy.png "Well, that seems like a mess")
 * The header image is the book's cover, presumably under the same license, *but* appears to have some material under copyright.
 * title: Real Life in Star Trek, Timescape
 * ![An apple rotting on a branch](/blog/assets/9782641042_93b87c525d_o.png "Look, a metaphor...")
 * The header image is [One Rotten Apple Might Spoil The Whole Bunch, Gitl](https://www.flickr.com/photos/37996646802@N01/9782641042) by [Alan Levine](https://www.flickr.com/photos/cogdog/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Free Culture Book Club — Pilogy, part 3
 * ![A collage of various images overlaid with the book's title in three rows of two letters, each in a different color](/blog/assets/pilogy.png "Well, that seems like a mess")
 * The header image is the book's cover, presumably under the same license, *but* appears to have some material under copyright.
 * title: Real Life in Star Trek, Descent, part 1
 * ![A golden spiraling/rippling image that seems to stretch into the distance](/blog/assets/20600727461_d93be356fa_o.png "Probably not an actual wormhole")
 * The header image is [Wormhole - Experimental Photograph](https://www.flickr.com/photos/59055261@N02/20600727461) by [Karthikeyan KC](https://www.flickr.com/photos/karthikeyankc/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Free Culture Book Club — Pilogy, part 4
 * ![A collage of various images overlaid with the book's title in three rows of two letters, each in a different color](/blog/assets/pilogy.png "Well, that seems like a mess")
 * The header image is the book's cover, presumably under the same license, *but* appears to have some material under copyright.
 * title: Real Life in Star Trek, Season 6, TNG
 * ![The Hubble Space Telescope](/blog/assets/GSFC-20171208-Archive-e002151.png "Still scanning the galaxy...in the next generation")
 * The header image is [Hubble Space Telescope](https://images.nasa.gov/details-GSFC_20171208_Archive_e002151) by NASA Goddard, in the public domain by NASA policy.
 * title: Free Culture Book Club — Sówka w świecie dnia
 * ![An owl appearing to smile, with mostly closed eyes and beak slightly open](/blog/assets/694499591_3616a3dcb2_o.png "Owl's well that...never mind")
 * The header image is [owl's smile!! :)](https://www.flickr.com/photos/30048871@N00/694499591) by [merec0](https://www.flickr.com/photos/merec0/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Descent, part 2
 * ![Disassembled parts of an electronic device, with some wires and circular objects formed into a stick figure to the left](/blog/assets/19980204002_1786623682_o.png "Probably not Lore")
 * The header image is [Hanimex CX333 Disassembled / Teardown / Deconstruction](https://www.flickr.com/photos/9322498@N03/19980204002) by [Robert D. Bruce](https://www.flickr.com/photos/stockrdb/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Free Culture Book Club — Meteorite
 * ![A foot-bridge from a landing platform to a facility perched on top of a column](/blog/assets/meteorite-introduction.png "I do wonder what purpose the place serves on a normal day...")
 * The header image is a screenshot of the game, presumably under the assets' license.
 * title: Real Life in Star Trek, Liaisons
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Free Culture Book Club — The Pink and Black Album
 * ![An owl appearing to smile, with mostly closed eyes and beak slightly open](/blog/assets/694499591_3616a3dcb2_o.png "Owl's well that...never mind")
 * The header image is [owl's smile!! :)](https://www.flickr.com/photos/30048871@N00/694499591) by [merec0](https://www.flickr.com/photos/merec0/), made available under the terms of the [Creative Commons Attribution 2.0 Generic](https://creativecommons.org/licenses/by/2.0/) license.
 * title: Real Life in Star Trek, Interface
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Gambit part 1
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Gambit part 2
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Phantasms
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Dark Page
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Attached
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Force of Nature
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Inheritance
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Parallels
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, The Pegasus
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Homeward
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Sub Rosa
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Lower Decks
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Thine Own Self
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Masks
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Eye Of The Beholder
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Genesis
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Journey's End
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Firstborn
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Bloodlines
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Emergence
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Preemptive Strike
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, All Good Things
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, All Good Things, part 2
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Season 7, TNG
 * ![The Hubble Space Telescope](/blog/assets/GSFC-20171208-Archive-e002151.png "Still scanning the galaxy...in the next generation")
 * The header image is [Hubble Space Telescope](https://images.nasa.gov/details-GSFC_20171208_Archive_e002151) by NASA Goddard, in the public domain by NASA policy.
 * title: Real Life in Star Trek, Star Trek Generations
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, First Contact
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Insurrection
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Free Culture Book Club —  The Transmogrification of Mr. Claus 
 * ![The book's cover, featuring the title in three-dimensional lettering between a star-like glow above and an Earth-like planet below](/blog/assets/bloodstar.png "Out of this...no, never mind")
 * The header image is the book's cover.
 * title: Real Life in Star Trek, Nemesis
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is []() by [](), made available under the terms of the [Creative Commons Attribution Share-Alike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.
 * title: Real Life in Star Trek, Films, TNG
 * ![The Hubble Space Telescope](/blog/assets/GSFC-20171208-Archive-e002151.png "Still scanning the galaxy...in the next generation")
 * The header image is [Hubble Space Telescope](https://images.nasa.gov/details-GSFC_20171208_Archive_e002151) by NASA Goddard, in the public domain by NASA policy.
 * title: Real Life in Star Trek, Next Generation Final Summary
 * ![Still scanning the galaxy](/blog/assets/eso0733a.png "Still scanning the galaxy")
 * The header image is [The planet, the galaxy and the laser](https://www.eso.org/public/images/eso0733a/) by the [ESO](https://www.eso.org) and [Yuri Beletsky](Yuri Beletsky), released under the terms of a [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) license.

Any image not listed or not associated with its own license (like the
Font Awesome files) are either extracted from the work being discussed
and therefore under the work's license or created by me.
