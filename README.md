# Entropy Arbitrage Assets

This repository hosts the non-text assets---mostly the images---from [Entropy Arbitrage](https://john.colagioia.net/blog/).  It's far too much to clutter the blog itself, but after a minor system glitch forcing me to reconstruct some parts of the blog, I'm reluctant to continue risking the assets to "just follow the credits of the post, if you ever need to rebuild everything."  It turns out that's not fun, and I'm glad that I had a temporary copy floating around.

There is no organization, here, since the files are merely consumed by Jekyll when building the blog.  To optimize the blog's loading speed, no image is wider than 740px.

Oh, and if this is the sort of thing that concerns you, I guess this should serve as an overall "spoiler warning" for the blog.  When I prepare posts in advance, I generally try to get at least the header image and a provisional title squared away.  So, you might find the occasional image or post title that hasn't yet been featured, giving away the premise of some future post.  I doubt that anybody needs my posts to be a surprise, but if you somehow do, well, you've been warned.

The grand tour can be found in [the license file](LICENSE.md), which shows each post title, links to each image used in the post, and the credits for that post.  The credits should include the source and licensing information for every image.  Every Tuesday, the license file is refreshed with code something like this.

```console
grep -e '^title: ' -e '!\[' -e '^\*\*Credits\*\*: ' _posts/* \
  | grep -v 'Someone' | grep -v 'hand-man-suit-meeting' \
  | sed 's/\*\*Credits\*\*: *//g' | cut -f2- -d':' \
  | sed 's/\/blog\/assets\///g' | sed 's/ > > //g' \
  | sed 's/^/ * /g' >> "${license}"
```

The "hand-man-suit-meeting" is my placeholder image---a man's torso holding up a blank card---and "Someone" is placeholder text in the post credits.  Everything else is to trim out the formatting.

